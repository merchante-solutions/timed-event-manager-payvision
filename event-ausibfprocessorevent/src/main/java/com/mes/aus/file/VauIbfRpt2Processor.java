package com.mes.aus.file;

import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import com.mes.aus.AusAccount;
import com.mes.aus.InboundFile;
import com.mes.aus.ProcessError;
import com.mes.aus.ResponseRecord;
import com.mes.aus.SysUtil;

/**
 * Processes VAU Report 2 files (reject files).  Generates response records.
 */
public class VauIbfRpt2Processor extends AutomatedProcess
{
  static Logger log = Logger.getLogger(VauIbfRpt2Processor.class);

  private InboundFile   ibf;
  private Date          fileDate;
  private int           lineNum;

  private int           detailCount;
  private ErrorManager  errMgr;
  private List          responses;

  public VauIbfRpt2Processor(boolean directFlag)
  {
    super(directFlag);
    errMgr = new ErrorManager(db);
  }
  public VauIbfRpt2Processor()
  {
    this(true);
  }

  private void addResponse(ResponseRecord response)
  {
    responses.add(response);
  }
  public List getResponses()
  {
    return responses;
  }

  /**
   * Reset internal variables, useful if processing multiple files.
   */
  private void reset(InboundFile ibf)
  {
    this.ibf = ibf;
    errMgr.resetErrors();
    responses = new ArrayList();
    detailCount = 0;
    lineNum = 0;
  }

  /**
   * Generate response record for a reject report detail line.
   */
  private void processDetail(String line, int lineNum)
  {
    String[] fields = VauFileUtil.parseReportLine(line);

    // reject code
    String rejectCode = fields[0];
    String rspCode = SysUtil.getResponseCode(SysUtil.SYS_VAU,rejectCode);
    log.debug("rejectCode '" + rejectCode + "' translated to response code '" + rspCode + "'");

    // merch num (replace 'M' with '9' to convert back to mes merch num)
    String merchNum = "9" + fields[2].substring(1);
    log.debug("merchNum: " + merchNum);

    // account data
    String accountNum  = fields[3];
    String expDate = fields[4];
    String accountType = AusAccount.determineAccountType(accountNum);
    if (accountType == null)
    {
      errMgr.addError(new ProcessError(
        ibf.getIbId(),lineNum,4,
        "Cannot determine old account type from account number",
        ProcessError.WARNING));
    }
    AusAccount account 
      = new AusAccount(accountNum,expDate,accountType);

    // discretionary data 1-10=reqf_id, 11-20=req_id, 21-30=obId
    // these may be empty in the case of a dummy detail response,
    // and trimmed in the case of obId
    long reqfId = 0;
    try
    { 
      reqfId = Long.parseLong(fields[5].substring(0,10).trim());
    }
    catch (Exception e) { }

    long reqId = 0;
    try
    {
      reqId = Long.parseLong(line.substring(10,20).trim());
    }
    catch (Exception e) { }

    // already available in inbound file object...
    long obId = 0;
    try
    {
      obId = Long.parseLong(line.substring(20).trim());
    }
    catch (Exception e) { }

    ResponseRecord response = new ResponseRecord();
    response.setCreateDate(Calendar.getInstance().getTime());
    response.setOldAcct(account);
    response.setReqfId(reqfId);
    response.setReqId(reqId);
    response.setIbId(ibf.getIbId());
    response.setObId(ibf.getObId());
    response.setSysCode(SysUtil.SYS_VAU);
    response.setMerchNum(merchNum);
    response.setRspCode(rspCode);
    addResponse(response);
  }

  /**
   * Process the file data.
   */
  public void process(InboundFile ibf)
  {
    reset(ibf);

    LineNumberReader in = null;
    String line = null;

    try
    {
      in = new LineNumberReader(new InputStreamReader(ibf.getInputStream()));

      // scan until reject details legend located
      while((line = in.readLine()) != null && !line.startsWith("Reject"));
      if (line == null)
      {
        errMgr.addError(new ProcessError(
          ibf.getIbId(),lineNum,0,"Unable to locate reject report legend",
          ProcessError.FATAL));
        return;
      }

      // discard blank line if present
      line = in.readLine();
      line = (line.startsWith("  ") ? in.readLine() : line);

      // process reject details until next blank line
      do
      {
        processDetail(line,in.getLineNumber());
      }
      while ((line = in.readLine()) != null && 
             !line.startsWith("  ") &&
             !errMgr.hasFatalError());

      // save response records unless fatal error occurred
      if (!errMgr.hasFatalError())
      {
        db.insertResponseRecords(responses);
      }
    }
    catch (Exception e)
    {
      errMgr.addError(new ProcessError(ibf.getIbId(),lineNum,0,"IO error: " + e));
      log.error("Error: " + e);
      notifyError(e,"process(ibf=" + ibf + ")");
    }
    finally
    {
      try { in.close(); } catch (Exception e) { }
      try
      {
        if (errMgr.hasError())
        {
          errMgr.logErrors();
        }
      }
      catch (Exception ie) { }
      try { ibf.release(); } catch (Exception e) { }
    }
  }
}
