package com.mes.aus.file;

import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import com.mes.aus.AusAccount;
import com.mes.aus.InboundFile;
import com.mes.aus.ProcessError;
import com.mes.aus.ResponseRecord;
import com.mes.aus.SysUtil;

public class DauIbfRspProcessor extends AutomatedProcess {
	static Logger log = Logger.getLogger(DauIbfRspProcessor.class);

	// may need to adjust this depending on ebcdic translation
	// causing extra characters at end of line
	public static final int LINE_LENGTH = 350;
	public static final int MAX_INSERT_BATCH = 1000;

	private InboundFile ibf;
	private Date fileDate;
	private int lineNum;
	private int detailCount;
	private List responses;
	private ErrorManager errMgr;

	public DauIbfRspProcessor(boolean directFlag) {
		super(directFlag);
		errMgr = new ErrorManager(db);
	}
	public DauIbfRspProcessor() {
		this(true);
	}
	private void addResponse(ResponseRecord response) {
		responses.add(response);
	}
	public List getResponses() {
		return responses;
	}
	/**
	 * Flip MMYY from Discover to YYMM for MES.
	 */
	private String convertExpDate(String expDate) {
		if (expDate.length() == 4) {
			return expDate.substring(2) + expDate.substring(0, 2);
		}
		return expDate;
	}
	/**
	 * Parse detail record. Generates response record objects.
	 */
	private void processDetail(String line, int lineNum) {
		// discover merch num (useless?)
		String merchNum = line.substring(2, 17).trim();

		// old account data
		String oldAccountNum = line.substring(17, 36).trim();
		String oldExpDate = convertExpDate(line.substring(36, 40).trim());
		String oldAccountType = AusAccount.determineAccountType(oldAccountNum);
		if (oldAccountType.equals(AusAccount.AT_INVALID)) {
			errMgr.addError(new ProcessError(ibf.getIbId(), lineNum, 13, "Cannot determine old account type from account number", ProcessError.WARNING));
		}
		AusAccount oldAccount = new AusAccount(oldAccountNum, oldExpDate, oldAccountType);

		// new account data
		String newAccountNum = line.substring(40, 59).trim();
		String newExpDate = convertExpDate(line.substring(59, 63).trim());
		AusAccount newAccount = null;
		if (newAccountNum.length() > 0 || newExpDate.length() > 0) {
			if (newAccountNum.length() == 0) {
				newAccountNum = oldAccountNum;
			}
			String newAccountType = AusAccount.determineAccountType(newAccountNum);
			if (newAccountType.equals(AusAccount.AT_INVALID)) {
				errMgr.addError(new ProcessError( ibf.getIbId(), lineNum, 45, "Cannot determine new account type from account number", ProcessError.WARNING));
			}
			newAccount = new AusAccount(newAccountNum, newExpDate, newAccountType);
		}
		else {
			newAccount = new AusAccount();
		}

		// response code
		String serviceCode = line.substring(63, 64).trim();
		String rspCode = SysUtil.getResponseCode(SysUtil.SYS_DAU, serviceCode);

		// override new exp date response if new account num included
		if (SysUtil.RC_NEWEXP.equals(rspCode) && !newAccountNum.equals(oldAccountNum)) {
			rspCode = SysUtil.RC_NEWACCT;
		}

		// previously sent flag, not used
		String psFlag = line.substring(64, 65).trim();

		// account change date, not used
		String acctChangeDate = line.substring(65, 73).trim();

		// discretionary data 1-12=reqf_id, 13-24=req_id, 25-36=obId
		long reqfId = 0;
		try {
			reqfId = Long.parseLong(line.substring(73, 85).trim());
		}
		catch (Exception e) {}

		long reqId = 0;
		try {
			reqId = Long.parseLong(line.substring(85, 97).trim());
		}
		catch (Exception e) {}

		// already available in inbound file object...
		long obId = 0;
		try {
			obId = Long.parseLong(line.substring(97, 109).trim());
		}
		catch (Exception e) {}

		// add a response record for this detail to the list
		ResponseRecord response = new ResponseRecord();
		response.setCreateDate(Calendar.getInstance().getTime());
		response.setOldAcct(oldAccount);
		response.setNewAcct(newAccount);
		response.setReqfId(reqfId);
		response.setReqId(reqId);
		response.setIbId(ibf.getIbId());
		response.setObId(ibf.getObId());
		response.setSysCode(SysUtil.SYS_DAU);
		// merch num will be loaded from original request...
		// response.setMerchNum(merchNum);
		response.setRspCode(rspCode);
		addResponse(response);
	}
	private void reset(InboundFile ibf) {
		this.ibf = ibf;
		errMgr.resetErrors();
		responses = new ArrayList();
		detailCount = 0;
		lineNum = 0;
	}
	/**
	 * Process the file data.
	 */
	public void process(InboundFile ibf) {
		reset(ibf);
		LineNumberReader in = null;
		String line = null;

		try {
			in = new LineNumberReader(new InputStreamReader(ibf.getInputStream()));
			while ((line = in.readLine()) != null && !errMgr.hasFatalError()) {
				if (!line.startsWith("05"))
					continue;
				lineNum = in.getLineNumber();
				processDetail(line, lineNum);
				if (responses.size() >= MAX_INSERT_BATCH) {
					db.insertResponseRecords(responses);
					responses.clear();
				}
			}
			if (responses.size() > 0) {
				db.insertResponseRecords(responses);
			}
		}
		catch (Exception e) {
			errMgr.addError(new ProcessError(ibf.getIbId(), lineNum, 0, "IO error: " + e));
			log.error("Error: " + e);
			notifyError(e, "process(ibf=" + ibf + ")");
		}
		finally {
			try {
				in.close();
			}
			catch (Exception e) {}
			try {
				if (errMgr.hasFatalError()) {
					db.purgeResponseRecords(ibf.getIbId());
				}
			}
			catch (Exception ie) {}
			try {
				if (errMgr.hasError()) {
					errMgr.logErrors();
				}
			}
			catch (Exception ie) {}
			try {
				ibf.release();
			}
			catch (Exception e) {}
		}
	}
}