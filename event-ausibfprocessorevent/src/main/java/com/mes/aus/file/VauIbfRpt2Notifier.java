package com.mes.aus.file;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.io.OutputStream;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import org.apache.log4j.Logger;
import com.mes.aus.InboundFile;
import com.mes.aus.Notifier;

/**
 * Processes VAU Report 2 file (reject files), sends truncated copy.
 */
public class VauIbfRpt2Notifier extends AutomatedProcess {
	static Logger log = Logger.getLogger(VauIbfRpt2Notifier.class);
	private Date fileDate;
	private int lineNum;
	private Set badMerchNums = new HashSet();

	public VauIbfRpt2Notifier(boolean directFlag) {
		super(directFlag);
	}
	public VauIbfRpt2Notifier() {
		this(true);
	}
	/**
	 * Truncates card number within a detail. Accumulates unenrolled merchant numbers.
	 */
	private String truncateDetail(String line) {
		String[] fields = VauFileUtil.parseReportLine(line);
		// reject code
		String rejectCode = fields[0];
		if (rejectCode.equals("M")) {
			String merchNum = "9" + fields[2].substring(1);
			badMerchNums.add(merchNum);
		}
		return line.substring(0, 29) + "XXXXXX" + line.substring(35);
	}
	private void write(OutputStream out, String line) throws Exception {
		out.write((line + "\n").getBytes());
	}
	/**
	 * Process the file data.
	 */
	public void process(InboundFile ibf) {
		LineNumberReader in = null;
		OutputStream out = null;
		String line = null;

		try {
			in = new LineNumberReader(new InputStreamReader(ibf.getInputStream()));
			String fileName = ibf.getFileName() + ".txt";
			out = new FileOutputStream(fileName);
			boolean inDetails = false;

			while ((line = in.readLine()) != null) {
				if (!inDetails) {
					// write out the line as is
					write(out, line);

					// look for legend line
					if (line.startsWith("Reject")) {
						inDetails = true;

						// skip over blank line
						write(out, in.readLine());
					}
				}
				else {
					// look for end of details
					if (line.startsWith("   ")) {
						inDetails = false;
						write(out, line);
					}
					// output a truncated detail
					else {

						write(out, truncateDetail(line));
					}
				}
			}
			out.flush();
			out.close();
			new Notifier(directFlag).notifyRejectFile(ibf, new File(fileName), badMerchNums);
		}
		catch (Exception e) {
			log.error("Error: " + e);
			notifyError(e, "process(ibf=" + ibf + ")");
		}
		finally {
			try {
				in.close();
			}
			catch (Exception e) {}
			try {
				out.close();
			}
			catch (Exception e) {}
			try {
				ibf.release();
			}
			catch (Exception e) {}
		}
	}
}
