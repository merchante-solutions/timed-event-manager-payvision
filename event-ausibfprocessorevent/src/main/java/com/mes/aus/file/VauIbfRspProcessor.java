package com.mes.aus.file;

import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import com.mes.aus.AusAccount;
import com.mes.aus.InboundFile;
import com.mes.aus.ProcessError;
import com.mes.aus.ResponseRecord;
import com.mes.aus.SysUtil;

public class VauIbfRspProcessor extends AutomatedProcess {
	static Logger log = Logger.getLogger(VauIbfRspProcessor.class);

	// may need to adjust this depending on ebcdic translation
	// causing extra characters at end of line
	public static final int LINE_LENGTH = 110;
	private static final int PR_ST_HEADER = 1;
	private static final int PR_ST_DETAIL = 2;
	private static final int PR_ST_TRAILER = 3;
	private static final int PR_ST_DONE = 4;

	private InboundFile ibf;
	private Date fileDate;
	private int lineNum;
	private int detailCount;
	private List responses;
	private ErrorManager errMgr;

	public VauIbfRspProcessor(boolean directFlag) {
		super(directFlag);
		errMgr = new ErrorManager(db);
	}
	public VauIbfRspProcessor() {
		this(true);
	}
	private void addResponse(ResponseRecord response) {
		responses.add(response);
	}
	public List getResponses() {
		return responses;
	}

	/**
	 * Parse header record.
	 */
	private void processHeader(String line, int lineNum) {
		if (!line.startsWith("0")) {
			errMgr.addError(new ProcessError(ibf.getIbId(), lineNum, 0, "Invalid header record type", ProcessError.FATAL));
			return;
		}

		if (line.length() < LINE_LENGTH) {
			errMgr.addError(new ProcessError(ibf.getIbId(), lineNum, line.length(), "Invalid header record length", ProcessError.FATAL));
			return;
		}
		// segment id not used...
		String segId = line.substring(1, 6).trim();

		// file date...use to tie empty file to original outbound file?
		String dateStr = line.substring(6, 12).trim();
		try {
			DateFormat df = new SimpleDateFormat("yyMMDD");
			fileDate = df.parse(dateStr);
		}
		catch (ParseException pe) {
			errMgr.addError(new ProcessError(ibf.getIbId(), lineNum, 32, "Invalid file date (YYMMDD): '" + dateStr + "'", ProcessError.FATAL));
			return;
		}

		// file seq num not used...
		String seqNum = line.substring(12, 17);

		// ob id not used...
		String obId = line.substring(17, 25).trim();
	}

	/**
	 * Parse detail record. Generates response record objects.
	 */
	private void processDetail(String line, int lineNum) {
		if (!line.startsWith("1")) {
			errMgr.addError(new ProcessError(ibf.getIbId(), lineNum, 0, "Invalid detail record type"));
			return;
		}

		if (line.length() < LINE_LENGTH) {
			errMgr.addError(new ProcessError(ibf.getIbId(), lineNum, line.length(), "Invalid detail record length"));
			return;
		}

		// merch num (replace 'M' with '9' to convert back to mes merch num)
		String merchNum = "9" + line.substring(2, 13).trim();

		// old account data
		String oldAccountNum = line.substring(13, 32).trim();
		String oldExpDate = line.substring(32, 36).trim();
		String oldAccountType = AusAccount.determineAccountType(oldAccountNum);
		if (oldAccountType.equals(AusAccount.AT_INVALID)) {
			errMgr.addError(new ProcessError(ibf.getIbId(), lineNum, 13, "Cannot determine old account type from account number", ProcessError.WARNING));
		}
		AusAccount oldAccount = new AusAccount(oldAccountNum, oldExpDate, oldAccountType);

		// new account data
		String newAccountNum = line.substring(36, 55).trim();
		AusAccount newAccount = null;
		if (newAccountNum.length() > 0) {
			String newExpDate = line.substring(55, 59).trim();
			String newAccountType = AusAccount.determineAccountType(newAccountNum);
			if (newAccountType.equals(AusAccount.AT_INVALID)) {
				errMgr.addError(new ProcessError(ibf.getIbId(), lineNum, 45,
						"Cannot determine new account type from account number", ProcessError.WARNING));
			}
			newAccount = new AusAccount(newAccountNum, newExpDate, newAccountType);
		}
		else {
			newAccount = new AusAccount();
		}

		// response code
		String serviceCode = line.substring(59, 60).trim();
		String rspCode = SysUtil.getResponseCode(SysUtil.SYS_VAU, serviceCode);

		// previously sent flag, not used...
		String psFlag = line.substring(60, 61).trim();

		// discretionary data 1-10=reqf_id, 11-20=req_id, 21-30=obId
		// these may be empty in the case of a dummy detail response
		long reqfId = 0;
		try {
			reqfId = Long.parseLong(line.substring(61, 71).trim());
		}
		catch (Exception e) {}

		long reqId = 0;
		try {
			reqId = Long.parseLong(line.substring(71, 81).trim());
		}
		catch (Exception e) {}

		// already available in inbound file object...
		long obId = 0;
		try {
			obId = Long.parseLong(line.substring(81, 91).trim());
		}
		catch (Exception e) {}

		// add a response record for this detail to the list
		ResponseRecord response = new ResponseRecord();
		response.setCreateDate(Calendar.getInstance().getTime());
		response.setOldAcct(oldAccount);
		response.setNewAcct(newAccount);
		response.setReqfId(reqfId);
		response.setReqId(reqId);
		response.setIbId(ibf.getIbId());
		response.setObId(obId);
		response.setSysCode(SysUtil.SYS_VAU);
		response.setMerchNum(merchNum);
		response.setRspCode(rspCode);
		addResponse(response);
	}

	private void processTrailer(String line, int lineNum) {
		if (line.length() < LINE_LENGTH) {
			errMgr.addError(new ProcessError(ibf.getIbId(), lineNum, line.length(), "Invalid trailer record length (" + line.length() + ")", ProcessError.FATAL));
			return;
		}
		String countStr = line.substring(6, 15).trim();
		try {
			int trailerCount = Integer.parseInt(countStr);
			if (trailerCount != detailCount) {
				errMgr.addError(new ProcessError(ibf.getIbId(), lineNum, 2,	"Trailer count mismatch (trailer count " + trailerCount
								+ " <> " + detailCount + " details)", ProcessError.FATAL));
				return;
			}
		}
		catch (NumberFormatException ne) {
			errMgr.addError(new ProcessError(ibf.getIbId(), lineNum, 2, "Invalid detail count '" + countStr + "' in trailer", ProcessError.FATAL));
		}
	}

	private void reset(InboundFile ibf) {
		this.ibf = ibf;
		errMgr.resetErrors();
		responses = new ArrayList();
		detailCount = 0;
		lineNum = 0;
	}

	/**
	 * Process the file data.
	 */
	public void process(InboundFile ibf) {
		reset(ibf);
		LineNumberReader in = null;
		int processState = PR_ST_HEADER;
		String line = null;

		try {
			in = new LineNumberReader(new InputStreamReader(ibf.getInputStream()));
			while ((line = in.readLine()) != null && !errMgr.hasFatalError()) {
				lineNum = in.getLineNumber();
				switch (processState) {
					case PR_ST_HEADER :
						processHeader(line, lineNum);
						processState = PR_ST_DETAIL;
						break;

					case PR_ST_DETAIL :
						if (!line.startsWith("9")) {
							processDetail(line, lineNum);
							++detailCount;
							break;
						}
						processState = PR_ST_TRAILER;

					case PR_ST_TRAILER :
						processTrailer(line, lineNum);
						processState = PR_ST_DONE;
						break;

					case PR_ST_DONE :
						errMgr.addError(new ProcessError(ibf.getIbId(), lineNum, 0, "Unexpected data after trailer"));
						return;
				}
			}

			if (!errMgr.hasFatalError() && processState != PR_ST_DONE)
				errMgr.addError(new ProcessError(ibf.getIbId(), lineNum, 0, "Unexpected end of file", ProcessError.FATAL));

			if (!errMgr.hasFatalError()) {
				db.purgeResponseRecords(ibf.getIbId());
				db.insertResponseRecords(responses);
			}
		}
		catch (Exception e) {
			errMgr.addError(new ProcessError(ibf.getIbId(), lineNum, 0, "IO error: " + e));
			log.error("Error: " + e);
			notifyError(e, "process(ibf=" + ibf + ")");
		}
		finally {
			try {
				in.close();
			}
			catch (Exception e) {}
			// TODO: implement error logging in db
			try {
				if (errMgr.hasError()) {
					errMgr.logErrors();
				}
			}
			catch (Exception ie) {}
			try {
				ibf.release();
			}
			catch (Exception e) {}
		}
	}
}
