package com.mes.aus.file;

import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import com.mes.aus.AusAccount;
import com.mes.aus.InboundFile;
import com.mes.aus.ProcessError;
import com.mes.aus.ResponseRecord;
import com.mes.aus.SysUtil;

public class AbuIbfProcessor extends AutomatedProcess {
	static Logger log = Logger.getLogger(AbuIbfProcessor.class);

	public static final int LINE_LENGTH = 118;
	private static final int PR_ST_HEADER = 1;
	private static final int PR_ST_DETAIL = 2;
	private static final int PR_ST_TRAILER = 3;
	private static final int PR_ST_DONE = 4;

	private InboundFile ibf;
	private Date fileDate;
	private int lineNum;
	private int detailCount;
	private List responses;
	private List errors;
	private boolean fatalFlag;

	public AbuIbfProcessor(boolean directFlag) {
		super(directFlag);
	}
	public AbuIbfProcessor() {
		this(true);
	}
	private void addError(ProcessError error) {
		errors.add(error);
		if (error.isFatal()) {
			fatalFlag = true;
		}
	}
	public int getErrorCount() {
		return errors.size();
	}
	public boolean hasError() {
		return getErrorCount() > 0;
	}
	public boolean hasFatalError() {
		return fatalFlag;
	}
	public ProcessError getError(int errorNum) {
		ProcessError error = null;
		if (errorNum < errors.size()) {
			error = (ProcessError) errors.get(errorNum);
		}
		return error;
	}
	private void addResponse(ResponseRecord response) {
		responses.add(response);
	}
	public List getResponses() {
		return responses;
	}
	/**
	 * Parse header record.
	 */
	private void processHeader(String line, int lineNum) {
		if (!line.startsWith("H")) {
			addError(new ProcessError(ibf.getIbId(), lineNum, 0, "Invalid header record type", ProcessError.FATAL));
			return;
		}

		if (line.length() != LINE_LENGTH) {
			addError(new ProcessError(ibf.getIbId(), lineNum, line.length(), "Invalid header record length", ProcessError.FATAL));
			return;
		}

		// not needed for anything?
		String acquirerIca = line.substring(21, 32).trim();

		// file date...use to tie empty file to original outbound file?
		String dateStr = line.substring(32, 38).trim();
		try {
			DateFormat df = new SimpleDateFormat("MMDDyy");
			fileDate = df.parse(dateStr);
		}
		catch (ParseException pe) {
			addError(new ProcessError(ibf.getIbId(), lineNum, 32, "Invalid file date: '" + dateStr + "'", ProcessError.FATAL));
			return;
		}
	}
	private boolean validateAccountType(String accountType) {
		return AusAccount.isValidAccountType(accountType);
	}
	private String determineAccountType(String accountNum) {
		if (accountNum != null && accountNum.length() > 1) {
			if (accountNum.startsWith("5")) {
				return AusAccount.AT_MC;
			}
			else if (accountNum.startsWith("4")) {
				return AusAccount.AT_VISA;
			}
		}
		return null;
	}

	/**
	 * Parse detail record. Generates response record objects.
	 */
	private void processDetail(String line, int lineNum) {
		if (!line.startsWith("D")) {
			addError(new ProcessError(ibf.getIbId(), lineNum, 0, "Invalid detail record type"));
			return;
		}

		if (line.length() != LINE_LENGTH) {
			addError(new ProcessError(ibf.getIbId(), lineNum, line.length(), "Invalid detail record length"));
			return;
		}
		// merch num
		String merchNum = GenUtil.trimZeroes(line.substring(1, 16).trim());

		// ob id, this is now needed due to multiple outbound file responses in inbound files
		long obId = 0;
		try {
			obId = Long.parseLong(line.substring(16, 26).trim());
		}
		catch (Exception e) {}

		// response code	
		String reasonId = line.substring(72, 78).trim();
		String rspInd = line.substring(98, 99).trim();
		String rspCode;
		if( ("VALID".equals(reasonId) || "UNKNWN".equals(reasonId)) && (!rspInd.isEmpty() && ("V".equals(rspInd) || "P".equals(rspInd) || "N".equals(rspInd))))
		{	
			rspCode = SysUtil.getResponseCode(SysUtil.SYS_ABU, rspInd);
		}
		else
		{
			rspCode = SysUtil.getResponseCode(SysUtil.SYS_ABU, reasonId);
		}

		// old account data
		String oldAccountNum = line.substring(26, 45).trim();
		String oldExpDate = line.substring(64, 68).trim();
		String oldAccountType = AusAccount.determineAccountType(oldAccountNum);
		if (oldAccountType.equals(AusAccount.AT_INVALID)) {
			addError(new ProcessError(ibf.getIbId(), lineNum, 26, "Cannot determine old account type from account number", ProcessError.WARNING));
		}
		AusAccount oldAccount = new AusAccount(oldAccountNum, oldExpDate, oldAccountType);

		// new account data
		String newAccountNum = line.substring(45, 64).trim();
		AusAccount newAccount = null;
		if (newAccountNum.length() > 0) {
			String newExpDate = line.substring(68, 72).trim();
			String newAccountType = AusAccount.determineAccountType(newAccountNum);
			if (newAccountType.equals(AusAccount.AT_INVALID)) {
				addError(new ProcessError(ibf.getIbId(), lineNum, 45, "Cannot determine new account type from account number", ProcessError.WARNING));
			}
			newAccount = new AusAccount(newAccountNum, newExpDate, newAccountType);
		}
		else {
			newAccount = new AusAccount();
		}

		// discretionary data 1-10=reqf_id, 11-20=req_id
		// these may be empty in the case of a dummy detail response
		long reqfId = 0;
		try {
			reqfId = Long.parseLong(line.substring(78, 88).trim());
		}
		catch (Exception e) {}

		long reqId = 0;
		try {
			reqId = Long.parseLong(line.substring(88, 98).trim());
		}
		catch (Exception e) {}

		// add a request record for this detail to the list
		ResponseRecord response = new ResponseRecord();
		response.setCreateDate(Calendar.getInstance().getTime());
		response.setOldAcct(oldAccount);
		response.setNewAcct(newAccount);
		response.setReqfId(reqfId);
		response.setReqId(reqId);
		response.setIbId(ibf.getIbId());
		response.setObId(obId);
		response.setSysCode(SysUtil.SYS_ABU);
		response.setMerchNum(merchNum);
		response.setRspCode(rspCode);
		addResponse(response);
	}

	private void processTrailer(String line, int lineNum) {
		if (line.length() != LINE_LENGTH) {
			addError(new ProcessError(ibf.getIbId(), lineNum, line.length(), "Invalid trailer record length (" + line.length() + ")", ProcessError.FATAL));
			return;
		}

		String countStr = line.substring(12, 21).trim();
		try {
			int trailerCount = Integer.parseInt(countStr);
			if (trailerCount != detailCount + 2) {
				addError(new ProcessError(ibf.getIbId(), lineNum, 2, "Trailer count mismatch (trailer count " + trailerCount 
						+ " <> " + detailCount + " details)", ProcessError.FATAL));
				return;
			}
		}
		catch (NumberFormatException ne) {
			addError(new ProcessError(ibf.getIbId(), lineNum, 2, "Invalid detail count '" + countStr + "' in trailer", ProcessError.FATAL));
		}
	}
	private void reset(InboundFile ibf) {
		this.ibf = ibf;
		errors = new ArrayList();
		responses = new ArrayList();
		fatalFlag = false;
		detailCount = 0;
		lineNum = 0;
	}
	/**
	 * Process the file data.
	 */
	public void process(InboundFile ibf) {
		reset(ibf);

		db.purgeResponseRecords(ibf.getIbId());

		LineNumberReader in = null;
		int processState = PR_ST_HEADER;
		String line = null;

		try {
			in = new LineNumberReader(new InputStreamReader(ibf.getInputStream()));
			
			while ((line = in.readLine()) != null && !hasFatalError()) {
				// HACK: ignore the weird trailing character at the end of mc file
				if (line.getBytes()[0] == 26)
					continue;

				lineNum = in.getLineNumber();
				switch (processState) {
					case PR_ST_HEADER :
						processHeader(line, lineNum);
						processState = PR_ST_DETAIL;
						break;

					case PR_ST_DETAIL :
						if (!line.startsWith("T")) {
							processDetail(line, lineNum);
							++detailCount;
							break;
						}
						processState = PR_ST_TRAILER;

					case PR_ST_TRAILER :
						processTrailer(line, lineNum);
						processState = PR_ST_HEADER;
						db.insertResponseRecords(responses);
						responses = new ArrayList();
						detailCount = 0;
						break;

				// may now be multiple header/detail/trailer sets per file
				// so look for header following trailer
				}
			}
			// should end looking for next header
			if (!hasFatalError() && processState != PR_ST_HEADER)
				addError(new ProcessError(ibf.getIbId(), lineNum, 0, "Unexpected end of file", ProcessError.FATAL));
		}
		catch (Exception e) {
			addError(new ProcessError(ibf.getIbId(), lineNum, 0, "IO error: " + e));
			log.error("Error: " + e);
			notifyError(e, "process(ibf=" + ibf + ")");
		}
		finally {
			try {
				in.close();
			}
			catch (Exception e) {}
			// TODO: implement error logging in db
			try {
				if (hasError()) {
					db.insertProcessErrors(errors);
				}
			}
			catch (Exception ie) {}
			try {
				ibf.release();
			}
			catch (Exception e) {}
		}
	}
}
