/*@lineinfo:filename=CBTQuarterlyData*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/startup/CBTQuarterlyData.sqlj $

  Description:

    AccountLookup

    Support bean for account lookup page in account maintenance.

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2010-11-11 15:57:20 -0800 (Thu, 11 Nov 2010) $
  Version            : $Revision: 18116 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.util.Calendar;
import org.apache.log4j.Logger;
import com.mes.constants.MesEmails;
import com.mes.database.SQLJConnectionBase;
import com.mes.net.MailMessage;
import com.mes.support.NumberFormatter;

public class CBTQuarterlyData extends EventBase
{
  static Logger log = Logger.getLogger(CBTQuarterlyData.class);
  
  public final String   INTEGER_FORMAT  = "###,###,##0;(###,###,##0)";
  public final String   CURRENCY_FORMAT = "$###,###,###,##0.00;($###,###,###,##0.00)";
  public boolean execute()
  {
    boolean   result = false;
    long      approvedApps;
    long      accountsOnFile;
    long      visaSalesCount;
    double    visaSalesVolume;
    long      mcSalesCount;
    double    mcSalesVolume;
    long      visaCBCount;
    double    visaCBVolume;
    long      mcCBCount;
    double    mcCBVolume;
    long      salesRepCount;
    
    String    quarter     = "";
    String    progress    = "";
    
    Calendar  activeCal   = Calendar.getInstance();
    Calendar  beginCal    = Calendar.getInstance();
    Calendar  endCal      = Calendar.getInstance();
    
    java.sql.Date activeDate;
    java.sql.Date beginDate;
    java.sql.Date endDate;
    
    try
    {
      connect();
      
      // determine dates of previous quarter based on current date
      Calendar now = Calendar.getInstance();
      
      activeCal.clear();
      beginCal.clear();
      endCal.clear();
      
      switch(now.get(Calendar.MONTH))
      {
        // these evaluate to 4th quarter of previous year
        case Calendar.JANUARY:
        case Calendar.FEBRUARY:
        case Calendar.MARCH:
          log.debug("4th quarter of last year (" + (now.get(Calendar.YEAR)-1) + ")");
          quarter = "Q4 " + (now.get(Calendar.YEAR) - 1);
          // set activeCal to December 1st
          activeCal.set(now.get(Calendar.YEAR)-1, Calendar.DECEMBER, 1);
          
          // set beginCal to October 1st
          beginCal.set(now.get(Calendar.YEAR)-1, Calendar.OCTOBER, 1);
          
          // set endCal to December 31st
          endCal.set(now.get(Calendar.YEAR)-1, Calendar.DECEMBER, 31);
          break;
          
        // these evaluate to 1st quarter of current year
        case Calendar.APRIL:
        case Calendar.MAY:
        case Calendar.JUNE:
          log.debug("1st quarter of this year");
          quarter = "Q1 " + now.get(Calendar.YEAR);
          // set activeCal to March 1st
          activeCal.set(now.get(Calendar.YEAR), Calendar.MARCH, 1);
          
          // set beginCal to January 1st
          beginCal.set(now.get(Calendar.YEAR), Calendar.JANUARY, 1);
          
          // set endCal to March 31st
          endCal.set(now.get(Calendar.YEAR), Calendar.MARCH, 31);
          break;
          
        // these evaluate to 2nd quarter of current year
        case Calendar.JULY:
        case Calendar.AUGUST:
        case Calendar.SEPTEMBER:
          log.debug("2nd quarter of this year");
          quarter = "Q2 " + now.get(Calendar.YEAR);
          // set activeCal to June 1st
          activeCal.set(now.get(Calendar.YEAR), Calendar.JUNE, 1);
          
          // set beginCal to April 1st
          beginCal.set(now.get(Calendar.YEAR), Calendar.APRIL, 1);
          
          // set endCal to June 30th
          endCal.set(now.get(Calendar.YEAR), Calendar.JUNE, 30);
          break;
          
        // these evaluate to 3rd quarter of curent year
        case Calendar.OCTOBER:
        case Calendar.NOVEMBER:
        case Calendar.DECEMBER:
          log.debug("3rd quarter of this year");
          quarter = "Q3 " + now.get(Calendar.YEAR);
          // set activeCal to September 1st
          activeCal.set(now.get(Calendar.YEAR), Calendar.SEPTEMBER, 1);
          
          // set beginCal to July 1st
          beginCal.set(now.get(Calendar.YEAR), Calendar.JULY, 1);
          
          // set endCal to September 30th
          endCal.set(now.get(Calendar.YEAR), Calendar.SEPTEMBER, 30);
          break;
      }
      
      // now get Dates from Calendars
      activeDate  = new java.sql.Date(activeCal.getTime().getTime());
      beginDate   = new java.sql.Date(beginCal.getTime().getTime());
      endDate     = new java.sql.Date(endCal.getTime().getTime());
      
      progress = "approved apps";
      
      // get approved applications
      /*@lineinfo:generated-code*//*@lineinfo:156^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(app.app_seq_num)
//          
//          from    application  app,
//                  merchant     m
//          where   trunc(app.app_created_date) between :beginDate and :endDate and
//                  app.app_type in (0, 1, 8, 28, 31) and
//                  app.app_seq_num = m.app_seq_num and
//                  m.merch_credit_status = 1
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(app.app_seq_num)\n         \n        from    application  app,\n                merchant     m\n        where   trunc(app.app_created_date) between  :1  and  :2  and\n                app.app_type in (0, 1, 8, 28, 31) and\n                app.app_seq_num = m.app_seq_num and\n                m.merch_credit_status = 1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.CBTQuarterlyData",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setDate(1,beginDate);
   __sJT_st.setDate(2,endDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   approvedApps = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:166^7*/
      
      progress = "accounts on file";
      // get accounts on file
      /*@lineinfo:generated-code*//*@lineinfo:170^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(gn.hh_merchant_number)
//          
//          from    monthly_extract_gn gn
//          where   gn.hh_active_date = :activeDate and
//                  gn.hh_bank_number in (3941, 3858) and
//                  (gn.g1_merchant_status not in ('C', 'I', 'Z') or gn.g1_merchant_status is null)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(gn.hh_merchant_number)\n         \n        from    monthly_extract_gn gn\n        where   gn.hh_active_date =  :1  and\n                gn.hh_bank_number in (3941, 3858) and\n                (gn.g1_merchant_status not in ('C', 'I', 'Z') or gn.g1_merchant_status is null)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.startup.CBTQuarterlyData",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setDate(1,activeDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   accountsOnFile = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:178^7*/
      
      progress = "visa sales";
      // get visa sales count and volume
      /*@lineinfo:generated-code*//*@lineinfo:182^7*/

//  ************************************************************
//  #sql [Ctx] { select  sum(pl.pl_number_of_sales),
//                  sum(pl.pl_sales_amount)
//          
//          from    monthly_extract_gn gn,
//                  monthly_extract_pl pl
//          where   gn.hh_bank_number in (3941, 3858) and
//                  gn.hh_active_date between :beginDate and last_day(:endDate) and
//                  pl.hh_load_sec = gn.hh_load_sec and
//                  pl.pl_plan_type like 'V%'
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  sum(pl.pl_number_of_sales),\n                sum(pl.pl_sales_amount)\n         \n        from    monthly_extract_gn gn,\n                monthly_extract_pl pl\n        where   gn.hh_bank_number in (3941, 3858) and\n                gn.hh_active_date between  :1  and last_day( :2 ) and\n                pl.hh_load_sec = gn.hh_load_sec and\n                pl.pl_plan_type like 'V%'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.startup.CBTQuarterlyData",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setDate(1,beginDate);
   __sJT_st.setDate(2,endDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   visaSalesCount = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   visaSalesVolume = __sJT_rs.getDouble(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:194^7*/
      
      progress = "mc sales";
      // get mc sales count and volume
      /*@lineinfo:generated-code*//*@lineinfo:198^7*/

//  ************************************************************
//  #sql [Ctx] { select  sum(pl.pl_number_of_sales),
//                  sum(pl.pl_sales_amount)
//          
//          from    monthly_extract_gn gn,
//                  monthly_extract_pl pl
//          where   gn.hh_bank_number in (3941, 3858) and
//                  gn.hh_active_date between :beginDate and last_day(:endDate) and
//                  pl.hh_load_sec = gn.hh_load_sec and
//                  pl.pl_plan_type like 'M%'
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  sum(pl.pl_number_of_sales),\n                sum(pl.pl_sales_amount)\n         \n        from    monthly_extract_gn gn,\n                monthly_extract_pl pl\n        where   gn.hh_bank_number in (3941, 3858) and\n                gn.hh_active_date between  :1  and last_day( :2 ) and\n                pl.hh_load_sec = gn.hh_load_sec and\n                pl.pl_plan_type like 'M%'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.startup.CBTQuarterlyData",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setDate(1,beginDate);
   __sJT_st.setDate(2,endDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   mcSalesCount = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   mcSalesVolume = __sJT_rs.getDouble(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:210^7*/
      
      progress = "visa chargebacks";
      // get visa chargeback count and volume
      /*@lineinfo:generated-code*//*@lineinfo:214^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(cb.tran_amount),
//                  sum(cb.tran_amount)
//          
//          from    network_chargebacks cb,
//                  mif m
//          where   m.bank_number in (3941, 3858) and
//                  m.merchant_number = cb.merchant_number and
//                  cb.incoming_date between :beginDate and last_day(:endDate) and
//                  substr(cb.card_number, 1, 1) = '4'
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(cb.tran_amount),\n                sum(cb.tran_amount)\n         \n        from    network_chargebacks cb,\n                mif m\n        where   m.bank_number in (3941, 3858) and\n                m.merchant_number = cb.merchant_number and\n                cb.incoming_date between  :1  and last_day( :2 ) and\n                substr(cb.card_number, 1, 1) = '4'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.startup.CBTQuarterlyData",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setDate(1,beginDate);
   __sJT_st.setDate(2,endDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   visaCBCount = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   visaCBVolume = __sJT_rs.getDouble(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:226^7*/
      
      progress = "mc chargebacks";
      // get mc chargeback count and volume
      /*@lineinfo:generated-code*//*@lineinfo:230^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(cb.tran_amount),
//                  sum(cb.tran_amount)
//          
//          from    network_chargebacks cb,
//                  mif m
//          where   m.bank_number in (3941, 3858) and
//                  m.merchant_number = cb.merchant_number and
//                  cb.incoming_date between :beginDate and last_day(:endDate) and
//                  substr(cb.card_number, 1, 1) = '5'
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(cb.tran_amount),\n                sum(cb.tran_amount)\n         \n        from    network_chargebacks cb,\n                mif m\n        where   m.bank_number in (3941, 3858) and\n                m.merchant_number = cb.merchant_number and\n                cb.incoming_date between  :1  and last_day( :2 ) and\n                substr(cb.card_number, 1, 1) = '5'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.startup.CBTQuarterlyData",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setDate(1,beginDate);
   __sJT_st.setDate(2,endDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   mcCBCount = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   mcCBVolume = __sJT_rs.getDouble(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:242^7*/
      
      progress = "sales reps";
      // get sales reps on record
      /*@lineinfo:generated-code*//*@lineinfo:246^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(user_id)
//          
//          from    users
//          where   type_id in (4, 16)
//                  and nvl(enabled, 'N') = 'Y'
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(user_id)\n         \n        from    users\n        where   type_id in (4, 16)\n                and nvl(enabled, 'N') = 'Y'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.startup.CBTQuarterlyData",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   salesRepCount = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:253^7*/
      
      // generate formatted strings
      String approvedAppsStr    = NumberFormatter.getLongString(approvedApps, INTEGER_FORMAT);
      String acctsOnFileStr     = NumberFormatter.getLongString(accountsOnFile, INTEGER_FORMAT);
      String visaSalesCountStr  = NumberFormatter.getLongString(visaSalesCount, INTEGER_FORMAT);
      String visaSalesAmountStr = NumberFormatter.getDoubleString(visaSalesVolume, CURRENCY_FORMAT);
      String mcSalesCountStr    = NumberFormatter.getLongString(mcSalesCount, INTEGER_FORMAT);
      String mcSalesAmountStr   = NumberFormatter.getDoubleString(mcSalesVolume, CURRENCY_FORMAT);
      String visaCBCountStr     = NumberFormatter.getLongString(visaCBCount, INTEGER_FORMAT);
      String visaCBAmountStr    = NumberFormatter.getDoubleString(visaCBVolume, CURRENCY_FORMAT);
      String mcCBCountStr       = NumberFormatter.getLongString(mcCBCount, INTEGER_FORMAT);
      String mcCBAmountStr      = NumberFormatter.getDoubleString(mcCBVolume, CURRENCY_FORMAT);
      String salesRepsStr       = NumberFormatter.getLongString(salesRepCount, INTEGER_FORMAT);
      
      // now generate email
      StringBuffer body = new StringBuffer();
      
      body.append("VISA " + quarter + " REPORT\n\n");
      
      body.append("Approved Applications:   ");
      body.append(approvedAppsStr);
      body.append("\n");
      body.append("Accounts on File:        ");
      body.append(acctsOnFileStr);
      body.append("\n\n");
      
      body.append("Visa Transaction Count:  ");
      body.append(visaSalesCountStr);
      body.append("\n");
      body.append("Visa Transaction Amount: ");
      body.append(visaSalesAmountStr);
      body.append("\n");
      body.append("MC Transaction Count:    ");
      body.append(mcSalesCountStr);
      body.append("\n");
      body.append("MC Transaction Amount:   ");
      body.append(mcSalesAmountStr);
      body.append("\n\n");
      
      body.append("Visa Chargeback Count:   ");
      body.append(visaCBCountStr);
      body.append("\n");
      body.append("Visa Chargeback Amount:  ");
      body.append(visaCBAmountStr);
      body.append("\n");
      body.append("MC Chargeback Count:     ");
      body.append(mcCBCountStr);
      body.append("\n");
      body.append("MC Chargeback Amount:    ");
      body.append(mcCBAmountStr);
      body.append("\n\n");
      
      body.append("Sales Reps On Record:    ");
      body.append(salesRepsStr);
      body.append("\n\n\n");
      
      body.append("John Firman\n");
      body.append("Merchant e-Solutions\n");
      body.append("jfirman@merchante-solutions.com\n");
      body.append("tel: 650.628.6832\n");
      body.append("fax: 650.620.9287\n");
      
      // send email
      MailMessage msg = new MailMessage();
      
      msg.setAddresses(MesEmails.MSG_ADDRS_CBT_QUARTERLY_DATA);
      msg.setSubject("VISA " + quarter + " REPORT for Merchant e-Solutions");
      msg.setText(body.toString());
      msg.send();
      
      result = true;
    }
    catch(Exception e)
    {
      log.error("problem after \"" + progress + "\"");
      logEntry("execute(" + progress + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
    
    return result;
  }
  
  public static void main(String[] args)
  {
    try
    {
      SQLJConnectionBase.initStandalone("DEBUG");
      
      CBTQuarterlyData grunt = new CBTQuarterlyData();
      
      grunt.execute();
    }
    catch(Exception e)
    {
      log.error("main(): " + e.toString());
    }
  }
}/*@lineinfo:generated-code*/