/*************************************************************************

  FILE: $URL: /src/main/com/mes/startup/AmexOptBlueConversion.java $

  Description:

  Created by         : $Author: ttran $
  Created Date       : $Date:   2017-01-04  $
  Last Modified By   : $Author:             $
  Last Modified Date : $Date:               $
  Version            : $Revision:0.1        $

  Change History:
     See Bitbucket

  Copyright (C) 2000-2014,2015 ,2016 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.startup;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Vector;
import org.apache.log4j.Logger;
import com.mes.net.MailMessage;
import com.mes.support.DateTimeFormatter;
import com.mes.support.PropertiesFile;

public class AmexOptBlueConversion extends EventBase 
{
  private static final long serialVersionUID = 1L;
  static Logger log = Logger.getLogger(AmexOptBlueConversion.class);

  private static final int NETWORK_FEE                       = 128;
  private static final int INBOUND_FEE                       = 215;
  private static final int NON_SWIPED_TRAN_FEE               = 220;
  private static final int NON_COMPLIANCE_FEE                = 221;
  private static final int DATA_QUALITY_FEE                  = 222;
  private static final int MARKUP_FEE                        = 101;

  private Long     NodeId                   = -1L;
  private Long     ProcSequence             = 0L;
  private String   crlf                     = "\r\n";
  private double   NetWorkFee               = 0.00;
  private double   InboundFee               = 0.00;
  private double   DataQualityFee           = 0.00;
  private double   NonComplianceFee         = 0.00;
  private double   NonSwipedTranFee         = 0.00;
  private double   ExistingEsaAccessFee     = 0.00;
  private double   MarkupFeeRate            = 0.00;
  private double   MarkupFeePerItem         = 0.00;
  private String   EmailNotifySender        = null;
  private String   EmailNotifyRecipients    = null;
  private Vector   Merchants                = new Vector();
  private Vector   OptBSuccessList          = new Vector();
  private Vector   PspSuccessList           = new Vector();
  private Vector   OptBErrorList            = new Vector();
  private Vector   PspErrorList             = new Vector();
  private Vector   OptBNABSuccessList       = new Vector();
  private Vector   OptBNABErrorList         = new Vector();
  private Vector   NABSuccessList           = new Vector();
  private Vector   NABErrorList             = new Vector();



  public static class AmexMerchant
  {
    protected   int     BankNumber          = -1;
    protected   long    NodeId              = -1L;
    protected   long    MerchantId          = -1L;
    protected   String  DbaName             = null;
    protected   long    AppSeqNum           = -1L;
    protected   long    OptbSeNumber        = -1L;
    protected   long    AmexSeNumber        = -1L;
    protected   String  AmexSeSource        = null;
    protected   Date    AmexConvDate        = null;
    protected   Date    AmexOptbConvDate    = null;
    protected   String  AmexPlan            = null;
    protected   String  MeSProcessing       = null;

    public AmexMerchant( ResultSet rs )
      throws java.sql.SQLException
    {
      BankNumber      = rs.getInt("bank_number");
      NodeId          = rs.getLong("node_id");
      MerchantId      = rs.getLong("merchant_number");
      DbaName         = rs.getString("dba_name");
      AppSeqNum       = rs.getLong("app_seq_num");
      AmexSeNumber    = rs.getLong("amex_se_number");
      AmexSeSource    = rs.getString("amex_se_source");
      AmexConvDate    = rs.getDate("amex_conversion_date");
      AmexPlan        = rs.getString("amex_plan");
      MeSProcessing   = rs.getString("mes_amex_processing");

      try{ OptbSeNumber = rs.getLong("amex_optblue_se_number"); } catch( Exception ignore ) {}
      try{ AmexOptbConvDate = rs.getDate("amex_optb_conversion_date"); } catch( Exception ignore ) {}
    }

    public long      getBankNumber()         { return( BankNumber );         }
    public long      getNodeId()             { return( NodeId );             }
    public long      getMerchantId()         { return( MerchantId );         }
    public String    getDbaName()            { return( DbaName );            }
    public long      getAppSeqNum()          { return( AppSeqNum );          }
    public long      getAmexSeNumber()       { return( AmexSeNumber );       }
    public String    getAmexSeSource()       { return( AmexSeSource );       }
    public Date      getAmexConvDate()       { return( AmexConvDate );       }
    public Date      getAmexOptbConvDate()   { return( AmexOptbConvDate );   }
    public String    getAmexPlan()           { return( AmexPlan );           }
    public long      getOptbSeNumber()       { return( OptbSeNumber );       }
    public String    getMeSProcessing()      { return( MeSProcessing );      }
  }

  protected void readProps() throws Exception
  {
    PropertiesFile props = new PropertiesFile("optblue.properties");

    NetWorkFee               = props.getDouble("network_fee_default", NetWorkFee);
    InboundFee               = props.getDouble("inbound_fee_default", InboundFee);
    DataQualityFee           = props.getDouble("data_quality_fee_default", DataQualityFee);
    NonComplianceFee         = props.getDouble("non_compliance_fee_default", NonComplianceFee);
    NonSwipedTranFee         = props.getDouble("non_swiped_tran_fee_default", NonSwipedTranFee);
    EmailNotifySender        = props.getString("optb_conv_email_notify_sender", null);
    EmailNotifyRecipients    = props.getString("optb_conv_email_notify_recipients", null);
  }

  private void recordBeginTS( long recId )
  {
    PreparedStatement ps          = null;
    ResultSet         rs          = null;
    String            qs          = null;
    Timestamp         ts          = null;

    try
    {
      ts = new Timestamp(Calendar.getInstance().getTime().getTime());

      qs = " update amex_optb_conversion_process   " + crlf +
           " set    process_begin_date = ?         " + crlf +
           " where  rec_id = ?                     ";
      ps = con.prepareStatement(qs);
      ps.setTimestamp(1, ts);
      ps.setLong(2, recId);
      ps.executeUpdate();
      commit();
    }
    catch( Exception e )
    {
      logEntry("recordBeginTS(" + recId + ") ",e.toString());
    }
    finally
    {
      try{ ps.close(); } catch( Exception ignore ) {}
    }
  }

  private void recordEndTS( long recId )
  {
    PreparedStatement ps          = null;
    ResultSet         rs          = null;
    String            qs          = null;
    Timestamp         ts          = null;

    try
    {
      ts = new Timestamp(Calendar.getInstance().getTime().getTime());

      qs = " update amex_optb_conversion_process   " + crlf +
           " set    process_end_date = ?           " + crlf +
           " where  rec_id = ?                     ";
      ps = con.prepareStatement(qs);
      ps.setTimestamp(1, ts);
      ps.setLong(2, recId);
      ps.executeUpdate();
      commit();
    }
    catch( Exception e )
    {
      logEntry("recordEndTS(" + recId + ") ",e.toString());
    }
    finally
    {
      try{ ps.close(); } catch( Exception ignore ) {}
    }
  }

  public boolean doOptBConversion( long nodeId )
  {
    PreparedStatement ps          = null;
    ResultSet         rs          = null;
    String            qs          = null;
    boolean           retVal      = true;
    long              optBlueSeNumber       = 0L;
    boolean bundledMerchant = false;
    

    try
    {
      Merchants.removeAllElements();

      qs = " select mf.bank_number,                                             " + crlf +
           "        org.org_group as node_id,                                   " + crlf +
           "        mf.merchant_number,                                         " + crlf +
           "        upper(mf.dba_name) as dba_name,                             " + crlf +
           "        nvl(mf.damexse,-1) as amex_se_number,                       " + crlf +
           "        mf.amex_conversion_date,                                    " + crlf +
           "        mf.amex_plan,                                               " + crlf +
           "        mf.mes_amex_processing,                                     " + crlf +
           "        mr.app_seq_num,                                             " + crlf +
           "        ( select case when count(*) > 0 then 'PSP' else 'ESA' end   " + crlf +
           "          from   mbs_banks                                          " + crlf +
           "          where  am_aggregators like '%' || mf.damexse || '%'       " + crlf +
           "        ) as amex_se_source                                         " + crlf +
           " from   mif mf,                                                     " + crlf +
           "        merchant mr,                                                " + crlf + 
           "        organization org,                                           " + crlf +
           "        group_merchant gm,                                          " + crlf +
           "        merchpayoption mpo                                          " + crlf +
           " where  org.org_group = ?                                           " + crlf +
           "        and gm.org_num = org.org_num                                " + crlf +
           "        and mr.merch_number = gm.merchant_number                    " + crlf +
           "        and mf.merchant_number = mr.merch_number                    " + crlf +
           "        and mf.test_account = 'N'                                   " + crlf +
           "        and mf.date_stat_chgd_to_dcb is null                        " + crlf +
           "        and mpo.app_seq_num(+) = mr.app_seq_num                     " + crlf +
           "        and mpo.cardtype_code(+) = 16                               " + crlf +
           "        and upper(nvl(mpo.amex_opt_blue(+),'N')) != 'Y'             ";

      ps = con.prepareStatement(qs);
      ps.setLong(1, nodeId);
      rs = ps.executeQuery();
      while( rs.next() )
      {
        Merchants.add( new AmexMerchant(rs) );
      }
      ps.close();
      rs.close();

      AmexMerchant merch = null;
      for( int i = 0; i < Merchants.size(); ++i )
      {
        merch = (AmexMerchant) Merchants.elementAt(i);

        log.info("===Amex OptBlue conversion===========================");
        log.info("===Merchant " + merch.getMerchantId());

        if( merch.getAmexSeSource().equals("ESA") )
        {
          log.info("===skip not PSP======================================");
          OptBErrorList.add("<not PSP>           " + merch.getMerchantId() + "  " + merch.getDbaName());
          continue;
        }
        
        if( merch.getAmexSeNumber() == -1L )
        {
          log.info("===skip not Amex accepted============================");
          OptBErrorList.add("<not AM accepted>   " + merch.getMerchantId() + "  " + merch.getDbaName());
          continue;
        }

        //lookup Amex OptBlue SE number
        qs = " select amex_optblue_se_number                        " + crlf +
             "  from  mif mf,                                       " + crlf +
             "        amex_se_mapping asm                           " + crlf +
             " where  mf.merchant_number = ?                        " + crlf +
             "        and asm.sic_code = mf.sic_code                " + crlf +
             "        and asm.cap_number = decode(?,'3943','1101618742','1101618049')" + crlf +
             "        and sysdate between valid_date_begin and valid_date_end";
        ps = con.prepareStatement(qs);
        ps.setLong(1, merch.getMerchantId());
        ps.setString(2, String.valueOf(merch.getBankNumber()) );
        rs = ps.executeQuery();
        if( rs.next() )
        {
          optBlueSeNumber = rs.getLong(1);
        }
        ps.close();
        rs.close();

        if( optBlueSeNumber == 0L )
        {
          log.info("===skip OPTB SE not found============================");
          OptBErrorList.add("<OPTB SE not found> " + merch.getMerchantId() + "  " + merch.getDbaName());
          continue;
        }

        // Updade MIF
        log.info("===update MIF " + merch.getMerchantId());
        log.info("===damexse/amex_plan/amex_conversion_date");
        log.info("===" + merch.getAmexSeNumber() + "/" + merch.getAmexPlan() + "/" + merch.getAmexConvDate() + " ==> " + optBlueSeNumber + "/DN/" + DateTimeFormatter.getCurDateString());
        qs = " update mif                                    " + crlf +
             " set    damexse = ?,                           " + crlf +
             "        amex_plan = 'DN',                      " + crlf +
             "        amex_conversion_date = trunc(sysdate), " + crlf +
             "        mes_amex_processing = 'Y',             " + crlf +
             "        mes_amex_suspended_days = 0            " + crlf +
             " where  merchant_number = ? ";
        ps = con.prepareStatement(qs);
        ps.setLong(1, optBlueSeNumber);
        ps.setLong(2, merch.getMerchantId());
        ps.executeUpdate();
        ps.close();
        commit();

        // Update TRIDENT_PROFILE
        log.info("===update TRIDENT_PROFILE " + merch.getMerchantId());
        qs = " update trident_profile       " + crlf +
             " set    amex_caid = ?,        " + crlf +
             "        amex_accept = 'Y'     " + crlf +
             " where  merchant_number = ?   ";
        ps = con.prepareStatement(qs);
        ps.setLong(1, optBlueSeNumber);
        ps.setLong(2, merch.getMerchantId());
        ps.executeUpdate();
        ps.close();
        commit();

        // Update MERCHPAYOPTION
        log.info("===update MERCHPAYOPTION " + merch.getAppSeqNum());
        qs = " update merchpayoption                             " + crlf +
             " set    merchpo_card_merch_number = ?,             " + crlf +
             "        amex_opt_blue = 'Y',                       " + crlf +
             "        amex_opt_blue_mkt_indicator = 'Y',         " + crlf +
             "        amex_optblue_submit_date = trunc(sysdate)  " + crlf +
             " where  app_seq_num = ? and cardtype_code = 16 ";
        ps = con.prepareStatement(qs);
        ps.setLong(1, optBlueSeNumber);
        ps.setLong(2, merch.getAppSeqNum());
        int rowsUpdated = ps.executeUpdate();
        ps.close();
        commit();

        if(rowsUpdated==0){
          qs = " select  max(nvl(card_sr_number,0)+1)          " + crlf +
               " from    merchpayoption                        " + crlf +
               " where   app_seq_num = ?                       ";
          ps = con.prepareStatement(qs);
          ps.setLong(1, merch.getAppSeqNum());
          rs = ps.executeQuery();
          int card_sr_number = 0;
          if(rs.next()){
            card_sr_number = rs.getInt(1);
          }
          ps.close();
          rs.close();

          log.info("===insert MerchPayOption" + merch.getAmexSeNumber());
          qs = " insert into merchpayoption                                                   " + crlf +
               "   (app_seq_num, cardtype_code, card_sr_number,  merchpo_card_merch_number,   " + crlf +
               "    amex_opt_blue, amex_opt_blue_mkt_indicator, amex_optblue_submit_date)     " + crlf +
               " values  (?,16,?,?,'Y','Y',trunc(sysdate)) ";
          ps = con.prepareStatement(qs);
          ps.setLong(1, merch.getAppSeqNum());
          ps.setInt(2, card_sr_number);
          ps.setLong(3, optBlueSeNumber);
          ps.executeUpdate();
          ps.close();
          commit();
        }

        // Create pricing entries
        log.info("===create pricing entries MBS_PRICING " + merch.getMerchantId());
        // Sunset the current entries (if any)
        qs = " update mbs_pricing                     " + crlf +
            "  set    valid_date_end = last_day(add_months(trunc(sysdate,'month'),-1)) " + crlf +
             " where  merchant_number = ?             " + crlf +
             "        and item_type in (?,?,?,?,?)    ";
        ps = con.prepareStatement(qs);
        ps.setLong(1, merch.getMerchantId());
        ps.setInt(2, NETWORK_FEE);
        ps.setInt(3, INBOUND_FEE);
        ps.setInt(4, NON_SWIPED_TRAN_FEE);
        ps.setInt(5, NON_COMPLIANCE_FEE);
        ps.setInt(6, DATA_QUALITY_FEE);
        ps.executeUpdate();
        ps.close();

        bundledMerchant = isAmexBundleMerchant(merch.getMerchantId());
        
        log.info("===Is the amex bundle merchant :" + bundledMerchant);
        
        qs = " call mbs_add_pricing(?, ?, 'AM', ?, 0.0) ";
        
        ps = con.prepareStatement(qs);
        ps.setLong(1, merch.getMerchantId());
        ps.setInt(2, NETWORK_FEE);
        if(bundledMerchant){
           ps.setDouble(3, 0.00);
        } else {
           ps.setDouble(3, NetWorkFee);
        }
        ps.executeUpdate();
        ps.close();

        ps = con.prepareStatement(qs);
        ps.setLong(1, merch.getMerchantId());
        ps.setInt(2, INBOUND_FEE);
        ps.setDouble(3, InboundFee);
        ps.executeUpdate();
        ps.close();
            
        ps = con.prepareStatement(qs);
        ps.setLong(1, merch.getMerchantId());
        ps.setInt(2, NON_SWIPED_TRAN_FEE);
        if(bundledMerchant){
            ps.setDouble(3, 0.00);
        } else {
            ps.setDouble(3, NonSwipedTranFee);
        }
        ps.executeUpdate();
        ps.close();

        ps = con.prepareStatement(qs);
        ps.setLong(1, merch.getMerchantId());
        ps.setInt(2, NON_COMPLIANCE_FEE);
        ps.setDouble(3, NonComplianceFee);
        ps.executeUpdate();
        ps.close();

        ps = con.prepareStatement(qs);
        ps.setLong(1, merch.getMerchantId());
        ps.setInt(2, DATA_QUALITY_FEE);
        ps.setDouble(3, DataQualityFee);
        ps.executeUpdate();
        ps.close();
        commit();

        log.info("===insert into AMEX_OPTB_CONVERSION_LOG");
        qs = " insert into amex_optb_conversion_log " +
             "   (process_sequence, process_ts, hierarchy_node, merchant_number, damexse, amex_se_source, amex_conv_date, amex_plan, mes_amex_processing) " +
             " values " +
             "   (?, sysdate, ?, ?, ?, ?, ?, ?, ?) ";
        ps = con.prepareStatement(qs);
        ps.setLong(   1, ProcSequence );
        ps.setLong(   2, merch.getNodeId() );
        ps.setLong(   3, merch.getMerchantId() );
        ps.setLong(   4, merch.getAmexSeNumber() );
        ps.setString( 5, merch.getAmexSeSource() );
        ps.setDate(   6, merch.getAmexConvDate() );
        ps.setString( 7, merch.getAmexPlan() );
        ps.setString( 8, merch.getMeSProcessing() );
        ps.executeUpdate();
        ps.close();
        commit();

        OptBSuccessList.add(merch.getMerchantId() + "   " + merch.getDbaName());
      }
    }
    catch( Exception e )
    {
      e.printStackTrace();
      retVal = false;
      logEntry("optBlueConversion(" + nodeId + ") ",e.toString());
    }
    finally
    {
      try{ ps.close(); } catch( Exception ingnore ) {}
      try{ rs.close(); } catch( Exception ingnore ) {}
    }

    return retVal;
  }

  public boolean isAmexBundleMerchant(long merchantId) {
      PreparedStatement ps          = null;
      ResultSet         rs          = null;
      String            qs          = null;
      boolean           bundleMerch = false;
      try{
          qs = "select merchant_number from AMEX_BUNDLE_MERCHANT where merchant_number=?";
          ps = con.prepareStatement(qs);
          ps.setLong(1, merchantId);
          rs = ps.executeQuery();
          while( rs.next() )
              {
                 bundleMerch = true;
              }
          ps.close();
          rs.close();
          } catch(Exception e){
             logEntry("AmexOptBlueConversion::isAmexBundleMerchant(merchantId) method for merchantId :"+ merchantId +". Exception occured: ",e.toString());
             try {
                 ps.close();
                 } catch (SQLException e1) {
                     e1.printStackTrace();
                     }
             try {
                 rs.close();
                 } catch (SQLException e1) {
                     e1.printStackTrace();
                     }
          } finally {
                ps=null;
                rs=null;
                qs=null;
          }
      return bundleMerch;
      }

public boolean doPSPConversion( long nodeId )
  {
    PreparedStatement ps          = null;
    ResultSet         rs          = null;
    String            qs          = null;
    boolean           retVal      = true;

    try
    {
      Merchants.removeAllElements();

      qs = " select mf.bank_number,                                          " + crlf +
           "        org.org_group             as node_id,                    " + crlf +
           "        mf.merchant_number,                                      " + crlf +
           "        upper(mf.dba_name)        as dba_name,                   " + crlf +
           "        decode(nvl(aocl.merchant_number,-1),-1,0,1)              " + crlf +
           "                                  as has_log_entry,              " + crlf +
           "        mf.amex_conversion_date   as amex_optb_conversion_date,  " + crlf +
           "        aocl.damexse              as amex_se_number,             " + crlf +
           "        aocl.amex_conv_date       as amex_conversion_date,       " + crlf +
           "        aocl.amex_plan,                                          " + crlf +
           "        mr.app_seq_num,                                          " + crlf +
           "        aocl.amex_se_source,                                     " + crlf +
           "        aocl.mes_amex_processing,                                " + crlf +
           "        mf.damexse                as amex_optblue_se_number      " + crlf +
           " from   mif mf,                                                  " + crlf +
           "        merchant mr,                                             " + crlf +
           "        merchpayoption mpo,                                      " + crlf +
           "        organization org,                                        " + crlf +
           "        group_merchant gm,                                       " + crlf +
           "        ( select *                                               " + crlf +
           "          from   amex_optb_conversion_log                        " + crlf +
           "          where  amex_se_source = 'PSP'                          " + crlf +
           "          order by rec_id desc                                   " + crlf +
           "        ) aocl                                                   " + crlf +
           " where  org.org_group = ?                                        " + crlf +
           "        and gm.org_num = org.org_num                             " + crlf +
           "        and mr.merch_number = gm.merchant_number                 " + crlf +
           "        and mf.merchant_number = mr.merch_number                 " + crlf +
           "        and mf.test_account = 'N'                                " + crlf +
           "        and mf.date_stat_chgd_to_dcb is null                     " + crlf +
           "        and mpo.app_seq_num(+) = mr.app_seq_num                  " + crlf +
           "        and mpo.cardtype_code(+) = 16                            " + crlf +
           "        and upper(nvl(mpo.amex_opt_blue(+),'N')) = 'Y'           " + crlf +
           "        and aocl.merchant_number(+) = mf.merchant_number         " + crlf +
           "        and rownum = 1                                           ";

      ps = con.prepareStatement(qs);
      ps.setLong(1, nodeId);
      rs = ps.executeQuery();
      while( rs.next() )
      {
        if( rs.getInt("has_log_entry") == 0 )
        {
          PspErrorList.add("<log not found>     " + rs.getString("merchant_number") + "  " + rs.getString("dba_name"));
          continue;
        }

        Merchants.add( new AmexMerchant(rs) );
      }
      ps.close();
      rs.close();

      AmexMerchant merch = null;
      for( int i = 0; i < Merchants.size(); ++i )
      {
        merch = (AmexMerchant) Merchants.elementAt(i);

        // Updade MIF
        log.info("===OptB/PSP conversion=================================");
        log.info("===update MIF " + merch.getMerchantId());
        log.info("===damexse/amex_plan/amex_conversion_date");
        log.info("===" + merch.getOptbSeNumber() + "/" + merch.getAmexPlan() + "/" + merch.getAmexOptbConvDate() + " ==> " + merch.getAmexSeNumber() + "/DN/" + merch.getAmexConvDate());
        qs = " update mif                                    " + crlf +
             " set    damexse = ?,                           " + crlf +
             "        amex_plan = ?,                         " + crlf +
             "        amex_conversion_date = ?,              " + crlf +
             "        mes_amex_processing = ?                " + crlf +
             " where  merchant_number = ? ";
        ps = con.prepareStatement(qs);
        ps.setLong(1, merch.getAmexSeNumber());
        ps.setString(2, merch.getAmexPlan());
        ps.setDate(3, merch.getAmexConvDate());
        ps.setString(4, merch.getMeSProcessing());
        ps.setLong(5, merch.getMerchantId() );
        ps.executeUpdate();
        ps.close();
        commit();

        // Update TRIDENT_PROFILE
        log.info("===update TRIDENT_PROFILE " + merch.getMerchantId());
        qs = " update trident_profile       " + crlf +
             " set    amex_caid = ?,        " + crlf +
             "        amex_accept = 'Y'     " + crlf +
             " where  merchant_number = ?   ";
        ps = con.prepareStatement(qs);
        ps.setLong(1, merch.getAmexSeNumber());
        ps.setLong(2, merch.getMerchantId());
        ps.executeUpdate();
        ps.close();
        commit();

        // Update MERCHPAYOPTION
        log.info("===update MERCHPAYOPTION " + merch.getAppSeqNum());
        qs = " update merchpayoption                             " + crlf +
             " set    merchpo_card_merch_number = ?,             " + crlf +
             "        amex_opt_blue = 'N',                       " + crlf +
             "        amex_opt_blue_mkt_indicator = null,        " + crlf +
             "        amex_optblue_submit_date = null            " + crlf +
             " where  app_seq_num = ? and cardtype_code = 16 ";
        ps = con.prepareStatement(qs);
        ps.setLong(1, merch.getAmexSeNumber());
        ps.setLong(2, merch.getAppSeqNum());
        ps.executeUpdate();
        ps.close();
        commit();

        // Remove pricing entries
        log.info("===sunset pricing entries MBS_PRICING " + merch.getMerchantId());
        qs = " update mbs_pricing                     " + crlf +
             " set    valid_date_end = last_day(add_months(trunc(sysdate,'month'),-1)) " + crlf +
             " where  merchant_number = ?             " + crlf +
             "        and item_type in (?,?,?,?,?)    ";
        ps = con.prepareStatement(qs);
        ps.setLong(1, merch.getMerchantId());
        ps.setInt(2, NETWORK_FEE);
        ps.setInt(3, INBOUND_FEE);
        ps.setInt(4, NON_SWIPED_TRAN_FEE);
        ps.setInt(5, NON_COMPLIANCE_FEE);
        ps.setInt(6, DATA_QUALITY_FEE);
        ps.executeUpdate();
        ps.close();

        log.info("===insert into AMEX_OPTB_CONVERSION_LOG");
        qs = " insert into amex_optb_conversion_log " +
             "   (process_sequence, process_ts, hierarchy_node, merchant_number, damexse, amex_se_source, amex_conv_date, amex_plan, mes_amex_processing) " +
             " values " +
             "   (?, sysdate, ?, ?, ?, 'OPTB', ?, 'DN', 'Y') ";
        ps = con.prepareStatement(qs);
        ps.setLong(   1, ProcSequence );
        ps.setLong(   2, merch.getNodeId() );
        ps.setLong(   3, merch.getMerchantId() );
        ps.setLong(   4, merch.getOptbSeNumber() );
        ps.setDate(   5, merch.getAmexOptbConvDate() );
        ps.executeUpdate();
        ps.close();
        commit();

        PspSuccessList.add(merch.getMerchantId() + "   " + merch.getDbaName());
      }
    }
    catch( Exception e )
    {
      e.printStackTrace();
      retVal = false;
      logEntry("optBlueConversion(" + nodeId + ") ",e.toString());
    }
    finally
    {
      try{ ps.close(); } catch( Exception ingnore ) {}
      try{ rs.close(); } catch( Exception ingnore ) {}
    }

    return retVal;
  }

public boolean doNABOptBConversion( long nodeId )
{
  PreparedStatement ps          = null;
  ResultSet         rs          = null;
  String            qs          = null;
  boolean           retVal      = true;
  long              optBlueSeNumber       = 0L;
  boolean bundledMerchant = false;
  

  try
  {
    Merchants.removeAllElements();

    qs = " select mf.bank_number,                                                 " + crlf +
         "        org.org_group as node_id,                                       " + crlf +
         "        mf.merchant_number,                                             " + crlf +
         "        upper(mf.dba_name) as dba_name,                                 " + crlf +
         "        nvl(mf.damexse,-1) as amex_se_number,                           " + crlf +
         "        mf.amex_conversion_date,                                        " + crlf +
         "        mf.amex_plan,                                                   " + crlf +
         "        mf.mes_amex_processing,                                         " + crlf +
         "        mr.app_seq_num,                                                 " + crlf +
         "        decode(nvl(mf.damexse,-1),-1,'NAB','NONNAB') as amex_se_source  " + crlf +
         " from   mif mf,                                                         " + crlf +
         "        merchant mr,                                                    " + crlf + 
         "        organization org,                                               " + crlf +
         "        group_merchant gm,                                              " + crlf +
         "        merchpayoption mpo                                              " + crlf +
         " where  org.org_group = ?                                               " + crlf +
         "        and gm.org_num = org.org_num                                    " + crlf +
         "        and mr.merch_number = gm.merchant_number                        " + crlf +
         "        and mf.merchant_number = mr.merch_number                        " + crlf +
         "        and mf.test_account = 'N'                                       " + crlf +
         "        and mf.date_stat_chgd_to_dcb is null                            " + crlf +
         "        and mpo.app_seq_num(+) = mr.app_seq_num                         " + crlf +
         "        and mpo.cardtype_code(+) = 16                                   " + crlf +
         "        and upper(nvl(mpo.amex_opt_blue(+),'N')) != 'Y'                 ";

    ps = con.prepareStatement(qs);
    ps.setLong(1, nodeId);
    rs = ps.executeQuery();
    while( rs.next() )
    {
      Merchants.add( new AmexMerchant(rs) );
    }
    ps.close();
    rs.close();

    AmexMerchant merch = null;
    for( int i = 0; i < Merchants.size(); ++i )
    {
      merch = (AmexMerchant) Merchants.elementAt(i);

      log.info("===Amex NAB - OptBlue conversion===========================");
      log.info("===Merchant " + merch.getMerchantId());

      if( merch.getAmexSeSource().equals("NONNAB") )
      {
        log.info("===skip not NAB======================================");
        OptBNABErrorList.add("<not NAB>           " + merch.getMerchantId() + "  " + merch.getDbaName());
        continue;
      }
      
      if( merch.getAmexSeNumber() != -1L )
      {
        log.info("===skip not NAB Amex Merchant============================");
        OptBNABErrorList.add("<not NAB Amex Merchant>   " + merch.getMerchantId() + "  " + merch.getDbaName());
        continue;
      }

      if( merch.getAmexSeNumber() == -1L )
      {
      
      //lookup Amex OptBlue SE number
      qs = " select amex_optblue_se_number                        " + crlf +
           "  from  mif mf,                                       " + crlf +
           "        amex_se_mapping asm                           " + crlf +
           " where  mf.merchant_number = ?                        " + crlf +
           "        and asm.sic_code = mf.sic_code                " + crlf +
           "        and asm.cap_number = decode(?,'3943','1101618742','1101618049')" + crlf +
           "        and sysdate between valid_date_begin and valid_date_end";
      ps = con.prepareStatement(qs);
      ps.setLong(1, merch.getMerchantId());
      ps.setString(2, String.valueOf(merch.getBankNumber()) );
      rs = ps.executeQuery();
      if( rs.next() )
      {
        optBlueSeNumber = rs.getLong(1);
      }
      ps.close();
      rs.close();

      if( optBlueSeNumber == 0L )
      {
        log.info("===skip OPTB SE not found============================");
        OptBNABErrorList.add("<OPTB SE not found> " + merch.getMerchantId() + "  " + merch.getDbaName());
        continue;
      }
      log.info("===converting NAB Amex Merchant============================");
      // Updade MIF
      log.info("===update MIF " + merch.getMerchantId());
      log.info("===damexse/amex_plan/amex_conversion_date");
      log.info("===" + merch.getAmexSeNumber() + "/" + merch.getAmexPlan() + "/" + merch.getAmexConvDate() + " ==> " + optBlueSeNumber + "/DN/" + DateTimeFormatter.getCurDateString());
      qs = " update mif                                    " + crlf +
           " set    damexse = ?,                           " + crlf +
           "        amex_plan = 'DN',                      " + crlf +
           "        amex_conversion_date = trunc(sysdate), " + crlf +
           "        mes_amex_processing = 'Y',             " + crlf +
           "        mes_amex_suspended_days = 0            " + crlf +
           " where  merchant_number = ? ";
      ps = con.prepareStatement(qs);
      ps.setLong(1, optBlueSeNumber);
      ps.setLong(2, merch.getMerchantId());
      ps.executeUpdate();
      ps.close();
      commit();

      // Update TRIDENT_PROFILE
      log.info("===update TRIDENT_PROFILE " + merch.getMerchantId());
      qs = " update trident_profile       " + crlf +
           " set    amex_caid = ?,        " + crlf +
           "        amex_accept = 'Y'     " + crlf +
           " where  merchant_number = ?   ";
      ps = con.prepareStatement(qs);
      ps.setLong(1, optBlueSeNumber);
      ps.setLong(2, merch.getMerchantId());
      ps.executeUpdate();
      ps.close();
      commit();

      // Update MERCHPAYOPTION
      log.info("===update MERCHPAYOPTION " + merch.getAppSeqNum());
      qs = " update merchpayoption                             " + crlf +
           " set    merchpo_card_merch_number = ?,             " + crlf +
           "        amex_opt_blue = 'Y',                       " + crlf +
           "        amex_opt_blue_mkt_indicator = 'Y',         " + crlf +
           "        amex_optblue_submit_date = trunc(sysdate)  " + crlf +
           " where  app_seq_num = ? and cardtype_code = 16 ";
      ps = con.prepareStatement(qs);
      ps.setLong(1, optBlueSeNumber);
      ps.setLong(2, merch.getAppSeqNum());
      int rowsUpdated = ps.executeUpdate();
      ps.close();
      commit();

      if(rowsUpdated==0){
        qs = " select  max(nvl(card_sr_number,0)+1)          " + crlf +
             " from    merchpayoption                        " + crlf +
             " where   app_seq_num = ?                       ";
        ps = con.prepareStatement(qs);
        ps.setLong(1, merch.getAppSeqNum());
        rs = ps.executeQuery();
        int card_sr_number = 0;
        if(rs.next()){
          card_sr_number = rs.getInt(1);
        }
        ps.close();
        rs.close();

        log.info("===insert MerchPayOption" + merch.getAmexSeNumber());
        qs = " insert into merchpayoption                                                   " + crlf +
             "   (app_seq_num, cardtype_code, card_sr_number,  merchpo_card_merch_number,   " + crlf +
             "    amex_opt_blue, amex_opt_blue_mkt_indicator, amex_optblue_submit_date)     " + crlf +
             " values  (?,16,?,?,'Y','Y',trunc(sysdate)) ";
        ps = con.prepareStatement(qs);
        ps.setLong(1, merch.getAppSeqNum());
        ps.setInt(2, card_sr_number);
        ps.setLong(3, optBlueSeNumber);
        ps.executeUpdate();
        ps.close();
        commit();
      }

      // Create pricing entries
      log.info("===create pricing entries MBS_PRICING " + merch.getMerchantId());
      // Sunset the current entries (if any)
      qs = " update mbs_pricing                     " + crlf +
          "  set    valid_date_end = last_day(add_months(trunc(sysdate,'month'),-1)) " + crlf +
           " where  merchant_number = ?             " + crlf +
           "        and item_subclass = 'AM'        " + crlf +
           "        and item_type in (?,?,?,?,?,?)    ";
      ps = con.prepareStatement(qs);
      ps.setLong(1, merch.getMerchantId());
      ps.setInt(2, NETWORK_FEE);
      ps.setInt(3, INBOUND_FEE);
      ps.setInt(4, NON_SWIPED_TRAN_FEE);
      ps.setInt(5, NON_COMPLIANCE_FEE);
      ps.setInt(6, DATA_QUALITY_FEE);
      ps.setInt(7, MARKUP_FEE);
      ps.executeUpdate();
      ps.close();
      
      log.info("===gather markup fee details");
      
      qs = " select rate, per_item         " + crlf +
              " from   amex_nab_markup_fee   " + crlf +
              " where   merchant_number = ?       " ;
      ps = con.prepareStatement(qs);
      ps.setLong(1, merch.getMerchantId());
      rs = ps.executeQuery();
      while( rs.next() )
      {
    	  MarkupFeeRate    = rs.getDouble("rate");
    	  MarkupFeePerItem   = rs.getDouble("per_item");
      }

      rs.close();
      ps.close();
      
      
      qs = " call mbs_add_pricing(?, ?, 'AM', ?, 0.0) ";
      
      ps = con.prepareStatement(qs);
      ps.setLong(1, merch.getMerchantId());
      ps.setInt(2, NETWORK_FEE);
      ps.setDouble(3, NetWorkFee);
      ps.executeUpdate();
      ps.close();

      ps = con.prepareStatement(qs);
      ps.setLong(1, merch.getMerchantId());
      ps.setInt(2, INBOUND_FEE);
      ps.setDouble(3, InboundFee);
      ps.executeUpdate();
      ps.close();
          
      ps = con.prepareStatement(qs);
      ps.setLong(1, merch.getMerchantId());
      ps.setInt(2, NON_SWIPED_TRAN_FEE);
      ps.setDouble(3, NonSwipedTranFee);
      ps.executeUpdate();
      ps.close();

      ps = con.prepareStatement(qs);
      ps.setLong(1, merch.getMerchantId());
      ps.setInt(2, NON_COMPLIANCE_FEE);
      ps.setDouble(3, NonComplianceFee);
      ps.executeUpdate();
      ps.close();

      ps = con.prepareStatement(qs);
      ps.setLong(1, merch.getMerchantId());
      ps.setInt(2, DATA_QUALITY_FEE);
      ps.setDouble(3, DataQualityFee);
      ps.executeUpdate();
      ps.close();
      
      qs = " call mbs_add_pricing(?, ?, 'AM', ?, ?) ";
      
      ps = con.prepareStatement(qs);
      ps.setLong(1, merch.getMerchantId());
      ps.setInt(2, MARKUP_FEE);
      ps.setDouble(3, MarkupFeeRate);
      ps.setDouble(4, MarkupFeePerItem);
      ps.executeUpdate();
      ps.close();
      commit();
      
      log.info("===update/insert optblue flag in TRNCHRG");
      
      qs = " update tranchrg                                    " + crlf +
           "  set    amex_optblue_pricing_type = 1              " + crlf +
           " where  cardtype_code = 16 and app_seq_num = ?      ";
      
      ps = con.prepareStatement(qs);
      ps.setLong(1, merch.getAppSeqNum());
      rowsUpdated = ps.executeUpdate();
      ps.close();
      commit();

      if(rowsUpdated==0){
    	  
    	  qs = " insert into tranchrg                                                   " + crlf +
                  "   (app_seq_num, cardtype_code, amex_optblue_pricing_type)     " + crlf +
                  " values  (?,16,1) ";
             ps = con.prepareStatement(qs);
             ps.setLong(1, merch.getAppSeqNum());
             ps.executeUpdate();
             ps.close();
             commit();
      }
      
      log.info("===insert into AMEX_OPTB_CONVERSION_LOG");
      
      qs = " insert into amex_optb_conversion_log " +
           "   (process_sequence, process_ts, hierarchy_node, merchant_number, damexse, amex_se_source, amex_conv_date, amex_plan, mes_amex_processing) " +
           " values " +
           "   (?, sysdate, ?, ?, ?, ?, sysdate, 'DN', 'Y') ";
      ps = con.prepareStatement(qs);
      ps.setLong(   1, ProcSequence );
      ps.setLong(   2, merch.getNodeId() );
      ps.setLong(   3, merch.getMerchantId() );
      ps.setLong(   4, merch.getAmexSeNumber() );
      ps.setString( 5, merch.getAmexSeSource() );
      ps.executeUpdate();
      ps.close();
      commit();

      OptBNABSuccessList.add(merch.getMerchantId() + "   " + merch.getDbaName());
    }
    }
  }
  catch( Exception e )
  {
    e.printStackTrace();
    retVal = false;
    logEntry("optBlueConversion(" + nodeId + ") ",e.toString());
  }
  finally
  {
    try{ ps.close(); } catch( Exception ingnore ) {}
    try{ rs.close(); } catch( Exception ingnore ) {}
  }

  return retVal;
}

public boolean doNABConversion(long nodeId){

    PreparedStatement ps          = null;
    ResultSet         rs          = null;
    String            qs          = null;
    boolean           retVal      = true;

    try
    {
      Merchants.removeAllElements();

      qs = " select mf.bank_number,                                          " + crlf +
           "        org.org_group             as node_id,                    " + crlf +
           "        mf.merchant_number,                                      " + crlf +
           "        upper(mf.dba_name)        as dba_name,                   " + crlf +
           "        decode(nvl(aocl.merchant_number,-2),-2,0,1)              " + crlf +
           "                                  as has_log_entry,              " + crlf +
           "        mf.amex_conversion_date   as amex_optb_conversion_date,  " + crlf +
           "        aocl.damexse              as amex_se_number,             " + crlf +
           "        aocl.amex_conv_date       as amex_conversion_date,       " + crlf +
           "        aocl.amex_plan,                                          " + crlf +
           "        mr.app_seq_num,                                          " + crlf +
           "        aocl.amex_se_source,                                     " + crlf +
           "        aocl.mes_amex_processing,                                " + crlf +
           "        mf.damexse                as amex_optblue_se_number      " + crlf +
           " from   mif mf,                                                  " + crlf +
           "        merchant mr,                                             " + crlf +
           "        merchpayoption mpo,                                      " + crlf +
           "        organization org,                                        " + crlf +
           "        group_merchant gm,                                       " + crlf +
           "        ( select *                                               " + crlf +
           "          from   amex_optb_conversion_log                        " + crlf +
           "          where  amex_se_source = 'NAB'                          " + crlf +
           "          order by rec_id desc                                   " + crlf +
           "        ) aocl                                                   " + crlf +
           " where  org.org_group = ?                                        " + crlf +
           "        and gm.org_num = org.org_num                             " + crlf +
           "        and mr.merch_number = gm.merchant_number                 " + crlf +
           "        and mf.merchant_number = mr.merch_number                 " + crlf +
           "        and mf.test_account = 'N'                                " + crlf +
           "        and mf.date_stat_chgd_to_dcb is null                     " + crlf +
           "        and mpo.app_seq_num(+) = mr.app_seq_num                  " + crlf +
           "        and mpo.cardtype_code(+) = 16                            " + crlf +
           "        and upper(nvl(mpo.amex_opt_blue(+),'N')) = 'Y'           " + crlf +
           "        and aocl.merchant_number(+) = mf.merchant_number         " + crlf +
           "        and rownum = 1                                           ";

      ps = con.prepareStatement(qs);
      ps.setLong(1, nodeId);
      rs = ps.executeQuery();
      while( rs.next() )
      {
        if( rs.getInt("has_log_entry") == 0 )
        {
        NABErrorList.add("<log not found>     " + rs.getString("merchant_number") + "  " + rs.getString("dba_name"));
          continue;
        }

        Merchants.add( new AmexMerchant(rs) );
      }
      ps.close();
      rs.close();

      AmexMerchant merch = null;
      for( int i = 0; i < Merchants.size(); ++i )
      {
        merch = (AmexMerchant) Merchants.elementAt(i);

        // Updade MIF
        log.info("===OptB/NAB conversion=================================");
        log.info("===update MIF " + merch.getMerchantId());
        log.info("===damexse/amex_plan/amex_conversion_date");
        log.info("===" + merch.getOptbSeNumber() + "/" + merch.getAmexPlan() + "/" + merch.getAmexOptbConvDate() + " ==> null/DN/" + merch.getAmexConvDate());
        qs = " update mif                                    " + crlf +
             " set    damexse = null,                           " + crlf +
             "        amex_plan = ?,                         " + crlf +
             "        amex_conversion_date = ?,              " + crlf +
             "        mes_amex_processing = ?                " + crlf +
             " where  merchant_number = ? ";
        ps = con.prepareStatement(qs);
        ps.setString(1, merch.getAmexPlan());
        ps.setDate(2, merch.getAmexConvDate());
        ps.setString(3, merch.getMeSProcessing());
        ps.setLong(4, merch.getMerchantId() );
        ps.executeUpdate();
        ps.close();
        commit();

        // Update TRIDENT_PROFILE
        log.info("===update TRIDENT_PROFILE " + merch.getMerchantId());
        qs = " update trident_profile       " + crlf +
             " set    amex_caid = ?,        " + crlf +
             "        amex_accept = 'Y'     " + crlf +
             " where  merchant_number = ?   ";
        ps = con.prepareStatement(qs);
        ps.setLong(1, merch.getAmexSeNumber());
        ps.setLong(2, merch.getMerchantId());
        ps.executeUpdate();
        ps.close();
        commit();

        // Update MERCHPAYOPTION
        log.info("===update MERCHPAYOPTION " + merch.getAppSeqNum());
        qs = " update merchpayoption                             " + crlf +
             " set    merchpo_card_merch_number = ?,             " + crlf +
             "        amex_opt_blue = 'N',                       " + crlf +
             "        amex_opt_blue_mkt_indicator = null,        " + crlf +
             "        amex_optblue_submit_date = null            " + crlf +
             " where  app_seq_num = ? and cardtype_code = 16 ";
        ps = con.prepareStatement(qs);
        ps.setLong(1, merch.getAmexSeNumber());
        ps.setLong(2, merch.getAppSeqNum());
        ps.executeUpdate();
        ps.close();
        commit();

        // Remove pricing entries
        log.info("===sunset pricing entries MBS_PRICING " + merch.getMerchantId());
        qs = " update mbs_pricing                     " + crlf +
             " set    valid_date_end = last_day(add_months(trunc(sysdate,'month'),-1)) " + crlf +
             " where  merchant_number = ?             " + crlf +
             "        and item_subclass = 'AM'        " + crlf +
             "        and item_type in (?,?,?,?,?,?)    ";
        ps = con.prepareStatement(qs);
        ps.setLong(1, merch.getMerchantId());
        ps.setInt(2, NETWORK_FEE);
        ps.setInt(3, INBOUND_FEE);
        ps.setInt(4, NON_SWIPED_TRAN_FEE);
        ps.setInt(5, NON_COMPLIANCE_FEE);
        ps.setInt(6, DATA_QUALITY_FEE);
        ps.setInt(7, MARKUP_FEE);
        ps.executeUpdate();
        ps.close();
        
        log.info("===update/insert optblue flag in TRNCHRG");
        
        qs = " update tranchrg                                    " + crlf +
             "  set    amex_optblue_pricing_type = null              " + crlf +
             " where  cardtype_code = 16 and app_seq_num = ?      ";
        
        ps = con.prepareStatement(qs);
        ps.setLong(1, merch.getAppSeqNum());
        ps.executeUpdate();
        ps.close();
        commit();

        log.info("===insert into AMEX_OPTB_CONVERSION_LOG");
        qs = " insert into amex_optb_conversion_log " +
             "   (process_sequence, process_ts, hierarchy_node, merchant_number, damexse, amex_se_source, amex_conv_date, amex_plan, mes_amex_processing) " +
             " values " +
             "   (?, sysdate, ?, ?, ?, 'OPTB', ?, 'DN', 'Y') ";
        ps = con.prepareStatement(qs);
        ps.setLong(   1, ProcSequence );
        ps.setLong(   2, merch.getNodeId() );
        ps.setLong(   3, merch.getMerchantId() );
        ps.setLong(   4, merch.getOptbSeNumber() );
        ps.setDate(   5, merch.getAmexOptbConvDate() );
        ps.executeUpdate();
        ps.close();
        commit();

        NABSuccessList.add(merch.getMerchantId() + "   " + merch.getDbaName());
      }
    }
    catch( Exception e )
    {
      e.printStackTrace();
      retVal = false;
      logEntry("optBlueConversion(" + nodeId + ") ",e.toString());
    }
    finally
    {
      try{ ps.close(); } catch( Exception ingnore ) {}
      try{ rs.close(); } catch( Exception ingnore ) {}
    }

    return retVal;
  
}


  private void sendStatusEmail()
  {
    StringBuffer      bodyText    = new StringBuffer();

    bodyText.append(OptBSuccessList.size() > 0 ? "The following PSP merchant(s) have been successfully converted to Amex OptBlue" + crlf : "");
    for( int i = 0; i < OptBSuccessList.size(); ++i ) {
      bodyText.append( OptBSuccessList.elementAt(i) + crlf );
    }

    bodyText.append(OptBErrorList.size() > 0 ? "The following PSP merchant(s) have not been converted to Amex OptBlue" + crlf : "");
    for( int i = 0; i < OptBErrorList.size(); ++i ) {
      bodyText.append( OptBErrorList.elementAt(i) + crlf );
    }

    bodyText.append(PspSuccessList.size() > 0 ? "The following merchant(s) have been successfully rolled back to Amex PSP" + crlf : "");
    for( int i = 0; i < PspSuccessList.size(); ++i ) {
      bodyText.append( PspSuccessList.elementAt(i) + crlf );
    }

    bodyText.append(PspErrorList.size() > 0 ? "The following merchant(s) have not been rolled back to Amex PSP" + crlf : "");
    for( int i = 0; i < PspErrorList.size(); ++i ) {
      bodyText.append( PspErrorList.elementAt(i) + crlf );
    }
    
    bodyText.append(OptBNABSuccessList.size() > 0 ? "The following NAB merchant(s) have been successfully converted to Amex OptBlue" + crlf : "");
    for( int i = 0; i < OptBNABSuccessList.size(); ++i ) {
      bodyText.append( OptBNABSuccessList.elementAt(i) + crlf );
    }
    
    bodyText.append(OptBNABErrorList.size() > 0 ? "The following NAB merchant(s) have not been converted to Amex OptBlue" + crlf : "");
    for( int i = 0; i < OptBNABErrorList.size(); ++i ) {
      bodyText.append( OptBNABErrorList.elementAt(i) + crlf );
    }
    
    bodyText.append(NABSuccessList.size() > 0 ? "The following merchant(s) have been successfully rolled back to Amex NAB" + crlf : "");
    for( int i = 0; i < NABSuccessList.size(); ++i ) {
      bodyText.append( NABSuccessList.elementAt(i) + crlf );
    }

    bodyText.append(NABErrorList.size() > 0 ? "The following merchant(s) have not been rolled back to Amex NAB" + crlf : "");
    for( int i = 0; i < NABErrorList.size(); ++i ) {
      bodyText.append( NABErrorList.elementAt(i) + crlf );
    }

    sendStatusEmail( "Amex OptBlue/PSP/NAB Conversion", bodyText.toString() );
  }

  private void sendStatusEmail(String subject, String bodyText )
  {
    MailMessage   mailer          = null;

    try
    {
      if( bodyText.equals("") )
      {
        return;
      }

      mailer = new MailMessage();

      mailer.setFrom( EmailNotifySender );
      mailer.addTo( EmailNotifyRecipients );
      mailer.setSubject( subject );
      mailer.setText( bodyText );
      mailer.send();
    }
    catch(Exception e)
    {
      logEntry("sendStatusEmail() ", e.toString());
    }
  }

  public void process()
  {
    PreparedStatement ps          = null;
    ResultSet         rs          = null;
    String            qs          = null;
    long              recId       = 0L;
    long              nodeId      = -1L;

    try
    {
      //process PSP to Amex OptBlue conversion
      qs = " select rec_id, hierarchy_node         " + crlf +
           " from   amex_optb_conversion_process   " + crlf +
           " where  process_type = 1               " + crlf +
           "        and process_sequence = ?       " ;
      ps = con.prepareStatement(qs);
      ps.setLong(1, ProcSequence);
      rs = ps.executeQuery();
      while( rs.next() )
      {
        recId    = rs.getLong("rec_id");
        nodeId   = rs.getLong("hierarchy_node");

        //mark begin time
        recordBeginTS(recId);

        if( doOptBConversion(nodeId) );
        {
          //mark end time
          recordEndTS(recId);
        }
      }

      rs.close();
      ps.close();

      //process PSP conversion
      qs = " select rec_id, hierarchy_node         " + crlf +
           " from   amex_optb_conversion_process   " + crlf +
           " where  process_type = 2               " + crlf +
           "        and process_sequence = ?       " ;
      ps = con.prepareStatement(qs);
      ps.setLong(1, ProcSequence);
      rs = ps.executeQuery();

      while( rs.next() )
      {
        recId    = rs.getLong("rec_id");
        nodeId   = rs.getLong("hierarchy_node");

        //mark begin time
        recordBeginTS(recId);

        if( doPSPConversion(nodeId) );
        {
          //mark end time
          recordEndTS(recId);
        }
      }

      rs.close();
      ps.close();
      
      //process NAB to Amex OptBlue conversion
      qs = " select rec_id, hierarchy_node         " + crlf +
           " from   amex_optb_conversion_process   " + crlf +
           " where  process_type = 3               " + crlf +
           "        and process_sequence = ?       " ;
      ps = con.prepareStatement(qs);
      ps.setLong(1, ProcSequence);
      rs = ps.executeQuery();

      while( rs.next() )
      {
        recId    = rs.getLong("rec_id");
        nodeId   = rs.getLong("hierarchy_node");

        //mark begin time
        recordBeginTS(recId);

        if( doNABOptBConversion(nodeId) );
        {
          //mark end time
          recordEndTS(recId);
        }
      }

      rs.close();
      ps.close();
      
      //process NAB conversion
      qs = " select rec_id, hierarchy_node         " + crlf +
           " from   amex_optb_conversion_process   " + crlf +
           " where  process_type = 4               " + crlf +
           "        and process_sequence = ?       " ;
      ps = con.prepareStatement(qs);
      ps.setLong(1, ProcSequence);
      rs = ps.executeQuery();

      while( rs.next() )
      {
        recId    = rs.getLong("rec_id");
        nodeId   = rs.getLong("hierarchy_node");

        //mark begin time
        recordBeginTS(recId);

        if( doNABConversion(nodeId) );
        {
          //mark end time
          recordEndTS(recId);
        }
      }

      rs.close();
      ps.close();

      sendStatusEmail();
    }
    catch(Exception e)
    {
      e.printStackTrace();
      log.info(e.toString());
      logEntry("process() ", e.toString());
    }
    finally
    {
      try { rs.close(); } catch (Exception e) {}
      try { ps.close(); } catch (Exception e) {}
    }
  }

  public boolean execute()
  {
    PreparedStatement ps          = null;
    ResultSet         rs          = null;
    String            qs          = null;
    int               recCount    = 0;

    try
    {
      connect();
      readProps();
      ps = con.prepareStatement(" select count(1)  from amex_optb_conversion_process  where process_sequence = ? ");
      ps.setLong(1, ProcSequence);
      rs = ps.executeQuery();
      rs.next();
      recCount = rs.getInt(1);
      ps.close();
      rs.close();

      if( recCount > 0 && ProcSequence == 0L )
      {
        // mark items
        ps = con.prepareStatement(" select amex_optb_conv_sequence.nextval  from dual ");
        rs = ps.executeQuery();
        rs.next();
        ProcSequence = rs.getLong(1);
        ps.close();
        rs.close();

        qs = " update amex_optb_conversion_process   " +
             " set    process_sequence = ?           " +
             " where  process_sequence = 0           " ;

        ps = con.prepareStatement(qs);
        ps.setLong(1, ProcSequence);
        ps.executeUpdate();
        ps.close();
        commit();
      }

      process();
    }
    catch(Exception e)
    {
      e.printStackTrace();
      log.info(e.toString());
      logEntry("buildFile() ", e.toString());
    }
    finally
    {
      try { rs.close(); } catch (Exception e) {}
      try { ps.close(); } catch (Exception e) {}
      cleanUp();
    }

    return true;
  }

  public static void main(String[] args)
  {
    AmexOptBlueConversion proc = null;
    try
    {
      proc = new AmexOptBlueConversion();
      if( args.length > 0 && args[0].equals("process") )
      {
        proc.ProcSequence = Long.parseLong(args[1]);
      }

      proc.execute();
    }
    catch(Exception e)
    {
      e.printStackTrace();
      log.error(e.toString());
    }
    finally
    {
      try{ proc.cleanUp(); } catch( Exception ee ) {}
      log.debug("Command line execution of AmexOptBlueConversion complete");
      Runtime.getRuntime().exit(0);
    }
  }
}