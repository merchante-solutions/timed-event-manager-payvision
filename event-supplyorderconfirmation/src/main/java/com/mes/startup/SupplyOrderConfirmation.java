/*@lineinfo:filename=SupplyOrderConfirmation*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/flatfile/GCFOrderFile.sqlj $

  Description:

    GCFOrderFile

    Concrete representation of OrderFile
    currently used by SupplyOrderLoad

  Last Modified By   : $Author: vbannikov $
  Last Modified Date : $Date: 2015-06-22 09:43:59 -0700 (Mon, 22 Jun 2015) $
  Version            : $Revision: 23700 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.StringTokenizer;
// log4j
import org.apache.log4j.Logger;
import com.jscape.inet.ftp.FtpFile;
import com.jscape.inet.ftps.Ftps;
import com.mes.config.MesDefaults;
import com.mes.constants.MesEmails;
import com.mes.database.SQLJConnectionBase;
//import com.mes.net.FTPClient;
//import com.mes.net.FTPTransferType;
import com.mes.net.MailMessage;
import com.mes.support.DateTimeFormatter;

public class SupplyOrderConfirmation extends EventBase
{

  // create class log category
  static Logger log = Logger.getLogger(SupplyOrderConfirmation.class);

  private String DOWNLOAD_DIRECTORY     = "download";
  private int MAX_FTP_ATTEMPTS          = 5;

  private ArrayList fileList            = null;
  private ArrayList deleteList          = null;
  private ArrayList errorList           = null;

  private Ftps   ftps         = null;
  private String FTPHOST      = null;
  private String FTPUSER      = null;
  private String FTPPASSWORD  = null;
  private int    FTPPORT      = 0;


  public SupplyOrderConfirmation()
  {
    fileList = new ArrayList();
    deleteList = new ArrayList();
    errorList = new ArrayList();

    try
    {
      FTPHOST      = MesDefaults.getString(MesDefaults.DK_GCF_FTPS_IP_ADDRESS);
      FTPUSER      = MesDefaults.getString(MesDefaults.DK_GCF_USER);
      FTPPASSWORD  = MesDefaults.getString(MesDefaults.DK_GCF_PASSWORD);
      FTPPORT      = MesDefaults.getInt(MesDefaults.DK_GCF_FTPS_PORT);

      log.debug("FTPHOST = "+FTPHOST);
      log.debug("FTPPORT = "+FTPPORT);
      log.debug("FTPUSER = "+FTPUSER);
      log.debug("FTPPASSWORD = "+FTPPASSWORD);

    }
    catch(Exception e)
    {
      logEntry("SupplyOrderConfirmation() - Cannot get properties for SFTP: ", e.getMessage());
    }
  }

 /**
  * METHOD execute()
  *
  * retrieves GCF file for supply ordering
  * adds info to supply order conf and service calls
  */
  public boolean execute()
  {
    boolean result = true;
    try
    {
      retrieveFiles();

      if(fileList.size() > 0)
      {
        processFiles();
      }
      else
      {
        throw new Exception(":: NOTE :: No confirmation file found - remote directory empty.");
      }

    }
    catch(Exception e)
    {

      errorList.add("Exception: "+e.getMessage());

      try { ftps.disconnect(); }catch(Exception e1){ }

      result = false;

    }
    finally
    {
      //even if the process fails mid-way
      //remove those files that were properly
      //processed before the Exception
      deleteFiles();
      _notify();

    }

    return result;
  }


  /**
   * retrieveFile() - grabs the order file(s).
   * In this case, via FTP
   **/
  private void retrieveFiles()
  throws Exception
  {
    log.debug("START retrieveFile()");

    int counter     = 1;
    boolean result  = false;

    while(result==false && counter<=MAX_FTP_ATTEMPTS)
    {
      try
      {
        initFTP();

        //run through list of files (FtpFile type)
        for(Enumeration list = ftps.getDirListing(); list.hasMoreElements();)
        {
          FtpFile token = (FtpFile)list.nextElement();
          fileList.add(token.getFilename());
          log.debug("adding file:: "+token.getFilename());
        }
/*
        //get the list of files - no mask needed as we want all of them
        //this will be the key for image retrieval
        String list = ftp.get("");

        //build the arraylist of file names
        //which will be used throughout the process
        StringTokenizer st = new StringTokenizer(list,"\n");

        while(st.hasMoreTokens())
        {
          String token = st.nextToken().trim();

          log.debug("next token = "+ token);

          fileList.add(token);
        }
*/
        //done, turn off the process
        result = true;

      }
      catch (Exception e)
      {

        //clear the list
        fileList.clear();

        //throw error if we're done trying
        if(counter == MAX_FTP_ATTEMPTS)
        {
         throw new Exception("Unable to contact remote FTP host.");
        }

        counter++;

        //otherwise, drop FTP
        try { ftps.disconnect(); }catch(Exception e1)
        {
          log.debug("Exception::"+e1.getMessage());
        }

        //and try again
        //might be a network thing...sleep it off.
        try
        {
         Thread.sleep(15000L);
        }
        catch (InterruptedException ie)
        {
        }
      }
    }

    log.debug("END retrieveFile()");
  }

  /**
   * processFile()
   * grabs file from FTP host and processes contained info
   **/

  private void processFiles()
  throws Exception
  {
    //log.debug("START processFile()");

    String fileName;
    BufferedReader reader = null;
    File fileArray;

    for(int i = 0; i < fileList.size(); i++)
    {
      fileName = (String)fileList.get(i);

      log.debug("processing... " + fileName);

      try
      {
        initFTP();

        fileArray = ftps.download(fileName);

        //get file from GCF
        reader = new BufferedReader(
                  new InputStreamReader(
                   new FileInputStream( fileArray )
                  )
                 );
        //process file, line by line
        //fail entire file if one line goes -
        //allows us to check file for corruption of data
        //without having to synch up
        String line = null;
        try
        {
          connect(false);

          while( (line = reader.readLine()) != null)
          {
            log.debug(line);
            processLine(line);
          }

          commit();

          //add file name to delete list
          deleteList.add(fileName);

        }
        catch(Exception e)
        {
          rollback();
          log.debug("above line had this error:"+e.getMessage());
          throw new Exception(e.getMessage()+ " - Unable to process line: " + line);
        }
        finally
        {
          cleanUp();
        }
      }
      catch(Exception e)
      {
        errorList.add(e.getMessage());
      }
      finally
      {
    	if(reader != null) { 
    	  reader.close();
    	}
        fileArray = null;
      }

    }

    log.debug("END processFile()");
  }

  private void processLine(String line)
  throws Exception
  {

    GCFData gData = new GCFData(line);

    //usually only one tracking number, but orders can be broken into
    //different shipments so we need seperate entries...
    String trackNum;
    for(int i = 0; i < gData.getTrackNums().size(); i++)
    {

      trackNum = (String)gData.getTrackNums().get(i);

      /*@lineinfo:generated-code*//*@lineinfo:319^7*/

//  ************************************************************
//  #sql [Ctx] { INSERT INTO supply_order_conf
//          values
//          (
//            :gData.orderNum,
//            :gData.shipDate,
//            :gData.GCFOrderNum,
//            :gData.shipMethod,
//            :trackNum,
//            :gData.cost,
//            :gData.freight,
//            :gData.merchantNum
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "INSERT INTO supply_order_conf\n        values\n        (\n           :1  ,\n           :2  ,\n           :3  ,\n           :4  ,\n           :5  ,\n           :6  ,\n           :7  ,\n           :8  \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"0com.mes.startup.SupplyOrderConfirmation",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,gData.orderNum);
   __sJT_st.setDate(2,gData.shipDate);
   __sJT_st.setString(3,gData.GCFOrderNum);
   __sJT_st.setString(4,gData.shipMethod);
   __sJT_st.setString(5,trackNum);
   __sJT_st.setDouble(6,gData.cost);
   __sJT_st.setDouble(7,gData.freight);
   __sJT_st.setLong(8,gData.merchantNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:333^7*/
    }

    //enter this info into call tracking as well
    int callSeq;

    /*@lineinfo:generated-code*//*@lineinfo:339^5*/

//  ************************************************************
//  #sql [Ctx] { select  service_call_sequence.nextval
//        
//        from    dual
//       };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  service_call_sequence.nextval\n       \n      from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.startup.SupplyOrderConfirmation",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   callSeq = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:344^5*/

    //8 = supplies, 14 = internal for type
    //used to be 22 for help email??
    /*@lineinfo:generated-code*//*@lineinfo:348^5*/

//  ************************************************************
//  #sql [Ctx] { insert into service_calls
//        (
//          sequence,
//          merchant_number,
//          call_date,
//          type,
//          login_name,
//          call_back,
//          status,
//          notes,
//          billable
//        )
//        values
//        (
//          :callSeq,
//          :gData.merchantNum,
//          sysdate,
//          14,
//          'system',
//          'N',
//          2,
//          :gData.getDescString(),
//          'N'
//        )
//       };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_4743 = gData.getDescString();
   String theSqlTS = "insert into service_calls\n      (\n        sequence,\n        merchant_number,\n        call_date,\n        type,\n        login_name,\n        call_back,\n        status,\n        notes,\n        billable\n      )\n      values\n      (\n         :1  ,\n         :2  ,\n        sysdate,\n        14,\n        'system',\n        'N',\n        2,\n         :3  ,\n        'N'\n      )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.startup.SupplyOrderConfirmation",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,callSeq);
   __sJT_st.setLong(2,gData.merchantNum);
   __sJT_st.setString(3,__sJT_4743);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:374^5*/
  }

  private void deleteFiles()
  {
    if( deleteList.size() > 0 )
    {
      try
      {
        initFTP();

        String fileName;
        for(int i = 0; i < deleteList.size(); i++ )
        {
          fileName = (String)deleteList.get(i);
          log.debug("deleting..."+ fileName);
          ftps.deleteFile(fileName);
        }
      }
      catch(Exception e)
      {
        //leave for next time,
        //processLine will not allow dups
      }
      finally
      {
        try { ftps.disconnect(); }catch(Exception e){ }
      }
    }
  }


  private void initFTP()
  throws Exception
  {
    if(ftps == null || !ftps.isConnected())
    {
      ftps = new Ftps(FTPHOST, FTPUSER, FTPPASSWORD, FTPPORT);
      ftps.connect();
      ftps.setAuto(true);
      //ftps.setAscii();
      ftps.setDir(DOWNLOAD_DIRECTORY);
    }
  }

  private void _notify()
  {

    StringBuffer message  = new StringBuffer("GCF files accessed:\n");
    try
    {
      for(int i = 0; i < fileList.size();i++)
      {
        message.append((String)fileList.get(i)).append("\n");
      }

      message.append("GCF files processed:\n");

      for(int i = 0; i < deleteList.size();i++)
      {
        message.append((String)deleteList.get(i)).append("\n");
      }

      //email list of errors with messages
      if(errorList!=null && errorList.size() > 0)
      {

        message.append("\nThe following problems occurred during supply order confirmation:\n\n");

        for(int i = 0; i< errorList.size();i++)
        {
          message.append((String)errorList.get(i)).append("\n");
        }
      }

      MailMessage msg = new MailMessage();
      msg.setAddresses(MesEmails.MSG_ADDRS_SUPPLY_ORDER_NOTIFY);
      msg.setSubject("GCF Supply Confirmation Process");
      msg.setText(message.toString());
      msg.send();
    }
    catch(Exception e)
    {
      logEntry("_notify()", e.getMessage() +" - "+message.toString());
    }

  }

  private class GCFData
  {
    protected long orderNum;
    protected java.sql.Date shipDate;
    protected long merchantNum;
    protected String GCFOrderNum;
    protected String shipMethod;
    private ArrayList trackNums;//Strings
    protected double cost;
    protected double freight;

    private String GCF_TRACK_DELIMITER = "[";

    public GCFData(String record)
    {
      init(record);
    }

    private void init (String record)
    {
      StringTokenizer st = new StringTokenizer(record,",");

      int counter = 1;

      while(st.hasMoreTokens())
      {

        String token = clean(st.nextToken().trim());

        switch(counter)
        {

          //Ship-to PO
          case 1:

            try
            {
              orderNum = Long.parseLong(token);
            }
            catch(NumberFormatException nfe)
            {
              //what to do?
            }
            break;

          //Ship Date
          case 2:

            setShipDate(token);

            break;

          //Merch Num
          case 3:

            try
            {
              merchantNum = Long.parseLong(token);
            }
            catch(NumberFormatException nfe)
            {
              //what to do?
            }
            break;

          //GCF Order Num
          case 4:

            GCFOrderNum = token;
            break;

          //Ship Method
          case 5:

            shipMethod = token;
            break;

          //Track num(s)
          case 6:

            processTrackingNumbers(token);

            break;

          //total cost (mes)
          case 7:

            try
            {
              cost = Double.parseDouble(token);
            }
            catch(NumberFormatException nfe)
            {
              //what to do?
            }
            break;

          //freight cost (mes)
          case 8:
            try
            {
              freight = Double.parseDouble(token);
            }
            catch(NumberFormatException nfe)
            {
              //what to do?
            }
            break;
        }

        counter++;
      }
    }

    public ArrayList getTrackNums()
    {
      if(trackNums == null )
      {
        trackNums = new ArrayList();
      }

      return trackNums;
    }

    private void processTrackingNumbers(String nums)
    {
      if(nums != null && nums.length() > 0 )
      {
          trackNums = new ArrayList();

          StringTokenizer st = new StringTokenizer(nums,GCF_TRACK_DELIMITER);

          while(st.hasMoreTokens())
          {
            trackNums.add(st.nextToken().trim());
          }

          //TODO: will change to this once sdk is upgraded
          //trackNums = new ArrayList(Arrays.asList(nums.split(GCF_TRACK_DELIMITER)));
      }
    }


    //necessary given that the string in question is surrounded by quotes
    //per GCF standard... if the quotes disappear, this will just return the
    //incoming string
    private String clean(String dirty)
    {
      return dirty.substring(1 , (dirty.length()-1) );
    }

    private void setShipDate (String token)
    {

      shipDate = DateTimeFormatter.parseSQLDate( token,
                                                 DateTimeFormatter.DEFAULT_DATE_FORMAT
                                                );
    }

    public String getDescString()
    {
      StringBuffer sb = new StringBuffer();
      sb.append("ACR Supply Order has been shipped: Tracking number");
      if(trackNums.size()>1)
      {
        sb.append("s");
      }
      sb.append(": ");
      for(int i=0;i<trackNums.size();i++)
      {
        sb.append((String)trackNums.get(i)).append(" ");
      }
      return sb.toString();
    }


    public String toString()
    {
      StringBuffer sb = new StringBuffer();
      sb.append("orderNum  = ").append(orderNum).append("\n");
      sb.append("shipDate  = ").append(shipDate).append("\n");
      sb.append("GCFOrderNum  = ").append(GCFOrderNum).append("\n");
      sb.append("shipMethod  = ").append(shipMethod).append("\n");
      sb.append("trackNums  = ").append(trackNums.size()).append("\n");
      sb.append("cost  = ").append(cost).append("\n");
      sb.append("freight  = ").append(freight).append("\n");
      sb.append("merchantNum  = ").append(merchantNum).append("\n");
      return sb.toString();
    }

  }


  public static void main( String[] args ) {
    try {
      if (args.length > 0 && args[0].equals("testproperties")) {
        EventBase.printKeyListStatus(new String[]{
                MesDefaults.DK_GCF_FTPS_IP_ADDRESS,
                MesDefaults.DK_GCF_USER,
                MesDefaults.DK_GCF_PASSWORD,
                MesDefaults.DK_GCF_FTPS_PORT
        });
      }


      SQLJConnectionBase.initStandalone();

      SupplyOrderConfirmation conf = new SupplyOrderConfirmation();

      if (conf.execute()) {
        System.out.println("SUCCESS!");
      } else {
        System.out.println("FAILURE!");
      }
    } catch (Exception e) {
      System.out.println(e.toString());
    }
  }


}/*@lineinfo:generated-code*/