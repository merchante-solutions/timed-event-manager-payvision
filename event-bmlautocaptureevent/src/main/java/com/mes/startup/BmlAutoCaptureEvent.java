package com.mes.startup;

import org.apache.log4j.Logger;
import com.mes.api.bml.BmlAutoCaptureJob;

public final class BmlAutoCaptureEvent extends EventBase
{
  static Logger log = Logger.getLogger(BmlAutoCaptureEvent.class);

  public boolean execute()
  {
    try
    {
      BmlAutoCaptureJob.run(getEventArg(0));
      return true;
    }
    catch (Exception e)
    {
      log.error("BML auto capture event error: " + e);
      logEvent(this.getClass().getName(), "execute()", e.toString());
      logEntry("execute()", e.toString());
    }

    return false;
  }
}