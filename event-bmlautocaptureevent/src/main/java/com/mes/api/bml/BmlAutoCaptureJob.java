package com.mes.api.bml;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;
import com.mes.api.ApiDb;
import com.mes.api.TridentApiConstants;
import com.mes.api.TridentApiProfile;
import com.mes.api.TridentApiRequest;
import com.mes.api.TridentApiResponse;
import com.mes.api.TridentApiTranBase;

public class BmlAutoCaptureJob
{
  static Logger log = Logger.getLogger(BmlAutoCaptureJob.class);

  private ApiDb apiDb = new ApiDb();
  private BmlDb bmlDb = new BmlDb();

  private void logError(BmlSaleTran sale, String errorMsg)
  {
    log.debug("logging error: " + errorMsg);
    bmlDb.logAutoCapture(sale.getApiTranId(),"-1",errorMsg);
  }

  private void logAutoCapture(BmlSaleTran sale, TridentApiResponse response)
  {
    String tranId = sale.getApiTranId();
    String tpgMsgId = response.getParameter(TridentApiConstants.FN_ERROR_CODE);
    String tpgMsg = response.getParameter(TridentApiConstants.FN_AUTH_RESP_TEXT);
    log.debug("logging capture: " + tranId + ", " + tpgMsgId + ", " + tpgMsg);
    bmlDb.logAutoCapture(tranId,tpgMsgId,tpgMsg);
  }

  private void execute(String tapiUrlStr)
  {
    // fetch all uncaptured sales
    ApiDb apiDb = new ApiDb();
    HashMap profiles = new HashMap();
    List sales = bmlDb._getUncapturedSales();
    log.debug("found " + sales.size() + " sales to auto capture...");

    for (Iterator i = sales.iterator(); i.hasNext();)
    {
      BmlSaleTran sale = (BmlSaleTran)i.next();

      log.debug("processing " + sale);

      // load profile
      String pid = sale.getProfileId();
      TridentApiProfile profile = (TridentApiProfile)profiles.get(sale.getProfileId());
      if (profile == null)
      {
        profile = apiDb.loadProfile(pid);
        profiles.put(pid,profile);
      }

      log.debug("profile " + pid + (profile != null ? "" : " NOT") + " found");

      // calculate capture amount
      // NOTE: this now takes the amount as the total amount
      BigDecimal amount = sale.getAmount();
      //amount.add(sale.getShipAmount());
      //amount.add(sale.getBmlTaxAmount());

      // load auto capture request
      TridentApiRequest request = new TridentApiRequest(tapiUrlStr);
      request.addArg(TridentApiConstants.FN_TRAN_TYPE,
        TridentApiTranBase.TT_BML_REQUEST);
      request.addArg(TridentApiConstants.FN_TID,pid);
      request.addArg(TridentApiConstants.FN_TERM_PASS,
        profile.getProfileKey());
      request.addArg(BmlCaptureTran.FN_BML_REQUEST,BmlBaseTran.BR_CAPTURE);
      request.addArg(BmlCaptureTran.FN_BML_AMOUNT,""+amount);
      request.addArg(BmlCaptureTran.FN_REF_TRAN_ID,sale.getApiTranId());
      request.addArg(BmlBaseTran.FN_CENTINEL_URL_OVERRIDE,
        sale.getCardinalCentinelUrl());
      request.addArg(BmlCaptureTran.FN_BML_AUTO_CAP,"Y");

      log.debug("capture request built: " + request);

      // send the request, log results
      boolean sendOk = false;
      try
      {
        TridentApiResponse response = request.post();
        logAutoCapture(sale,response);
      }
      catch (Exception e)
      {
        log.error("Error sending request: " + e);
        e.printStackTrace();
        logError(sale,""+e);
      }
    }
  }

  public static void run(String tapiUrlStr)
  {
    BmlAutoCaptureJob job = new BmlAutoCaptureJob();
    job.execute(tapiUrlStr);
  }
}