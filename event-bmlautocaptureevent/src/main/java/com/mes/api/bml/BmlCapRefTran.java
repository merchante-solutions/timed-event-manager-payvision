package com.mes.api.bml;

public abstract class BmlCapRefTran extends BmlBaseTran
{
  protected BmlCaptureTran capTran;

  public BmlCapRefTran(String tranId)
  {
    super(tranId);
  }

  public boolean hasAuthRef()
  {
    return false;
  }

  public boolean hasCapRef()
  {
    return true;
  }

  public boolean loadCapTran()
  {
    if (capTran == null)
    {
      capTran = BmlDb.lookupRefCap(this);
    }
    return capTran != null;
  }

  public BmlCaptureTran getCapTran()
  {
    return capTran;
  }
}