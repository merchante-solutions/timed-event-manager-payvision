package com.mes.api.bml;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import org.apache.log4j.Logger;
import com.mes.api.TridentApiConstants;

public class BmlOrderIdEntry implements Serializable
{
  //
  // if you do not specify the serial version ID then Java will default
  // to a UID based on the fields in the class (see <jdk>/bin/serialver).  
  // failure to specify a static version will cause a mismatch exception 
  // if the object definition is changed.
  //
  // NOTE: it is necessary to specify a static serialVersionUID for all
  //       child classes.
  //
  static final long serialVersionUID    = TridentApiConstants.ApiVersionId;

  static Logger log = Logger.getLogger(BmlOrderIdEntry.class);

  public final static String TT_LOOKUP      = "L";
  public final static String TT_AUTHORIZE   = "A";
  public final static String TT_CAPTURE     = "C";

  private long   recId = -1L;
  private Date   tranDate;
  private String tranId;
  private String tranType;
  private String orderId;

  public BmlOrderIdEntry()
  {
  }
  public BmlOrderIdEntry(long recId, Date tranDate, String tranId, 
    String tranType, String orderId)
  {
    setRecId(recId);
    setTranDate(tranDate);
    setTranId(tranId);
    setTranType(tranType);
    setOrderId(orderId);
  }
  public BmlOrderIdEntry(Date tranDate, String tranId, String tranType, 
    String orderId)
  {
    this(-1L,tranDate,tranId,tranType,orderId);
  }

  public void setRecId(long recId)
  {
    this.recId = recId;
  }
  public long getRecId()
  {
    return recId;
  }

  public void setTranDate(Date tranDate)
  {
    this.tranDate = tranDate;
  }
  public Date getTranDate()
  {
    return tranDate;
  }
  public Timestamp getTranTs()
  {
    return BmlDb.toTimestamp(tranDate);
  }
  public void setTranTs(Timestamp tranTs)
  {
    tranDate = BmlDb.toDate(tranTs);
  }

  public void setTranId(String tranId)
  {
    this.tranId = tranId;
  }
  public String getTranId()
  {
    return tranId;
  }

  public void setTranType(String tranType)
  {
    if (!TT_LOOKUP.equals(tranType)
        && !TT_AUTHORIZE.equals(tranType)
        && !TT_CAPTURE.equals(tranType))
    {
      throw new RuntimeException("Invalid order entry tran type '" 
        + (tranType == null ? "(null)" : tranType) + "'");
    }
    this.tranType = tranType;
  }
  public String getTranType()
  {
    return tranType;
  }

  public void setOrderId(String orderId)
  {
    this.orderId = orderId;
  }
  public String getOrderId()
  {
    return orderId;
  }

  public boolean isLookup()
  {
    return TT_LOOKUP.equals(tranType);
  }

  public boolean isAuthorize()
  {
    return TT_AUTHORIZE.equals(tranType);
  }

  public boolean isCapture()
  {
    return TT_CAPTURE.equals(tranType);
  }

  public String toString()
  {
    return "BmlOrderEntryId [ "
      + "recId: " + recId
      + ", tranDate: " + tranDate
      + ", tranId: " + tranId
      + ", tranType: " + tranType
      + ", orderId: " + orderId
      + " ]";
  }
}
