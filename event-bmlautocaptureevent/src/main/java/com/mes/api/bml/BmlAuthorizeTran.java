package com.mes.api.bml;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Iterator;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.api.TridentApiConstants;
import com.mes.api.TridentApiProfile;
import com.mes.support.HttpHelper;

public class BmlAuthorizeTran extends BmlBaseTran
{
  //
  // if you do not specify the serial version ID then Java will default
  // to a UID based on the fields in the class (see <jdk>/bin/serialver).  
  // failure to specify a static version will cause a mismatch exception 
  // if the object definition is changed.
  //
  // NOTE: it is necessary to specify a static serialVersionUID for all
  //       child classes.
  //
  static final long serialVersionUID    = TridentApiConstants.ApiVersionId;

  static Logger log = Logger.getLogger(BmlAuthorizeTran.class);

  // request data

  // tpg api request field names (received from merchant)
  public static final String  FN_BML_AMOUNT           = "amount";
  public static final String  FN_BML_SHIP_AMOUNT      = "ship_amount";
  public static final String  FN_BML_TAX_AMOUNT       = "tax_amount";
  public static final String  FN_BML_ORDER_DESC       = "order_desc";
  public static final String  FN_BML_ORDER_NUM        = "order_num";
  public static final String  FN_BML_PROMO_CODE       = "promo_code";
  public static final String  FN_BML_IP_ADDRESS       = "ip_address";
  public static final String  FN_BML_LOOKUP_TRAN_ID   = "lookup_tran_id";
  public static final String  FN_BML_REQ_ACCT_NUM     = "account_num";
  public static final String  FN_BML_CUST_REG_DATE    = "cust_reg_date";
  public static final String  FN_BML_CUST_FLAG        = "cust_flag";
  public static final String  FN_BML_CAT_CODE         = "cat_code";
  public static final String  FN_BML_TRAN_MODE        = "tran_mode";
  public static final String  FN_BML_PROD_CODE        = "prod_code";
  public static final String  FN_BML_TERMS            = "terms";
  public static final String  FN_BML_BILL_FIRST_NAME  = "bill_first_name";
  public static final String  FN_BML_BILL_MIDDLE_NAME = "bill_middle_name";
  public static final String  FN_BML_BILL_LAST_NAME   = "bill_last_name";
  public static final String  FN_BML_BILL_ADDR1       = "bill_addr1";
  public static final String  FN_BML_BILL_ADDR2       = "bill_addr2";
  public static final String  FN_BML_BILL_CITY        = "bill_city";
  public static final String  FN_BML_BILL_STATE       = "bill_state";
  public static final String  FN_BML_BILL_ZIP         = "bill_zip";
  public static final String  FN_BML_BILL_PHONE1      = "bill_phone1";
  public static final String  FN_BML_BILL_PHONE2      = "bill_phone2";
  public static final String  FN_BML_BILL_EMAIL       = "bill_email";
  public static final String  FN_BML_SHIP_FIRST_NAME  = "ship_first_name";
  public static final String  FN_BML_SHIP_MIDDLE_NAME = "ship_middle_name";
  public static final String  FN_BML_SHIP_LAST_NAME   = "ship_last_name";
  public static final String  FN_BML_SHIP_ADDR1       = "ship_addr1";
  public static final String  FN_BML_SHIP_ADDR2       = "ship_addr2";
  public static final String  FN_BML_SHIP_CITY        = "ship_city";
  public static final String  FN_BML_SHIP_STATE       = "ship_state";
  public static final String  FN_BML_SHIP_ZIP         = "ship_zip";
  public static final String  FN_BML_SHIP_PHONE1      = "ship_phone1";
  public static final String  FN_BML_SHIP_PHONE2      = "ship_phone2";
  public static final String  FN_BML_SHIP_EMAIL       = "ship_email";
  public static final String  FN_BML_HAS_CHK_ACCT     = "has_checking_acct";
  public static final String  FN_BML_HAS_SAV_ACCT     = "has_savings_acct";
  public static final String  FN_BML_RES_STATUS       = "residence_status";
  public static final String  FN_BML_DOB              = "dob";
  public static final String  FN_BML_SSN              = "ssn";
  public static final String  FN_BML_HH_INCOME        = "household_income";
  public static final String  FN_BML_EMP_YEARS        = "years_at_employer";
  public static final String  FN_BML_RES_YEARS        = "years_at_residence";

  public static final String  FN_BML_SALE_FLAG        = "sale_flag";

  protected BigDecimal  amount                  = new BigDecimal(0).setScale(2);
  protected BigDecimal  shipAmount              = new BigDecimal(0).setScale(2);
  protected BigDecimal  taxAmount               = new BigDecimal(0).setScale(2);
  protected String      orderNum                = null;
  protected String      orderDesc               = null;
  protected String      promoCode               = "Default";
  protected String      ipAddress               = null;
  protected String      lookupTranId            = null;
  protected String      bmlAcctNum              = null;
  protected String      custRegDate             = null;
  protected String      custFlag                = null;
  protected String      catCode                 = null;
  protected String      tranMode                = null;
  protected String      prodCode                = null;
  protected String      terms                   = null;

  // consumer data
  protected String      billingAddress1         = null;
  protected String      billingAddress2         = null;
  protected String      billingCity             = null;
  protected String      billingCountryCode      = null;
  protected String      billingFirstName        = null;
  protected String      billingMiddleName       = null;
  protected String      billingLastName         = null;
  protected String      billingPhone1           = null;
  protected String      billingPhone2           = null;
  protected String      billingState            = null;
  protected String      billingZip              = null;
  protected String      billingEmail            = null;
  protected String      shippingAddress1        = null;
  protected String      shippingAddress2        = null;
  protected String      shippingCity            = null;
  protected String      shippingCountryCode     = null;
  protected String      shippingFirstName       = null;
  protected String      shippingMiddleName      = null;
  protected String      shippingLastName        = null;
  protected String      shippingPhone1          = null;
  protected String      shippingPhone2          = null;
  protected String      shippingState           = null;
  protected String      shippingZip             = null;
  protected String      shippingEmail           = null;

  // call center data
  protected String      hasCheckingAccount      = null;
  protected String      hasSavingsAccount       = null;
  protected String      residenceStatus         = null;
  protected String      dob                     = null;
  protected String      ssn                     = null;
  protected BigDecimal  householdIncome         = new BigDecimal(-1).setScale(2);
  protected String      yearsAtEmployer         = null;
  protected String      yearsAtResidence        = null;

  protected int         reconId;

  protected boolean     saleFlag;

  // response data

  // tpg api response field names (sent to merchant)
  public static final String  FN_BML_STATUS_CODE      = "status_code";
  public static final String  FN_BML_AUTH_CODE        = "auth_code";

  public BmlAuthorizeTran(String tranId)
  {
    super(tranId);
  }

  private void setConsumerData(HttpServletRequest request)
  {
    billingFirstName    = HttpHelper.getString(request,FN_BML_BILL_FIRST_NAME,null);
    billingMiddleName   = HttpHelper.getString(request,FN_BML_BILL_MIDDLE_NAME,null);
    billingLastName     = HttpHelper.getString(request,FN_BML_BILL_LAST_NAME,null);
    billingAddress1     = HttpHelper.getString(request,FN_BML_BILL_ADDR1,null);
    billingAddress2     = HttpHelper.getString(request,FN_BML_BILL_ADDR2,null);
    billingCity         = HttpHelper.getString(request,FN_BML_BILL_CITY,null);
    billingState        = HttpHelper.getString(request,FN_BML_BILL_STATE,null);
    billingZip          = HttpHelper.getString(request,FN_BML_BILL_ZIP,null);
    billingPhone1       = HttpHelper.getString(request,FN_BML_BILL_PHONE1,null);
    billingPhone2       = HttpHelper.getString(request,FN_BML_BILL_PHONE2,null);
    billingEmail        = HttpHelper.getString(request,FN_BML_BILL_EMAIL,null);
    shippingFirstName   = HttpHelper.getString(request,FN_BML_SHIP_FIRST_NAME,null);
    shippingMiddleName  = HttpHelper.getString(request,FN_BML_SHIP_MIDDLE_NAME,null);
    shippingLastName    = HttpHelper.getString(request,FN_BML_SHIP_LAST_NAME,null);
    shippingAddress1    = HttpHelper.getString(request,FN_BML_SHIP_ADDR1,null);
    shippingAddress2    = HttpHelper.getString(request,FN_BML_SHIP_ADDR2,null);
    shippingCity        = HttpHelper.getString(request,FN_BML_SHIP_CITY,null);
    shippingState       = HttpHelper.getString(request,FN_BML_SHIP_STATE,null);
    shippingZip         = HttpHelper.getString(request,FN_BML_SHIP_ZIP,null);
    shippingPhone1      = HttpHelper.getString(request,FN_BML_SHIP_PHONE1,null);
    shippingPhone2      = HttpHelper.getString(request,FN_BML_SHIP_PHONE2,null);
    shippingEmail       = HttpHelper.getString(request,FN_BML_SHIP_EMAIL,null);
  }

  private void setCallCenterData(HttpServletRequest request)
  {
    hasCheckingAccount  = HttpHelper.getString(request,FN_BML_HAS_CHK_ACCT,null);
    hasSavingsAccount   = HttpHelper.getString(request,FN_BML_HAS_SAV_ACCT,null);
    residenceStatus     = HttpHelper.getString(request,FN_BML_RES_STATUS,null);
    dob                 = HttpHelper.getString(request,FN_BML_DOB,null);
    ssn                 = HttpHelper.getString(request,FN_BML_SSN,null);
    householdIncome     = new BigDecimal(HttpHelper.getString(request,FN_BML_HH_INCOME,"-1")).setScale(2);
    yearsAtEmployer     = HttpHelper.getString(request,FN_BML_EMP_YEARS,null);
    yearsAtResidence    = HttpHelper.getString(request,FN_BML_RES_YEARS,null);
  }

  public void setProperties(HttpServletRequest request)
  {
    super.setProperties(request);
    amount              = new BigDecimal(HttpHelper.getString(request,FN_BML_AMOUNT,"0")).setScale(2);
    shipAmount          = new BigDecimal(HttpHelper.getString(request,FN_BML_SHIP_AMOUNT,"0")).setScale(2);
    taxAmount           = new BigDecimal(HttpHelper.getString(request,FN_BML_TAX_AMOUNT,"0")).setScale(2);
    orderNum            = HttpHelper.getString(request,FN_BML_ORDER_NUM,null);
    orderDesc           = HttpHelper.getString(request,FN_BML_ORDER_DESC,null);
    promoCode           = HttpHelper.getString(request,FN_BML_PROMO_CODE,null);
    ipAddress           = HttpHelper.getString(request,FN_BML_IP_ADDRESS,null);
    bmlAcctNum          = HttpHelper.getString(request,FN_BML_REQ_ACCT_NUM,null);
    custRegDate         = HttpHelper.getString(request,FN_BML_CUST_REG_DATE,null);
    custFlag            = HttpHelper.getString(request,FN_BML_CUST_FLAG,null);
    catCode             = HttpHelper.getString(request,FN_BML_CAT_CODE,null);
    tranMode            = HttpHelper.getString(request,FN_BML_TRAN_MODE,null);
    prodCode            = HttpHelper.getString(request,FN_BML_PROD_CODE,null);
    terms               = HttpHelper.getString(request,FN_BML_TERMS,null);
    setConsumerData(request);
    setCallCenterData(request);
  }

  public BigDecimal getAmount()
  {
    return amount;
  }

  public String getBillingAddress1()
  {
    return billingAddress1;
  }

  public String getBillingAddress2()
  {
    return billingAddress2;
  }

  public String getBillingCity()
  {
    return billingCity;
  }

  public String getBillingEmail()
  {
    return billingEmail;
  }

  public String getBillingFirstName()
  {
    return billingFirstName;
  }

  public String getBillingLastName()
  {
    return billingLastName;
  }

  public String getBillingMiddleName()
  {
    return billingMiddleName;
  }

  public String getBillingPhone1()
  {
    return billingPhone1;
  }

  public String getBillingPhone2()
  {
    return billingPhone2;
  }

  public String getBillingState()
  {
    return billingState;
  }

  public String getBillingZip()
  {
    return billingZip;
  }

  public String getBmlAcctNum()
  {
    return bmlAcctNum;
  }

  public String getCatCode()
  {
    return catCode;
  }

  public String getCustFlag()
  {
    return custFlag;
  }

  public String getCustRegDate()
  {
    return custRegDate;
  }

  public String getDob()
  {
    return dob;
  }

  public String getHasCheckingAccount()
  {
    return hasCheckingAccount;
  }

  public String getHasSavingsAccount()
  {
    return hasSavingsAccount;
  }

  public BigDecimal getHouseholdIncome()
  {
    return householdIncome;
  }

  public String getIpAddress()
  {
    return ipAddress;
  }

  public String getLookupTranId()
  {
    return lookupTranId;
  }

  public String getOrderDesc()
  {
    return orderDesc;
  }

  public String getOrderNum()
  {
    return orderNum;
  }

  public String getProdCode()
  {
    return prodCode;
  }

  public String getPromoCode()
  {
    return promoCode;
  }
  
  public String getResidenceStatus()
  {
    return residenceStatus;
  }

  public BigDecimal getShipAmount( )
  {
    return shipAmount;
  }
  
  public String getShippingAddress1()
  {
    return shippingAddress1;
  }

  public String getShippingAddress2()
  {
    return shippingAddress2;
  }

  public String getShippingCity()
  {
    return shippingCity;
  }

  public String getShippingEmail()
  {
    return shippingEmail;
  }

  public String getShippingFirstName()
  {
    return shippingFirstName;
  }

  public String getShippingLastName()
  {
    return shippingLastName;
  }

  public String getShippingMiddleName()
  {
    return shippingMiddleName;
  }

  public String getShippingPhone1()
  {
    return shippingPhone1;
  }

  public String getShippingPhone2()
  {
    return shippingPhone2;
  }

  public String getShippingState()
  {
    return shippingState;
  }

  public String getShippingZip()
  {
    return shippingZip;
  }

  // Bml added to differentiate with base class accessor
  public BigDecimal getBmlTaxAmount()
  {
    return taxAmount;
  }

  public String getSsn()
  {
    return ssn;
  }

  public String getTerms()
  {
    return terms;
  }

  public String getTranMode()
  {
    return tranMode;
  }

  public String getYearsAtEmployer()
  {
    return yearsAtEmployer;
  }

  public String getYearsAtResidence()
  {
    return yearsAtResidence;
  }

  public boolean isSale()
  {
    return saleFlag;
  }

  /**
   * Authorize requests must refer to a lookup transaction in order to load
   * the lookup response order id.
   */
  public String getRefTranType()
  {
    return BmlOrderIdEntry.TT_LOOKUP;
  }

  /**
   * Data validation
   */

  public boolean validateConsumerData()
  {
    if (isBlank(billingFirstName))
    {
      setError(tac.ER_BML_BILL_FIRST_NAME_REQUIRED);
    }
    else if (isBlank(billingLastName))
    {
      setError(tac.ER_BML_BILL_LAST_NAME_REQUIRED);
    }
    else if (isBlank(billingAddress1))
    {
      setError(tac.ER_BML_BILL_ADDR1_REQUIRED);
    }
    else if (isBlank(billingCity))
    {
      setError(tac.ER_BML_BILL_CITY_REQUIRED);
    }
    else if (isBlank(billingState))
    {
      setError(tac.ER_BML_BILL_STATE_REQUIRED);
    }
    else if (isBlank(billingZip))
    {
      setError(tac.ER_BML_BILL_ZIP_REQUIRED);
    }
    else if (isBlank(billingPhone1))
    {
      setError(tac.ER_BML_BILL_PHONE_REQUIRED);
    }
    else if (isBlank(shippingFirstName))
    {
      setError(tac.ER_BML_SHIP_FIRST_NAME_REQUIRED);
    }
    else if (isBlank(shippingLastName))
    {
      setError(tac.ER_BML_SHIP_LAST_NAME_REQUIRED);
    }
    else if (isBlank(shippingAddress1))
    {
      setError(tac.ER_BML_SHIP_ADDR1_REQUIRED);
    }
    else if (isBlank(shippingCity))
    {
      setError(tac.ER_BML_SHIP_CITY_REQUIRED);
    }
    else if (isBlank(shippingState))
    {
      setError(tac.ER_BML_SHIP_STATE_REQUIRED);
    }
    else if (isBlank(shippingZip))
    {
      setError(tac.ER_BML_SHIP_ZIP_REQUIRED);
    }
    else if ("S".equals(tranMode) && isBlank(billingEmail))
    {
      setError(tac.ER_BML_ECOMM_EMAIL_REQUIRED);
    }

    return !hasError();
  }

  public boolean isBmlValid()
  {
    if (Profile.getBmlMerchId(promoCode) == null)
    {
      setError(tac.ER_BML_INVALID_PROMO_CODE);
    }
    // the req order id will be present if a valid 
    // tran id was sent in the request...
    else if (isBlank(bmlAcctNum) && isBlank(bmlReqOrderId) && isBlank(dob))
    {
      setError(tac.ER_BML_ACCT_OR_TRAN_ID_REQUIRED);
    }
    else if (amount.doubleValue() == 0)
    {
      setError(tac.ER_BML_AMOUNT_REQUIRED);
    }
    else if (isBlank(orderNum))
    {
      setError(tac.ER_BML_ORDER_NUM_REQUIRED);
    }
    else if (isBlank(custRegDate))
    {
      setError(tac.ER_BML_CUST_REG_DATE_REQUIRED);
    }
    else if (isBlank(custFlag))
    {
      setError(tac.ER_BML_CUST_FLAG_REQUIRED);
    }
    else if (isBlank(catCode))
    {
      setError(tac.ER_BML_CAT_CODE_REQUIRED);
    }
    else if (isBlank(tranMode))
    {
      setError(tac.ER_BML_TRAN_MODE_REQUIRED);
    }
    else if (isBlank(terms))
    {
      setError(tac.ER_BML_TERMS_REQUIRED);
    }
    else if ("S".equals(tranMode) && isBlank(ipAddress))
    {
      setError(tac.ER_BML_ECOMM_IP_ADDR_REQUIRED);
    }

    // validate consumer data
    if (!validateConsumerData())
    {
      return false;
    }

    return !hasError();
  }

  /**
   * Response accessors
   */

  public String getStatusCode()
  {
    return getResponseValue(FN_BML_STATUS_CODE);
  }

  public String getRespAcctNum()
  {
    return getResponseValue(FN_BML_RESP_ACCT_NUM);
  }

  public String getBmlAuthCode()
  {
    return getResponseValue(FN_BML_AUTH_CODE);
  }

  /**
   * DB loading
   */

  private void setConsumerData(ResultSet rs) throws Exception
  {
    billingFirstName    = rs.getString(FN_BML_BILL_FIRST_NAME);
    billingMiddleName   = rs.getString(FN_BML_BILL_MIDDLE_NAME);
    billingLastName     = rs.getString(FN_BML_BILL_LAST_NAME);
    billingAddress1     = rs.getString(FN_BML_BILL_ADDR1);
    billingAddress2     = rs.getString(FN_BML_BILL_ADDR2);
    billingCity         = rs.getString(FN_BML_BILL_CITY);
    billingState        = rs.getString(FN_BML_BILL_STATE);
    billingZip          = rs.getString(FN_BML_BILL_ZIP);
    billingPhone1       = rs.getString(FN_BML_BILL_PHONE1);
    billingPhone2       = rs.getString(FN_BML_BILL_PHONE2);
    billingEmail        = rs.getString(FN_BML_BILL_EMAIL);
    shippingFirstName   = rs.getString(FN_BML_SHIP_FIRST_NAME);
    shippingMiddleName  = rs.getString(FN_BML_SHIP_MIDDLE_NAME);
    shippingLastName    = rs.getString(FN_BML_SHIP_LAST_NAME);
    shippingAddress1    = rs.getString(FN_BML_SHIP_ADDR1);
    shippingAddress2    = rs.getString(FN_BML_SHIP_ADDR2);
    shippingCity        = rs.getString(FN_BML_SHIP_CITY);
    shippingState       = rs.getString(FN_BML_SHIP_STATE);
    shippingZip         = rs.getString(FN_BML_SHIP_ZIP);
    shippingPhone1      = rs.getString(FN_BML_SHIP_PHONE1);
    shippingPhone2      = rs.getString(FN_BML_SHIP_PHONE2);
    shippingEmail       = rs.getString(FN_BML_SHIP_EMAIL);
  }

  private void setCallCenterData(ResultSet rs) throws Exception
  {
    hasCheckingAccount  = rs.getString(FN_BML_HAS_CHK_ACCT);
    hasSavingsAccount   = rs.getString(FN_BML_HAS_SAV_ACCT);
    residenceStatus     = rs.getString(FN_BML_RES_STATUS);
    dob                 = rs.getString(FN_BML_DOB);
    ssn                 = rs.getString(FN_BML_SSN);
    householdIncome     = rs.getBigDecimal(FN_BML_HH_INCOME);
    householdIncome = (householdIncome == null ? new BigDecimal(-1) : householdIncome).setScale(2);
    yearsAtEmployer     = rs.getString(FN_BML_EMP_YEARS);
    yearsAtResidence    = rs.getString(FN_BML_RES_YEARS);
  }    

  private void setResponseData(ResultSet rs) throws Exception
  {
    setResponseData(FN_BML_TRAN_ID,       rs.getString("bml_tran_id"));
    setResponseData(FN_BML_RESP_ORDER_ID, rs.getString("bml_order_id"));
    setResponseData(FN_BML_STATUS_CODE,   rs.getString("status_code"));
    setResponseData(FN_BML_RESP_ACCT_NUM, rs.getString("account_num"));
    setResponseData(FN_BML_AUTH_CODE,     rs.getString("auth_code"));
  }

  public void setBmlRequestType()
  {
    bmlRequest = BR_AUTHORIZE;
  }

  public void setTranData(ResultSet rs) throws Exception
  {
    // requested order id 
    // NOTE: most other transactions use 'bml_order_id' 
    //       for the requested order id, and do not have
    //       db column for a response order id
    bmlReqOrderId = rs.getString("req_bml_order_id");

    // response order id
    setResponseData(FN_BML_RESP_ORDER_ID, rs.getString("bml_order_id"));

    amount              = rs.getBigDecimal(FN_BML_AMOUNT);
    amount = (amount == null ? new BigDecimal(0) : amount).setScale(2);
    shipAmount          = rs.getBigDecimal(FN_BML_SHIP_AMOUNT);
    shipAmount = (shipAmount == null ? new BigDecimal(0) : shipAmount).setScale(2);
    taxAmount           = rs.getBigDecimal(FN_BML_TAX_AMOUNT);
    taxAmount = (taxAmount == null ? new BigDecimal(0) : taxAmount).setScale(2);
    orderNum            = rs.getString(FN_BML_ORDER_NUM);
    orderDesc           = rs.getString(FN_BML_ORDER_DESC);
    promoCode           = rs.getString(FN_BML_PROMO_CODE);
    ipAddress           = rs.getString(FN_BML_IP_ADDRESS);
    bmlAcctNum          = rs.getString(FN_BML_REQ_ACCT_NUM);
    custRegDate         = rs.getString(FN_BML_CUST_REG_DATE);
    custFlag            = rs.getString(FN_BML_CUST_FLAG);
    catCode             = rs.getString(FN_BML_CAT_CODE);
    tranMode            = rs.getString(FN_BML_TRAN_MODE);
    prodCode            = rs.getString(FN_BML_PROD_CODE);
    terms               = rs.getString(FN_BML_TERMS);
    saleFlag            = "Y".equals((""+rs.getString(FN_BML_SALE_FLAG)).toUpperCase());
    setConsumerData(rs);
    setCallCenterData(rs);
    setResponseData(rs);
  }

  public String toString()
  {
    TridentApiProfile profile = getProfile();
    String pid = (profile != null ? profile.getProfileId() : "unavailable");
    String reqData = 
      getClass().getName() + " [ "
      + "\nrecId: " + getRecId()
      + "\nreqDate: " + requestDate
      + "\ntridentTranId: " + getTridentTranId()
      + "\nprofileId: " + pid
      + "\namount: " + amount 
      + "\nshipAmount: " + shipAmount
      + "\ntaxAmount: " + taxAmount
      + "\norderNum: " + (orderNum == null ? "(null)" : orderNum)
      + "\norderDesc: " + (orderDesc == null ? "(null)" : orderDesc)
      + "\npromoCode: " + (promoCode == null ? "(null)" : promoCode)
      + "\nipAddress: " + (ipAddress == null ? "(null)" : ipAddress)
      + "\nlookupTranId: " + (lookupTranId == null ? "(null)" : lookupTranId)
      //+ "\nreqOrderId: " + (bmlReqOrderId == null ? "(null)" : bmlReqOrderId)
      + "\nrefTranId: " + (refTranId == null ? "(null)" : refTranId)
      + "\nbmlAcctNum: " + (bmlAcctNum == null ? "(null)" : bmlAcctNum)
      + "\ncustRegDate: " + (custRegDate == null ? "(null)" : custRegDate)
      + "\ncustFlag: " + (custFlag == null ? "(null)" : custFlag)
      + "\ncatCode: " + (catCode == null ? "(null)" : catCode)
      + "\ntranMode: " + (tranMode == null ? "(null)" : tranMode)
      + "\nprodCode: " + (prodCode == null ? "(null)" : prodCode)
      + "\nterms: " + (terms == null ? "(null)" : terms)
      + "\nbillingAddress1: " + (billingAddress1 == null ? "(null)" : billingAddress1)
      + "\nbillingAddress2: " + (billingAddress2 == null ? "(null)" : billingAddress2)
      + "\nbillingCity: " + (billingCity == null ? "(null)" : billingCity)
      + "\nbillingCountryCode: " + (billingCountryCode == null ? "(null)" : billingCountryCode)
      + "\nbillingFirstName: " + (billingFirstName == null ? "(null)" : billingFirstName)
      + "\nbillingMiddleName: " + (billingMiddleName == null ? "(null)" : billingMiddleName)
      + "\nbillingLastName: " + (billingLastName == null ? "(null)" : billingLastName)
      + "\nbillingPhone1: " + (billingPhone1 == null ? "(null)" : billingPhone1)
      + "\nbillingPhone2: " + (billingPhone2 == null ? "(null)" : billingPhone2)
      + "\nbillingState: " + (billingState == null ? "(null)" : billingState)
      + "\nbillingZip: " + (billingZip == null ? "(null)" : billingZip)
      + "\nbillingEmail: " + (billingEmail == null ? "(null)" : billingEmail)
      + "\nshippingAddress1: " + (shippingAddress1 == null ? "(null)" : shippingAddress1)
      + "\nshippingAddress2: " + (shippingAddress2 == null ? "(null)" : shippingAddress2)
      + "\nshippingCity: " + (shippingCity == null ? "(null)" : shippingCity)
      + "\nshippingCountryCode: " + (shippingCountryCode == null ? "(null)" : shippingCountryCode)
      + "\nshippingFirstName: " + (shippingFirstName == null ? "(null)" : shippingFirstName)
      + "\nshippingMiddleName: " + (shippingMiddleName == null ? "(null)" : shippingMiddleName)
      + "\nshippingLastName: " + (shippingLastName == null ? "(null)" : shippingLastName)
      + "\nshippingPhone1: " + (shippingPhone1 == null ? "(null)" : shippingPhone1)
      + "\nshippingPhone2: " + (shippingPhone2 == null ? "(null)" : shippingPhone2)
      + "\nshippingState: " + (shippingState == null ? "(null)" : shippingState)
      + "\nshippingZip: " + (shippingZip == null ? "(null)" : shippingZip)
      + "\nshippingEmail: " + (shippingEmail == null ? "(null)" : shippingEmail)
      + "\nhasCheckingAccount: " + (hasCheckingAccount == null ? "(null)" : hasCheckingAccount)
      + "\nhasSavingsAccount: " + (hasSavingsAccount == null ? "(null)" : hasSavingsAccount)
      + "\nresidenceStatus: " + (residenceStatus == null ? "(null)" : residenceStatus)
      + "\ndob: " + (dob == null ? "(null)" : dob)
      + "\nssn: " + (ssn == null ? "(null)" : ssn)
      + "\nhouseholdIncome: " + householdIncome
      + "\nyearsAtEmployer: " + (yearsAtEmployer == null ? "(null)" : yearsAtEmployer)
      + "\nyearsAtResidence: " + (yearsAtResidence == null ? "(null)" : yearsAtResidence)
      + "\ntpg_msg_id: " + (getErrorCode() == null ? "(null)" : getErrorCode())
      + "\ntpg_msg: " + (getErrorDesc() == null ? "(null)" : getErrorDesc())
      + "\nbml_error_num: " + (getBmlErrorNum() == null ? "(null)" : getBmlErrorNum())
      + "\nbml_error_desc: " + (getBmlErrorDesc() == null ? "(null)" : getBmlErrorDesc())
      + "\nbml_reason_code: " + (getBmlReasonCode() == null ? "(null)" : getBmlReasonCode())
      + "\nbml_reason_desc: " + (getBmlReasonDesc() == null ? "(null)" : getBmlReasonDesc())
      + "\nis sale?: " + isSale()
      + "\ncardinal url: " + getCardinalCentinelUrl();

    StringBuffer buf = new StringBuffer(reqData);
    for (Iterator i = responseData.entrySet().iterator(); i.hasNext();)
    {
      Map.Entry entry = (Map.Entry)i.next();
      buf.append("\n" + entry.getKey() + ": " + entry.getValue());
    }

    buf.append("\n]");
    return ""+buf;
  }    
}