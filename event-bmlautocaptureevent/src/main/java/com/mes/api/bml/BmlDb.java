package com.mes.api.bml;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import com.mes.database.SQLJConnectionBase;
import com.mes.support.MesEncryption;
import com.mes.support.TridentTools;

public class BmlDb extends SQLJConnectionBase
{
  static Logger log = Logger.getLogger(BmlDb.class);

  private static SimpleDateFormat defaultSdf = new SimpleDateFormat(
    "'<nobr>'EEE M/d/yy'</nobr> <nobr>'h:mma'</nobr>'");
  private static String formatHtmlDate(Date date, SimpleDateFormat sdf)
  {
    if (date == null)
    {
      return "--";
    }
    return sdf.format(date);
  }
  public static String formatHtmlDate(Date date)
  {
    return formatHtmlDate(date,defaultSdf);
  }
  public static String formatHtmlDate(Date date, String formatStr)
  {
    return formatHtmlDate(date,new SimpleDateFormat(formatStr));
  }

  public static Date toDate(Timestamp ts)
  {
    return ts != null ? new Date(ts.getTime()) : null;
  }
  public static Timestamp toTimestamp(Date date)
  {
    return date != null ? new Timestamp(date.getTime()) : null;
  }

  private long getNewId() throws SQLException
  {
    PreparedStatement ps = null;
    ResultSet rs = null;

    try
    {
      // NOTE: assumes db connected
      ps = con.prepareStatement(" select bml_id_sequence.nextval from dual ");
      rs = ps.executeQuery();
      rs.next();
      long newId = rs.getLong(1);
      rs.close();
      ps.close();
      return newId;
    }
    finally
    {
      try { rs.close(); } catch (Exception e) {}
      try { ps.close(); } catch (Exception e) {}
    }
  }

  private void assignId(BmlBaseTran tran) throws Exception
  {
    if (tran.getRecId() != 0L) return;
    tran.setRecId(getNewId());
  }

  private void logOrderIdEntry(String tranType, String tranId, String orderId)
    throws SQLException
  {
    PreparedStatement ps = null;

    BmlOrderIdEntry entry = new BmlOrderIdEntry(getNewId(), 
      Calendar.getInstance().getTime(),tranId,tranType,orderId);

    try
    {
      // NOTE: assumes db connected
      ps = con.prepareStatement(
        " insert into bml_api_order_ids (   " +
        "  rec_id,                          " +
        "  tran_ts,                         " +
        "  tran_id,                         " +
        "  tran_type,                       " +
        "  order_id )                       " +
        " values (                          " +
        "  ?, ?, ?, ?, ? )                  ");

      ps.setLong      (1, entry.getRecId());
      ps.setTimestamp (2, entry.getTranTs());
      ps.setString    (3, entry.getTranId());
      ps.setString    (4, entry.getTranType());
      ps.setString    (5, entry.getOrderId());
      ps.executeUpdate();
    }
    finally
    {
      try { ps.close(); } catch (Exception e) {}
    }
  }

  public boolean storeLookup(BmlLookupTran lookupTran)
  {
    try
    {
      connect();
      logOrderIdEntry(BmlOrderIdEntry.TT_LOOKUP,lookupTran.getTridentTranId(),
        lookupTran.getBmlRespOrderId());
      return true;
    }
    catch (Exception e)
    {
      log.error("Error: " + e);
      log.debug(""+lookupTran);
    }
    finally
    {
      cleanUp();
    }
    return false;
  }

  public boolean storeAuthorization(BmlAuthorizeTran authTran)
  {
    PreparedStatement ps = null;
    
    try
    {
      connect();

      assignId(authTran);

      ps = con.prepareStatement(
        " insert into bml_api_auths (       " +
        "  rec_id,                          " +
        "  request_ts,                      " +
        "  tran_id,                         " +
        "  profile_id,                      " +
        "  amount,                          " +
        "  ship_amount,                     " +
        "  tax_amount,                      " +
        "  order_num,                       " +
        "  order_desc,                      " +
        "  promo_code,                      " +
        "  cust_reg_date,                   " +
        "  cust_flag,                       " +
        "  cat_code,                        " +
        "  tran_mode,                       " +
        "  terms,                           " +
        "  bill_first_name,                 " +
        "  bill_middle_name,                " +
        "  bill_last_name,                  " +
        "  bill_addr1,                      " +
        "  bill_addr2,                      " +
        "  bill_city,                       " +
        "  bill_state,                      " +
        "  bill_zip,                        " +
        "  bill_phone1,                     " +
        "  bill_phone2,                     " +
        "  bill_email,                      " +
        "  ship_first_name,                 " +
        "  ship_middle_name,                " +
        "  ship_last_name,                  " +
        "  ship_addr1,                      " +
        "  ship_addr2,                      " +
        "  ship_city,                       " +
        "  ship_state,                      " +
        "  ship_zip,                        " +
        "  ship_phone1,                     " +
        "  ship_phone2,                     " +
        "  ship_email,                      " +
        "  has_checking_acct,               " +
        "  has_savings_acct,                " +
        "  residence_status,                " +
        "  dob,                             " +
        "  ssn,                             " +
        "  household_income,                " +
        "  years_at_employer,               " +
        "  years_at_residence,              " +
        "  tpg_msg_id,                      " +
        "  tpg_msg,                         " +
        "  bml_order_id,                    " +
        "  bml_tran_id,                     " +
        "  status_code,                     " +
        "  account_num,                     " +
        "  auth_code,                       " +
        "  bml_error_num,                   " +
        "  bml_error_desc,                  " +
        "  bml_reason_code,                 " +
        "  bml_reason_desc,                 " +
        "  ip_address,                      " +
        "  prod_code,                       " +
        "  server_name,                     " +
        "  req_account_num,                 " +
        "  req_bml_order_id,                " +
        "  sale_flag,                       " +
        "  host_url,                        "+
        "  account_num_enc )                " +
        " values (                          " +
        "  ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, " +
        "  ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, " +
        "  ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, " +
        "  ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, " +
        "  ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,    " +
        "  ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )   ");

      ps.setLong      (1, authTran.getRecId());
      ps.setTimestamp (2, toTimestamp(authTran.getRequestDate()));
      ps.setString    (3, authTran.getTridentTranId());                         
      ps.setString    (4, authTran.getProfile().getProfileId());
      ps.setBigDecimal(5, authTran.getAmount());
      ps.setBigDecimal(6, authTran.getShipAmount());
      ps.setBigDecimal(7, authTran.getBmlTaxAmount());
      ps.setString    (8, authTran.getOrderNum());
      ps.setString    (9, authTran.getOrderDesc());
      ps.setString    (10,authTran.getPromoCode());
      ps.setString    (11,authTran.getCustRegDate());
      ps.setString    (12,authTran.getCustFlag());
      ps.setString    (13,authTran.getCatCode());
      ps.setString    (14,authTran.getTranMode());
      ps.setString    (15,authTran.getTerms());
      ps.setString    (16,authTran.getBillingFirstName());
      ps.setString    (17,authTran.getBillingMiddleName());
      ps.setString    (18,authTran.getBillingLastName());
      ps.setString    (19,authTran.getBillingAddress1());
      ps.setString    (20,authTran.getBillingAddress2());
      ps.setString    (21,authTran.getBillingCity());
      ps.setString    (22,authTran.getBillingState());
      ps.setString    (23,authTran.getBillingZip());
      ps.setString    (24,authTran.getBillingPhone1());
      ps.setString    (25,authTran.getBillingPhone2());
      ps.setString    (26,authTran.getBillingEmail());
      ps.setString    (27,authTran.getShippingFirstName());
      ps.setString    (28,authTran.getShippingMiddleName());
      ps.setString    (29,authTran.getShippingLastName());
      ps.setString    (30,authTran.getShippingAddress1());
      ps.setString    (31,authTran.getShippingAddress2());
      ps.setString    (32,authTran.getShippingCity());
      ps.setString    (33,authTran.getShippingState());
      ps.setString    (34,authTran.getShippingZip());
      ps.setString    (35,authTran.getShippingPhone1());
      ps.setString    (36,authTran.getShippingPhone2());
      ps.setString    (37,authTran.getShippingEmail());
      ps.setString    (38,authTran.getHasCheckingAccount());
      ps.setString    (39,authTran.getHasSavingsAccount());
      ps.setString    (40,authTran.getResidenceStatus());
      ps.setString    (41,authTran.getDob());
      ps.setString    (42,authTran.getSsn());
      ps.setBigDecimal(43,authTran.getHouseholdIncome());
      ps.setString    (44,authTran.getYearsAtEmployer());
      ps.setString    (45,authTran.getYearsAtResidence());
      ps.setString    (46,authTran.getErrorCode());
      ps.setString    (47,authTran.getErrorDesc());
      ps.setString    (48,authTran.getBmlRespOrderId());
      ps.setString    (49,authTran.getBmlTranId());
      ps.setString    (50,authTran.getStatusCode());
      ps.setString    (52,authTran.getBmlAuthCode());
      ps.setString    (53,authTran.getBmlErrorNum());
      ps.setString    (54,authTran.getBmlErrorDesc());
      ps.setString    (55,authTran.getBmlReasonCode());
      ps.setString    (56,authTran.getBmlReasonDesc());
      ps.setString    (57,authTran.getIpAddress());
      ps.setString    (58,authTran.getProdCode());
      ps.setString    (59,authTran.getServerName());
      ps.setString    (60,authTran.getBmlAcctNum());
      ps.setString    (61,authTran.getBmlReqOrderId());
      ps.setString    (62,(authTran.isSale() ? "Y" : "N"));
      ps.setString    (63,authTran.getCardinalCentinelUrl());

      // encode account num
      String accountNum = authTran.getRespAcctNum();
      byte[] encodedAccountNum = null;
      if (accountNum != null)
      {
        encodedAccountNum = 
          MesEncryption.getClient().encrypt(accountNum.getBytes());
        // this truncates actually
        accountNum = TridentTools.encodeCardNumber(accountNum);  
      }
      ps.setString    (51,accountNum);
      ps.setBytes     (64,encodedAccountNum);

      ps.executeUpdate();

      logOrderIdEntry(BmlOrderIdEntry.TT_AUTHORIZE,
        authTran.getTridentTranId(),authTran.getBmlRespOrderId());

      return true;
    }
    catch (Exception e)
    {
      log.error("Error: " + e);
      log.debug(""+authTran);
    }
    finally
    {
      cleanUp(ps);
    }
    return false;
  }

  public boolean storeCapture(BmlCaptureTran capTran)
  {
    PreparedStatement ps = null;
    
    try
    {
      connect();

      assignId(capTran);

      ps = con.prepareStatement(
        " insert into bml_api_captures (  " +
        "  rec_id,                        " +
        "  request_ts,                    " +
        "  tran_id,                       " +
        "  profile_id,                    " +
        "  amount,                        " +
        "  tpg_msg_id,                    " +
        "  tpg_msg,                       " +
        "  bml_order_id,                  " +
        "  bml_tran_id,                   " +
        "  status_code,                   " +
        "  bml_error_num,                 " +
        "  bml_error_desc,                " +
        "  bml_reason_code,               " +
        "  bml_reason_desc,               " +
        "  auth_tran_id,                  " +
        "  server_name,                   " +
        "  recon_id,                      " +
        "  first_cap_flag,                " +
        "  auto_cap_flag,                 " +
        "  host_url )                     " +
        " values (                        " +
        "  ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,  " +
        "  ?, ?, ?, ?, ?, ?, ?, ?, ?, ? ) ");

      ps.setLong      (1, capTran.getRecId());
      ps.setTimestamp (2, toTimestamp(capTran.getRequestDate()));
      ps.setString    (3, capTran.getTridentTranId());
      ps.setString    (4, capTran.getProfile().getProfileId());
      ps.setBigDecimal(5, capTran.getAmount());
      ps.setString    (6, capTran.getErrorCode());
      ps.setString    (7, capTran.getErrorDesc());
      ps.setString    (8, capTran.getBmlRespOrderId());
      ps.setString    (9, capTran.getBmlTranId());
      ps.setString    (10,capTran.getStatusCode());
      ps.setString    (11,capTran.getBmlErrorNum());
      ps.setString    (12,capTran.getBmlErrorDesc());
      ps.setString    (13,capTran.getBmlReasonCode());
      ps.setString    (14,capTran.getBmlReasonDesc());
      ps.setString    (15,capTran.getAuthTran().getTridentTranId());
      ps.setString    (16,capTran.getServerName());
      ps.setInt       (17,capTran.getReconId());
      ps.setString    (18,(capTran.isFirstCapture() ? "Y" : "N"));
      ps.setString    (19,(capTran.isAutoCapture() ? "Y" : "N"));
      ps.setString    (20,capTran.getCardinalCentinelUrl());

      ps.executeUpdate();

      logOrderIdEntry(BmlOrderIdEntry.TT_CAPTURE,capTran.getTridentTranId(),
        capTran.getBmlRespOrderId());

      return true;
    }
    catch (Exception e)
    {
      log.error("Error: " + e);
      e.printStackTrace();
    }
    finally
    {
      cleanUp(ps);
    }
    return false;
  }

  public boolean storeReauth(BmlReauthorizeTran reauthTran)
  {
    PreparedStatement ps = null;
    
    try
    {
      connect();

      assignId(reauthTran);

      ps = con.prepareStatement(
        " insert into bml_api_reauths (   " +
        "  rec_id,                        " +
        "  request_ts,                    " +
        "  tran_id,                       " +
        "  profile_id,                    " +
        "  amount,                        " +
        "  tpg_msg_id,                    " +
        "  tpg_msg,                       " +
        "  bml_order_id,                  " +
        "  bml_tran_id,                   " +
        "  status_code,                   " +
        "  bml_error_num,                 " +
        "  bml_error_desc,                " +
        "  bml_reason_code,               " +
        "  bml_reason_desc,               " +
        "  account_num,                   " +
        "  auth_tran_id,                  " +
        "  server_name,                   " +
        "  host_url,                      " +
        "  account_num_enc )              " +
        " values (                        " +
        "  ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,  " +
        "  ?, ?, ?, ?, ?, ?, ?, ?, ? )    ");

      ps.setLong      (1, reauthTran.getRecId());
      ps.setTimestamp (2, new Timestamp(reauthTran.getRequestDate().getTime()));
      ps.setString    (3, reauthTran.getTridentTranId());
      ps.setString    (4, reauthTran.getProfile().getProfileId());
      ps.setBigDecimal(5, reauthTran.getAmount());
      ps.setString    (6, reauthTran.getErrorCode());
      ps.setString    (7, reauthTran.getErrorDesc());
      ps.setString    (8, reauthTran.getBmlRespOrderId());
      ps.setString    (9, reauthTran.getBmlTranId());
      ps.setString    (10,reauthTran.getStatusCode());
      ps.setString    (11,reauthTran.getBmlErrorNum());
      ps.setString    (12,reauthTran.getBmlErrorDesc());
      ps.setString    (13,reauthTran.getBmlReasonCode());
      ps.setString    (14,reauthTran.getBmlReasonDesc());
      ps.setString    (16,reauthTran.getAuthTran().getTridentTranId());
      ps.setString    (17,reauthTran.getServerName());
      ps.setString    (18,reauthTran.getCardinalCentinelUrl());

      // encode account num
      String accountNum = reauthTran.getAccountNum();
      byte[] encodedAccountNum = null;
      if (accountNum != null)
      {
        encodedAccountNum = 
          MesEncryption.getClient().encrypt(accountNum.getBytes());
        // this truncates actually
        accountNum = TridentTools.encodeCardNumber(accountNum);  
      }
      ps.setString    (15,accountNum);
      ps.setBytes     (19,encodedAccountNum);

      ps.executeUpdate();
      return true;
    }
    catch (Exception e)
    {
      log.error("Error: " + e);
      e.printStackTrace();
    }
    finally
    {
      cleanUp(ps);
    }
    return false;
  }

  public boolean storeRefund(BmlRefundTran refTran)
  {
    PreparedStatement ps = null;
    
    try
    {
      connect();

      assignId(refTran);

      ps = con.prepareStatement(
        " insert into bml_api_refunds (   " +
        "  rec_id,                        " +
        "  request_ts,                    " +
        "  tran_id,                       " +
        "  profile_id,                    " +
        "  amount,                        " +
        "  tpg_msg_id,                    " +
        "  tpg_msg,                       " +
        "  bml_order_id,                  " +
        "  bml_tran_id,                   " +
        "  status_code,                   " +
        "  bml_error_num,                 " +
        "  bml_error_desc,                " +
        "  bml_reason_code,               " +
        "  bml_reason_desc,               " +
        "  cap_tran_id,                   " +
        "  server_name,                   " +
        "  recon_id,                      " +
        "  host_url )                     " +
        " values (                        " +
        "  ?, ?, ?, ?, ?, ?, ?, ?, ?,     " +
        "  ?, ?, ?, ?, ?, ?, ?, ?, ? )    ");

      ps.setLong      (1, refTran.getRecId());
      ps.setTimestamp (2, new Timestamp(refTran.getRequestDate().getTime()));
      ps.setString    (3, refTran.getTridentTranId());
      ps.setString    (4, refTran.getProfile().getProfileId());
      ps.setBigDecimal(5, refTran.getAmount());
      ps.setString    (6, refTran.getErrorCode());
      ps.setString    (7, refTran.getErrorDesc());
      ps.setString    (8, refTran.getBmlRespOrderId());
      ps.setString    (9, refTran.getBmlTranId());
      ps.setString    (10,refTran.getStatusCode());
      ps.setString    (11,refTran.getBmlErrorNum());
      ps.setString    (12,refTran.getBmlErrorDesc());
      ps.setString    (13,refTran.getBmlReasonCode());
      ps.setString    (14,refTran.getBmlReasonDesc());
      ps.setString    (15,refTran.getCapTran().getTridentTranId());
      ps.setString    (16,refTran.getServerName());
      ps.setInt       (17,refTran.getReconId());
      ps.setString    (18,refTran.getCardinalCentinelUrl());
      ps.executeUpdate();
      return true;
    }
    catch (Exception e)
    {
      log.error("Error: " + e);
      e.printStackTrace();
    }
    finally
    {
      cleanUp(ps);
    }
    return false;
  }

  public static boolean storeTransaction(BmlBaseTran bmlTran)
  {
    if (bmlTran instanceof BmlAuthorizeTran)
    {
      return (new BmlDb()).storeAuthorization((BmlAuthorizeTran)bmlTran);
    }
    else if (bmlTran instanceof BmlLookupTran)
    {
      return (new BmlDb()).storeLookup((BmlLookupTran)bmlTran);
    }
    else if (bmlTran instanceof BmlCaptureTran)
    {
      return (new BmlDb()).storeCapture((BmlCaptureTran)bmlTran);
    }
    else if (bmlTran instanceof BmlReauthorizeTran)
    {
      return (new BmlDb()).storeReauth((BmlReauthorizeTran)bmlTran);
    }
    else if (bmlTran instanceof BmlRefundTran)
    {
      return (new BmlDb()).storeRefund((BmlRefundTran)bmlTran);
    }

    throw new ClassCastException("Invalid transaction type: " 
      + bmlTran.getClass().getName());
  }

  private List lookupAuthorizes(Date fromDate, Date toDate)
  {
    PreparedStatement ps = null;
    ResultSet rs = null;
    
    try
    {
      connect();

      ps = con.prepareStatement(
        " select * from bml_api_auths         " +
        " where                               " +
        "  trunc(request_ts) between ? and ?  " +
        " order by request_ts desc            ");
      ps.setTimestamp(1,new Timestamp(fromDate.getTime()));
      ps.setTimestamp(2,new Timestamp(toDate.getTime()));
      rs = ps.executeQuery();
      List auths = new ArrayList();
      while (rs.next())
      {
        BmlAuthorizeTran authTran = new BmlAuthorizeTran(rs.getString("tran_id"));
        authTran.setData(rs);
        auths.add(authTran);
      }
      return auths;
    }
    catch (Exception e)
    {
      log.error("Error: " + e);
      e.printStackTrace();
    }
    finally
    {
      cleanUp(ps,rs);
    }
    return null;
  }

  public static List lookupAuths(Date fromDate, Date toDate)
  {
    return (new BmlDb()).lookupAuthorizes(fromDate,toDate);
  }

  private List lookupCaptures(Date fromDate, Date toDate)
  {
    PreparedStatement ps = null;
    ResultSet rs = null;
    
    try
    {
      connect();

      ps = con.prepareStatement(
        " select * from bml_api_captures      " +
        " where                               " +
        "  trunc(request_ts) between ? and ?  " +
        " order by request_ts desc            ");
      ps.setTimestamp(1,new Timestamp(fromDate.getTime()));
      ps.setTimestamp(2,new Timestamp(toDate.getTime()));
      rs = ps.executeQuery();
      List captures = new ArrayList();
      while (rs.next())
      {
        BmlCaptureTran capTran 
          = new BmlCaptureTran(rs.getString("tran_id"),rs.getInt("recon_id"));
        capTran.setData(rs);
        captures.add(capTran);
      }
      return captures;
    }
    catch (Exception e)
    {
      log.error("Error: " + e);
      e.printStackTrace();
    }
    finally
    {
      cleanUp(ps,rs);
    }
    return null;
  }

  public static List lookupCaps(Date fromDate, Date toDate)
  {
    return (new BmlDb()).lookupCaptures(fromDate,toDate);
  }

  /**
   * Looks up an authorize using a trident tran id.
   */
  private BmlAuthorizeTran lookupAuthorize(String tranId)
  {
    PreparedStatement ps = null;
    ResultSet rs = null;
    
    try
    {
      connect();

      ps = con.prepareStatement(
        " select * from bml_api_auths where tran_id = ? ");
      ps.setString(1,tranId);
      rs = ps.executeQuery();
      if (rs.next())
      {
        BmlAuthorizeTran authTran 
          = new BmlAuthorizeTran(tranId);
        authTran.setData(rs);
        return authTran;
      }
    }
    catch (Exception e)
    {
      log.error("Error: " + e);
      e.printStackTrace();
    }
    finally
    {
      cleanUp(ps,rs);
    }
    return null;
  }

  public static BmlAuthorizeTran lookupAuth(String tranId)
  {
    return (new BmlDb()).lookupAuthorize(tranId);
  }

  /**
   * Looks up a reference authorize using the order_id in a referencing tran.
  private BmlAuthorizeTran lookupRefAuthorize(String orderId)
  {
    PreparedStatement ps = null;
    ResultSet rs = null;
    
    try
    {
      connect();

      ps = con.prepareStatement(
        " select * from bml_api_auths where bml_order_id = ? ");
      ps.setString(1,orderId);
      rs = ps.executeQuery();
      if (rs.next())
      {
        BmlAuthorizeTran authTran 
          = new BmlAuthorizeTran(rs.getString("tran_id"));
        authTran.setData(rs);
        return authTran;
      }
    }
    catch (Exception e)
    {
      log.error("Error: " + e);
      e.printStackTrace();
    }
    finally
    {
      cleanUp(ps,rs);
    }
    return null;
  }
   */

  public static BmlAuthorizeTran lookupRefAuth(String tranId)
  {
    return (new BmlDb()).lookupAuthorize(tranId);
  }
  public static BmlAuthorizeTran lookupRefAuth(BmlAuthRefTran refTran)
  {
    return lookupRefAuth(refTran.getRefTranId());
  }

  /**
   * Looks up a capture using a trident tran id.
   */
  private BmlCaptureTran lookupCapture(String tranId)
  {
    PreparedStatement ps = null;
    ResultSet rs = null;
    
    try
    {
      connect();

      ps = con.prepareStatement(
        " select * from bml_api_captures where tran_id = ? ");
      ps.setString(1,tranId);
      rs = ps.executeQuery();
      if (rs.next())
      {
        BmlCaptureTran capTran 
          = new BmlCaptureTran(tranId,rs.getInt("recon_id"));
        capTran.setData(rs);
        return capTran;
      }
    }
    catch (Exception e)
    {
      log.error("Error: " + e);
      e.printStackTrace();
    }
    finally
    {
      cleanUp(ps,rs);
    }
    return null;
  }

  public static BmlCaptureTran lookupCap(String tranId)
  {
    return (new BmlDb()).lookupCapture(tranId);
  }

  /**
   * Looks up a reference capture using the order_id.
  private BmlCaptureTran lookupRefCapture(String orderId)
  {
    PreparedStatement ps = null;
    ResultSet rs = null;
    
    try
    {
      connect();

      ps = con.prepareStatement(
        " select * from bml_api_captures where bml_order_id = ? ");
      ps.setString(1,orderId);
      rs = ps.executeQuery();
      if (rs.next())
      {
        BmlCaptureTran capTran 
          = new BmlCaptureTran(rs.getString("tran_id"),rs.getInt("recon_id"));
        capTran.setData(rs);
        return capTran;
      }
    }
    catch (Exception e)
    {
      log.error("Error: " + e);
      e.printStackTrace();
    }
    finally
    {
      cleanUp(ps,rs);
    }
    return null;
  }
   */

  public static BmlCaptureTran lookupRefCap(String tranId)
  {
    return (new BmlDb()).lookupCapture(tranId);
  }
  public static BmlCaptureTran lookupRefCap(BmlCapRefTran refTran)
  {
    return lookupRefCap(refTran.getRefTranId());
  }

  /**
   * Find the number of captures associated with a given auth tran id.
   */
  private int getCaptureCount(String tranId)
  {
    PreparedStatement ps = null;
    ResultSet rs = null;
    
    try
    {
      connect();

      ps = con.prepareStatement(
        " select count(*)           " +
        " from bml_api_captures     " +
        " where                     " +
        "  auth_tran_id = ?         " +
        "  and tpg_msg_id = '000'   ");
      ps.setString(1,tranId);
      rs = ps.executeQuery();
      if (rs.next())
      {
        return rs.getInt(1);
      }
    }
    catch (Exception e)
    {
      log.error("Error: " + e);
      e.printStackTrace();
    }
    finally
    {
      cleanUp(ps,rs);
    }
    return 0;
  }

  public static int getCapCount(String tranId)
  {
    return (new BmlDb()).getCaptureCount(tranId);
  }

  /**
   * Looks up first order entry from specified number of days ago.
   */
  public BmlOrderIdEntry lookupFirstOrderEntry(int daysAgo)
  {
    PreparedStatement ps = null;
    ResultSet rs = null;
    
    try
    {
      connect();

      ps = con.prepareStatement(
        " select                            " +
        "  rec_id,                          " +
        "  tran_ts,                         " +
        "  tran_id,                         " +
        "  tran_type,                       " +
        "  order_id                         " +
        " from                              " +
        "  bml_api_order_ids                " +
        " where                             " +
        "  tran_ts >= trunc( sysdate - ? )  " +
        " order by tran_ts                  ");
      ps.setInt(1,daysAgo);
      rs = ps.executeQuery();
      if (rs.next())
      {
        BmlOrderIdEntry entry = new BmlOrderIdEntry();
        entry.setRecId(rs.getLong("rec_id"));
        entry.setTranTs(rs.getTimestamp("tran_ts"));
        entry.setTranId(rs.getString("tran_id"));
        entry.setTranType(rs.getString("tran_type"));
        entry.setOrderId(rs.getString("order_id"));
        return entry;
      }
    }
    catch (Exception e)
    {
      log.error("Error: " + e);
      e.printStackTrace();
    }
    finally
    {
      cleanUp(ps,rs);
    }
    return null;
  }

  /**
   * Given a rec id this will fetch a list of order entries with a later 
   * date than that  of the specified record.  If more than the maximum
   * are found the list will contain only the first x records.
   */
  public List lookupNextEntries(long recId, int max)
  {
    PreparedStatement ps = null;
    ResultSet rs = null;
    List entries = new ArrayList();
    
    try
    {
      connect();

      ps = con.prepareStatement(
        " select                          " +
        "  i2.rec_id,                     " +
        "  i2.tran_ts,                    " +
        "  i2.tran_id,                    " +
        "  i2.tran_type,                  " +
        "  i2.order_id                    " +
        " from                            " +
        "  bml_api_order_ids i1,          " +
        "  bml_api_order_ids i2           " +
        " where                           " +
        "  i1.rec_id = ?                  " +
        "  and i2.tran_ts >= i1.tran_ts   " +
        "  and i2.rec_id <> i1.rec_id     " +
        " order by i2.tran_ts             ");
      ps.setLong(1,recId);
      rs = ps.executeQuery();
      int count = 0;
      while (rs.next() && count < max)
      {
        BmlOrderIdEntry entry = new BmlOrderIdEntry();
        entry.setRecId(rs.getLong("rec_id"));
        entry.setTranTs(rs.getTimestamp("tran_ts"));
        entry.setTranId(rs.getString("tran_id"));
        entry.setTranType(rs.getString("tran_type"));
        entry.setOrderId(rs.getString("order_id"));
        entries.add(entry);
        ++count;
      }
    }
    catch (Exception e)
    {
      log.error("Error: " + e);
      e.printStackTrace();
    }
    finally
    {
      cleanUp(ps,rs);
    }
    return entries;
  }    

  public BmlOrderIdEntry lookupEntryByTranId(String tranId)
  {
    PreparedStatement ps = null;
    ResultSet rs = null;
    
    try
    {
      connect();

      ps = con.prepareStatement(
        " select              " +
        "  rec_id,            " +
        "  tran_ts,           " +
        "  tran_id,           " +
        "  tran_type,         " +
        "  order_id           " +
        " from                " +
        "  bml_api_order_ids  " +
        " where               " +
        "  tran_id = ?        " +
        " order by tran_ts    ");
      ps.setString(1,tranId);
      rs = ps.executeQuery();
      if (rs.next())
      {
        BmlOrderIdEntry entry = new BmlOrderIdEntry();
        entry.setRecId(rs.getLong("rec_id"));
        entry.setTranTs(rs.getTimestamp("tran_ts"));
        entry.setTranId(rs.getString("tran_id"));
        entry.setTranType(rs.getString("tran_type"));
        entry.setOrderId(rs.getString("order_id"));
        return entry;
      }
    }
    catch (Exception e)
    {
      log.error("Error: " + e);
      e.printStackTrace();
    }
    finally
    {
      cleanUp(ps,rs);
    }
    return null;
  }

  /**
   * Returns list of uncaptured sales transactions (auths that have not been
   * autocaptured).  To fetch all sales, set max to 0.
   */
  public List _getUncapturedSales(int max)
  {
    PreparedStatement ps = null;
    ResultSet rs = null;
    List sales = new ArrayList();
    
    try
    {
      connect();

      ps = con.prepareStatement(
        " select                              " +
        "  a.*                                " +
        " from                                " +
        "  bml_api_auths a,                   " +
        "  bml_api_captures c,                " +
        "  bml_api_autocap_log l              " +
        " where                               " +
        "  a.sale_flag = 'Y'                  " +
        "  and a.tpg_msg_id = '000'           " +
        "  and a.tran_id = c.auth_tran_id(+)  " +
        "  and c.tran_id is null              " +
        "  and a.tran_id = l.tran_id(+)       " +
        "  and l.tran_id is null              ");
      rs = ps.executeQuery();
      int count = 0;
      while (rs.next() && (count < max || max == 0))
      {
        BmlSaleTran sale = new BmlSaleTran(rs.getString("tran_id"));
        sale.setData(rs);
        sales.add(sale);
        ++count;
      }
    }
    catch (Exception e)
    {
      log.error("Error: " + e);
      e.printStackTrace();
    }
    finally
    {
      cleanUp(ps,rs);
    }
    return sales;
  }
  public List _getUncapturedSales()
  {
    return _getUncapturedSales(0);
  }
  public static List getUncapturedSales(int max)
  {
    return (new BmlDb())._getUncapturedSales(max);
  }
  public static List getUncapturedSales()
  {
    return getUncapturedSales(0);
  }

  public boolean logAutoCapture(String tranId, String tpgMsgId, String tpgMsg)
  {
    PreparedStatement ps = null;
    
    try
    {
      connect();

      long recId = getNewId();

      ps = con.prepareStatement(
        " insert into bml_api_autocap_log ( " +
        "  rec_id,                          " +
        "  tran_ts,                         " +
        "  tran_id,                         " +
        "  tpg_msg_id,                      " +
        "  tpg_msg )                        " +
        " values ( ?, sysdate, ?, ?, ? )    ");

      ps.setLong      (1, recId);
      ps.setString    (2, tranId);
      ps.setString    (3, tpgMsgId);
      ps.setString    (4, tpgMsg);
      ps.executeUpdate();
      return true;
    }
    catch (Exception e)
    {
      log.error("Error: " + e);
      e.printStackTrace();
    }
    finally
    {
      cleanUp(ps);
    }
    return false;
  }
}