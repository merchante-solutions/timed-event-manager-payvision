package com.mes.api.bml;

import java.sql.ResultSet;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.api.TridentApiConstants;
import com.mes.support.HttpHelper;

public class BmlLookupTran extends BmlBaseTran
{
  //
  // if you do not specify the serial version ID then Java will default
  // to a UID based on the fields in the class (see <jdk>/bin/serialver).  
  // failure to specify a static version will cause a mismatch exception 
  // if the object definition is changed.
  //
  // NOTE: it is necessary to specify a static serialVersionUID for all
  //       child classes.
  //
  static final long serialVersionUID    = TridentApiConstants.ApiVersionId;

  static Logger log = Logger.getLogger(BmlLookupTran.class);

  // request data

  // tpg api request field names (received from merchant)
  public static final String  FN_BML_AMOUNT           = "amount";
  public static final String  FN_BML_SHIP_AMOUNT      = "ship_amount";
  public static final String  FN_BML_TAX_AMOUNT       = "tax_amount";
  public static final String  FN_BML_ORDER_NUM        = "order_num";
  public static final String  FN_BML_ORDER_DESC       = "order_desc";
  public static final String  FN_BML_PROMO_CODE       = "promo_code";
  public static final String  FN_BML_IP_ADDRESS       = "ip_address";
  public static final String  FN_BML_BILL_FIRST_NAME  = "bill_first_name";
  public static final String  FN_BML_BILL_MIDDLE_NAME = "bill_middle_name";
  public static final String  FN_BML_BILL_LAST_NAME   = "bill_last_name";
  public static final String  FN_BML_BILL_ADDR1       = "bill_addr1";
  public static final String  FN_BML_BILL_ADDR2       = "bill_addr2";
  public static final String  FN_BML_BILL_CITY        = "bill_city";
  public static final String  FN_BML_BILL_STATE       = "bill_state";
  public static final String  FN_BML_BILL_ZIP         = "bill_zip";
  public static final String  FN_BML_BILL_PHONE1      = "bill_phone1";
  public static final String  FN_BML_BILL_PHONE2      = "bill_phone2";
  public static final String  FN_BML_BILL_EMAIL       = "bill_email";
  public static final String  FN_BML_SHIP_FIRST_NAME  = "ship_first_name";
  public static final String  FN_BML_SHIP_MIDDLE_NAME = "ship_middle_name";
  public static final String  FN_BML_SHIP_LAST_NAME   = "ship_last_name";
  public static final String  FN_BML_SHIP_ADDR1       = "ship_addr1";
  public static final String  FN_BML_SHIP_ADDR2       = "ship_addr2";
  public static final String  FN_BML_SHIP_CITY        = "ship_city";
  public static final String  FN_BML_SHIP_STATE       = "ship_state";
  public static final String  FN_BML_SHIP_ZIP         = "ship_zip";
  public static final String  FN_BML_SHIP_PHONE1      = "ship_phone1";
  public static final String  FN_BML_SHIP_PHONE2      = "ship_phone2";
  public static final String  FN_BML_SHIP_EMAIL       = "ship_email";

  protected double            amount                  = 0.0;
  protected double            shipAmount              = 0.0;
  protected double            taxAmount               = 0.0;
  protected String            orderNum                = null;
  protected String            orderDesc               = null;
  protected String            promoCode               = "Default";
  protected String            ipAddress               = null;

  // consumer data
  protected String            billingAddress1         = null;
  protected String            billingAddress2         = null;
  protected String            billingCity             = null;
  protected String            billingCountryCode      = null;
  protected String            billingFirstName        = null;
  protected String            billingMiddleName       = null;
  protected String            billingLastName         = null;
  protected String            billingPhone1           = null;
  protected String            billingPhone2           = null;
  protected String            billingState            = null;
  protected String            billingZip              = null;
  protected String            billingEmail            = null;
  protected String            shippingAddress1        = null;
  protected String            shippingAddress2        = null;
  protected String            shippingCity            = null;
  protected String            shippingCountryCode     = null;
  protected String            shippingFirstName       = null;
  protected String            shippingMiddleName      = null;
  protected String            shippingLastName        = null;
  protected String            shippingPhone1          = null;
  protected String            shippingPhone2          = null;
  protected String            shippingState           = null;
  protected String            shippingZip             = null;
  protected String            shippingEmail           = null;

  // response data

  // tpg api response field names (sent to merchant)
  public static final String  FN_BML_ENROLLED         = "enroll_status";
  public static final String  FN_BML_ACS_URL          = "acs_url";
  public static final String  FN_BML_PA_REQ           = "pa_request";

  public BmlLookupTran(String tranId)
  {
    super(tranId);
  }

  private void setConsumerData(HttpServletRequest request)
  {
    billingFirstName    = HttpHelper.getString(request,FN_BML_BILL_FIRST_NAME,null);
    billingMiddleName   = HttpHelper.getString(request,FN_BML_BILL_MIDDLE_NAME,null);
    billingLastName     = HttpHelper.getString(request,FN_BML_BILL_LAST_NAME,null);
    billingAddress1     = HttpHelper.getString(request,FN_BML_BILL_ADDR1,null);
    billingAddress2     = HttpHelper.getString(request,FN_BML_BILL_ADDR2,null);
    billingCity         = HttpHelper.getString(request,FN_BML_BILL_CITY,null);
    billingState        = HttpHelper.getString(request,FN_BML_BILL_STATE,null);
    billingZip          = HttpHelper.getString(request,FN_BML_BILL_ZIP,null);
    billingPhone1       = HttpHelper.getString(request,FN_BML_BILL_PHONE1,null);
    billingPhone2       = HttpHelper.getString(request,FN_BML_BILL_PHONE2,null);
    billingEmail        = HttpHelper.getString(request,FN_BML_BILL_EMAIL,null);
    shippingFirstName   = HttpHelper.getString(request,FN_BML_SHIP_FIRST_NAME,null);
    shippingMiddleName  = HttpHelper.getString(request,FN_BML_SHIP_MIDDLE_NAME,null);
    shippingLastName    = HttpHelper.getString(request,FN_BML_SHIP_LAST_NAME,null);
    shippingAddress1    = HttpHelper.getString(request,FN_BML_SHIP_ADDR1,null);
    shippingAddress2    = HttpHelper.getString(request,FN_BML_SHIP_ADDR2,null);
    shippingCity        = HttpHelper.getString(request,FN_BML_SHIP_CITY,null);
    shippingState       = HttpHelper.getString(request,FN_BML_SHIP_STATE,null);
    shippingZip         = HttpHelper.getString(request,FN_BML_SHIP_ZIP,null);
    shippingPhone1      = HttpHelper.getString(request,FN_BML_SHIP_PHONE1,null);
    shippingPhone2      = HttpHelper.getString(request,FN_BML_SHIP_PHONE2,null);
    shippingEmail       = HttpHelper.getString(request,FN_BML_SHIP_EMAIL,null);
  }

  public void setProperties(HttpServletRequest request)
  {
    super.setProperties(request);
    amount              = HttpHelper.getDouble(request,FN_BML_AMOUNT,0.0);
    shipAmount          = HttpHelper.getDouble(request,FN_BML_SHIP_AMOUNT,0.0);
    taxAmount           = HttpHelper.getDouble(request,FN_BML_TAX_AMOUNT,0.0);
    orderNum            = HttpHelper.getString(request,FN_BML_ORDER_NUM,null);
    orderDesc           = HttpHelper.getString(request,FN_BML_ORDER_DESC,null);
    promoCode           = HttpHelper.getString(request,FN_BML_PROMO_CODE,null);
    ipAddress           = HttpHelper.getString(request,FN_BML_IP_ADDRESS,null);
    setConsumerData(request);
  }

  public double getAmount()
  {
    return amount;
  }

  public String getBillingAddress1()
  {
    return billingAddress1;
  }

  public String getBillingAddress2()
  {
    return billingAddress2;
  }

  public String getBillingCity()
  {
    return billingCity;
  }

  public String getBillingEmail()
  {
    return billingEmail;
  }

  public String getBillingFirstName()
  {
    return billingFirstName;
  }

  public String getBillingLastName()
  {
    return billingLastName;
  }

  public String getBillingMiddleName()
  {
    return billingMiddleName;
  }

  public String getBillingPhone1()
  {
    return billingPhone1;
  }

  public String getBillingPhone2()
  {
    return billingPhone2;
  }

  public String getBillingState()
  {
    return billingState;
  }

  public String getBillingZip()
  {
    return billingZip;
  }

  public String getIpAddress()
  {
    return ipAddress;
  }

  public String getOrderDesc()
  {
    return orderDesc;
  }

  public String getOrderNum()
  {
    return orderNum;
  }

  public String getPromoCode()
  {
    return promoCode;
  }
  
  public double getShipAmount( )
  {
    return shipAmount;
  }
  
  public String getShippingAddress1()
  {
    return shippingAddress1;
  }

  public String getShippingAddress2()
  {
    return shippingAddress2;
  }

  public String getShippingCity()
  {
    return shippingCity;
  }

  public String getShippingEmail()
  {
    return shippingEmail;
  }

  public String getShippingFirstName()
  {
    return shippingFirstName;
  }

  public String getShippingLastName()
  {
    return shippingLastName;
  }

  public String getShippingMiddleName()
  {
    return shippingMiddleName;
  }

  public String getShippingPhone1()
  {
    return shippingPhone1;
  }

  public String getShippingPhone2()
  {
    return shippingPhone2;
  }

  public String getShippingState()
  {
    return shippingState;
  }

  public String getShippingZip()
  {
    return shippingZip;
  }

  public double getTaxAmount()
  {
    return taxAmount;
  }

  /**
   * Data validation
   */

  public boolean validateConsumerData()
  {
    if (isBlank(billingFirstName))
    {
      setError(tac.ER_BML_BILL_FIRST_NAME_REQUIRED);
    }
    else if (isBlank(billingLastName))
    {
      setError(tac.ER_BML_BILL_LAST_NAME_REQUIRED);
    }
    else if (isBlank(billingAddress1))
    {
      setError(tac.ER_BML_BILL_ADDR1_REQUIRED);
    }
    else if (isBlank(billingCity))
    {
      setError(tac.ER_BML_BILL_CITY_REQUIRED);
    }
    else if (isBlank(billingState))
    {
      setError(tac.ER_BML_BILL_STATE_REQUIRED);
    }
    else if (isBlank(billingZip))
    {
      setError(tac.ER_BML_BILL_ZIP_REQUIRED);
    }
    else if (isBlank(billingPhone1))
    {
      setError(tac.ER_BML_BILL_PHONE_REQUIRED);
    }
    else if (isBlank(shippingFirstName))
    {
      setError(tac.ER_BML_SHIP_FIRST_NAME_REQUIRED);
    }
    else if (isBlank(shippingLastName))
    {
      setError(tac.ER_BML_SHIP_LAST_NAME_REQUIRED);
    }
    else if (isBlank(shippingAddress1))
    {
      setError(tac.ER_BML_SHIP_ADDR1_REQUIRED);
    }
    else if (isBlank(shippingCity))
    {
      setError(tac.ER_BML_SHIP_CITY_REQUIRED);
    }
    else if (isBlank(shippingState))
    {
      setError(tac.ER_BML_SHIP_STATE_REQUIRED);
    }
    else if (isBlank(shippingZip))
    {
      setError(tac.ER_BML_SHIP_ZIP_REQUIRED);
    }

    return !hasError();
  }

  public boolean isBmlValid()
  {
    // validate consumer data
    if (!validateConsumerData())
    {
      return false;
    }

    if (Profile.getBmlMerchId(promoCode) == null)
    {
      setError(tac.ER_BML_INVALID_PROMO_CODE);
    }
    //else if (isBlank(promoCode))
    //{
    //  setError(tac.ER_BML_PROMO_CODE_REQUIRED);
    //}
    else if (amount == 0)
    {
      setError(tac.ER_BML_AMOUNT_REQUIRED);
    }
    //else if (shipAmount == 0)
    //{
    //  setError(tac.ER_BML_SHIP_AMOUNT_REQUIRED);
    //}
    else if (isBlank(orderNum))
    {
      setError(tac.ER_BML_ORDER_NUM_REQUIRED);
    }

    return !hasError();
  }

  /**
   * DB loading
   */

  public void setBmlRequestType()
  {
    bmlRequest = BR_LOOKUP;
  }

  public void setTranData(ResultSet rs)
  {
  }
}