package com.mes.api.bml;

import org.apache.log4j.Logger;
import com.mes.api.TridentApiConstants;

public class BmlSaleTran extends BmlAuthorizeTran
{
  //
  // if you do not specify the serial version ID then Java will default
  // to a UID based on the fields in the class (see <jdk>/bin/serialver).  
  // failure to specify a static version will cause a mismatch exception 
  // if the object definition is changed.
  //
  // NOTE: it is necessary to specify a static serialVersionUID for all
  //       child classes.
  //
  static final long serialVersionUID    = TridentApiConstants.ApiVersionId;

  static Logger log = Logger.getLogger(BmlSaleTran.class);

  public BmlSaleTran(String tranId)
  {
    super(tranId);
    saleFlag = true;
  }
}
