package com.mes.api.bml;

import java.math.BigDecimal;
import java.sql.ResultSet;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.api.TridentApiConstants;
import com.mes.support.HttpHelper;

public class BmlCaptureTran extends BmlAuthRefTran
{
  //
  // if you do not specify the serial version ID then Java will default
  // to a UID based on the fields in the class (see <jdk>/bin/serialver).  
  // failure to specify a static version will cause a mismatch exception 
  // if the object definition is changed.
  //
  // NOTE: it is necessary to specify a static serialVersionUID for all
  //       child classes.
  //
  static final long serialVersionUID    = TridentApiConstants.ApiVersionId;

  static Logger log = Logger.getLogger(BmlCaptureTran.class);

  // request data

  // tpg api request field names (received from merchant)
  public static final String  FN_BML_AMOUNT           = "amount";
  public static final String  FN_BML_RECON_ID         = "recon_id";
  public static final String  FN_BML_AUTO_CAP         = "auto_cap";

  protected BigDecimal  amount                        = new BigDecimal(0).setScale(2);
  protected int         reconId;
  protected boolean     autoCapFlag;

  // response data

  // tpg api response field names (sent to merchant)
  public static final String  FN_BML_STATUS_CODE      = "status_code";

  protected boolean     firstCapFlag;

  public BmlCaptureTran(String tranId, int reconId)
  {
    super(tranId);
    this.reconId = reconId;
  }

  public void setProperties(HttpServletRequest request)
  {
    super.setProperties(request);
    amount        = new BigDecimal(HttpHelper.getString(request,FN_BML_AMOUNT,"0")).setScale(2);
    autoCapFlag   = HttpHelper.getString(request,FN_BML_AUTO_CAP,"N").toUpperCase().equals("Y");
    firstCapFlag  = (BmlDb.getCapCount(refTranId) == 0);

    // if this is an first capture then the capture record needs to use the
    // same trident tran id as the auth
    if (firstCapFlag && loadAuthTran())
    {
      setApiTranId(getAuthTran().getApiTranId());
    }
  }

  public BigDecimal getAmount()
  {
    return amount;
  }

  public int getReconId()
  {
    return reconId;
  }

  public boolean isFirstCapture()
  {
    return firstCapFlag;
  }

  public boolean isAutoCapture()
  {
    return autoCapFlag;
  }

  /**
   * Capture requests must refer to an authorize transaction in order to load
   * the auth response order id.
   */
  public String getRefTranType()
  {
    return BmlOrderIdEntry.TT_AUTHORIZE;
  }

  /**
   * Data validation
   */

  public boolean isBmlValid()
  {
    if (!isAutoCapture() && getAuthTran().isSale())
    {
      setError(tac.ER_BML_SALE_CAP_INVALID);
    }
    else if (amount.doubleValue() == 0)
    {
      setError(tac.ER_BML_AMOUNT_REQUIRED);
    }

    return !hasError();
  }

  /**
   * Response accessors
   */

  public String getStatusCode()
  {
    return getResponseValue(FN_BML_STATUS_CODE);
  }

  /**
   * DB loading
   */

  private void setResponseData(ResultSet rs) throws Exception
  {
    setResponseData(FN_BML_TRAN_ID,       rs.getString("bml_tran_id"));
    setResponseData(FN_BML_RESP_ORDER_ID, rs.getString("bml_order_id"));
    setResponseData(FN_BML_STATUS_CODE,   rs.getString(FN_BML_STATUS_CODE));
  }

  public void setBmlRequestType()
  {
    bmlRequest = BR_CAPTURE;
  }

  public void setTranData(ResultSet rs) throws Exception
  {
    bmlReqOrderId = rs.getString("bml_order_id");
    amount        = rs.getBigDecimal(FN_BML_AMOUNT).setScale(2);
    firstCapFlag  = "Y".equals((""+rs.getString("first_cap_flag")).toUpperCase());
    autoCapFlag   = "Y".equals((""+rs.getString("auto_cap_flag")).toUpperCase());
    setResponseData(rs);
  }
}