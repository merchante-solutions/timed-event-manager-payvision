package com.mes.api.bml;

public abstract class BmlAuthRefTran extends BmlBaseTran
{
  protected BmlAuthorizeTran authTran;

  public BmlAuthRefTran(String tranId)
  {
    super(tranId);
  }

  public boolean hasAuthRef()
  {
    return true;
  }

  public boolean loadAuthTran()
  {
    if (authTran == null)
    {
      authTran = BmlDb.lookupRefAuth(this);
    }
    return authTran != null;
  }

  public boolean hasCapRef()
  {
    return false;
  }

  public BmlAuthorizeTran getAuthTran()
  {
    return authTran;
  }
}