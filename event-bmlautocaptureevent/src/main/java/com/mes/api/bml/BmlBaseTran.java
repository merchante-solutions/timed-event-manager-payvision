package com.mes.api.bml;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.api.TridentApiConstants;
import com.mes.api.TridentApiTransaction;
import com.mes.database.SleepyCatManager;
import com.mes.mvc.ParameterEncoder;
import com.mes.support.HttpHelper;
import com.mes.support.MesEncryption;

public abstract class BmlBaseTran extends TridentApiTransaction
{
  //
  // if you do not specify the serial version ID then Java will default
  // to a UID based on the fields in the class (see <jdk>/bin/serialver).  
  // failure to specify a static version will cause a mismatch exception 
  // if the object definition is changed.
  //
  // NOTE: it is necessary to specify a static serialVersionUID for all
  //       child classes.
  //
  static final long serialVersionUID    = TridentApiConstants.ApiVersionId;

  static Logger log = Logger.getLogger(BmlBaseTran.class);

  public static TridentApiConstants tac = new TridentApiConstants();

  // bill me later request types (extends bml tran type)
  public static final String  BR_LOOKUP           = "lookup";
  public static final String  BR_AUTHENTICATE     = "authenticate";
  public static final String  BR_AUTHORIZE        = "authorize";
  public static final String  BR_SALE             = "sale";
  public static final String  BR_CAPTURE          = "capture";
  public static final String  BR_REAUTHORIZE      = "reauthorize";
  public static final String  BR_REFUND           = "refund";
  public static final String  BR_INVALID          = "invalid";

  // request data

  // tpg api request field names (received from merchant)
  public static final String  FN_BML_REQUEST      = "bml_request";
  public static final String  FN_BML_REQ_ORDER_ID = "order_id";
  public static final String  FN_CENTINEL_URL_OVERRIDE  
                                                  = "centinel_url_override";
  public static final String  FN_REF_TRAN_ID      = "transaction_id";


  protected String  bmlRequest                    = BR_INVALID;
  protected String  bmlReqOrderId                 = null;
  protected String  refTranId                     = null;

  // response data

  // tpg api response field names (sent to merchant)
  public static final String  FN_BML_TRAN_ID      = "bml_tran_id";
  public static final String  FN_BML_RESP_ORDER_ID
                                                  = "order_id";
  public static final String  FN_BML_ERROR_NUM    = "error_num";
  public static final String  FN_BML_ERROR_DESC   = "error_desc";
  public static final String  FN_BML_REASON_CODE  = "reason_code";
  public static final String  FN_BML_REASON_DESC  = "reason_desc";
  public static final String  FN_BML_RESP_ACCT_NUM
                                                  = "account_num";

  protected Map responseData                      = new HashMap();

  protected Date requestDate                      = null;

  public BmlBaseTran(String tranId)
  {
    super(tranId);
  }

  /**
   * Request field accessors
   */

  public void setBmlRequest(String bmlRequest)
  {
    this.bmlRequest = bmlRequest;
  }
  public String getBmlRequest()
  {
    return bmlRequest;
  }

  public String getBmlReqOrderId()
  {
    return bmlReqOrderId;
  }

  public String getRefTranId()
  {
    return refTranId;
  }

  /**
   * Property loading from http request
   */
  public void setProperties(HttpServletRequest request)
  {
    ProfileId   = HttpHelper.getString(request,tac.FN_TID,null);
    TranType    = HttpHelper.getString(request,tac.FN_TRAN_TYPE,TT_INVALID);
    refTranId   = HttpHelper.getString(request,FN_REF_TRAN_ID,null);
    requestDate = new Date(Calendar.getInstance().getTime().getTime());
    bmlRequest  = HttpHelper.getString(request,FN_BML_REQUEST,BR_INVALID);
    String urlOverride = HttpHelper.getString(request,FN_CENTINEL_URL_OVERRIDE,null);
    if (urlOverride != null)
    {
      CardinalCentinelUrl = urlOverride;
    }
    //bmlReqOrderId = HttpHelper.getString(request,FN_BML_REQ_ORDER_ID,null);
    // this is now fetched if possible using the reference tran id
    bmlReqOrderId = getBmlOrderId(refTranId);
  }

  /**
   * Attempts to lookup a BML order id using a reference tran id.
   */
  private String getBmlOrderId(String refTranId)
  {
    String orderId = null;
    try
    {
      if (refTranId != null)
      {
        String refTranType = getRefTranType();
        if (refTranType != null)
        {
          log.debug("looking for order id entry with id " + refTranId);
          // first look in local entry storage
          SleepyCatManager mgr = SleepyCatManager.getInstance(
            TridentApiConstants.getApiDatabasePath());
          Map entryMap = 
            mgr.getStoredMap(TridentApiConstants.API_TABLE_BML_ENTRIES);
          BmlOrderIdEntry entry = (BmlOrderIdEntry)entryMap.get(refTranId);

          // not found locally, look to oracle
          if (entry == null)
          {
            log.debug("no local copy, looking to oracle...");
            BmlDb db = new BmlDb();
            entry = db.lookupEntryByTranId(refTranId);
          }

          // no entry found locally or in oracle, give up
          if (entry == null)
          {
            log.debug("no entry found.");
          }
          else
          {
            // HACK: new tran id scheme causes duplicate tran ids on auths
            // and first capture, so need to allow subsequent captures to use
            // first capture order id entry (which has replaced the auth 
            // tran id in the sleepy cat table, no tran id duplicates allowed 
            // there)
            //
            // authents   require     lookup entries
            // authors    require     lookup entries
            // sales      require     lookup entries
            // captures   require     auth or capture entries
            // reauths    require     auth or capture entries
            // refunds    require     capture entries
            if ( ( entry.isLookup() && 
                   ( BR_AUTHORIZE.equals(bmlRequest) ||
                     BR_AUTHENTICATE.equals(bmlRequest) ||
                     BR_SALE.equals(bmlRequest) ) ) ||
                 ( ( entry.isAuthorize() || entry.isCapture() ) && 
                   ( BR_CAPTURE.equals(bmlRequest) || 
                     BR_REAUTHORIZE.equals(bmlRequest) ) ) ||
                 ( entry.isCapture() && BR_REFUND.equals(bmlRequest) ) )
            {
              log.debug("found order entry " + entry);
              orderId = entry.getOrderId();
            }
            else
            {
              log.debug("order entry tran type '" + entry.getTranType()
                + "' does not match request type '" + bmlRequest + "'");
            }
          }
        }
        else
        {
          log.warn("Reference tran id " + refTranId + " in request"
            + " but no reference tran type specified");
        }
      }
    }
    catch (Exception e)
    {
      log.error("Error getBmlOrderId(" + refTranId + "): " + e);
      e.printStackTrace();
    }
    return orderId;
  }

  /**
   * Returns what tran type the tran id should reference.  Null by default,
   * classes that require a reference tran id to another transaction should
   * return the tran type as defined by BmlOrderIdEntry.
   */
  public String getRefTranType()
  {
    return null;
  }

  /**
   * Hooks for the auth/cap ref variants
   */

  public boolean hasAuthRef()
  {
    return false;
  }

  public boolean loadAuthTran()
  {
    return false;
  }

  public boolean hasCapRef()
  {
    return false;
  }

  public boolean loadCapTran()
  {
    return false;
  }

  /**
   * Abstract method to be implemented in child classes for specific 
   * validations.
   */
  public abstract boolean isBmlValid();

  /**
   * Generic validations happen here.  Tran-specific validation occurs in
   * child class isBmlValid() methods.
   */

  public boolean isValid()
  {
    setError(tac.ER_NONE);

    // validate tid/profile
    if (ProfileId == null || Profile == null)
    {
      setError(tac.ER_INVALID_PROFILE);
    }
    else if (!Profile.isBmlEnabled())
    {
      setError(tac.ER_BML_NOT_ENABLED);
    }
    else if (hasAuthRef() && !loadAuthTran())
    {
      log.debug("Failed to load auth tran");
      setError(tac.ER_BML_AUTH_NOT_FOUND);
    }
    else if (hasCapRef() && !loadCapTran())
    {
      log.debug("Failed to load capture tran");
      setError(tac.ER_BML_CAP_NOT_FOUND);
    }
    // authorizations may or may not require a tran id, 
    // let the auth tran validate this
    else if (isBlank(refTranId) && getRefTranType() != null
      && !BR_AUTHORIZE.equals(bmlRequest) && !BR_SALE.equals(bmlRequest))
    {
      setError(tac.ER_BML_REF_TRAN_ID_REQUIRED);
    }
    else if (isBlank(bmlReqOrderId) && getRefTranType() != null
      && !BR_AUTHORIZE.equals(bmlRequest) && !BR_SALE.equals(bmlRequest))
    {
      setError(tac.ER_BML_INVALID_REF_TRAN_ID);
    }

    return !hasError() && isBmlValid();
  }

  /**
   * Response data is stored as name/value pairs in a map retrieved
   * from the Centinel response object.
   */

  public Map getResponseData()
  {
    return responseData;
  }
  public String getResponseValue(String name)
  {
    return (String)responseData.get(name);
  }
  public void clearResponseData()
  {
    responseData.clear();
  }
  public void setResponseData(String name, String value)
  {
    responseData.put(name,value);
  }

  /**
   * Encodes the TPG tran id, msg id and msg text along with any name value
   * pairs present in the response data map.  Returns a string suitable
   * for http posting (URLEncoded name value pairs).  This is initially
   * used to add data to redirects that need to post data (such as returning
   * from BML ACSUrl).
   */
  public String encodeResponse(String tranId, String msgId, String msgText)
  {
    try
    {
      ParameterEncoder parms = new ParameterEncoder();

      // standard tpg response fields
      parms.add(tac.FN_TRAN_ID,tranId);
      parms.add(tac.FN_ERROR_CODE,msgId);
      parms.add(tac.FN_AUTH_RESP_TEXT,msgText);
      
      // request-specific response data
      for (Iterator i = responseData.entrySet().iterator(); i.hasNext();)
      {
        Map.Entry e = (Map.Entry)i.next();
        String name = (String)e.getKey();
        // HACK: don't return order_id or bml_tran_id anymore
        if (FN_BML_RESP_ORDER_ID.equals(name) || FN_BML_TRAN_ID.equals(name))
        {
          continue;
        }
        String value = (String)e.getValue();
        if (value == null || value.length() == 0)
        {
          continue;
        }
        parms.add(name,value);
      }
      return parms.encode();
    }
    catch (Exception e)
    {
      log.error("Error encoding response: " + e);
    }
    return "Error encoding response";
  }

  /**
   * Encodes response, fetches error code, desc.
   */

  public String encodeResponse()
  {
    String tranId = ApiTranId;
    String msgId = getErrorCode();
    String msgText = getErrorDesc();
    if (msgText == null || msgText.equals(""))
    {
      msgText = tac.getErrorDesc(msgId);
    }
    if (msgText == null || msgText.equals(""))
    {
      msgText = "OK";
    }
    return encodeResponse(ApiTranId,msgId,msgText);
  }

  /**
   * Response accessors
   */

  public String getBmlRespOrderId()
  {
    return getResponseValue(FN_BML_RESP_ORDER_ID);
  }

  public String getBmlTranId()
  {
    return getResponseValue(FN_BML_TRAN_ID);
  }

  public String getBmlErrorNum()
  {
    return getResponseValue(FN_BML_ERROR_NUM);
  }
  
  public String getBmlErrorDesc()
  {
    return getResponseValue(FN_BML_ERROR_DESC);
  }

  public String getBmlReasonCode()
  {
    return getResponseValue(FN_BML_REASON_CODE);
  }

  public String getBmlReasonDesc()
  {
    return getResponseValue(FN_BML_REASON_DESC);
  }

  /**
   * DB loading
   */

  public void setRecId(long recId)
  {
    RecId = recId;
  }

  public Date getRequestDate()
  {
    return requestDate;
  }

  public abstract void setBmlRequestType();

  public abstract void setTranData(ResultSet rs) throws Exception;

  private boolean resultColumnExists(ResultSet rs, String name) 
    throws Exception
  {
    ResultSetMetaData md = rs.getMetaData();
    int column = md.getColumnCount();
    while (column > 0)
    {
      if (md.getColumnName(column).toLowerCase().equals(name))
      {
        log.debug("result column '" + name + "' found.");
        return true;
      }
      --column;
    }
    log.debug("result column '" + name + "' NOT found.");
    return false;
  }

  private String decode(String encodedNum) throws Exception
  {
    return MesEncryption.getClient().decrypt(encodedNum);
  }

  public void setData(ResultSet rs) throws Exception
  {
    // these are stored in TridentApiTransaction data
    TranType      = TT_BML_REQUEST;
    ProfileId     = rs.getString(tac.FN_TID);
    RecId         = rs.getLong("rec_id");
    ServerName    = rs.getString("server_name");

    requestDate      = new Date(rs.getTimestamp("request_ts").getTime());

    if (resultColumnExists(rs,"host_url"))
    {
      String hostUrl = rs.getString("host_url");
      if (hostUrl != null)
      {
        setCardinalCentinelUrl(hostUrl);
      }
    }

    // if encoded account numb is present in result, decode it
    // and store it as response data, but if not look for
    // untruncated account num to store
    String encodedNum = null;    
    if (resultColumnExists(rs,"account_num_enc"))
    {
      encodedNum = rs.getString("account_num_enc");
    }
    if (encodedNum != null)
    {
      setResponseData(FN_BML_RESP_ACCT_NUM,decode(encodedNum));
    }
    else
    {
      String accountNum = null;
      if (resultColumnExists(rs,"account_num"))
      {
        accountNum = rs.getString("account_num");
      }
      if (accountNum != null)
      {
        setResponseData(FN_BML_RESP_ACCT_NUM,accountNum);
      }
    }

    setErrorCode(rs.getString("tpg_msg_id"));
    setErrorDesc(rs.getString("tpg_msg"));
    setBmlRequestType();
    setTranData(rs);
  }
}