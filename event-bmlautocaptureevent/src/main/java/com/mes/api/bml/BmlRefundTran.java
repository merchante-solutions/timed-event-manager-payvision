package com.mes.api.bml;

import java.math.BigDecimal;
import java.sql.ResultSet;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.api.TridentApiConstants;
import com.mes.support.HttpHelper;

public class BmlRefundTran extends BmlCapRefTran
{
  //
  // if you do not specify the serial version ID then Java will default
  // to a UID based on the fields in the class (see <jdk>/bin/serialver).  
  // failure to specify a static version will cause a mismatch exception 
  // if the object definition is changed.
  //
  // NOTE: it is necessary to specify a static serialVersionUID for all
  //       child classes.
  //
  static final long serialVersionUID    = TridentApiConstants.ApiVersionId;

  static Logger log = Logger.getLogger(BmlRefundTran.class);

  // request data

  // tpg api request field names (received from merchant)
  public static final String  FN_BML_AMOUNT           = "amount";
  public static final String  FN_BML_RECON_ID         = "recon_id";

  protected BigDecimal  amount              = new BigDecimal(0).setScale(2);
  protected int         reconId;

  // response data

  // tpg api response field names (sent to merchant)
  public static final String  FN_BML_STATUS_CODE      = "status_code";

  public BmlRefundTran(String tranId, int reconId)
  {
    super(tranId);
    this.reconId = reconId;
  }

  public void setProperties(HttpServletRequest request)
  {
    super.setProperties(request);
    amount        = new BigDecimal(HttpHelper.getString(request,FN_BML_AMOUNT,"0")).setScale(2);
  }

  public BigDecimal getAmount()
  {
    return amount;
  }

  public int getReconId()
  {
    return reconId;
  }

  /**
   * Refund requests must refer to a capture transaction in order to load
   * the capture response order id.
   */
  public String getRefTranType()
  {
    return BmlOrderIdEntry.TT_CAPTURE;
  }

  /**
   * Data validation
   */

  public boolean isBmlValid()
  {
    if (amount.doubleValue() == 0)
    {
      setError(tac.ER_BML_AMOUNT_REQUIRED);
    }

    return !hasError();
  }

  /**
   * Response accessors
   */

  public String getStatusCode()
  {
    return getResponseValue(FN_BML_STATUS_CODE);
  }

  /**
   * DB loading
   */

  public void setBmlRequestType()
  {
    bmlRequest = BR_REFUND;
  }

  public void setTranData(ResultSet rs) throws Exception
  {
    bmlReqOrderId = rs.getString("bml_order_id");
    amount        = rs.getBigDecimal(FN_BML_AMOUNT).setScale(2);
  }
}