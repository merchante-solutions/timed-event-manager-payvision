/*@lineinfo:filename=NotifyNewAccounts*//*@lineinfo:user-code*//*@lineinfo:1^1*/package com.mes.startup;

import java.sql.ResultSet;
import java.util.Vector;
import com.mes.constants.MesEmails;
import com.mes.net.MailMessage;
import sqlj.runtime.ResultSetIterator;

public class NotifyNewAccounts extends EventBase
{
  public  Vector accts  = new Vector();
  
  public boolean execute()
  {
    boolean           result = true;
    ResultSetIterator it = null;
    ResultSet         rs = null;
    try
    {
      connect();
      
      String procBanks = ( getEventArg(0) == null ? "3941" : getEventArg(0) );
      
      // see if there are any items to notify on
      int recCount = 0;
      /*@lineinfo:generated-code*//*@lineinfo:27^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(1)
//          
//          from    mif_new_accounts
//          where   process_sequence = 0
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(1)\n         \n        from    mif_new_accounts\n        where   process_sequence = 0";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.NotifyNewAccounts",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:33^7*/
      
      long processSequence = 0L;
      if( recCount > 0 )
      {
        /*@lineinfo:generated-code*//*@lineinfo:38^9*/

//  ************************************************************
//  #sql [Ctx] { select  mas_sequence.nextval
//            
//            from    dual
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  mas_sequence.nextval\n           \n          from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.startup.NotifyNewAccounts",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   processSequence = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:43^9*/
        
        /*@lineinfo:generated-code*//*@lineinfo:45^9*/

//  ************************************************************
//  #sql [Ctx] { update  mif_new_accounts
//            set     process_sequence = :processSequence
//            where   process_sequence = 0
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  mif_new_accounts\n          set     process_sequence =  :1 \n          where   process_sequence = 0";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.startup.NotifyNewAccounts",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,processSequence);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:50^9*/
        
        /*@lineinfo:generated-code*//*@lineinfo:52^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  lpad(nvl(mr.merc_cntrl_number,0),11,' ')    as control_number,
//                    lpad(nvl(app.appsrctype_code,'N/A'),4,' ')  as app_type,
//                    mf.merchant_number                          as merchant_number,
//                    mf.dba_name                                 as dba_name,
//                    mf.bank_number                              as bank_number,
//                    mf.group_2_association                      as portfolio,
//                    mf.dmdsnum                                  as discover_number
//            from    mif_new_accounts mna,
//                    mif mf,
//                    merchant mr,
//                    application app
//            where   mna.process_sequence = :processSequence
//                    and mna.merchant_number = mf.merchant_number
//                    and mna.merchant_number = mr.merch_number(+)
//                    and mr.app_seq_num = app.app_seq_num(+)                
//            order by mf.bank_number, mf.merchant_number                  
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  lpad(nvl(mr.merc_cntrl_number,0),11,' ')    as control_number,\n                  lpad(nvl(app.appsrctype_code,'N/A'),4,' ')  as app_type,\n                  mf.merchant_number                          as merchant_number,\n                  mf.dba_name                                 as dba_name,\n                  mf.bank_number                              as bank_number,\n                  mf.group_2_association                      as portfolio,\n                  mf.dmdsnum                                  as discover_number\n          from    mif_new_accounts mna,\n                  mif mf,\n                  merchant mr,\n                  application app\n          where   mna.process_sequence =  :1 \n                  and mna.merchant_number = mf.merchant_number\n                  and mna.merchant_number = mr.merch_number(+)\n                  and mr.app_seq_num = app.app_seq_num(+)                \n          order by mf.bank_number, mf.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.startup.NotifyNewAccounts",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,processSequence);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.startup.NotifyNewAccounts",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:70^9*/
        
        rs = it.getResultSet();
        
        StringBuffer msgBody = new StringBuffer("New Accounts Uploaded:\n\n");
        msgBody.append("Control Num Type  Merchant #     DBA Name               Bank\n");
        msgBody.append("----------- ---- ------------ ------------------------- ----\n");
        while( rs.next() )
        {
          msgBody.append(rs.getString("control_number"));
          msgBody.append(" ");
          msgBody.append(rs.getString("app_type"));
          msgBody.append(" ");
          msgBody.append(rs.getString("merchant_number"));
          msgBody.append(" ");
          msgBody.append(rs.getString("dba_name"));
          msgBody.append(" ");
          msgBody.append(rs.getString("bank_number"));
          msgBody.append("\n");
          
          // add account to cleanup list
          accts.add(new AccountCleanup(rs));
        }
        
        rs.close();
        it.close();
        
        MailMessage msg = new MailMessage();
        msg.setAddresses(MesEmails.MSG_ADDRS_NEW_ACCOUNT_NOTIFY); // 167
        msg.setSubject("MBS Accounts Loaded: " + com.mes.support.DateTimeFormatter.getCurDateTimeString());
        msg.setText(msgBody.toString());
        msg.send();
        
        // clean up
        postAccountCleanup();
      }
      
    }
    catch(Exception e)
    {
      logEntry("execute()", e.toString());
      result = false;
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    
    return( result );
  }
  
  private void postAccountCleanup()
  {
    try
    {
      for( int i = 0; i < accts.size(); ++i )
      {
        AccountCleanup ac = (AccountCleanup)accts.elementAt(i);
        
        if( 601172000000000L <= ac.discoverNumber && 601172999999999L >= ac.discoverNumber )
        {
          /*@lineinfo:generated-code*//*@lineinfo:133^11*/

//  ************************************************************
//  #sql [Ctx] { call convert_discover_map(:ac.merchantNumber, :ac.discoverNumber)
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "BEGIN convert_discover_map( :1 ,  :2 )\n          \n; END;";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.startup.NotifyNewAccounts",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,ac.merchantNumber);
   __sJT_st.setLong(2,ac.discoverNumber);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:136^11*/
        }
        else
        {
          /*@lineinfo:generated-code*//*@lineinfo:140^11*/

//  ************************************************************
//  #sql [Ctx] { call convert_discover_map(:ac.merchantNumber)
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "BEGIN convert_discover_map( :1 )\n          \n; END;";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.startup.NotifyNewAccounts",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,ac.merchantNumber);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:143^11*/
        }
        
        // set up amex processing for google
        if( ac.bankNumber == 3941 && ac.portfolio == 400137 ) // google hierarchy
        {
          // convert to amex processing at 2.89%
          /*@lineinfo:generated-code*//*@lineinfo:150^11*/

//  ************************************************************
//  #sql [Ctx] { call convert_amex_merchant(:ac.merchantNumber, 2.89, '1046000048')
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "BEGIN convert_amex_merchant( :1 , 2.89, '1046000048')\n          \n; END;";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.startup.NotifyNewAccounts",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,ac.merchantNumber);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:153^11*/
          
          // make sure amex fuding is 0 days delay
          /*@lineinfo:generated-code*//*@lineinfo:156^11*/

//  ************************************************************
//  #sql [Ctx] { update  mif
//              set     mes_amex_suspended_days = 0
//              where   merchant_number = :ac.merchantNumber
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  mif\n            set     mes_amex_suspended_days = 0\n            where   merchant_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"7com.mes.startup.NotifyNewAccounts",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,ac.merchantNumber);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:161^11*/
        }
      }
    }
    catch(Exception e)
    {
      logEntry("postAccountCleanup()", e.toString());
    }
  }
  
  public class AccountCleanup
  {
    public long merchantNumber  = 0L;
    public long discoverNumber  = 0L;
    public int  bankNumber      = 0;
    public int  portfolio       = 0;
    
    public AccountCleanup( ResultSet rs )
    {
      try
      {
        merchantNumber  = rs.getLong("merchant_number");
        bankNumber      = rs.getInt("bank_number");
        portfolio       = rs.getInt("portfolio");
        discoverNumber  = rs.getLong("discover_number");
      }
      catch(Exception e)
      {
        logEntry("AccountCleanup()", e.toString());
      }
    }
  }
  
  public static void main(String[] args)
  {
    com.mes.database.SQLJConnectionBase.initStandalone();
    
    NotifyNewAccounts grunt = new NotifyNewAccounts();
    
    try
    {
      grunt.setEventArgs("3941|3942|3943|3858");
      
      grunt.execute();
    }
    catch(Exception e)
    {
      System.out.println("main(): " + e.toString());
    }
  }
}/*@lineinfo:generated-code*/