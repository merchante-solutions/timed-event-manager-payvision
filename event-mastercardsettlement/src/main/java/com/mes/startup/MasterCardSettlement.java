/*@lineinfo:filename=MasterCardSettlement*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/startup/MasterCardSettlement.sqlj $

  Description:

  Last Modified By   : $Author: vbannikov $
  Last Modified Date : $Date: 2015-06-22 09:43:59 -0700 (Mon, 22 Jun 2015) $
  Version            : $Revision: 23700 $

  Change History:
     See SVN database

  Copyright (C) 2000-2010,2011 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.Calendar;
import com.mes.config.MesDefaults;
import com.mes.constants.MesEmails;
import com.mes.settlement.SettlementRecord;
import sqlj.runtime.ResultSetIterator;

public class MasterCardSettlement extends MasterCardFileBase
{
  public MasterCardSettlement( )
  {
    PropertiesFilename          = "mc-settlement.properties";

    CardTypeTable               = "mc_settlement";
    CardTypeTableActivity       = "mc_settlement_activity";
    CardTypePostTotals          = "mc_settle";
    CardTypeFileDesc            = "MC Settlement";
    DkOutgoingHost              = MesDefaults.DK_MC_EP_HOST;
    DkOutgoingUser              = MesDefaults.DK_MC_EP_USER;
    DkOutgoingPassword          = MesDefaults.DK_MC_EP_PASSWORD;
    DkOutgoingPath              = MesDefaults.DK_MC_EP_OUTGOING_PATH;
    DkOutgoingUseBinary         = true;
    DkOutgoingSendFlagFile      = true;
    SettlementAddrsNotify       = MesEmails.MSG_ADDRS_OUTGOING_MC_NOTIFY;
    SettlementAddrsFailure      = MesEmails.MSG_ADDRS_OUTGOING_MC_FAILURE;
    SettlementRecMT             = SettlementRecord.MT_FIRST_PRESENTMENT;
  }
  
  protected boolean processTransactions( )
  {
    int                       bankNumber        = Integer.parseInt( getEventArg(0) );
    String                    bankNumberClause  = (bankNumber == 9999) ? "" : " and mc.bank_number = " + bankNumber + " ";
    Date                      cpd               = null;
    ResultSetIterator         it                = null;
    int                       recCount          = 0;
    long                      workFileId        = 0L;
    String                    workFilename      = null;

    try
    {
      if ( TestFilename == null )
      {
        FileTimestamp   = new Timestamp(Calendar.getInstance().getTime().getTime());

        cpd             = getSettlementDate();

        workFilename    = generateFilename(( "R".equals(ActionCode) ? "mc_reversals" : "mc_settle" )    // base name
                                          + String.valueOf(bankNumber),                                 // bank number
                                          ".ipm");                                                      // file extension
        workFileId      = loadFilenameToLoadFileId(workFilename);

        if( ActionCode == null )
        {
          /*@lineinfo:generated-code*//*@lineinfo:81^11*/

//  ************************************************************
//  #sql [Ctx] { update  mc_settlement   mc
//              set     mc.output_filename  = :workFilename,
//                      mc.output_file_id   = :workFileId,
//                      mc.settlement_date  = :cpd
//              where   mc.output_file_id = 0
//                      and mc.output_filename is null
//                      and mc.test_flag = 'N'
//                      :bankNumberClause
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   java.lang.StringBuffer __sjT_sb = new java.lang.StringBuffer();
   __sjT_sb.append("update  mc_settlement   mc\n            set     mc.output_filename  =  ? ,\n                    mc.output_file_id   =  ? ,\n                    mc.settlement_date  =  ? \n            where   mc.output_file_id = 0\n                    and mc.output_filename is null\n                    and mc.test_flag = 'N'\n                     ");
   __sjT_sb.append(bankNumberClause);
   String __sjT_sql = __sjT_sb.toString();
   String __sjT_tag = "0com.mes.startup.MasterCardSettlement:" + __sjT_sql;
   String theSqlTS = __sjT_sql;
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,__sjT_tag,theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,workFilename);
   __sJT_st.setLong(2,workFileId);
   __sJT_st.setDate(3,cpd);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:91^11*/
        }
        else
        {
          /*@lineinfo:generated-code*//*@lineinfo:95^11*/

//  ************************************************************
//  #sql [Ctx] { update  mc_settlement_activity   mca
//              set     mca.load_filename  = :workFilename,
//                      mca.load_file_id   = :workFileId,
//                      mca.settlement_date  = :cpd
//              where   mca.load_file_id = 0
//                      and mca.load_filename is null
//                      and mca.action_code = :ActionCode
//                      and exists
//                      (
//                        select  mc.rec_id
//                        from    mc_settlement mc
//                        where   mc.rec_id = mca.rec_id
//                                :bankNumberClause
//                      )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   java.lang.StringBuffer __sjT_sb = new java.lang.StringBuffer();
   __sjT_sb.append("update  mc_settlement_activity   mca\n            set     mca.load_filename  =  ? ,\n                    mca.load_file_id   =  ? ,\n                    mca.settlement_date  =  ? \n            where   mca.load_file_id = 0\n                    and mca.load_filename is null\n                    and mca.action_code =  ? \n                    and exists\n                    (\n                      select  mc.rec_id\n                      from    mc_settlement mc\n                      where   mc.rec_id = mca.rec_id\n                               ");
   __sjT_sb.append(bankNumberClause);
   __sjT_sb.append(" \n                    )");
   String __sjT_sql = __sjT_sb.toString();
   String __sjT_tag = "1com.mes.startup.MasterCardSettlement:" + __sjT_sql;
   String theSqlTS = __sjT_sql;
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,__sjT_tag,theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,workFilename);
   __sJT_st.setLong(2,workFileId);
   __sJT_st.setDate(3,cpd);
   __sJT_st.setString(4,ActionCode);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:111^11*/
        }
        // only process if there were records updated 
        recCount = Ctx.getExecutionContext().getUpdateCount();
      }
      else    // re-build a previous file
      {
        FileTimestamp   = new Timestamp( getFileDate(TestFilename).getTime() );
        workFilename    = TestFilename;
        workFileId      = loadFilenameToLoadFileId(workFilename);
        recCount        = 1;   // always re-build
      }

      if ( recCount > 0 )
      {
        // select the transactions to process
        if( ActionCode == null )
        {
          /*@lineinfo:generated-code*//*@lineinfo:129^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  mc.*
//              from    mc_settlement             mc
//              where   mc.output_file_id = :workFileId
//                      and mc.test_flag = 'N'
//              order by mc.batch_id, mc.batch_record_id
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mc.*\n            from    mc_settlement             mc\n            where   mc.output_file_id =  :1 \n                    and mc.test_flag = 'N'\n            order by mc.batch_id, mc.batch_record_id";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.startup.MasterCardSettlement",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,workFileId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.startup.MasterCardSettlement",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:136^11*/
        }
        else
        {
 {
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
	//@formatter:off
   String theSqlTS =  " SELECT mc.*,mca.reproc external_reject"
   		+ " FROM mc_settlement_activity mca, mc_settlement mc "
   		+ " WHERE mca.load_file_id = :1 "
   		+ "   AND mca.action_code    = :2 "
   		+ "   AND mc.rec_id          = mca.rec_id "
   		+ "ORDER BY mc.batch_id, mc.batch_record_id";
   //@formatter:on
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.startup.MasterCardSettlement",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,workFileId);
   __sJT_st.setString(2,ActionCode);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.startup.MasterCardSettlement",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:149^11*/
        }

        FileReferenceId = buildFileReferenceId( workFilename, ("R".equals(ActionCode) ? "1" : "0") );
        // full file reversal, change filename
        if ( FileReversalInd != null )
        {
          int fileBankNumber  = getFileBankNumber(workFilename);
          workFilename = generateFilename("mc_rsettle" + String.valueOf(fileBankNumber), ".ipm");
        }
        handleSelectedRecords( it, workFilename,workFileId );
       
      }
    }
    catch( Exception e )
    {
      logEvent(this.getClass().getName(), "processTransactions()", e.toString());
      logEntry("processTransactions()", e.toString());
    }
    finally
    {
      try{ it.close(); } catch(Exception e) {}
    }
    return( true );
  }

  public static void main( String[] args )
  {
    MasterCardSettlement              test          = null;
    
    try
    { 
        if (args.length > 0 && args[0].equals("testproperties")) {
            EventBase.printKeyListStatus(new String[]{
                    MesDefaults.DK_MC_EP_HOST,
                    MesDefaults.DK_MC_EP_USER,
                    MesDefaults.DK_MC_EP_PASSWORD,
                    MesDefaults.DK_MC_EP_OUTGOING_PATH
            });
        }

      test = new MasterCardSettlement();
      test.executeCommandLine( args );
    }
    catch( Exception e )
    {
      log.debug(e.toString());
    }
    finally
    {
      try{ test.cleanUp(); } catch( Exception e ) {}
      Runtime.getRuntime().exit(0);
    }
  }
}/*@lineinfo:generated-code*/