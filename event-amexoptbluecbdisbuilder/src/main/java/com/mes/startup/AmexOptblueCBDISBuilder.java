package com.mes.startup;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import org.apache.log4j.Logger;
import com.mes.clearing.utils.ClearingConstants;
import com.mes.config.MesDefaults;
import com.mes.constants.MesEmails;
import com.mes.constants.MesFlatFiles;
import com.mes.constants.MesQueues;
import com.mes.flatfile.FlatFileRecord;
import com.mes.queues.QueueTools;
import com.mes.support.DateTimeFormatter;

public class AmexOptblueCBDISBuilder extends AmexFileBase {

	private static final long serialVersionUID = 1L;	
	static Logger log = Logger.getLogger(AmexOptblueCBDISBuilder.class);
	
	private String loadFilename = null; 
	private long fileBatchId = 0l; 
	
	/**
	 *  Initializes SFTP endpoints 
	 */
	public AmexOptblueCBDISBuilder() {
		
		// TODO Auto-generated constructor stub
		DkOutgoingHost = MesDefaults.DK_AMEX_OUTGOING_HOST;
		DkOutgoingUser = MesDefaults.DK_AMEX_OUTGOING_USER;
		DkOutgoingPassword = MesDefaults.DK_AMEX_OUTGOING_PASSWORD;
		DkOutgoingPath = MesDefaults.DK_AMEX_OPTB_OUTGOING_PATH;
		SettlementAddrsNotify = MesEmails.MSG_ADDRS_AMEX_CB_NOTIFY;
		SettlementAddrsFailure = MesEmails.MSG_ADDRS_AMEX_CB_NOTIFY;
		DkOutgoingUseBinary = false;
		DkOutgoingSendFlagFile = true;
	}

	
	/**
	 * Point of entry
	 * @return
	 */
	protected boolean processTransactions() {
		
		String said = "";
		BufferedWriter out = null;
		String outgoingFilename = null;
		
		try {
			connect( true );			
				
			// The loadFilename is internal and is used for marking records to process / interenally 
			if(loadFilename == null) {
				String filePrefix = DisputeResponseFilePrefixes.get(CBDIS);
				loadFilename = generateFilename(filePrefix);
			}
			
			if(fileBatchId == 0) {
				getFileBatchId();
			}
			
			for (int i = 0; i < EventArgsVector.size(); i++) {
				said = (String) EventArgsVector.elementAt(i);	            
			}			
			
			log.debug("processing transactions for loadFilename = " + loadFilename + ", fileBatchId = " + fileBatchId + ", SAID = " + said);
			
			markRecordsForProcessing(said);
			
			//outgoingFilename = generateResponseFileName();
					
			out = new BufferedWriter(new FileWriter(loadFilename, false));
			
			buildHeader(out, said);			
			int recCount = buildDetail(out);
			buildTrailer(out, said);
			
			out.flush();
			out.close();
			
			// Send file out iff there were records to send out.
			if(recCount > 0) {
				sendFile(loadFilename);
				createRecordForImaging();				
				moveCasesToCompletedQueue(said);
				
			} else {
				File cbdis = new File( loadFilename );
				cbdis.delete();
				
				log.info("CBDIS : No records hence file created with just Hdr and Trl deleted.");
			}
			
		} catch (IOException e) {
			logEvent( this.getClass().getName(), "processTransactions()", e.toString() );
			logEntry( "processTransactions()", e.toString() );
			
		} finally {
			try {
				if(out != null)
					out.close();
				
			} catch(IOException ioe) {
				logEvent( this.getClass().getName(), "processTransactions()", ioe.toString() );
				logEntry( "processTransactions()", ioe.toString() );
			}
	        
		}
		
		return true;
	}
	
	
	protected void buildHeader(BufferedWriter out, String said) {
		log.debug("CBDIS buildHeader");
		
		try {
			FlatFileRecord cbdisHdr = new FlatFileRecord(MesFlatFiles.DEF_TYPE_AMEX_OPTBLUE_CBDIS_HDR);
			
			cbdisHdr.setFieldData("rec_type", REC_TYPE_HDR);
			cbdisHdr.setFieldData("said", said);
			cbdisHdr.setFieldData("datatype", CBDIS);
			
			String filePrefix = DisputeResponseFilePrefixes.get(CBDIS);
			int prefixLength = filePrefix.length();
			java.util.Date originalFileCreationDate = 
					DateTimeFormatter.parseDate(loadFilename.substring((prefixLength+1),(prefixLength+1+6)),"MMddyy");
			
			cbdisHdr.setFieldData("ccyyddd", DateTimeFormatter.getFormattedDate(originalFileCreationDate, "yyyyDDD") ); // This has to be original file creation date even if it is a retransmission
			cbdisHdr.setFieldData("0hhmmss", "0" + DateTimeFormatter.getFormattedDate(Calendar.getInstance().getTime(), "HHmmss"));
			cbdisHdr.setFieldData("stars_fileseq_nb", loadFilename.substring((prefixLength+8),(prefixLength+11)));
			cbdisHdr.setFieldData("file_batch_id", fileBatchId);
						
			out.write(cbdisHdr.spew());
			out.newLine();
			
		} catch(IOException ioe) {
			logError( "buildHeader()", ioe.toString() );			
		}	
		 
	}
	
	protected int buildDetail(BufferedWriter out) {
    
		log.debug("CBDIS buildDetail");
		
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		
		int paramIndex = 1;
		int recCount = 0;
		
		try {
			FlatFileRecord cbdisDetail = new FlatFileRecord(MesFlatFiles.DEF_TYPE_AMEX_OPTBLUE_CBDIS_DTL);
			
			pStmt = con.prepareStatement( SQL_GET_CBDIS_RECORDS.toString() );
			pStmt.setString(paramIndex++, loadFilename);
			pStmt.setLong(paramIndex++, loadFilenameToLoadFileId( loadFilename ));
			
			rs = pStmt.executeQuery();
						
			while(rs.next()) {
				cbdisDetail.resetAllFields();				
				cbdisDetail.setAllFieldData(rs);
			
				out.write( cbdisDetail.spew() );
				out.newLine();
				
				recCount++;
			}	
			
		} catch(IOException ioe) {
			logError( "buildDetail()", ioe.toString() );
			
		} catch(SQLException sqe) {
			logError( "buildDetail()", sqe.toString() );
			
		}
		
		return recCount;
	}


	protected void buildTrailer(BufferedWriter out, String said) {
		log.debug("CBDIS buildTrailer");
		
		try {
			FlatFileRecord cbdisTrl = new FlatFileRecord(MesFlatFiles.DEF_TYPE_AMEX_OPTBLUE_CBDIS_TRL);
			
			cbdisTrl.setFieldData("rec_type", REC_TYPE_TRL);
			cbdisTrl.setFieldData("said", said);
			cbdisTrl.setFieldData("datatype", CBDIS);
			cbdisTrl.setFieldData("ccyyddd", DateTimeFormatter.getFormattedDate(Calendar.getInstance().getTime(), "yyyyDDD") ); // This has to be original file creation date even if it is a retransmission
			cbdisTrl.setFieldData("0hhmmss", "0" + DateTimeFormatter.getFormattedDate(Calendar.getInstance().getTime(), "HHmmss"));
			
			String filePrefix = DisputeResponseFilePrefixes.get(CBDIS);
			int prefixLength = filePrefix.length();			
			cbdisTrl.setFieldData("stars_fileseq_nb", loadFilename.substring((prefixLength+8),(prefixLength+11)));
									
			out.write(cbdisTrl.spew());
			out.newLine();
			
		} catch(IOException ioe) {
			logError( "buildTrailer()", ioe.toString() );			
		}	
	}

	protected void getFileBatchId() {
		PreparedStatement pStmt = null;
		ResultSet rs = null;
								
		try {					
			pStmt = con.prepareStatement( SQL_GET_CBDIS_BATCHID.toString() );			
			
			rs = pStmt.executeQuery();
			if(rs.next())
				fileBatchId = rs.getLong(1);
			
		} catch (java.sql.SQLException sqe) {
			
		} finally {
			try {
				if(rs != null) rs.close();
			} catch(Exception ignore) {}
			
			try {
				if(pStmt != null) pStmt.close();
			} catch(Exception ignore) {} 
		}		
		
	}
	
	protected int markRecordsForProcessing(String said) {
		
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		
		int paramIndex = 1;
		int count = 0;
		
		try {
			long loadFileId = loadFilenameToLoadFileId( loadFilename );
			
			pStmt = con.prepareStatement( SQL_MARK_OUTGOING_CBDIS_RECORDS.toString() );			
			pStmt.setString( paramIndex++, loadFilename );
			pStmt.setLong( paramIndex++, loadFileId );
			pStmt.setLong( paramIndex++, fileBatchId );
			pStmt.setString( paramIndex++, said );
			
			count = pStmt.executeUpdate();
			log.debug(count + "records marked for processing for CBDIS");
			
		} catch(SQLException sqe) {
			logError( "markRecordsForProcessing()", sqe.toString() );
			
		}	finally {
			try {
				if(rs != null) rs.close();
			} catch(Exception ignore) {}
			
			try {
				if(pStmt != null) pStmt.close();
			} catch(Exception ignore) {} 
		}		
		
		return count;
	}
	  
	
	
	/*
	 * Helper functions
	 */
	
	/**
	 * Files go out with a specific prefix
	 * MERCHESOBTST.CBDIS, MERCHESOBTST.RESPN
	 * @param fileType
	 * @return
	 */
	protected String generateResponseFileName(){    	
    	
    	String filePrefix = isTestMode ? TEST_PREFIX : PROD_PREFIX;    			
		
    	String fileName = filePrefix + CBDIS;
		
    	return fileName;
    }	
	
	
	private void createRecordForImaging() {
		
		PreparedStatement pStmt = null;
				
		int paramIndex = 1;
		int count = 0;
		
		try {			
			pStmt = con.prepareStatement( SQL_CREATE_IMGIN_PROCESS_ENTRY.toString() );			
			
			pStmt.setInt( paramIndex++, ClearingConstants.PROC_TYPE_CBDIS .getValue());
			pStmt.setString( paramIndex++, loadFilename );
			pStmt.setLong( paramIndex++, fileBatchId );
			
			count = pStmt.executeUpdate();
			log.debug(count + "record inserted in dispute_imgin_process");
			
		} catch(SQLException sqe) {
			logError( "createRecordForImaging()", sqe.toString() );
			
		}	finally {
			
			try {
				if(pStmt != null) pStmt.close();
			} catch(Exception ignore) {} 
		}	
		
	}
	
	private void moveCasesToCompletedQueue(String said) {
		
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		
		int paramIndex = 1;
		int colIndex = 1;
		int count = 0;
		
		try {
			long loadFileId = loadFilenameToLoadFileId( loadFilename );
			
			pStmt = con.prepareStatement( SQL_GET_OUTGOING_CBDIS_RECORDS.toString() );			
			pStmt.setString( paramIndex++, loadFilename );
			pStmt.setLong( paramIndex++, loadFileId );
			pStmt.setLong( paramIndex++, fileBatchId );
			pStmt.setString( paramIndex++, said );
			
			rs = pStmt.executeQuery();
			while( rs.next() ) {
				QueueTools.moveQueueItem(rs.getLong(colIndex), RESPOND_TO_AMEX_QUEUE, AMEX_COMPLETED_CHARGEBACK_QUEUE, null, "");
				count++;
			}
			log.debug(count + "records moved to Amex Completed Chargeback Queue");
			
		} catch(SQLException sqe) {
			logError( "moveCasesToCompletedQueue()", sqe.toString() );
			
		}	finally {
			try {
				if(rs != null) rs.close();
			} catch(Exception ignore) {}
			
			try {
				if(pStmt != null) pStmt.close();
			} catch(Exception ignore) {} 
		}		
		
	}
	
	public static void main(String args[]) {
		AmexOptblueCBDISBuilder disputeResponseGenerator = null;
		int idx = 0;
		
		try {
			if (args.length > 0 && args[idx].equals("printtestprops")) {
					EventBase.printKeyListStatus(new String[] { MesDefaults.DK_AMEX_HOST, MesDefaults.DK_AMEX_USER,
							MesDefaults.DK_AMEX_PASSWORD, MesDefaults.DK_AMEX_OUTGOING_PATH });				
			} else if (args.length > 0 && args[idx].equals("test")) {
				isTestMode = true;
				idx = 1;
			}

			disputeResponseGenerator = new AmexOptblueCBDISBuilder();
			disputeResponseGenerator.connect();
							
			if ("execute".equals(args[idx])) {
				disputeResponseGenerator.setEventArgs(args[++idx]);

				// Following 2 need to be set in case of retransmission
				if(args.length > ++idx)
					disputeResponseGenerator.loadFilename = args[idx];
				if(args.length > ++idx)
					disputeResponseGenerator.fileBatchId = Long.parseLong(args[idx]);
				
				disputeResponseGenerator.execute();
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				disputeResponseGenerator.cleanUp();
			} catch (Exception e) { }
						
		}
		
		Runtime.getRuntime().exit(0);
	}
	
	// Load Types
	private final static String	CBDIS = "CBDIS";
		
	// Record Types
	private final static String REC_TYPE_HEADER = "H";
	private final static String REC_TYPE_DETAIL = "D";
	private final static String REC_TYPE_TRAILER = "T";
			
	// Mes Defined Dispute File Prefixes
	private static final Map<String, String> DisputeResponseFilePrefixes = new HashMap<String, String>() {
		{
			put(CBDIS, "amex_optbcbdis" ); 	// Amex Optblue Outgoing CBDIS File	
		}
	};
	
	// Amex Defined File Prefixes
	private final static String PROD_PREFIX = "MERCHESOBPRD.";
	private final static String TEST_PREFIX = "MERCHESOBTST.";
	
	// Loading mode
	private static boolean isTestMode = false;
	
	// Record Types
	private final static String REC_TYPE_HDR = "H"; 
	private final static String REC_TYPE_DTL = "D";
	private final static String REC_TYPE_TRL = "T";
	
	// Queues
	private final static int RESPOND_TO_AMEX_QUEUE = 3006;
	private final static int Q_ITEM_TYPE = MesQueues.Q_ITEM_TYPE_CHARGEBACK;	
	private final static int AMEX_COMPLETED_CHARGEBACK_QUEUE = MesQueues.Q_CHARGEBACKS_COMPLETED;
	
	// Other constants
	private final static int DISPUTE_TEXT_AREA_LENGTH = 437;
		
	// ************** SQL Queries
	
	// ** CBDIS
	private static final StringBuffer SQL_MARK_OUTGOING_CBDIS_RECORDS = new StringBuffer("");
	private static final StringBuffer SQL_GET_CBDIS_BATCHID = new StringBuffer("");
	private static final StringBuffer SQL_GET_CBDIS_RECORDS = new StringBuffer("");
	private static final StringBuffer SQL_GET_OUTGOING_CBDIS_RECORDS = new StringBuffer("");
	
	private static final StringBuffer SQL_CREATE_IMGIN_PROCESS_ENTRY = new StringBuffer("");
	
	private static final String NL = " \n";
	
	static {		
		// ** CBDIS
		SQL_GET_CBDIS_BATCHID.append (
			"select amex_optb_cb_batchid_seq.nextval" + NL +
			"from dual" + NL);
		
		SQL_MARK_OUTGOING_CBDIS_RECORDS.append (
			"update network_chargeback_amex_optb ncao" + NL + 
			"set ncao.output_filename = ?, ncao.output_file_id = ?, ncao.batch_id = ? " + NL + 
			"where ncao.output_filename is null" + NL + 
			"and ncao.load_sec in (" + NL + 
			"	select q.id" + NL + 
			"	from q_data q" + NL +  
			"	where q.date_created > sysdate - 30" + NL +  
			"		and q.item_type = " + Q_ITEM_TYPE + NL + 
			"		and q.type = " + RESPOND_TO_AMEX_QUEUE + NL + 
			"		and q.source = 'amex'" + NL +
			" ) " + NL +
			"and ncao.said = ?"
		);
		
		SQL_GET_CBDIS_RECORDS.append(
				"select " + NL +
				"	 'D' as rec_type, " + NL +
				"	 fin_ind, " + NL +
				"	 se_numb, " + NL + 
				"    dukpt_decrypt_wrapper(card_number_enc) as cm_acct_numb," + NL + 
				"    current_case_number," + NL + 
				"    current_action_number," + NL + 
				"    previous_case_number," + NL + 
				"    previous_action_number," + NL + 
				"    resolution," + NL + 
				"    from_system," + NL + 
				"    rejects_to_system," + NL + 
				"    disputes_to_system," + NL + 
				"    to_char(date_of_adjustment, 'yyyymmdd') as date_of_adjustment," + NL + 
				"    to_char(date_of_charge, 'yyyymmdd') as date_of_charge," + NL +				 
				"    amex_id," + NL + 
				"    case_type," + NL + 
				"    loc_numb," + NL + 
				"    cb_reas_code," + NL + 
				"    case when cb_amount = 0 then ' ' " + NL +
				"		else replace( to_char( cb_amount,'S0000000000000.00' ), '+', ' ' ) end as cb_amount," + NL + 
				"    cb_adjustment_number," + NL + 
				"    cb_resolution_adj_number," + NL + 
				"    cb_reference_code," + NL + 
				"    case when billed_amount = 0 then ' ' " + NL +
				"		else replace( to_char( billed_amount,'S0000000000000.00' ), '+', ' ' ) end as billed_amount," + NL +				
				"    case when soc_amount = 0 or soc_amount = '' then ' ' " + NL +
				"		else replace( to_char( soc_amount,'S0000000000000.00' ), '+', ' ' ) end as soc_amount," + NL +								 
				"    soc_invoice_number," + NL + 
				"    roc_invoice_number," + NL + 
				"    foreign_amt," + NL + 
				"    currency," + NL + 
				"    supp_to_follow," + NL + 
				"    cm_name1," + NL + 
				"    cm_name2," + NL + 
				"    cm_addr1," + NL + 
				"    cm_addr2," + NL + 
				"    cm_city_state," + NL + 
				"    cm_zip," + NL + 
				"    cm_first_name_1," + NL + 
				"    cm_middle_name_1," + NL + 
				"    cm_last_name_1," + NL + 
				"    dukpt_decrypt_wrapper(cm_orig_acct_num) as cm_orig_acct_num," + NL + 
				"    cm_orig_name," + NL + 
				"    cm_orig_first_name," + NL + 
				"    cm_orig_middle_name," + NL + 
				"    cm_orig_last_name," + NL + 
				"    note1," + NL + 
				"    note2," + NL + 
				"    note3," + NL + 
				"    note4," + NL + 
				"    note5," + NL + 
				"    note6," + NL + 
				"    note7," + NL + 
				"    triumph_seq_no," + NL + 
				"    seller_id," + NL + 
				"    airline_tkt_num," + NL + 
				"    al_sequence_number," + NL + 
				"    folio_ref," + NL + 
				"    merch_order_num," + NL + 
				"    merch_order_date," + NL + 
				"    canc_num," + NL + 
				"    canc_date," + NL + 
				"    fincap_tracking_id," + NL + 
				"    fincap_file_seq_num," + NL + 
				"    fincap_batch_number," + NL + 
				"    fincap_batch_invoice_dt," + NL + 
				"    label1," + NL + 
				"    data1," + NL + 
				"    label2," + NL + 
				"    data2," + NL + 
				"    label3," + NL + 
				"    data3," + NL + 
				"    label4," + NL + 
				"    data4," + NL + 
				"    label5," + NL + 
				"    data5," + NL + 
				"    label6," + NL + 
				"    data6," + NL + 
				"    label7," + NL + 
				"    data7," + NL + 
				"    label8," + NL + 
				"    data8," + NL + 
				"    label9," + NL + 
				"    data9," + NL + 
				"    label10," + NL + 
				"    data10," + NL + 
				"    label11," + NL + 
				"    data11," + NL + 
				"    dukpt_decrypt_wrapper(label12) as label12," + NL + 
				"    data12, " + NL +
				"	 '01' as dispute_code, " + NL +
				"	 cb_adjustment_number as adj_number_area, " + NL +
				"	 replace( to_char( cb_amount,'S0000000000000.00' ), '+', ' ' ) as dispute_amount, " + NL +
				"	 case when length(qnotes.note) > " + DISPUTE_TEXT_AREA_LENGTH + " then substr(qnotes.note, 0, " + DISPUTE_TEXT_AREA_LENGTH + ") else  qnotes.note end as dispute_text_area " + NL +
				"from network_chargeback_amex_optb ncao, " + NL +
				"	( select  qd.id, qn.note, qn.seq" + NL + 
				"	  from q_data qd, q_notes qn, q_notes qn_max_seq" + NL + 
				"	  where qd.type = " + RESPOND_TO_AMEX_QUEUE + NL + 
				"			and qd.item_type = " + Q_ITEM_TYPE + NL + 
				"			and qn.id = qd.id" + NL + 
				"			and qn.item_type = qd.item_type" + NL + 
				"			and qn_max_seq.id = qn.id" + NL + 
				"			and qn_max_seq.item_type = qn.item_type" + NL + 
				"	  group by  qd.id, qn.note, qn.seq" + NL + 
				"	  having qn.seq = max(qn_max_seq.seq)" + NL + 
				" ) qnotes" + NL +  
				"where ncao.output_filename = ?" + NL + 
				"	and ncao.output_file_id = ?" + NL +
				"	and ncao.load_sec = qnotes.id (+) " + NL 			
			);
		
		SQL_CREATE_IMGIN_PROCESS_ENTRY.append(
			"insert into dispute_imgin_process(rec_id, process_type, load_filename, batch_id) " + NL +
			"values (mes.dispute_imgin_process_seq.nextval, ?, ?, ?)"
			);
		
		SQL_GET_OUTGOING_CBDIS_RECORDS.append (
			"select load_sec " + NL + 
			"from network_chargeback_amex_optb ncao, q_data qd " + NL + 
			"where ncao.output_filename = ? " + NL + 
			"and ncao.output_file_id = ? " + NL + 
			"and ncao.batch_id = ? " + NL + 
			"and ncao.said = ? " + NL +
			"and ncao.load_sec = qd.id " + NL + 
			"and qd.type = " + RESPOND_TO_AMEX_QUEUE + NL
			);		 

	}	
	
	
}
