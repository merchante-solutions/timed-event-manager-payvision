Dear Merchant: 

Attached, please find a PDF Chargeback Notification letter outlining details regarding your recent chargeback. We ask that you would please read it carefully and respond accordingly.

If you have any questions regarding our new chargeback letter generation format, or you have trouble opening this attachment, please call 1-888-288-2692. 

Thank You,

Merchant Services
Chargeback Department
Merchant e-Solutions