/*@lineinfo:filename=UATPSettlement*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/startup/UATPSettlement.sqlj $

  Description:

  Last Modified By   : $Author: vbannikov $
  Last Modified Date : $Date: 2015-06-22 09:43:59 -0700 (Mon, 22 Jun 2015) $
  Version            : $Revision: 23700 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Calendar;
import com.mes.config.MesDefaults;
import com.mes.constants.MesEmails;
import com.mes.constants.MesFlatFiles;
import com.mes.database.SQLJConnectionBase;
import com.mes.flatfile.FlatFileRecord;
import com.mes.net.MailMessage;
import com.mes.support.DateTimeFormatter;
import com.mes.support.MesMath;
import com.mes.support.NumberFormatter;
import sqlj.runtime.ResultSetIterator;

public class UATPSettlement extends EventBase
{
  public  static final int          ZIP_BUFFER_LEN      = 4096;
  
  private double            BatchCreditsAmount    = 0.0;
  private int               BatchCreditsCount     = 0;
  private double            BatchDebitsAmount     = 0.0;
  private int               BatchDebitsCount      = 0;
  private int               BatchRecordCount      = 0;
  private int               FileBatchCount        = 0;
  private double            FileCreditsAmount     = 0.0;
  private int               FileCreditsCount      = 0;
  private double            FileDebitsAmount      = 0.0;
  private int               FileDebitsCount       = 0;
  private int               FileInvoiceCount      = 0;
  private int               FileRecordCount       = 0;
  private int               InvoiceBatchCount     = 0;
  private double            InvoiceCreditsAmount  = 0.0;
  private int               InvoiceCreditsCount   = 0;
  private double            InvoiceDebitsAmount   = 0.0;
  private int               InvoiceDebitsCount    = 0;
  private String            InvoiceNumber         = null;
  private int               InvoiceRecordCount    = 0;
  private int               InvoiceSeqNum         = 0;
  
  private boolean           TestMode              = false;
  private int               TestSeqNum            = 0;
  
  public final static String  MES_MERCHANT_CODE   = "A17";
  
  public String  ARCHIVE_HOST        = "";
  public String  ARCHIVE_USER        = "";
  public String  ARCHIVE_PASSWORD    = "";
  public String  ARCHIVE_PATH        = "";
  
  public UATPSettlement( )
  {
    PropertiesFilename = "uatp-settlement.properties";
    
    try
    {
      ARCHIVE_HOST        = MesDefaults.getString(MesDefaults.DK_ARCHIVE_HOST); 
      ARCHIVE_USER        = MesDefaults.getString(MesDefaults.DK_ARCHIVE_USER); 
      ARCHIVE_PASSWORD    = MesDefaults.getString(MesDefaults.DK_ARCHIVE_PASS); 
      ARCHIVE_PATH        = MesDefaults.getString(MesDefaults.DK_ARCHIVE_PATH);
    }
    catch(Exception e)
    {
      logEntry("UATPSettlement()", e.toString());
    }
  }
  
  private void buildBatchHeader( BufferedWriter out, ResultSet resultSet )
  {
    FlatFileRecord    ffd               = null;
    
    try
    {
      // build the batch header
      ffd = new FlatFileRecord(MesFlatFiles.DEF_TYPE_ATCAN_BATCH_HEADER);
      ffd.setAllFieldData(resultSet);
      ffd.setFieldData("seq_num",++FileRecordCount);
      ffd.setFieldData("invoice_date",Calendar.getInstance().getTime());
      ffd.setFieldData("ccsp_invoice_number",InvoiceNumber);
      out.write( ffd.spew() );
      out.newLine();
      
      // reset the batch totals
      BatchCreditsAmount  = 0.0;
      BatchDebitsAmount   = 0.0;
      BatchRecordCount    = 0;
      BatchCreditsCount   = 0;
      BatchDebitsCount    = 0;
      
      ++InvoiceBatchCount;
      ++FileBatchCount;
    }
    catch(Exception e)
    {
      logEntry("buildBatchHeader()", e.toString());
    }
    finally
    {
      try { ffd.cleanUp(); } catch(Exception e) {}
    }
  }
  
  public void buildBatchTrailer( BufferedWriter out )
  {
    FlatFileRecord      ffd     = null;
      
    try
    {
      // create the batch trailer record
      ffd = new FlatFileRecord(MesFlatFiles.DEF_TYPE_ATCAN_BATCH_TRAILER);
      ffd.setFieldData("seq_num",++FileRecordCount);
      ffd.setFieldData("batch_count", BatchRecordCount);
      ffd.setFieldData("batch_amount", Math.abs(BatchDebitsAmount - BatchCreditsAmount));
      ffd.setFieldData("batch_amount_signed", Math.abs(BatchDebitsAmount - BatchCreditsAmount));
      ffd.setFieldData("debit_credit_ind",((BatchDebitsAmount - BatchCreditsAmount) < 0.0 ? "CR" : "DB"));
      ffd.setFieldData("ccsp_invoice_number",InvoiceNumber);
      out.write( ffd.spew() );
      out.newLine();
    }
    catch(Exception e)
    {
      logEntry("buildBatchTrailer()", e.toString());
    }
    finally
    {
      try { ffd.cleanUp(); } catch(Exception e) {}
    }
  }
  
  private void buildDetailRecord( BufferedWriter out, ResultSet resultSet )
  {
    String                airlineTicket   = null;
    FlatFileRecord        ffd             = null;
    long                  merchantId      = 0L;
    int                   offset          = 0;
    StringBuffer          passengerData   = new StringBuffer();
    int                   recCount        = 0;
    String                refNum          = null;
    long                  rowSeq          = 0L;
    StringBuffer          ticketNumber    = new StringBuffer();
    
    try
    {
      ffd = new FlatFileRecord(MesFlatFiles.DEF_TYPE_ATCAN_DETAIL_RECORD);
      ffd.connect(true);
      ffd.setAllFieldData(resultSet);
      ffd.setFieldData("seq_num",++FileRecordCount);
      ffd.setFieldData("ccsp_invoice_number",InvoiceNumber);
      
      // build the ticket number (TDNR) and passenger
      // specific data (PXDA) fields.
      airlineTicket = resultSet.getString("airline_ticket_number");
      refNum        = resultSet.getString("invoice_number");
      merchantId    = resultSet.getLong("merchant_number");
      rowSeq        = resultSet.getLong("row_seq");
      
      // 'A17' + last 10 of ref # or ticket #
      ticketNumber.append("A17");
      
      if ( airlineTicket != null )
      {
        // per Mercy Joseph UATP 02/11/2005:
        //
        // do not perform the duplicate encoding 
        // workaround until UATP can get it approved 
        // by the airlines etc.  
        //
        // Until then allow the duplicates to 
        // be detected/rejected by UATP and work
        // them manually with: 
        //
        // Dave Yanbrofski (sp?)
        // UATP Auth/Settlement
        // 202.626.4120
        //
        
        // ticket number is 'A17' + last 10-digits of airline ticket #
        offset = Math.max( 0, (airlineTicket.length()-10) );
        ticketNumber.append(airlineTicket.substring(offset));

        // transaction is a Ticket Related Charge (TRC), 
        // include the original airline ticket number
        passengerData.append("TRC    ");
        passengerData.append(airlineTicket);
      }
      else    // do not have a valid airline ticket number
      {
        // use the 'A17' + last 10-digits of sabre ref #
        offset = Math.max( 0, (refNum.length()-10) );
        ticketNumber.append(refNum.substring(offset));
      
        // transaction is a Non Ticket Charge (NTC),
        // no additional passenger data required
        passengerData.append("NTC");
      }
      
      // put the data into the fixed format
      ffd.setFieldData("ticket_number",ticketNumber.toString());
      ffd.setFieldData("passenger_specific_data",passengerData.toString());
      
      out.write(ffd.spew());
      out.newLine();
      
      // MesFlatFiles.DEF_TYPE_ATCAN_DETAIL_EXT_REC
      
      // update the totals
      if ( resultSet.getString("debit_credit_ind").equals("C") )
      {
        BatchCreditsCount++;
        BatchCreditsAmount += resultSet.getDouble("signed_for_amount");
        InvoiceCreditsCount++;
        InvoiceCreditsAmount += resultSet.getDouble("signed_for_amount");
        FileCreditsCount++;
        FileCreditsAmount += resultSet.getDouble("signed_for_amount");
      }
      else
      {
        BatchDebitsCount++;
        BatchDebitsAmount += resultSet.getDouble("signed_for_amount");
        InvoiceDebitsCount++;
        InvoiceDebitsAmount += resultSet.getDouble("signed_for_amount");;
        FileDebitsCount++;
        FileDebitsAmount += resultSet.getDouble("signed_for_amount");;
      }        
      BatchRecordCount++;
      InvoiceRecordCount++;
    }
    catch(Exception e)
    {
      logEntry("buildDetailRec()", e.toString());
    }
    finally
    {
      try { ffd.cleanUp(); } catch(Exception ee) {}
    }
  }
  
  private void buildFileHeader( BufferedWriter out, ResultSet resultSet )
  {
    FlatFileRecord    ffd               = null;
    Timestamp         procTS            = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:273^7*/

//  ************************************************************
//  #sql [Ctx] { select  sysdate 
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  sysdate  \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.UATPSettlement",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   procTS = (java.sql.Timestamp)__sJT_rs.getTimestamp(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:277^7*/
      
      // reset the file totals
      FileCreditsAmount = 0.0;
      FileDebitsAmount  = 0.0;
      FileCreditsCount  = 0;
      FileDebitsCount   = 0;
      FileRecordCount   = 0;
      FileBatchCount    = 0;
      FileInvoiceCount  = 0;
      
      // build the transmission header
      ffd = new FlatFileRecord(MesFlatFiles.DEF_TYPE_ATCAN_FILE_HEADER);
      ffd.connect(true);
      ffd.setAllFieldData(resultSet);
      ffd.setFieldData("seq_num",++FileRecordCount);
      ffd.setFieldData("processing_date",procTS);
      ffd.setFieldData("processing_time",procTS);
      ffd.setFieldData("test_prod_status",(inTestMode() ? "TEST" : "PROD"));
      out.write( ffd.spew() );
      out.newLine();
    }
    catch(Exception e)
    {
      logEntry("buildFileHeader()", e.toString());
    }
    finally
    {
      try { ffd.cleanUp(); } catch(Exception e) {}
    }
  }
  
  public void buildFileTrailer( BufferedWriter out )
  {
    FlatFileRecord      ffd     = null;
      
    try
    {
      // set record count and total
      ffd = new FlatFileRecord(MesFlatFiles.DEF_TYPE_ATCAN_FILE_TRAILER);
      ffd.setFieldData("seq_num",++FileRecordCount);
      ffd.setFieldData("batch_total_count", FileBatchCount);
      ffd.setFieldData("invoice_total_count", FileInvoiceCount);
      ffd.setFieldData("file_debits_total_amount_signed", FileDebitsAmount);
      ffd.setFieldData("file_credits_total_amount_signed", FileCreditsAmount);
      out.write( ffd.spew() );
      out.newLine();
    }
    catch(Exception e)
    {
      logEntry("buildFileTrailer()", e.toString());
    }
    finally
    {
      try { ffd.cleanUp(); } catch(Exception e) {}
    }
  }
  
  private void buildInvoiceHeader( BufferedWriter out, ResultSet resultSet )
  {
    StringBuffer      buffer            = new StringBuffer();
    Calendar          cal               = Calendar.getInstance();
    FlatFileRecord    ffd               = null;
    
    try
    {
      // build the transmission header
      ffd = new FlatFileRecord(MesFlatFiles.DEF_TYPE_ATCAN_INVOICE_HEADER);
      ffd.connect(true);
      ffd.setAllFieldData(resultSet);
      ffd.setFieldData("seq_num",++FileRecordCount);
      ffd.setFieldData("ccsp_invoice_date",Calendar.getInstance().getTime());
      
      //
      // build the invoice number
      //  format iiiimmmyjjjnnn
      //    iiii  = issuer code
      //    mmm   = merchant code
      //    y     = year (ex: 3 = 2003)
      //    jjj   = julian date
      //    nnn   = sequence number
      //
      buffer.append(resultSet.getString("issuer_code"));
      buffer.append(MES_MERCHANT_CODE);
      buffer.append( (Integer.toString(cal.get(Calendar.YEAR))).substring(3,4) );
      buffer.append( NumberFormatter.getPaddedInt(cal.get(Calendar.DAY_OF_YEAR),3) );
      buffer.append( NumberFormatter.getPaddedInt(++InvoiceSeqNum,3) );
      InvoiceNumber = buffer.toString();
      // set the invoice number
      ffd.setFieldData("ccsp_invoice_number",InvoiceNumber);
      
      out.write( ffd.spew() );
      out.newLine();
      
      // reset the invoice totals
      InvoiceCreditsAmount  = 0.0;
      InvoiceDebitsAmount   = 0.0;
      InvoiceRecordCount    = 0;
      InvoiceCreditsCount   = 0;
      InvoiceDebitsCount    = 0;
      InvoiceBatchCount     = 0;
      
      ++FileInvoiceCount;
    }
    catch(Exception e)
    {
      logEntry("buildInvoiceHeader()", e.toString());
    }
    finally
    {
      try { ffd.cleanUp(); } catch(Exception e) {}
    }
  }
  
  public void buildInvoiceTrailer( BufferedWriter out )
  {
    FlatFileRecord      ffd     = null;
      
    try
    {
      // set record count and total
      ffd = new FlatFileRecord(MesFlatFiles.DEF_TYPE_ATCAN_INVOICE_TRAILER);
      ffd.setFieldData("seq_num",++FileRecordCount);
      ffd.setFieldData("batch_count_per_invoice",InvoiceBatchCount);
      ffd.setFieldData("credits_total_signed", InvoiceCreditsAmount );
      ffd.setFieldData("debits_total_signed", InvoiceDebitsAmount );
      ffd.setFieldData("ccsp_invoice_number",InvoiceNumber);
      out.write( ffd.spew() );
      out.newLine();
    }
    catch(Exception e)
    {
      logEntry("buildInvoiceTrailer()", e.toString());
    }
    finally
    {
      try { ffd.cleanUp(); } catch(Exception e) {}
    }
  }
  
  /*
  ** METHOD execute
  **
  ** Entry point from timed event manager.
  */
  public boolean execute()
  {
    StringBuffer              buffer              = new StringBuffer();
    String                    dateString          = null;
    char                      debitCreditInd      = '\0';
    ResultSetIterator         it                  = null;
    char                      lastDebitCreditInd  = '\0';
    int                       lastIssuerCode      = 0;
    int                       issuerCode          = 0;
    BufferedWriter            out                 = null;
    int                       recCount            = 0;
    ResultSet                 resultSet           = null;
    int                       sequenceId          = 0;
    String                    workFilename        = null;
    
    try
    {
      // get an automated timeout exempt connection
      connect(true);
      
      if ( TestSeqNum != 0 )  // re-build a previous file
      {
        sequenceId = TestSeqNum;    
        
        /*@lineinfo:generated-code*//*@lineinfo:446^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(load_filename)  
//            from    uatp_settlement_process usp
//            where   process_sequence = :sequenceId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(load_filename)   \n          from    uatp_settlement_process usp\n          where   process_sequence =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.startup.UATPSettlement",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,sequenceId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:451^9*/
      }
      else    // new file
      {
        /*@lineinfo:generated-code*//*@lineinfo:455^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(load_filename)  
//            from    uatp_settlement_process usp
//            where   process_sequence is null
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(load_filename)   \n          from    uatp_settlement_process usp\n          where   process_sequence is null";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.startup.UATPSettlement",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:460^9*/
      
        if ( recCount > 0 )
        {
          /*@lineinfo:generated-code*//*@lineinfo:464^11*/

//  ************************************************************
//  #sql [Ctx] { select  uatp_settlement_sequence.nextval 
//              from    dual
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  uatp_settlement_sequence.nextval  \n            from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.startup.UATPSettlement",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   sequenceId = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:468^11*/
      
          /*@lineinfo:generated-code*//*@lineinfo:470^11*/

//  ************************************************************
//  #sql [Ctx] { update  uatp_settlement_process
//              set     process_sequence = :sequenceId
//              where   process_sequence is null
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  uatp_settlement_process\n            set     process_sequence =  :1 \n            where   process_sequence is null";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.startup.UATPSettlement",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,sequenceId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:475^11*/
        
          recordTimestampBegin( sequenceId );
        }
      }
      
      if ( recCount > 0 )
      {
        // select the transactions to process
        /*@lineinfo:generated-code*//*@lineinfo:484^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ 
//                        ordered
//                        use_nl(usp dt)
//                        index(dt idx_ddf_ext_dt_filename)
//                    */
//                    mf.merchant_number              as merchant_number,
//                    mf.dba_name                     as pos_name,
//                    mf.dmcity                       as place_of_issue,
//                    dt.row_sequence                 as row_seq,
//                    (
//                      decode( dt.acq_invoice_number,
//                              null, '',
//                              ('PNR ' || dt.acq_invoice_number) || ' ' ) ||
//                      'PH ' || substr(mf.phone_1,1,3) ||
//                      '-' || substr(mf.phone_1,4,3) ||
//                      '-' || substr(mf.phone_1,7)
//                    )                               as customer_file_reference,
//                    dt.reference_number             as invoice_number,
//                    nvl( mrs.iata_number, 
//                    ( '9' || substr( to_char(mf.merchant_number),
//                                     length(to_char(mf.merchant_number))-6
//                                   ) )
//                       )                            as agent_code,
//                    dt.batch_date                   as batch_date,
//                    dt.batch_date                   as invoice_date,
//                    dt.transaction_date             as tran_date,
//                    dt.transaction_date             as issue_date,
//                    dt.auth_code                    as auth_code,
//                    dt.batch_number                 as batch_number,
//                    decode( (length(nvl(dt.purchase_id,'bad')) + 
//                              is_number( nvl(dt.purchase_id,'bad') )),
//                            14, dt.purchase_id, 
//                            null )                  as airline_ticket_number,
//                    -- chop leading '0'
//                    to_number(
//                      dukpt_decrypt_wrapper(dt.card_number_enc)
//                    )                               as card_number,
//                    substr( dt.card_number, 1, 4 )  as issuer_code,
//                    dt.transaction_amount           as signed_for_amount,
//                    dt.debit_credit_indicator       as debit_credit_ind,
//                    decode( dt.debit_credit_indicator,
//                            'C','CR', 
//                            'DB' )                  as debit_credit_code,
//                    nvl(dt.passenger_name,                          
//                        'NOTAVAILABLE/X')           as passenger_name
//            from    uatp_settlement_process       usp,
//                    daily_detail_file_ext_dt      dt,
//                    mif                           mf,
//                    merchant                      mr,
//                    merchant_sabre                mrs
//            where   usp.process_sequence = :sequenceId and
//                    dt.load_filename = usp.load_filename and
//                    (
//                      nvl(usp.hierarchy_node,0) = 0 or
//                      exists
//                      (
//                        select  gm.merchant_number
//                        from    organization    o,
//                                group_merchant  gm
//                        where   o.org_group = usp.hierarchy_node and
//                                gm.org_num = o.org_num and
//                                gm.merchant_number = dt.merchant_number
//                      )
//                    ) and
//                    dt.card_type = 'PL' and
//                    substr(dt.card_number,1,1) in ('1','2') and -- uatp cards only
//                    mf.merchant_number = dt.merchant_number and
//                    mr.merch_number(+) = mf.merchant_number and
//                    mrs.app_seq_num(+) = mr.app_seq_num
//            order by issuer_code, debit_credit_code, row_seq
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ \n                      ordered\n                      use_nl(usp dt)\n                      index(dt idx_ddf_ext_dt_filename)\n                  */\n                  mf.merchant_number              as merchant_number,\n                  mf.dba_name                     as pos_name,\n                  mf.dmcity                       as place_of_issue,\n                  dt.row_sequence                 as row_seq,\n                  (\n                    decode( dt.acq_invoice_number,\n                            null, '',\n                            ('PNR ' || dt.acq_invoice_number) || ' ' ) ||\n                    'PH ' || substr(mf.phone_1,1,3) ||\n                    '-' || substr(mf.phone_1,4,3) ||\n                    '-' || substr(mf.phone_1,7)\n                  )                               as customer_file_reference,\n                  dt.reference_number             as invoice_number,\n                  nvl( mrs.iata_number, \n                  ( '9' || substr( to_char(mf.merchant_number),\n                                   length(to_char(mf.merchant_number))-6\n                                 ) )\n                     )                            as agent_code,\n                  dt.batch_date                   as batch_date,\n                  dt.batch_date                   as invoice_date,\n                  dt.transaction_date             as tran_date,\n                  dt.transaction_date             as issue_date,\n                  dt.auth_code                    as auth_code,\n                  dt.batch_number                 as batch_number,\n                  decode( (length(nvl(dt.purchase_id,'bad')) + \n                            is_number( nvl(dt.purchase_id,'bad') )),\n                          14, dt.purchase_id, \n                          null )                  as airline_ticket_number,\n                  -- chop leading '0'\n                  to_number(\n                    dukpt_decrypt_wrapper(dt.card_number_enc)\n                  )                               as card_number,\n                  substr( dt.card_number, 1, 4 )  as issuer_code,\n                  dt.transaction_amount           as signed_for_amount,\n                  dt.debit_credit_indicator       as debit_credit_ind,\n                  decode( dt.debit_credit_indicator,\n                          'C','CR', \n                          'DB' )                  as debit_credit_code,\n                  nvl(dt.passenger_name,                          \n                      'NOTAVAILABLE/X')           as passenger_name\n          from    uatp_settlement_process       usp,\n                  daily_detail_file_ext_dt      dt,\n                  mif                           mf,\n                  merchant                      mr,\n                  merchant_sabre                mrs\n          where   usp.process_sequence =  :1  and\n                  dt.load_filename = usp.load_filename and\n                  (\n                    nvl(usp.hierarchy_node,0) = 0 or\n                    exists\n                    (\n                      select  gm.merchant_number\n                      from    organization    o,\n                              group_merchant  gm\n                      where   o.org_group = usp.hierarchy_node and\n                              gm.org_num = o.org_num and\n                              gm.merchant_number = dt.merchant_number\n                    )\n                  ) and\n                  dt.card_type = 'PL' and\n                  substr(dt.card_number,1,1) in ('1','2') and -- uatp cards only\n                  mf.merchant_number = dt.merchant_number and\n                  mr.merch_number(+) = mf.merchant_number and\n                  mrs.app_seq_num(+) = mr.app_seq_num\n          order by issuer_code, debit_credit_code, row_seq";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.startup.UATPSettlement",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,sequenceId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.startup.UATPSettlement",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:556^9*/
        resultSet = it.getResultSet();
      
        recCount = 0;   // re-use recCount
      
        while( resultSet.next() )
        {
          // first record, build the outgoing load_filename
          if ( workFilename == null )
          {
            // setup a temp filename
            dateString = DateTimeFormatter.getFormattedDate(Calendar.getInstance().getTime(),"MMddyy");
            buffer.setLength(0);
            buffer.append("uatp3941");
            buffer.append("_");
            buffer.append(dateString);
            buffer.append("_");
            buffer.append( NumberFormatter.getPaddedInt( getFileId(dateString), 3 ) );
            buffer.append(".dat");
    
            workFilename = buffer.toString();
            
            // open the file and output the transmission header
            out = new BufferedWriter( new FileWriter( workFilename, false ) );
            buildFileHeader(out,resultSet);
          }
          issuerCode      = resultSet.getInt("issuer_code");
          debitCreditInd  = resultSet.getString("debit_credit_ind").charAt(0);
          
          if ( issuerCode != lastIssuerCode )
          {
            if ( lastIssuerCode != 0 )
            {
              buildBatchTrailer(out);
              buildInvoiceTrailer(out);
            }
            buildInvoiceHeader(out,resultSet);
            buildBatchHeader(out,resultSet);
            lastIssuerCode = 0;
          }
          else if ( debitCreditInd != lastDebitCreditInd )
          {
            if ( lastDebitCreditInd != '\0' )
            {
              buildBatchTrailer(out);   // end the current batch
            }  
            buildBatchHeader(out,resultSet);
          }
          buildDetailRecord(out,resultSet);
          
          ++recCount;
          
          lastIssuerCode      = issuerCode;
          lastDebitCreditInd  = debitCreditInd;
        }
        resultSet.close();
        it.close();        
        
        // handle the boundary condition
        if ( recCount > 0 )
        {
          buildBatchTrailer(out);     // end the last batch
          buildInvoiceTrailer(out);   // invoice trailer
          buildFileTrailer(out);      // file trailer
          out.close();
          
          if ( !inTestMode() )
          {
            sendFile(workFilename);
          }
        }
        
        // only make the timestamp when not
        // re-generating a previous sequence
        if ( sequenceId != TestSeqNum )
        {
          recordTimestampEnd( sequenceId, workFilename );
        }          
      }
    }
    catch( Exception e )
    {
      logEntry("execute()", e.toString());
    }
    finally
    {
      try{ it.close(); } catch(Exception e) {}
      cleanUp();
    }
    
    return( true );
  }
  
  protected boolean inTestMode()
  {
    return( TestMode );
  }
  
  protected void recordTimestampBegin( long seqId )
  {
    Timestamp     beginTime   = null;
    
    try
    {
      beginTime = new Timestamp(Calendar.getInstance().getTime().getTime());
    
      /*@lineinfo:generated-code*//*@lineinfo:662^7*/

//  ************************************************************
//  #sql [Ctx] { update  uatp_settlement_process
//          set     process_begin_date = :beginTime
//          where   process_sequence = :seqId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  uatp_settlement_process\n        set     process_begin_date =  :1 \n        where   process_sequence =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"6com.mes.startup.UATPSettlement",theSqlTS);
   // set IN parameters
   __sJT_st.setTimestamp(1,beginTime);
   __sJT_st.setLong(2,seqId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:667^7*/
    }
    catch(Exception e)
    {
      logEntry("recordTimestampBegin()", e.toString());
      setError("recordTimestampBegin(): " + e.toString());
    }
  }
  
  protected void recordTimestampEnd( long seqId, String workFilename )
  {
    Timestamp     beginTime   = null;
    long          elapsed     = 0L; 
    Timestamp     endTime     = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:684^7*/

//  ************************************************************
//  #sql [Ctx] { select  distinct least(usp.process_begin_date) 
//          from    uatp_settlement_process   usp
//          where   usp.process_sequence = :seqId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  distinct least(usp.process_begin_date)  \n        from    uatp_settlement_process   usp\n        where   usp.process_sequence =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.startup.UATPSettlement",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,seqId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   beginTime = (java.sql.Timestamp)__sJT_rs.getTimestamp(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:689^7*/
      endTime = new Timestamp(Calendar.getInstance().getTime().getTime());
      elapsed = (endTime.getTime() - beginTime.getTime());
    
      /*@lineinfo:generated-code*//*@lineinfo:693^7*/

//  ************************************************************
//  #sql [Ctx] { update  uatp_settlement_process
//          set     process_end_date = :endTime,
//                  process_elapsed = :DateTimeFormatter.getFormattedTimestamp(elapsed),
//                  output_filename = :workFilename
//          where   process_sequence = :seqId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_4888 = DateTimeFormatter.getFormattedTimestamp(elapsed);
   String theSqlTS = "update  uatp_settlement_process\n        set     process_end_date =  :1 ,\n                process_elapsed =  :2 ,\n                output_filename =  :3 \n        where   process_sequence =  :4";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"8com.mes.startup.UATPSettlement",theSqlTS);
   // set IN parameters
   __sJT_st.setTimestamp(1,endTime);
   __sJT_st.setString(2,__sJT_4888);
   __sJT_st.setString(3,workFilename);
   __sJT_st.setLong(4,seqId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:700^7*/
    }
    catch(Exception e)
    {
      logEntry("recordTimestampEnd()", e.toString());
      setError("recordTimestampEnd(): " + e.toString());
    }
  }
  
  protected boolean sendFile( String dataFilename )
  {
    boolean       success         = false;
    StringBuffer  status          = new StringBuffer();
    
    try
    {    
      connect();
      
      try
      {
        success = sendDataFile( dataFilename, 
                                MesDefaults.getString(MesDefaults.DK_UATP_OUTGOING_HOST),
                                MesDefaults.getString(MesDefaults.DK_UATP_OUTGOING_USER),
                                MesDefaults.getString(MesDefaults.DK_UATP_OUTGOING_PASSWORD),
                                MesDefaults.getString(MesDefaults.DK_UATP_OUTGOING_PATH),
                                false,
                                true );
                                
        if ( success == true )
        {
          archiveDailyFile( dataFilename );
        
          status.append("[");
          status.append(DateTimeFormatter.getFormattedDate(Calendar.getInstance().getTime(),"MM/dd/yyyy hh:mm:ss"));
          status.append("] Successfully processed outgoing UATP file: ");
          status.append(dataFilename);
          status.append("\n\n");
        }
        else
        {
          status.append("[");
          status.append(DateTimeFormatter.getFormattedDate(Calendar.getInstance().getTime(),"MM/dd/yyyy hh:mm:ss"));
          status.append("] Failed to process outgoing UATP file: ");
          status.append(dataFilename);
          status.append("\n\n");
        }
        
        status.append("File Statistics\n");
        status.append("================================================\n");
        status.append("Record Count  : " + FileRecordCount);
        status.append("\n");
        status.append("Batch Count   : " + FileBatchCount);
        status.append("\n");
        status.append("Invoice Count : " + FileInvoiceCount);
        status.append("\n");
        status.append("Debits Count  : " + FileDebitsCount );
        status.append("\n");
        status.append("Debits Amount : " + MesMath.toCurrency(FileDebitsAmount) );
        status.append("\n");
        status.append("Credits Count : " + FileCreditsCount );
        status.append("\n");
        status.append("Credits Amount: " + MesMath.toCurrency(FileCreditsAmount) );
      }
      catch(Exception ftpe)
      {
        logEntry("sendFile(" + dataFilename + ")", ftpe.toString());
        status.append(ftpe.toString());
      }
      
      // send a status email
      sendStatusEmail(success, dataFilename, status.toString() );
    }      
    catch( Exception e )
    {
      logEntry("sendFile(" + dataFilename + ")", e.toString());
    }
    finally
    {
    }
    return( success );
  }    
  
  private void sendStatusEmail(boolean success, String fileName, String message)
  {
    StringBuffer  subject         = new StringBuffer("");
    StringBuffer  body            = new StringBuffer("");
    
    try
    {
      // post the file totals for the systems group to use for confirmation
      postFileTotals( fileName,"uatp",
                      FileRecordCount,
                      FileBatchCount,
                      FileDebitsCount,
                      FileDebitsAmount,
                      FileCreditsCount,
                      FileCreditsAmount );
    
      if(success)
      {
        subject.append("Outgoing UATP Success - " + fileName);
      }
      else
      {
        subject.append("Outgoing UATP Error - " + fileName);
        
        // make sure the filename is in 
        // the message text for error research
        body.append("Filename: ");
        body.append(fileName);
        body.append("\n\n");
      }
      body.append(message);
      
      MailMessage msg = new MailMessage();
      
      if(success)
      {
        msg.setAddresses(MesEmails.MSG_ADDRS_OUTGOING_UATP_NOTIFY);
      }
      else
      {
        msg.setAddresses(MesEmails.MSG_ADDRS_OUTGOING_UATP_FAILURE);
      }
      
      msg.setSubject(subject.toString());
      msg.setText(body.toString());
      msg.send();
    }
    catch(Exception e)
    {
      logEntry("sendStatusEmail()", e.toString());
    }
  }
  
  protected void setTestMode( boolean inTest )
  {
    TestMode = inTest;
  }
  
  protected void setTestSeqNum( int seqNum )
  {
    TestSeqNum = seqNum;
  }
  
  public static void main( String[] args )
  {
      try
      {

          if (args.length > 0 && args[0].equals("testproperties")) {
              EventBase.printKeyListStatus(new String[]{
                      MesDefaults.DK_ARCHIVE_HOST,
                      MesDefaults.DK_ARCHIVE_USER,
                      MesDefaults.DK_ARCHIVE_PASS,
                      MesDefaults.DK_ARCHIVE_PATH,
                      MesDefaults.DK_UATP_OUTGOING_HOST,
                      MesDefaults.DK_UATP_OUTGOING_USER,
                      MesDefaults.DK_UATP_OUTGOING_PASSWORD,
                      MesDefaults.DK_UATP_OUTGOING_PATH
              });
          }
      } catch (Exception e) {
          System.out.println(e.toString());
      }

    UATPSettlement        test          = null;
    
    try
    {
      SQLJConnectionBase.initStandalone();
      
      test = new UATPSettlement();
      test.setTestMode( true );
      test.setTestSeqNum( Integer.parseInt(args[0]) );
      test.execute();
    }
    finally
    {
    }
  }
}/*@lineinfo:generated-code*/