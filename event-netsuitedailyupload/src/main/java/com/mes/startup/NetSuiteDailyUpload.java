/*@lineinfo:filename=NetSuiteDailyUpload*//*@lineinfo:user-code*//*@lineinfo:1^1*/package com.mes.startup;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.Vector;
import org.apache.log4j.Logger;
import com.mes.config.MesDefaults;
import com.mes.net.MailMessage;
import com.mes.support.DateTimeFormatter;
import com.mes.support.NumberFormatter;
import com.netsuite.webservices.platform.core_2019_1.CustomFieldList;
import com.netsuite.webservices.platform.core_2019_1.CustomFieldRef;
import com.netsuite.webservices.platform.core_2019_1.LongCustomFieldRef;
import com.netsuite.webservices.platform.core_2019_1.Record;
import com.netsuite.webservices.platform.core_2019_1.RecordRef;
import com.netsuite.webservices.platform.core_2019_1.StringCustomFieldRef;
import com.netsuite.webservices.platform.core_2019_1.types.RecordType;
import com.netsuite.webservices.platform.messages_2019_1.WriteResponse;
import com.netsuite.webservices.transactions.general_2019_1.JournalEntry;
import com.netsuite.webservices.transactions.general_2019_1.JournalEntryLine;
import com.netsuite.webservices.transactions.general_2019_1.JournalEntryLineList;
import sqlj.runtime.ResultSetIterator;


public class NetSuiteDailyUpload extends NetSuiteEventBase
{
  static Logger log = Logger.getLogger(NetSuiteDailyUpload.class);
  
  public static final String  DAILY_SALES_ACTIVITY_ACCOUNT_ID      = "4361";  //9991 Clearing - Daily Activity
  public static final int     DAILY_JE_SALES_ACTIVITY              = 7;
  
  public static final String[][] DAILY_JE_RECORDS =
  {
    { "0"  , "Funding Summary" },
    { "1"  , "Visa Clearing" },
    { "2"  , "Mastercard Clearing" },
    { "3"  , "Discover Clearing" },
    { "4"  , "Incoming Chargebacks" },
    { "5"  , "Debit Adjustments" },
    { "6"  , "Merchant Deposits" },
    { "7"  , "Sales Activity" },
  };
  
  public static final String[][] JE_SALES_ACTIVITY_CUSTOM_FIELDS =
  {
    { "785"  , "custcol_dailytransactionvolume" },
    { "784"  , "custcol_dailysalesvolume" },
  };
  
  private Vector    ReportRows            = new Vector();
  private int       BankNumber            = -1;
  private int       RecordId              = -1;
  
  private static final String SQL_GET_NS_BANKNUMBER = 
            "SELECT  nsa.bank_number AS bank_number "
          + "FROM    netsuite_accounts nsa "
          + "WHERE   ( :1           = -1 "
          + "          AND nsa.bank_number   IN (3858,3941, 3943, 3003) "
          + "          OR nsa.bank_number   = :2 )"
          + "GROUP BY  nsa.bank_number "
          + "ORDER BY  nsa.bank_number";
  
  private static final String SQL_GET_JE_SUMMARY = ""
          + "SELECT nsa.bank_number AS bank_number, "
          + "       nsa.record_id   AS record_id, "
          + "       nsa.record_name AS record_name "
          + "FROM   netsuite_accounts nsa "
          + "WHERE    ( :1           = -1 "
          + "          AND nsa.bank_number IN (3941,3858, 3943, 3003) "
          + "          OR nsa.bank_number   = :2 "
          + "         ) "
          + "         AND ( :3 = -1 OR nsa.record_id     = :4 ) "
          + "GROUP BY nsa.bank_number, "
          + "         nsa.record_id, "
          + "         nsa.record_name "
          + "ORDER BY nsa.bank_number, "
          + "         nsa.record_id";
  
  private static final String SQL_GET_FUNDING_SUMMARY = ""
          + "SELECT "
          + "  /* index (sm pk_ddf_summary) */ "
          + "  sm.batch_date                                                                                                                                                       AS batch_date, "
          + "  sm.bank_number                                                                                                                                                      AS bank_number, "
          + "  SUM( (sm.bank_sales_amount + sm.nonbank_sales_amount) - (sm.bank_credits_amount + sm.nonbank_credits_amount) - (sm.bank_reject_amount + sm.nonbank_reject_amount) ) AS total_amount, "
          + "  SUM( sm.visa_sales_amount  - sm.visa_credits_amount + NVL(sm.visa_fx_markup_amount,0) )                                                                             AS vs_amount, "
          + "  SUM( sm.mc_sales_amount    - sm.mc_credits_amount + NVL(sm.mc_fx_markup_amount,0) )                                                                                 AS mc_amount, "
          + "  SUM( sm.debit_sales_amount - sm.debit_credits_amount )                                                                                                              AS db_amount, "
          + "  SUM( CASE WHEN sm.load_filename LIKE '256s%' THEN 1 ELSE 0 END * "
          + "       CASE WHEN EXISTS "
          + "      (SELECT 1 FROM mif mf WHERE sm.merchant_number = mf.merchant_number AND mf.damexse NOT IN "
          + "        (SELECT DISTINCT amex_optblue_se_number FROM  amex_se_mapping ) "
          + "      )  THEN 1 ELSE 0 "
          + "      END * (sm.amex_sales_amount - sm.amex_credits_amount) "
          + "     )                                                                                                                                                                AS sabre_am_amount, "
          + "  SUM( CASE WHEN sm.load_filename LIKE '256s%' THEN 0  ELSE 1 END * "
          + "  CASE WHEN EXISTS "
          + "       (SELECT 1 FROM mif mf WHERE sm.merchant_number = mf.merchant_number AND mf.damexse NOT IN "
          + "         (SELECT DISTINCT amex_optblue_se_number FROM amex_se_mapping ) "
          + "       ) THEN 1 ELSE 0 "
          + "       END * (sm.amex_sales_amount - sm.amex_credits_amount) "
          + "     )                                                                                                                                                                AS am_amount, "
          + "  SUM( CASE WHEN sm.load_filename LIKE '256s%' THEN 1 ELSE 0 END * "
          + "       CASE WHEN EXISTS (SELECT 1 FROM mif mf WHERE sm.merchant_number = mf.merchant_number "
          + "            AND mf.damexse IN "
          + "        (SELECT DISTINCT amex_optblue_se_number FROM amex_se_mapping ) "
          + "      ) THEN 1 ELSE 0 END * (sm.amex_sales_amount - sm.amex_credits_amount) "
          + "    )                                                                                                                                                                 AS sabre_am_optb_amount, "
          + "  SUM( CASE WHEN sm.load_filename NOT LIKE '256s%' THEN 1 ELSE 0 END * "
          + "       CASE WHEN EXISTS (SELECT 1 FROM mif mf WHERE sm.merchant_number = mf.merchant_number "
          + "            AND mf.damexse IN "
          + "        (SELECT DISTINCT amex_optblue_se_number FROM amex_se_mapping ) "
          + "      ) THEN 1 ELSE 0 END * (sm.amex_sales_amount - sm.amex_credits_amount) "
          + "     )                                                                                                                                                                AS am_optb_amount, "
          + "  SUM( sm.disc_sales_amount   - sm.disc_credits_amount )                          AS ds_amount, "
          + "  SUM( sm.diners_sales_amount - sm.diners_credits_amount )                        AS dn_amount, "
          + "  SUM( sm.jcb_sales_amount    - sm.jcb_credits_amount )                           AS jc_amount, "
          + "  SUM( CASE "
          + "    WHEN sm.load_filename LIKE '256s%' "
          + "    THEN 1 "
          + "    ELSE 0 "
          + "  END                    * -- uatp \n "
          + "  (sm.other_sales_amount - sm.other_credits_amount) ) AS sabre_uatp_amount, "
          + "  SUM( CASE "
          + "    WHEN sm.load_filename LIKE '256s%' "
          + "    THEN 0 "
          + "    ELSE 1 "
          + "  END * -- not uatp \n "
          + "  CASE "
          + "    WHEN sm.load_filename LIKE 'bml%' "
          + "    THEN 0 "
          + "    ELSE 1 "
          + "  END                    * -- not bml \n "
          + "  (sm.other_sales_amount - sm.other_credits_amount) ) AS ot_amount, "
          + "  SUM( "
          + "  CASE "
          + "    WHEN sm.load_filename LIKE 'bml%' "
          + "    THEN 1 "
          + "    ELSE 0 "
          + "  END                                  * -- bml \n "
          + "  (sm.other_sales_amount               - sm.other_credits_amount) )      AS bml_amount, "
          + "  SUM( sm.bank_reject_amount           + sm.nonbank_reject_amount )      AS rejects_amount, "
          + "  SUM( NVL(sm.visa_fx_markup_amount,0) + NVL(sm.mc_fx_markup_amount,0) ) AS fx_markup_amount, "
          + "  0                                                                      AS fx_markup_amount_amex "
          + "FROM daily_detail_file_summary sm "
          + "WHERE :1           != :2 "
          + "AND ( :3            = - 1 "
          + "AND sm.bank_number   IN (3941,3943,3858,3003) "
          + "OR sm.bank_number   = :4 ) "
          + "AND sm.batch_date BETWEEN :5          - 1 AND :6 "
          + "AND SUBSTR(sm.load_filename,1,3) NOT IN ( 'ddf','bml') "
          + "GROUP BY sm.batch_date, "
          + "  sm.bank_number "
          + "ORDER BY sm.batch_date, "
          + "  sm.bank_number";
  
  private static final String SQL_GET_CLEARING_SUMMARY = ""
          + "SELECT dcs.bank_number                AS bank_number, "
          + "  dcs.card_type                       AS card_type, "
          + "  dcs.funding_amount                  AS outgoing_amount, "
          + "  dcs.actual_amount                   AS actual_amount, "
          + "  dcs.reversals_amount                AS reversals_amount, "
          + "  dcs.incoming_reversals_amount       AS incoming_reversals_amount, "
          + "  dcs.fx_adjustment_amount            AS fx_adjustment_amount, "
          + "  dcs.reprocessed_rejects_amount      AS reprocessed_rejects_amount, "
          + "  dcs.rejects_amount                  AS rejects_amount, "
          + "  dcs.second_presentment_rejects_amt  AS represented_rejects_amount, "
          + "  dcs.reimbursement_fees              AS reimbursement_fees, "
          + "  dcs.assoc_fees                      AS association_fees, "
          + "  dcs.fee_collection_amount           AS fee_collection_amount, "
          + "  dcs.second_presentments_amount      AS represented_chargebacks_amount, "
          + "  dcs.incoming_chargebacks_amount     AS incoming_chargebacks_amount, "
          + "  ( dcs.sales_amount - dcs.credits_amount + dcs.cash_advance_amount + dcs.reprocessed_rejects_amount "
          + "  - dcs.reversals_amount - dcs.rejects_amount + dcs.incoming_reversals_amount "
          + "  + dcs.fx_adjustment_amount - dcs.incoming_chargebacks_amount + dcs.second_presentments_amount "
          + "  - dcs.second_presentment_rejects_amt + dcs.fee_collection_amount + dcs.reimbursement_fees + dcs.assoc_fees )    AS settlement_amount "
          + "FROM daily_clearing_summary dcs "
          + "WHERE :1             != :2 "
          + "AND ( :3              = -1 "
          + "AND dcs.bank_number    IN (3941,3943,3858,3003) "
          + "OR dcs.bank_number    = :4 ) "
          + "AND dcs.clearing_date = :5 "
          + "AND dcs.card_type    IN ('VS', 'MC', 'DS') "
          + "ORDER BY dcs.bank_number, "
          + "  dcs.card_type";
  
  private static final String SQL_GET_DEPOSIT_SUMMARY = ""
          + "SELECT mf.bank_number                                                                AS bank_number, "
          + "  ad.entry_description                                                               AS entry_desc, "
          + "  DECODE(mf.bank_number,3858, ad.post_date            -1, ad.post_date)              AS post_date, "
          + "  SUM( DECODE(adtc.debit_credit_indicator, 'C', 1, 0) * ad.ach_amount )              AS credit_amount, "
          + "  SUM( DECODE(adtc.debit_credit_indicator, 'D', 1, 0) * ad.ach_amount )              AS debit_amount, "
          + "  SUM( DECODE(adtc.debit_credit_indicator, 'C',       -1, 1) * ad.ach_amount )       AS total_amount, "
          + "  SUM(                                                -1 * ad.sponsored_amount )     AS sponsored_amount, "
          + "  SUM(                                                -1 * ad.non_sponsored_amount ) AS non_sponsored_amount "
          + "FROM ach_trident_detail ad, "
          + "  ach_detail_tran_code adtc, "
          + "  mif mf, "
          + "  bankserv_ach_descriptions bad "
          + "WHERE :1                 != :2 "
          + "AND ad.post_date          = :3 - 1 "
          + "AND ad.entry_description != 'MERCH ADJ' "
          + "AND (ad.load_filename LIKE 'mach394100000%' "
          + "OR ad.load_filename LIKE 'mach385800000%' "
          + "OR ad.load_filename LIKE 'mach394300000%'"
          + "OR ad.load_filename LIKE 'mach300300000%') "
          + "AND NVL(ad.disqualified, 'N') = 'N' "
          + "AND ad.transaction_code       = adtc.ach_detail_trans_code "
          + "AND ad.merchant_number        = mf.merchant_number "
          + "AND ( :4                      = -1 "
          + "OR mf.bank_number             = :5 ) "
          + "AND (ad.post_date             > '17-Dec-2009' "
          + "OR NVL(mf.test_account,'N')   = 'N') "
          + "AND ad.manual_description_id  = bad.description_id(+) "
          + "GROUP BY ad.entry_description, "
          + "  ad.post_date, "
          + "  mf.bank_number "
          + "ORDER BY mf.bank_number, "
          + "  ad.entry_description";
  
  private static final String SQL_GET_JE_DETAILS = ""
          + "SELECT     nsa.bank_number AS bank_number, "
          + "           nsa.record_id        AS record_id, "
          + "           nsa.record_name      AS record_name, "
          + "           nsa.account_id       AS account_id, "
          + "           nsa.account_desc     AS account_desc, "
          + "           nsa.field_order      AS field_order "
          + "FROM       netsuite_accounts nsa "
          + "WHERE      :1            != :2 "
          + "           AND  ( :3             = -1 "
          + "            AND nsa.bank_number   IN (3941,3858, 3943, 3003) "
          + "            OR nsa.bank_number   = :4 ) "
          + "           AND ( :5             = -1 "
          + "            OR nsa.record_id     = :6 ) "
          + "ORDER BY   nsa.bank_number, "
          + "           nsa.record_id, "
          + "           nsa.field_order";
  
  private static final String SQL_GET_SALES_ACTIVITY = ""
          + "SELECT       sm.batch_date                                       AS batch_date, "
          + "             mf.bank_number                                      AS bank_number, "
          + "             SUM(sm.bank_sales_count  + sm.nonbank_sales_count)  AS sales_count, "
          + "             SUM(sm.bank_sales_amount + sm.nonbank_sales_amount) AS sales_amount "
          + "FROM         mif mf, "
          + "             daily_detail_file_summary sm "
          + "WHERE        :1               = -1 "
          + "             AND ( :2               = -1 "
          + "               OR :3                  = :4 ) "
          + "             AND mf.bank_number    IN (3941,3943,3858,3003) "
          + "             AND sm.merchant_number = mf.merchant_number "
          + "             AND sm.batch_date      = :5 "
          + "GROUP BY     sm.batch_date, "
          + "             mf.bank_number";
  
  public class SummaryData
  {
    public Date     BatchDate             = null;
    public String   BankNumber            = null;
    public Vector   VsCarryover           = new Vector();
    public Vector   ClearingData          = new Vector();
    public Vector   FundingData           = new Vector();
    public Vector   JeRecords             = new Vector();
    
    // JE lines
    public Vector   TotalFunding          = new Vector();
    public Vector   VisaClearing          = new Vector();
    public Vector   MastercardClearing    = new Vector();
    public Vector   DiscoverClearing      = new Vector();
    public Vector   IncomingChargebacks   = new Vector();
    public Vector   DebitAdjustments      = new Vector();
    public Vector   MerchantDeposits      = new Vector();
    
    public DepositSummary AchAmexDeposit     = null;
    public DepositSummary AchMerchDeposit    = null;
    public DepositSummary AchMerchChargeback = null;
    public DepositSummary AchMerchAdjustment = null;
    public SalesActivity  SalesData          = null;
    
    public SummaryData( ResultSet resultSet )
      throws java.sql.SQLException
    {
      BankNumber    = resultSet.getString("bank_number");
    }
    
    public SummaryData( String bankNumber )
    {
      BankNumber    = bankNumber;
    }
    
    public String  getBankNumber()          { return BankNumber; }
    public Vector  getFundingData()         { return FundingData; }
    public Vector  IncomingChargebacks()    { return ClearingData; }
    public SalesActivity getSalesData()     { return SalesData; }
    
    public Vector  getTotalFunding()        { return TotalFunding; }
    public Vector  getVisaClearing()        { return VisaClearing; }
    public Vector  getMastercardClearing()  { return MastercardClearing; }
    public Vector  getDiscoverClearing()    { return DiscoverClearing; }
    public Vector  getIncomingChargebacks() { return IncomingChargebacks; }
    public Vector  getDebitAdjustments()    { return DebitAdjustments; }
    public Vector  getMerchantDeposits()    { return MerchantDeposits; }
    public Vector  getJeRecords()           { return JeRecords; }
    
    public void    addFundingData( FundingSummary summaryData )    { FundingData.add(summaryData); }
    public void    addClearingData( ClearingSummary summaryData )  { ClearingData.add( summaryData ); }
    public void    addVsCarryover( CarryoverSummary summaryData )  { VsCarryover.add( summaryData ); }
    public void    addJeRecord( JeRecord jeRecord )                { JeRecords.add( jeRecord ); }
    public void    setBankNumber( String bankNumber )              { BankNumber = bankNumber; }
    public void    setSalesData( SalesActivity salesData )         { SalesData = salesData; }
    
    public void setDepositData( DepositSummary ds )
    {
      if( ds.getEntryDesc().equals("AMEX DEP") )
      {
        AchAmexDeposit = ds;
      }
      else if( ds.getEntryDesc().equals("MERCH DEP") )
      {
        AchMerchDeposit = ds;
      }
      else if( ds.getEntryDesc().equals("MERCH CHBK") )
      {
        AchMerchChargeback = ds;
      }
      else if( ds.getEntryDesc().equals("MERCH ADJ") )
      {
        AchMerchAdjustment = ds;
      }
    }
    
    public void addJournalEntryLine( LineItem line, int jeId, int fieldId )
    {
      double amount = 0.0;
      switch( jeId )
      {
        case 0:
          line.setAmount( getFundingAmount(fieldId) );
          TotalFunding.add(line);
          break;
        case 1:
          line.setAmount( getClearingAmount( "VS", fieldId) );
          VisaClearing.add(line);
          break;
        case 2:
          line.setAmount( getClearingAmount( "MC", fieldId) );
          MastercardClearing.add(line);
          break;
        case 3:
          line.setAmount( getClearingAmount("DS",fieldId) );
          DiscoverClearing.add(line);
          break;
        case 4:
          line.setAmount( getMerchChargeback(fieldId) );
          IncomingChargebacks.add(line);
          break;
        case 5:
          line.setAmount( getMerchAdjustment(fieldId) );
          DebitAdjustments.add(line);
          break;
        case 6:
          line.setAmount( getMerchDeposit(fieldId) );
          MerchantDeposits.add(line);
          break;
        default:
          break;
      }
      log.debug(line.getDoubleAmount() + " " + BankNumber + " " + line.getItemId() + " " + line.getItemName() );
    }
    
    private double getVsCarryoverAmount( Date batchDate )
    {
      double retVal = 0.0;
      
      for( int i = 0; i < VsCarryover.size(); ++ i )
      {
        CarryoverSummary sm = (CarryoverSummary)VsCarryover.elementAt(i);
        if( sm.getBatchDate().equals(batchDate) )
        {
          retVal = sm.getAmount();
        }
      }
      
      return retVal;
    }
    
    public double getPrevDayVsCarryoverAmount()
    {
      return( getVsCarryoverAmount(getPrevBatchDate()) );
    }
    
    public double getVsCarryoverAmount()
    {
      return( getVsCarryoverAmount(ActiveDate) );
    }
    
    public double getMerchAdjustment( int fieldId )
    {
      double retVal = 0.0;
      
      try
      {
        switch( fieldId )
        {
          case 0:        // debit adjustment
            retVal = AchMerchAdjustment.getTotalAmount();
            break;
          case 1:        // debit adjustment
            retVal = -1 * AchMerchAdjustment.getTotalAmount();
            break;
          default:
            break;
        }
      }
      catch( Exception e )
      {
      }
      
      return retVal;
    }
    
    public double getMerchChargeback( int fieldId )
    {
      double retVal = 0.0;
      
      try
      {
        switch( fieldId )
        {
          case 0:        // Incoming chargebacks
            if( BankNumber.equals("3943") )
            {
              retVal = AchMerchChargeback.getDebitAmount();
            }
            else
            {
              retVal = AchMerchChargeback.getTotalAmount();
            }
            break;
          case 1:        // Incoming chargebacks
            if( BankNumber.equals("3943") )
            {
              retVal = -1 * AchMerchChargeback.getDebitAmount();
            }
            else
            {
              retVal = -1 * AchMerchChargeback.getTotalAmount();
              
            }
            break;
          case 2:        // Incoming chargebacks
            if( BankNumber.equals("3943") )
            {
              retVal = -1 * AchMerchChargeback.getCreditAmount();
            }
            break;
          case 3:        // Incoming chargebacks
            if( BankNumber.equals("3943") )
            {
              retVal = AchMerchChargeback.getCreditAmount();
            }
            break;
          default:
            break;
        }
      }
      catch( Exception e )
      {
      }
      
      return retVal;
    }
    
    public double getMerchDeposit( int fieldId )
    {
      double retVal = 0.0;
      
      try
      {
        FundingSummary fs = getPrevDayFundingSummary();
        switch( fieldId )
        {
          case 0:        // merch deposit
            retVal = AchMerchDeposit.getSponsoredAmount();
            break;
          case 1:        // amex merch deposit
            retVal = AchAmexDeposit.getTotalAmount();
            break;
          case 2:
            retVal = AchMerchDeposit.getNonSponsoredAmount();
            break;
          case 3:
            retVal = fs.getVisaAmount();
            break;
          case 4: 
            retVal = fs.getMCardAmount();
            break;
          case 5: 
            retVal = fs.getDebitAmount();
            break;
          case 6: 
            retVal = fs.getAmexOptbAmount() + fs.getSabreAmexOptbAmount();
            break;
          case 7: 
            retVal = fs.getAmexAmount();
            break;
          case 8: 
            retVal = fs.getDiscAmount();
            break;
          case 9: 
            retVal = fs.getSabreAmexAmount();
            break;
          case 10: 
            retVal = fs.getSabreUatpAmount();
            break;
          case 11: 
            retVal = -1 * fs.getRejectAmount();
            break;
          case 12: 
            retVal = -1 * fs.getStdSuspendedAmount();
            break;
          case 13: 
            retVal = -1 * fs.getAmexSuspendedAmount();
            break;
          case 14:
            retVal = fs.getStdReleasedAmount(); 
            break;
          case 15:
            retVal = fs.getAmexReleasedAmount();
            break;
          case 16: 
            retVal = -1 * (fs.getStdDisqualAmount() + fs.getAmexDisqualAmount());
            break;
          case 17: 
            retVal = -1 * (fs.getStdDiscountAmount() + fs.getAmexDiscountAmount()) ;
            break;
          case 18:
            retVal = -1 * (AchMerchDeposit.getTotalAmount() + AchAmexDeposit.getTotalAmount());
            retVal = retVal - (fs.getVisaAmount() + fs.getMCardAmount() + 
                     fs.getDebitAmount() + fs.getAmexAmount() + fs.getAmexOptbAmount() + fs.getDiscAmount() + fs.getSabreAmexAmount() + fs.getSabreAmexOptbAmount() + fs.getSabreUatpAmount() - 
                     fs.getRejectAmount() - fs.getStdSuspendedAmount() - fs.getAmexSuspendedAmount() + fs.getStdReleasedAmount() + 
                     fs.getAmexReleasedAmount() - fs.getStdDisqualAmount() - fs.getAmexDisqualAmount() - fs.getStdDiscountAmount() - 
                     fs.getAmexDiscountAmount() - fs.getFxMarkupAmount());
            retVal = Double.parseDouble(NumberFormatter.getDoubleString(retVal,"0.00"));
            break;
          case 19: 
            retVal = -1 * fs.getFxMarkupAmount();
            break;
          default:
            break;
        }
      }
      catch( Exception e )
      {
      }
      
      return retVal;
    }
    
    public double getClearingAmount( String cardType, int fieldId )
    {
      int fundingFieldId = -1;
      double retVal = 0.0;
      try
      {
        for( int i = 0; i < ClearingData.size(); ++ i )
        {
          ClearingSummary sm = (ClearingSummary)ClearingData.elementAt(i);
          if( !sm.getCardType().equals(cardType) )
          {
            continue;
          }
          
          switch( fieldId )
          {
            case 0:      // settlement amount
              retVal = sm.getActualAmount();
              break;
            case 1:      // clearing amount
              retVal = -1 * getFundingAmount(cardType);
              break;
            case 2:      // VS, MC, DS, Interchange Fees
              retVal = -1 * sm.getReimbursementFees();
              break;  
            case 3:      // VS, MC, DS, Assoc Fees
              retVal = -1 * sm.getAssociationFees();
              break;    
            case 4:      // VS, MC, DS, CB represented
              retVal = -1 * (sm.getRepresentedChargebacks() - sm.getRepresentedRejects());
              break;  
            case 5:      // VS, MC, DS, Incoming CB
              retVal = sm.getIncomingChargebacks();
              break;     
            case 6:      // VS, MC, DS, settlement difference
              retVal = sm.getSettlementAmount() - sm.getActualAmount();
              break;
            case 7:      // VS, MC, DS, funding difference
              retVal = getFundingAmount(cardType) - sm.getOutgoingAmount();
              break;
            case 8:      // VS, MC, DS, Reversals + Incoming Reversals
              retVal = sm.getReversals() + (-1 * sm.getIncomingReversals());
              break;  
            case 9:      // VS, MC, DS, Reprocessed Rejects
              retVal = -1 * sm.getReprocessedRejects();
              break;  
            case 10:     // VS, MC, DS, Incoming Rejects
              retVal = sm.getIncomingRejects();
              break;         
            case 11:     // VS, MC Fee Collection
              retVal = -1 * sm.getFeeCollectionAmount();
              break;
            case 12:     // VS, MC FX adjustment
              retVal = -1 * sm.getFxAdjustmentAmount();
              break;
            case 13:     // MC FX clearing variance
              retVal = 0.0;
              break;
            case 14:     // 3858 VS Cycle 2 Outgoing
              retVal = -1 * getPrevDayVsCarryoverAmount();
              break;
            case 15:     // 3858 VS Cycle 2 Incoming
              retVal = getVsCarryoverAmount();
              break;
            case 16:     // 3858 VS Carryover variance
              retVal = getPrevDayVsCarryoverAmount() -  getVsCarryoverAmount();
              break;
            default:
              break;
          }
        }
      }
      catch( Exception e )
      {
      }
      
      return retVal;
    }
    
    public double getFundingAmount( String cardType )
    {
      double      retVal      = 0.0;
      int         dayOfWeek   = -1;
      Calendar    cal         = Calendar.getInstance();
      
      cal.setTime(ActiveDate);
      dayOfWeek =  cal.get( Calendar.DAY_OF_WEEK);
      
      for( int i = 0; i < FundingData.size(); ++ i )
      {
        FundingSummary sm = (FundingSummary)FundingData.elementAt(i);
        // VS has clearing and funding on Sun, return only amount for Monday
        // MC/DS have no clearing on Sun, funding on Sun, so Sun clearing amount is included in Mon
        switch( dayOfWeek )
        {
          case Calendar.MONDAY:         
            if( cardType.equals("VS") )
            {
              retVal = (sm.getBatchDate().equals(ActiveDate) ? sm.getFundingAmount(cardType) : 0.0);
            }
            else
            {
              retVal += sm.getFundingAmount( cardType );
            }
            break;
          
          default:
            retVal = (sm.getBatchDate().equals(ActiveDate) ? sm.getFundingAmount(cardType) : 0.0);
            break;
        }
      }
      
      return retVal;
    }
    
    public FundingSummary getPrevDayFundingSummary()
    {
      return getFundingSummary( getPrevBatchDate() );
    }
    
    public FundingSummary getFundingSummary( Date batchDate )
    {
      FundingSummary retVal = null;
      
      for( int i = 0; i < FundingData.size(); ++ i )
      {
        FundingSummary sm = (FundingSummary)FundingData.elementAt(i);
        if( sm.getBatchDate().equals(batchDate) )
        {
          retVal = sm;
        }
      }
      
      return retVal;
    }
    
    public double getFundingAmount( int fieldId )
    {
      double retVal = 0.0;
      
      try
      {
        FundingSummary sm = getFundingSummary(ActiveDate);
        switch( fieldId )
        {
          case 0:  retVal = sm.getVisaAmount();           break;
          case 1:  retVal = sm.getMCardAmount();          break;
          case 2:  retVal = sm.getDebitAmount();          break;
          case 3:  retVal = sm.getAmexOptbAmount();       break;
          case 4:  retVal = sm.getAmexAmount();           break;
          case 5:  retVal = sm.getDiscAmount();           break;
          case 6:  retVal = sm.getSabreAmexAmount();      break;
          case 7:  retVal = sm.getSabreUatpAmount();      break;
          case 8:  retVal = -1 * sm.getVisaAmount();      break;
          case 9:  retVal = -1 * sm.getMCardAmount();     break;
          case 10: retVal = -1 * sm.getDebitAmount();     break;
          case 11: retVal = -1 * sm.getAmexOptbAmount();  break;
          case 12: retVal = -1 * sm.getAmexAmount();      break;
          case 13: retVal = -1 * sm.getDiscAmount();      break;
          case 14: retVal = -1 * sm.getSabreAmexAmount(); break;
          case 15: retVal = -1 * sm.getSabreUatpAmount(); break;
          default:                                        break;
        }
      }
      catch( Exception e )
      {
        e.printStackTrace();
      }
      
      return retVal;
    }
  }
  
  public class CarryoverSummary
  {
    public Date    BatchDate                  = null;
    public Date    SettlementDate             = null;
    public double  Amount                     = 0.0;
    
    public CarryoverSummary( ResultSet resultSet )
        throws java.sql.SQLException
    {
      BatchDate           = resultSet.getDate("batch_date");
      SettlementDate      = resultSet.getDate("settlement_date");
      Amount              = resultSet.getDouble("amount");
    }
    
    public double getAmount()          { return Amount; }
    public Date  getBatchDate()        { return BatchDate; }
    public Date  getSettlementDate()   { return SettlementDate; }

    public void  setAmount( double amount )               { Amount = amount; }
    public void  setBatchDate( Date batchDate )           { BatchDate = batchDate; }
    public void  setSettlementDate( Date settlementDate ) { SettlementDate = settlementDate; }
  }
  
  public class ClearingSummary
  {
    public String  CardType                   = null;
    public double  ActualAmount               = 0.0;
    public double  OutgoingAmount             = 0.0;
    public double  Reversals                  = 0.0;
    public double  IncomingReversals          = 0.0;
    public double  IncomingRejects            = 0.0;
    public double  ReprocessedRejects         = 0.0;
    public double  RepresentedRejects         = 0.0;
    public double  ReimbursementFees          = 0.0;
    public double  AssociationFees            = 0.0;
    public double  FeeCollectionAmount        = 0.0;
    public double  RepresentedChargebacks     = 0.0;
    public double  IncomingChargebacks        = 0.0;
    public double  SettlementAmount           = 0.0;
    public double  FxAdjustmentAmount         = 0.0;
    
    public ClearingSummary( ResultSet resultSet )
      throws java.sql.SQLException
    {
      CardType                   = resultSet.getString("card_type");
      ActualAmount               = resultSet.getDouble("actual_amount");
      OutgoingAmount             = resultSet.getDouble("outgoing_amount");
      Reversals                  = resultSet.getDouble("reversals_amount");
      IncomingReversals          = resultSet.getDouble("incoming_reversals_amount");
      IncomingRejects            = resultSet.getDouble("rejects_amount");
      ReprocessedRejects         = resultSet.getDouble("reprocessed_rejects_amount");
      RepresentedRejects         = resultSet.getDouble("represented_rejects_amount");
      ReimbursementFees          = resultSet.getDouble("reimbursement_fees");
      AssociationFees            = resultSet.getDouble("association_fees");
      FeeCollectionAmount        = resultSet.getDouble("fee_collection_amount");
      RepresentedChargebacks     = resultSet.getDouble("represented_chargebacks_amount");
      IncomingChargebacks        = resultSet.getDouble("incoming_chargebacks_amount");
      SettlementAmount           = resultSet.getDouble("settlement_amount");
      FxAdjustmentAmount         = resultSet.getDouble("fx_adjustment_amount");
    }
    
    public String  getCardType()                   { return CardType; }
    public double  getActualAmount()              { return ActualAmount; }
    public double  getOutgoingAmount()              { return OutgoingAmount; }
    public double  getFxAdjustmentAmount()         { return FxAdjustmentAmount; }
    public double  getReversals()          { return Reversals; }
    public double  getIncomingReversals()          { return IncomingReversals; }
    public double  getIncomingRejects()              { return IncomingRejects; }
    public double  getReprocessedRejects()   { return ReprocessedRejects; }
    public double  getRepresentedRejects()   { return RepresentedRejects; }
    public double  getReimbursementFees()          { return ReimbursementFees; }
    public double  getAssociationFees()            { return AssociationFees; }
    public double  getFeeCollectionAmount()        { return FeeCollectionAmount; }
    public double  getRepresentedChargebacks()  { return RepresentedChargebacks; }
    public double  getIncomingChargebacks()     { return IncomingChargebacks; }
    public double  getSettlementAmount()           { return SettlementAmount; }
  }
  
  public class DepositSummary
  {
    public String  EntryDesc           = null;
    public Date    PostDate            = null;
    public double  CreditAmount        = 0.0;
    public double  DebitAmount         = 0.0;
    public double  SponsoredAmount     = 0.0;
    public double  NonSponsoredAmount  = 0.0;
    public double  TotalAmount         = 0.0;
    
    public DepositSummary( ResultSet resultSet )
      throws java.sql.SQLException
    {
      EntryDesc       = resultSet.getString("entry_desc");
      PostDate        = resultSet.getDate("post_date");
      CreditAmount    = resultSet.getDouble("credit_amount");
      DebitAmount     = resultSet.getDouble("debit_amount");
      TotalAmount     = resultSet.getDouble("total_amount");
      try{ SponsoredAmount       = resultSet.getDouble("sponsored_amount"); } catch( Exception e ) {}
      try{ NonSponsoredAmount    = resultSet.getDouble("non_sponsored_amount"); } catch( Exception e ) {}
    }
    
    public String  getEntryDesc()           { return EntryDesc; }
    public Date    getAchPostDate()         { return PostDate; }
    public double  getCreditAmount()        { return CreditAmount; }
    public double  getDebitAmount()         { return DebitAmount; }
    public double  getSponsoredAmount()     { return SponsoredAmount; }
    public double  getNonSponsoredAmount()  { return NonSponsoredAmount; }
    public double  getTotalAmount()         { return TotalAmount; }
  }
  
  public class FundingSummary
  {
    public    Date        BatchDate              = null;
    public    double      VisaAmount             = 0.0;
    public    double      MCardAmount            = 0.0;
    public    double      DebitAmount            = 0.0;
    public    double      AmexAmount             = 0.0;
    public    double      DiscAmount             = 0.0;
    public    double      RejectAmount           = 0.0;
    public    double      SabreAmexAmount        = 0.0;
    public    double      SabreUatpAmount        = 0.0;
    public    double      FxMarkupAmount         = 0.0;
    public    double      TotalSettleAmount      = 0.0;
    public    double      StdSuspendedAmount     = 0.0;
    public    double      AmexSuspendedAmount    = 0.0;
    public    double      StdDisqualAmount       = 0.0;
    public    double      AmexDisqualAmount      = 0.0;
    public    double      StdDiscountAmount      = 0.0;
    public    double      AmexDiscountAmount     = 0.0;
    public    double      StdReleasedAmount      = 0.0;
    public    double      AmexReleasedAmount     = 0.0;
    public    double      StdFundingAmount       = 0.0;
    public    double      AmexFundingAmount      = 0.0;
    public    double      AmexOptbAmount         = 0.0;
    public    double      SabreAmexOptbAmount    = 0.0;
    public    double      AmexOptbSuspendedAmount= 0.0;
    public    double      AmexOptbDisqualAmount  = 0.0;
    public    double      AmexOptbDiscountAmount = 0.0;
    public    double      AmexOptbReleasedAmount = 0.0;
    public    double      AmexOptbFundingAmount  = 0.0;
    
    public FundingSummary( ResultSet resultSet )
      throws java.sql.SQLException
    {
      BatchDate            = resultSet.getDate("batch_date");
      VisaAmount           = resultSet.getDouble("vs_amount");
      MCardAmount          = resultSet.getDouble("mc_amount");
      DebitAmount          = resultSet.getDouble("db_amount");
      AmexAmount           = resultSet.getDouble("am_amount");
      DiscAmount           = resultSet.getDouble("ds_amount");
      SabreAmexAmount      = resultSet.getDouble("sabre_am_amount");
      SabreUatpAmount      = resultSet.getDouble("sabre_uatp_amount");
      RejectAmount         = resultSet.getDouble("rejects_amount");
      FxMarkupAmount       = resultSet.getDouble("fx_markup_amount");
      TotalSettleAmount    = resultSet.getDouble("total_amount");
      AmexOptbAmount       = resultSet.getDouble("am_optb_amount");
      SabreAmexOptbAmount  = resultSet.getDouble("sabre_am_optb_amount");
    }
    
    public FundingSummary()
    {
    }
    
    public Date   getBatchDate()               { return BatchDate; }
    public double getVisaAmount()              { return VisaAmount; }
    public double getMCardAmount()             { return MCardAmount; }
    public double getDebitAmount()             { return DebitAmount; }
    public double getAmexAmount()              { return AmexAmount; }
    public double getDiscAmount()              { return DiscAmount; }
    public double getRejectAmount()            { return RejectAmount; }
    public double getFxMarkupAmount()          { return FxMarkupAmount; }
    public double getSabreAmexAmount()         { return SabreAmexAmount; }
    public double getSabreUatpAmount()         { return SabreUatpAmount; }
    public double getTotalSettleAmount()       { return TotalSettleAmount; }
    public double getStdSuspendedAmount()      { return StdSuspendedAmount; }
    public double getAmexSuspendedAmount()     { return AmexSuspendedAmount; }
    public double getStdDisqualAmount()        { return StdDisqualAmount; }
    public double getAmexDisqualAmount()       { return AmexDisqualAmount; }
    public double getStdDiscountAmount()       { return StdDiscountAmount; }
    public double getAmexDiscountAmount()      { return AmexDiscountAmount; }
    public double getStdReleasedAmount()       { return StdReleasedAmount; }
    public double getAmexReleasedAmount()      { return AmexReleasedAmount; }
    public double getStdFundingAmount()        { return StdFundingAmount; }
    public double getAmexFundingAmount()       { return AmexFundingAmount; }
    public double getAmexOptbAmount()          { return AmexOptbAmount; }
    public double getSabreAmexOptbAmount()     { return SabreAmexOptbAmount; }
    public double getAmexOptbSuspendedAmount() { return AmexOptbSuspendedAmount; }
    public double getAmexOptbDisqualAmount()   { return AmexOptbDisqualAmount; }
    public double getAmexOptbDiscountAmount()  { return AmexOptbDiscountAmount; }
    public double getAmexOptbReleasedAmount()  { return AmexOptbReleasedAmount; }
    public double getAmexOptbFundingAmount()   { return AmexOptbFundingAmount; }

    public void setStdSuspendedAmount( double amount )     { StdSuspendedAmount = amount; }
    public void setAmexSuspendedAmount( double amount )    { AmexSuspendedAmount = amount; }
    public void setStdDisqualAmount( double amount )       { StdDisqualAmount = amount; }
    public void setAmexDisqualAmount( double amount )      { AmexDisqualAmount =amount ; }
    public void setStdDiscountAmount( double amount )      { StdDiscountAmount = amount; }
    public void setAmexDiscountAmount(double amount )      { AmexDiscountAmount = amount; }
    public void setStdReleasedAmount( double amount )      { StdReleasedAmount = amount; }
    public void setAmexReleasedAmount(double amount )      { AmexReleasedAmount = amount; }
    public void setStdFundingAmount( double amount )       { StdFundingAmount = amount; }
    public void setAmexFundingAmount(double amount )       { AmexFundingAmount = amount; }
    public void setAmexOptbSuspendedAmount(double amount ) { AmexOptbSuspendedAmount = amount; }
    public void setAmexOptbDisqualAmount(double amount )   { AmexOptbDisqualAmount = amount; }
    public void setAmexOptbDiscountAmount(double amount )  { AmexOptbDiscountAmount = amount; }
    public void setAmexOptbReleasedAmount(double amount )  { AmexOptbReleasedAmount = amount; }
    public void setAmexOptbFundingAmount(double amount )   { AmexOptbFundingAmount = amount; }
    
    public double getFundingAmount( String cardType )
    {
      double retVal = 0.0;
      
      if( cardType.equals("VS") )
      {
        retVal = VisaAmount;
      }
      else if( cardType.equals("MC") )
      {
        retVal = MCardAmount;
      }
      else if( cardType.equals("AM") )
      {
        retVal = AmexAmount;
      }
      else if( cardType.equals("DS") )
      {
        retVal = DiscAmount;
      }
      
      return retVal; 
    }
  }
  
  public class JeRecord
  {
    public int    RecordId         = -1;
    public String RecordName       = null;
    
    public JeRecord( ResultSet resultSet )
        throws java.sql.SQLException
    {
      RecordId        = resultSet.getInt("record_id");
      RecordName      = resultSet.getString("record_name");
    }
    
    public int     getRecordId()    { return RecordId; }
    public String  getRecordName()  { return RecordName; }
  }
  
  public class SalesActivity
  {
    public long    SalesCount     = -1L;
    public double  SalesAmount    = 0.0;
    
    public SalesActivity( ResultSet resultSet )
        throws java.sql.SQLException
    {
      SalesCount        = resultSet.getLong("sales_count");
      SalesAmount       = resultSet.getDouble("sales_amount");
    }
    
    public long    getSalesCount()    { return SalesCount; }
    public double  getSalesAmount()   { return SalesAmount; }
  }
  
  public NetSuiteDailyUpload()
  {
  }
  
  private void setDepositData( ResultSet rs )
  {
    try
    {
      DepositSummary ds = new DepositSummary( rs );
      for( int i = 0; i < ReportRows.size(); ++i )
      {
        SummaryData sm = (SummaryData)ReportRows.elementAt(i);
        if( sm.getBankNumber().equals(rs.getString("bank_number")) )
        {
          sm.setDepositData( ds );
        }
      }
    }
    catch( Exception e )
    {
      e.printStackTrace();
    }
  }
  
  public JournalEntry buildSalesActivityRecord()
  {
    double           totSalesAmount   = 0.0;
    long             totSalesCount    = 0L;
    JournalEntry     journalEntry     = null;
    RecordRef        account          = null; 
    CustomFieldRef   fieldRef         = null;
    
    try
    {
      journalEntry  = buildJeRecord();
      
      for( int i = 0; i < ReportRows.size(); ++i )
      {
        SummaryData sm   = (SummaryData)ReportRows.elementAt(i);
        totSalesAmount   += sm.getSalesData().getSalesAmount();
        totSalesCount    += sm.getSalesData().getSalesCount();
      }
      
      account = new RecordRef();
      account.setInternalId( DAILY_SALES_ACTIVITY_ACCOUNT_ID );
      CustomFieldRef[]  custFields     = new CustomFieldRef[JE_SALES_ACTIVITY_CUSTOM_FIELDS.length];
      
      for( int k = 0; k < JE_SALES_ACTIVITY_CUSTOM_FIELDS.length ; ++k )
      {
        switch( k )
        {
          case 0:
            fieldRef = new LongCustomFieldRef( JE_SALES_ACTIVITY_CUSTOM_FIELDS[k][1],"0", totSalesCount );
            break;
          case 1:
            totSalesAmount = Double.parseDouble(NumberFormatter.getDoubleString(totSalesAmount,"0.00"));
            fieldRef = new StringCustomFieldRef( JE_SALES_ACTIVITY_CUSTOM_FIELDS[k][1], "0", String.valueOf(totSalesAmount) );
            break;
        }
        custFields[k] = fieldRef;
      }
      
      CustomFieldList   custFieldList  = new CustomFieldList(custFields);
      
      JournalEntryLine[] jeLines  = new JournalEntryLine[2];
      for( int j = 0; j < jeLines.length; ++j )
      {
        jeLines[j] = new JournalEntryLine();
        jeLines[j].setAccount( account );
        switch( j )
        {
          case 0:
            jeLines[j].setDebit( 0.0 );
            jeLines[j].setCustomFieldList( custFieldList );
            break;
          case 1:
            jeLines[j].setCredit( 0.0 );
            break;
        }
      }
      
      JournalEntryLineList jeLineList = new JournalEntryLineList();
      jeLineList.setLine(jeLines);
      journalEntry.setLineList(jeLineList);
    }
    catch( Exception e )
    {
      logEntry( "buildSalesActivityRecord()", e.toString() );
    }
    
    return journalEntry;
  }
  
  public JournalEntry buildJeRecord()
  {
    Calendar         cal            = null; 
    JournalEntry     journalEntry   = null;
    RecordRef        currency       = new RecordRef();
    RecordRef        subsidiary     = new RecordRef();
    RecordRef        custForm       = new RecordRef();

    try
    {
      journalEntry  = new JournalEntry();
      cal = Calendar.getInstance();
      cal.setTime( ActiveDate );
      currency.setInternalId( CURRENCY_ID_USD ) ;
      currency.setType( RecordType.currency );
      subsidiary.setInternalId( SUBSIDIARY[0][0] );
      custForm.setInternalId(JE_CUSTOM_FORMS[0][0]);
      
      journalEntry.setTranDate( cal );
      journalEntry.setCurrency( currency );
      journalEntry.setExchangeRate( CURRENCY_XRATE_USD );
      journalEntry.setSubsidiary( subsidiary );
      journalEntry.setCustomForm( custForm );

    }
    catch( Exception e )
    {
    }
    
    return journalEntry;
  }
  
  public JournalEntry buildJeRecord( SummaryData sm, int recordId )
  {
    String                 entryDesc      = null;
    String                 accountId      = null;
    double                 amount         = 0.0;
    Vector                 rowData        = null;
    Vector                 lineList       = new Vector();
    JournalEntry           journalEntry   = null;
    LineItem               lineItem       = null;
    JournalEntryLineList   jeLineList     = null;
    JournalEntryLine       jeLine         = null;
    JournalEntryLine[]     jeLines        = null;
    RecordRef              account        = null; 
    
    try
    {
      journalEntry  = buildJeRecord();
      log.debug("buildJeRecord() recordId: " + recordId);

      if( journalEntry == null )
      {
        return null;
      }
      
      entryDesc = DAILY_JE_RECORDS[recordId][1];
      switch( recordId )
      {
        case 0:
          rowData = sm.getTotalFunding();
          entryDesc += " " + sm.getBankNumber();
          break;
        case 1:
          rowData = sm.getVisaClearing();
          entryDesc += " " + sm.getBankNumber();
          break;
        case 2:
          rowData = sm.getMastercardClearing();
          entryDesc += " " + sm.getBankNumber();
          break;
        case 3:
          rowData = sm.getDiscoverClearing();
          break;
        case 4:
          rowData = sm.getIncomingChargebacks();
          break;
        case 5:
          rowData = sm.getDebitAdjustments();
          break;
        case 6:
          rowData = sm.getMerchantDeposits();
          entryDesc += " " + sm.getBankNumber();
          break;
        default:
          break;
      }
      
      if( rowData == null )
      {
        return null; 
      }
      
      for( int i = 0; i < rowData.size(); i++ )
      {
        lineItem   = (LineItem)rowData.elementAt(i);
        accountId  = lineItem.getItemId();
        amount     = Double.parseDouble(NumberFormatter.getDoubleString(lineItem.getDoubleAmount(),"0.00"));
        if( !accountId.equals("-1") )
        {
          account = new RecordRef();
          account.setInternalId( accountId );
          jeLine = new JournalEntryLine();
          jeLine.setMemo(entryDesc);
          jeLine.setAccount( account );
          if( amount < 0 )
          {
            log.debug("jeLine set amount = " + (amount * -1));
            jeLine.setCredit( amount * -1 );
          }
          else
          {
            log.debug("jeLine set amount = " + amount);
            jeLine.setDebit( amount );
          }
          lineList.add(jeLine);
        }
      }
      
      jeLines    = new JournalEntryLine[lineList.size()];
      for( int j = 0; j < lineList.size(); j++ )
      {
        jeLines[j] = (JournalEntryLine)lineList.elementAt(j);
      }
      jeLineList = new JournalEntryLineList(jeLines, false);
      journalEntry.setLineList(jeLineList);
    }
    catch( Exception e )
    {
      logEntry( "buildJeRecord()", e.toString() );
    }
    
    return journalEntry;
  }
  
  public void buildJeList()
  {
    Vector jeList   = new Vector();
    
    try
    {
      for( int i = 0; i < ReportRows.size(); ++i )
      {
        SummaryData sm = (SummaryData)ReportRows.elementAt(i);
        if( RecordId == -1 )
        {
          for( int j = 0; j < DAILY_JE_RECORDS.length-1; ++j )
          {
            jeList.add(buildJeRecord(sm, j));
          }
        }
        else
        {
          log.debug("jeList.add(buildJeRecord(sm, RecordId))");
          jeList.add(buildJeRecord(sm, RecordId));
        }
      }
      
      if( BankNumber == -1 && (RecordId == -1 || RecordId == 7) )
      {
        jeList.add(buildSalesActivityRecord());
      }
      
      RecordList = new Record[jeList.size()];
      for( int j = 0; j < jeList.size(); j++ )
      {
        RecordList[j] = (JournalEntry)jeList.elementAt(j);
      }
    }
    catch( Exception e )
    {
      e.printStackTrace();
      logEntry( "buildJeList()", e.toString() );
    }
  }
  
  public double formatNumber( double amount )
  {
    double retVal = 0.0;
    try
    {
      retVal = Double.parseDouble(NumberFormatter.getDoubleString(amount,"0.00"));
    }
    catch(Exception e)
    {
    }
    
    return retVal;
  }
  
  public Date getPrevBatchDate()
  {
    Date   prevDate    = null;
    
    Calendar cal = Calendar.getInstance();
    cal.setTime(ActiveDate);
    cal.add(Calendar.DAY_OF_MONTH, -1);
    prevDate = new java.sql.Date( cal.getTime().getTime() );
    
    return prevDate;
  }
  
  public void loadData()
  {
    ResultSetIterator   it          = null;
    ResultSet           rs          = null;

    try
    {
      loadJeSummary();
      loadFundingSummary();
      loadClearingSummary();
      loadVisaCarryover();
      loadDepositSummary();
      loadJeDetails();
      loadSalesActivity();
    }
    catch( Exception e )
    {
      e.printStackTrace();
    }
  }
  
  public void loadJeSummary()
  {
    ResultSetIterator   it          = null;
    ResultSet           rs          = null;
    
    try
    {
{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.NetSuiteDailyUpload",SQL_GET_NS_BANKNUMBER);
   // set IN parameters
   __sJT_st.setInt(1,BankNumber);
   __sJT_st.setInt(2,BankNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.startup.NetSuiteDailyUpload",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1131^7*/
      
      rs = it.getResultSet();
      while( rs.next() )
      {
        ReportRows.add( new SummaryData(rs.getString("bank_number") ) );
      }
      
{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.startup.NetSuiteDailyUpload",SQL_GET_JE_SUMMARY);
   // set IN parameters
   __sJT_st.setInt(1,BankNumber);
   __sJT_st.setInt(2,BankNumber);
   __sJT_st.setInt(3,RecordId);
   __sJT_st.setInt(4,RecordId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.startup.NetSuiteDailyUpload",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1150^7*/
      
      rs = it.getResultSet();
      while( rs.next() )
      {
        for( int i = 0; i < ReportRows.size(); ++i )
        {
          SummaryData sm = (SummaryData)ReportRows.elementAt(i);
          if( sm.getBankNumber().equals(rs.getString("bank_number")) )
          {
            sm.addJeRecord(new JeRecord(rs));
          }
        }
      }
      
      it.close();
    }
    catch( java.sql.SQLException e )
    {
      logEntry( "loadJeSummary()", e.toString() );
    }
    finally
    {
      try { it.close(); } catch( Exception e ) { }
    }
  }
  
  public void loadJeDetails()
  {
    ResultSetIterator   it          = null;
    ResultSet           rs          = null;
    
    try
    {
 
{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.startup.NetSuiteDailyUpload",SQL_GET_JE_DETAILS);
   // set IN parameters
   __sJT_st.setInt(1,RecordId);
   __sJT_st.setInt(2,DAILY_JE_SALES_ACTIVITY);
   __sJT_st.setInt(3,BankNumber);
   __sJT_st.setInt(4,BankNumber);
   __sJT_st.setInt(5,RecordId);
   __sJT_st.setInt(6,RecordId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.startup.NetSuiteDailyUpload",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1198^7*/
      
      rs = it.getResultSet();
      while( rs.next() )
      {
        int      recordId   = rs.getInt("record_id");
        LineItem item       = new LineItem(rs.getString("account_id"), rs.getString("account_desc"), 0.0 );
        for( int i = 0; i < ReportRows.size(); ++i )
        {
          SummaryData sm = (SummaryData)ReportRows.elementAt(i);
          if( sm.getBankNumber().equals(rs.getString("bank_number")) )
          {
            sm.addJournalEntryLine(item, recordId, rs.getInt("field_order"));
          }
        }
      }
      
      it.close();
    }
    catch( java.sql.SQLException e )
    {
      logEntry( "loadJeDetails()", e.toString() );
    }
    finally
    {
      try { it.close(); } catch( Exception e ) { }
    }
  }
  
  public double loadVisaCarryover()
  {
    ResultSetIterator   it          = null;
    ResultSet           rs          = null;
    double              retVal      = 0.0;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:1235^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  vs.bank_number        as bank_number,
//                  vs.batch_date         as batch_date,
//                  vs.settlement_date    as settlement_date,
//                  sum(decode(vs.debit_credit_indicator,'C',-1,1) * vs.transaction_amount)  
//                                        as amount
//          from    visa_settlement vs
//          where   :RecordId != :DAILY_JE_SALES_ACTIVITY
//                  and (:BankNumber = -1  or :BankNumber = 3858)
//                  and vs.merchant_number in (select mf.merchant_number from mif mf where mf.bank_number = 3858)
//                  and vs.batch_date between :ActiveDate -1 and :ActiveDate
//                  and vs.batch_date != nvl(vs.settlement_date,'31-dec-9999')
//                  and not vs.output_filename like 'hold%'
//          group by vs.bank_number, vs.batch_date, vs.settlement_date
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  vs.bank_number        as bank_number,\n                vs.batch_date         as batch_date,\n                vs.settlement_date    as settlement_date,\n                sum(decode(vs.debit_credit_indicator,'C',-1,1) * vs.transaction_amount)  \n                                      as amount\n        from    visa_settlement vs\n        where    :1  !=  :2 \n                and ( :3  = -1  or  :4  = 3858)\n                and vs.merchant_number in (select mf.merchant_number from mif mf where mf.bank_number = 3858)\n                and vs.batch_date between  :5  -1 and  :6 \n                and vs.batch_date != nvl(vs.settlement_date,'31-dec-9999')\n                and not vs.output_filename like 'hold%'\n        group by vs.bank_number, vs.batch_date, vs.settlement_date";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.startup.NetSuiteDailyUpload",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,RecordId);
   __sJT_st.setInt(2,DAILY_JE_SALES_ACTIVITY);
   __sJT_st.setInt(3,BankNumber);
   __sJT_st.setInt(4,BankNumber);
   __sJT_st.setDate(5,ActiveDate);
   __sJT_st.setDate(6,ActiveDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.startup.NetSuiteDailyUpload",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1250^7*/
      
      rs = it.getResultSet();
      while( rs.next() )
      {
        CarryoverSummary cs = new CarryoverSummary( rs );
        
        for( int i = 0; i < ReportRows.size(); ++i )
        {
          SummaryData sm = (SummaryData)ReportRows.elementAt(i);
          if( sm.getBankNumber().equals(rs.getString("bank_number")) )
          {
            sm.addVsCarryover( cs );
          }
        }
      }
    }
    catch( java.sql.SQLException e )
    {
    }
    finally
    {
      try { it.close(); } catch( Exception e ) { }
    }
    
    return retVal;
  }
  
  public void loadClearingSummary()
  {
    ResultSetIterator   it          = null;
    ResultSet           rs          = null;
    
    try
    {
{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.startup.NetSuiteDailyUpload",SQL_GET_CLEARING_SUMMARY);
   // set IN parameters
   __sJT_st.setInt(1,RecordId);
   __sJT_st.setInt(2,DAILY_JE_SALES_ACTIVITY);
   __sJT_st.setInt(3,BankNumber);
   __sJT_st.setInt(4,BankNumber);
   __sJT_st.setDate(5,ActiveDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.startup.NetSuiteDailyUpload",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1322^7*/
      
      rs = it.getResultSet();
      while( rs.next() )
      {
        ClearingSummary cs = new ClearingSummary( rs );
        for( int i = 0; i < ReportRows.size(); ++i )
        {
          SummaryData sm = (SummaryData)ReportRows.elementAt(i);
          if( sm.getBankNumber().equals(rs.getString("bank_number")) )
          {
            sm.addClearingData( cs );
          }
        }
      }
      it.close();
    }
    catch( java.sql.SQLException e )
    {
      e.printStackTrace();
      logEntry( "loadClearingSummary()", e.toString() );
    }
    finally
    {
      try { it.close(); } catch( Exception e ) { }
    }
  }
  
  public void loadDepositSummary()
  {
    ResultSetIterator   it          = null;
    ResultSet           rs          = null;
    
    try
    {
{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.startup.NetSuiteDailyUpload",SQL_GET_DEPOSIT_SUMMARY);
   // set IN parameters
   __sJT_st.setInt(1,RecordId);
   __sJT_st.setInt(2,DAILY_JE_SALES_ACTIVITY);
   __sJT_st.setDate(3,ActiveDate);
   __sJT_st.setInt(4,BankNumber);
   __sJT_st.setInt(5,BankNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.startup.NetSuiteDailyUpload",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1390^7*/

      rs = it.getResultSet();
      while( rs.next() )
      {
        setDepositData(rs);
      }
      
      // 3858 and 3941 MERCH ADJ
      /*@lineinfo:generated-code*//*@lineinfo:1399^7*/

//  ************************************************************
//  #sql [Ctx] it = { select substr(trim(ad.load_filename), 5, 4)      as bank_number,
//                 ad.entry_description                      as entry_desc,
//                 ad.post_date                              as post_date,
//                 sum( decode(adtc.debit_credit_indicator, 'C', 1, 0) 
//                      * ad.ach_amount )                    as credit_amount,
//                 sum( decode(adtc.debit_credit_indicator, 'D', 1, 0) 
//                      * ad.ach_amount )                    as debit_amount,
//                 sum( decode(adtc.debit_credit_indicator, 'C', -1, 1) 
//                      * ad.ach_amount )                    as total_amount
//          from    ach_trident_detail    ad,
//                  ach_detail_tran_code  adtc,
//                  mif                   mf,
//                  bankserv_ach_descriptions bad
//          where   :RecordId != :DAILY_JE_SALES_ACTIVITY 
//                  and ad.post_date = :ActiveDate - 2
//                  and ad.entry_description = 'MERCH ADJ'
//                  and (ad.load_filename like 'mach394100000%' or
//                       ad.load_filename like 'mach385800000%')
//                  and nvl(ad.disqualified, 'N') = 'N'
//                  and ad.transaction_code = adtc.ach_detail_trans_code
//                  and ad.merchant_number = mf.merchant_number
//                  and (:BankNumber = -1 or mf.bank_number = :BankNumber) 
//                  and (ad.post_date > '17-Dec-2009' or  nvl(mf.test_account,'N') = 'N')
//                  and ad.manual_description_id = bad.description_id(+)
//          group by  ad.entry_description, ad.post_date, substr(trim(ad.load_filename), 5, 4)
//          order by substr(trim(ad.load_filename), 5, 4), ad.entry_description
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select substr(trim(ad.load_filename), 5, 4)      as bank_number,\n               ad.entry_description                      as entry_desc,\n               ad.post_date                              as post_date,\n               sum( decode(adtc.debit_credit_indicator, 'C', 1, 0) \n                    * ad.ach_amount )                    as credit_amount,\n               sum( decode(adtc.debit_credit_indicator, 'D', 1, 0) \n                    * ad.ach_amount )                    as debit_amount,\n               sum( decode(adtc.debit_credit_indicator, 'C', -1, 1) \n                    * ad.ach_amount )                    as total_amount\n        from    ach_trident_detail    ad,\n                ach_detail_tran_code  adtc,\n                mif                   mf,\n                bankserv_ach_descriptions bad\n        where    :1  !=  :2  \n                and ad.post_date =  :3  - 2\n                and ad.entry_description = 'MERCH ADJ'\n                and (ad.load_filename like 'mach394100000%' or\n                     ad.load_filename like 'mach385800000%')\n                and nvl(ad.disqualified, 'N') = 'N'\n                and ad.transaction_code = adtc.ach_detail_trans_code\n                and ad.merchant_number = mf.merchant_number\n                and ( :4  = -1 or mf.bank_number =  :5 ) \n                and (ad.post_date > '17-Dec-2009' or  nvl(mf.test_account,'N') = 'N')\n                and ad.manual_description_id = bad.description_id(+)\n        group by  ad.entry_description, ad.post_date, substr(trim(ad.load_filename), 5, 4)\n        order by substr(trim(ad.load_filename), 5, 4), ad.entry_description";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.startup.NetSuiteDailyUpload",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,RecordId);
   __sJT_st.setInt(2,DAILY_JE_SALES_ACTIVITY);
   __sJT_st.setDate(3,ActiveDate);
   __sJT_st.setInt(4,BankNumber);
   __sJT_st.setInt(5,BankNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"6com.mes.startup.NetSuiteDailyUpload",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1427^7*/
      
      rs = it.getResultSet();
      while( rs.next() )
      {
        setDepositData(rs);
      }
    }
    catch( java.sql.SQLException e )
    {
      e.printStackTrace();
      logEntry( "loadDepositSummary()", e.toString() );
    }
    finally
    {
      try { it.close(); } catch( Exception e ) { }
    }
  }
  
  public void loadFundingSummary()
  {
    ResultSetIterator   it          = null;
    ResultSet           rs          = null;
    Date                prevBatchDate = getPrevBatchDate();
    
    try
    {
{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.startup.NetSuiteDailyUpload",SQL_GET_FUNDING_SUMMARY);
   // set IN parameters
   __sJT_st.setInt(1,RecordId);
   __sJT_st.setInt(2,DAILY_JE_SALES_ACTIVITY);
   __sJT_st.setInt(3,BankNumber);
   __sJT_st.setInt(4,BankNumber);
   __sJT_st.setDate(5,ActiveDate);
   __sJT_st.setDate(6,ActiveDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"7com.mes.startup.NetSuiteDailyUpload",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1503^7*/
     
      rs = it.getResultSet();
      while( rs.next() )
      {
        FundingSummary fsm = new FundingSummary(rs);
        for( int i = 0; i < ReportRows.size(); ++i )
        {
          SummaryData sm = (SummaryData)ReportRows.elementAt(i);
          if( sm.getBankNumber().equals(rs.getString("bank_number")) )
          {
            sm.addFundingData( fsm );
          }
        }
      }
      it.close();

      /*@lineinfo:generated-code*//*@lineinfo:1520^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  ats.bank_number                           as bank_number,
//                  ats.batch_date                            as batch_date,
//                  sum( case when get_cpd_ach(ats.batch_date) = ats.post_date_actual then 1 else 0 end
//                       * case when ats.entry_description = 'MERCH DEP' then 1 else 0 end
//                       * case when not ats.disqualified_date is null and get_cpd_ach(ats.disqualified_date) = get_cpd_ach(ats.batch_date) then 0 else 1 end
//                       * decode(adtc.debit_credit_indicator, 'C', 1, -1) 
//                       * ats.ach_amount )                   as std_standard_amount,
//                  sum( case when get_cpd_ach(ats.batch_date) = ats.post_date_actual then 1 else 0 end
//                       * case when ats.entry_description = 'AMEX DEP' then 1 else 0 end
//                       * case when not ats.disqualified_date is null and get_cpd_ach(ats.disqualified_date) = get_cpd_ach(ats.batch_date) then 0 else 1 end
//                       * decode(adtc.debit_credit_indicator, 'C', 1, -1) 
//                       * ats.ach_amount )                   as amex_standard_amount,
//                  sum( case when get_cpd_ach(ats.batch_date) != ats.post_date_actual then 1 else 0 end
//                       * case when ats.entry_description = 'MERCH DEP' then 1 else 0 end
//                       * case when not ats.disqualified_date is null and get_cpd_ach(ats.disqualified_date) = get_cpd_ach(ats.batch_date) then 0 else 1 end
//                       * decode(adtc.debit_credit_indicator, 'C', 1, -1) 
//                       * ats.ach_amount )                   as std_suspended_amount,
//                  sum( case when get_cpd_ach(ats.batch_date) != ats.post_date_actual then 1 else 0 end
//                       * case when ats.entry_description = 'AMEX DEP' then 1 else 0 end
//                       * case when not ats.disqualified_date is null and get_cpd_ach(ats.disqualified_date) = get_cpd_ach(ats.batch_date) then 0 else 1 end
//                       * decode(adtc.debit_credit_indicator, 'C', 1, -1) 
//                       * ats.ach_amount )                   as amex_suspended_amount,
//                  sum( case when not ats.disqualified_date is null and get_cpd_ach(ats.disqualified_date) = get_cpd_ach(ats.batch_date) then 1 else 0 end
//                       * case when ats.entry_description = 'MERCH DEP' then 1 else 0 end
//                       * decode(adtc.debit_credit_indicator, 'C', 1, -1) 
//                       * ats.ach_amount )                   as std_disqual_amount,
//                  sum( case when not ats.disqualified_date is null and get_cpd_ach(ats.disqualified_date) = get_cpd_ach(ats.batch_date) then 1 else 0 end
//                       * case when ats.entry_description = 'AMEX DEP' then 1 else 0 end
//                       * decode(adtc.debit_credit_indicator, 'C', 1, -1) 
//                       * ats.ach_amount )                   as amex_disqual_amount,
//                  sum( case when ats.entry_description = 'MERCH DEP' then 1 else 0 end
//                       * (ats.sales_amount - ats.credits_amount) )  
//                                                            as std_gross_amount,
//                  sum( case when ats.entry_description = 'AMEX DEP' then 1 else 0 end
//                       * (ats.sales_amount - ats.credits_amount) )  
//                                                            as amex_gross_amount,
//                  sum( case when ats.entry_description = 'MERCH DEP' then 1 else 0 end
//                       * (ats.daily_discount_amount + ats.daily_ic_amount) )  
//                                                            as std_disc_ic_amount,    
//                  sum( case when ats.entry_description = 'AMEX DEP' then 1 else 0 end
//                       * (ats.daily_discount_amount + ats.daily_ic_amount) )  
//                                                            as amex_disc_ic_amount,
//                  sum( case when ats.entry_description = 'MERCH DEP' then 1 else 0 end
//                       * decode(adtc.debit_credit_indicator, 'C', 1, -1)
//                       * ats.ach_amount )                   as std_net_amount,
//                  sum( case when ats.entry_description = 'AMEX DEP' then 1 else 0 end
//                       * decode(adtc.debit_credit_indicator, 'C', 1, -1)
//                       * ats.ach_amount )                   as amex_net_amount
//          from    ach_trident_statement   ats,
//                  ach_detail_tran_code    adtc
//          where   :RecordId != :DAILY_JE_SALES_ACTIVITY
//                  and ((:BankNumber = -1 and ats.bank_number in (3941,3943,3858)) or ats.bank_number = :BankNumber)
//                  and ats.batch_date = :prevBatchDate
//                  and ats.post_date between :prevBatchDate-15 and :prevBatchDate+180
//                  and ( ats.entry_description = 'AMEX DEP' 
//                        or ats.entry_description = nvl('MERCH DEP', uid))
//                  and adtc.ach_detail_trans_code = ats.transaction_code
//          group by ats.bank_number, ats.batch_date
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  ats.bank_number                           as bank_number,\n                ats.batch_date                            as batch_date,\n                sum( case when get_cpd_ach(ats.batch_date) = ats.post_date_actual then 1 else 0 end\n                     * case when ats.entry_description = 'MERCH DEP' then 1 else 0 end\n                     * case when not ats.disqualified_date is null and get_cpd_ach(ats.disqualified_date) = get_cpd_ach(ats.batch_date) then 0 else 1 end\n                     * decode(adtc.debit_credit_indicator, 'C', 1, -1) \n                     * ats.ach_amount )                   as std_standard_amount,\n                sum( case when get_cpd_ach(ats.batch_date) = ats.post_date_actual then 1 else 0 end\n                     * case when ats.entry_description = 'AMEX DEP' then 1 else 0 end\n                     * case when not ats.disqualified_date is null and get_cpd_ach(ats.disqualified_date) = get_cpd_ach(ats.batch_date) then 0 else 1 end\n                     * decode(adtc.debit_credit_indicator, 'C', 1, -1) \n                     * ats.ach_amount )                   as amex_standard_amount,\n                sum( case when get_cpd_ach(ats.batch_date) != ats.post_date_actual then 1 else 0 end\n                     * case when ats.entry_description = 'MERCH DEP' then 1 else 0 end\n                     * case when not ats.disqualified_date is null and get_cpd_ach(ats.disqualified_date) = get_cpd_ach(ats.batch_date) then 0 else 1 end\n                     * decode(adtc.debit_credit_indicator, 'C', 1, -1) \n                     * ats.ach_amount )                   as std_suspended_amount,\n                sum( case when get_cpd_ach(ats.batch_date) != ats.post_date_actual then 1 else 0 end\n                     * case when ats.entry_description = 'AMEX DEP' then 1 else 0 end\n                     * case when not ats.disqualified_date is null and get_cpd_ach(ats.disqualified_date) = get_cpd_ach(ats.batch_date) then 0 else 1 end\n                     * decode(adtc.debit_credit_indicator, 'C', 1, -1) \n                     * ats.ach_amount )                   as amex_suspended_amount,\n                sum( case when not ats.disqualified_date is null and get_cpd_ach(ats.disqualified_date) = get_cpd_ach(ats.batch_date) then 1 else 0 end\n                     * case when ats.entry_description = 'MERCH DEP' then 1 else 0 end\n                     * decode(adtc.debit_credit_indicator, 'C', 1, -1) \n                     * ats.ach_amount )                   as std_disqual_amount,\n                sum( case when not ats.disqualified_date is null and get_cpd_ach(ats.disqualified_date) = get_cpd_ach(ats.batch_date) then 1 else 0 end\n                     * case when ats.entry_description = 'AMEX DEP' then 1 else 0 end\n                     * decode(adtc.debit_credit_indicator, 'C', 1, -1) \n                     * ats.ach_amount )                   as amex_disqual_amount,\n                sum( case when ats.entry_description = 'MERCH DEP' then 1 else 0 end\n                     * (ats.sales_amount - ats.credits_amount) )  \n                                                          as std_gross_amount,\n                sum( case when ats.entry_description = 'AMEX DEP' then 1 else 0 end\n                     * (ats.sales_amount - ats.credits_amount) )  \n                                                          as amex_gross_amount,\n                sum( case when ats.entry_description = 'MERCH DEP' then 1 else 0 end\n                     * (ats.daily_discount_amount + ats.daily_ic_amount) )  \n                                                          as std_disc_ic_amount,    \n                sum( case when ats.entry_description = 'AMEX DEP' then 1 else 0 end\n                     * (ats.daily_discount_amount + ats.daily_ic_amount) )  \n                                                          as amex_disc_ic_amount,\n                sum( case when ats.entry_description = 'MERCH DEP' then 1 else 0 end\n                     * decode(adtc.debit_credit_indicator, 'C', 1, -1)\n                     * ats.ach_amount )                   as std_net_amount,\n                sum( case when ats.entry_description = 'AMEX DEP' then 1 else 0 end\n                     * decode(adtc.debit_credit_indicator, 'C', 1, -1)\n                     * ats.ach_amount )                   as amex_net_amount\n        from    ach_trident_statement   ats,\n                ach_detail_tran_code    adtc\n        where    :1  !=  :2 \n                and (( :3  = -1 and ats.bank_number in (3941,3943,3858,3003)) or ats.bank_number =  :4 )\n                and ats.batch_date =  :5 \n                and ats.post_date between  :6 -15 and  :7 +180\n                and ( ats.entry_description = 'AMEX DEP' \n                      or ats.entry_description = nvl('MERCH DEP', uid))\n                and adtc.ach_detail_trans_code = ats.transaction_code\n        group by ats.bank_number, ats.batch_date";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.startup.NetSuiteDailyUpload",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,RecordId);
   __sJT_st.setInt(2,DAILY_JE_SALES_ACTIVITY);
   __sJT_st.setInt(3,BankNumber);
   __sJT_st.setInt(4,BankNumber);
   __sJT_st.setDate(5,prevBatchDate);
   __sJT_st.setDate(6,prevBatchDate);
   __sJT_st.setDate(7,prevBatchDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"8com.mes.startup.NetSuiteDailyUpload",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1580^7*/
      
      rs = it.getResultSet();
      while( rs.next() )
      {
        for( int i = 0; i < ReportRows.size(); ++i )
        {
          SummaryData sm = (SummaryData)ReportRows.elementAt(i);
          if( sm.getBankNumber().equals(rs.getString("bank_number")) )
          {
            FundingSummary fs = sm.getFundingSummary(prevBatchDate);
            fs.setStdSuspendedAmount( rs.getDouble("std_suspended_amount"));
            fs.setAmexSuspendedAmount( rs.getDouble("amex_suspended_amount"));
            fs.setStdDisqualAmount( rs.getDouble("std_disqual_amount"));
            fs.setAmexDisqualAmount( rs.getDouble("amex_disqual_amount"));
            fs.setStdDiscountAmount(rs.getDouble("std_disc_ic_amount"));
            fs.setAmexDiscountAmount(rs.getDouble("amex_disc_ic_amount"));
          }
        }
      }
      it.close();

      /*@lineinfo:generated-code*//*@lineinfo:1602^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  ats.bank_number                           as bank_number,
//                  ad.transmission_date                      as batch_date,
//                  sum( case when get_cpd_ach(ats.batch_date) = ats.post_date_actual then 1 else 0 end ) 
//                                                            as standard_count, 
//                  sum( case when get_cpd_ach(ats.batch_date) = ats.post_date_actual then 1 else 0 end 
//                       * decode(adtc.debit_credit_indicator, 'C', 1, -1) 
//                       * ats.ach_amount )                   as standard_amount, 
//                  sum( case when get_cpd_ach(ats.batch_date) != ats.post_date_actual then 1 else 0 end 
//                       * case when ats.entry_description = 'MERCH DEP' then 1 else 0 end 
//                       * case when ats.disqualified = 'Y' then 0 else 1 end ) 
//                                                            as std_released_count, 
//                  sum( case when get_cpd_ach(ats.batch_date) != ats.post_date_actual then 1 else 0 end 
//                       * case when ats.entry_description = 'MERCH DEP' then 1 else 0 end
//                       * decode(adtc.debit_credit_indicator, 'C', 1, -1) 
//                       * decode(nvl(ats.disqualified,'N'), 'Y', 0, 1) 
//                       * ats.ach_amount )                   as std_released_amount, 
//                  sum( case when get_cpd_ach(ats.batch_date) != ats.post_date_actual then 1 else 0 end 
//                       * case when ats.entry_description = 'AMEX DEP' then 1 else 0 end 
//                       * case when ats.disqualified = 'Y' then 0 else 1 end ) 
//                                                            as amex_released_count, 
//                  sum( case when get_cpd_ach(ats.batch_date) != ats.post_date_actual then 1 else 0 end 
//                       * case when ats.entry_description = 'AMEX DEP' then 1 else 0 end
//                       * decode(adtc.debit_credit_indicator, 'C', 1, -1) 
//                       * decode(nvl(ats.disqualified,'N'), 'Y', 0, 1) 
//                       * ats.ach_amount )                   as amex_released_amount, 
//                  sum( ats.daily_discount_amount + 
//                       ats.daily_ic_amount )                as daily_disc_ic, 
//                  count(1)                                  as total_count, 
//                  sum( decode(adtc.debit_credit_indicator, 'C', 1, -1) 
//                       * ats.ach_amount )                   as total_amount 
//          from    ach_trident_detail      ad, 
//                  ach_trident_statement   ats, 
//                  ach_detail_tran_code    adtc 
//          where   :RecordId != :DAILY_JE_SALES_ACTIVITY 
//                  and (:BankNumber = -1 and ats.bank_number in (3941,3943,3858) or ats.bank_number = :BankNumber)
//                  and ad.origin_node like ats.bank_number||'%' 
//                  and ad.transmission_date = :prevBatchDate
//                  and ad.post_date between (:ActiveDate-decode(:prevBatchDate,to_date('07/28/2010','mm/dd/yyyy'),30,2)) and :ActiveDate
//                  and ( ad.entry_description = 'AMEX DEP' 
//                        or ad.entry_description = nvl('MERCH DEP', uid))
//                  and ad.tsys_load_filename is null   -- ignore all TSYS entries
//                  and ats.trace_number = ad.trace_number 
//                  and ats.post_date between :prevBatchDate-15 and :ActiveDate+180
//                  and adtc.ach_detail_trans_code = ats.transaction_code 
//          group by ats.bank_number, ad.transmission_date
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  ats.bank_number                           as bank_number,\n                ad.transmission_date                      as batch_date,\n                sum( case when get_cpd_ach(ats.batch_date) = ats.post_date_actual then 1 else 0 end ) \n                                                          as standard_count, \n                sum( case when get_cpd_ach(ats.batch_date) = ats.post_date_actual then 1 else 0 end \n                     * decode(adtc.debit_credit_indicator, 'C', 1, -1) \n                     * ats.ach_amount )                   as standard_amount, \n                sum( case when get_cpd_ach(ats.batch_date) != ats.post_date_actual then 1 else 0 end \n                     * case when ats.entry_description = 'MERCH DEP' then 1 else 0 end \n                     * case when ats.disqualified = 'Y' then 0 else 1 end ) \n                                                          as std_released_count, \n                sum( case when get_cpd_ach(ats.batch_date) != ats.post_date_actual then 1 else 0 end \n                     * case when ats.entry_description = 'MERCH DEP' then 1 else 0 end\n                     * decode(adtc.debit_credit_indicator, 'C', 1, -1) \n                     * decode(nvl(ats.disqualified,'N'), 'Y', 0, 1) \n                     * ats.ach_amount )                   as std_released_amount, \n                sum( case when get_cpd_ach(ats.batch_date) != ats.post_date_actual then 1 else 0 end \n                     * case when ats.entry_description = 'AMEX DEP' then 1 else 0 end \n                     * case when ats.disqualified = 'Y' then 0 else 1 end ) \n                                                          as amex_released_count, \n                sum( case when get_cpd_ach(ats.batch_date) != ats.post_date_actual then 1 else 0 end \n                     * case when ats.entry_description = 'AMEX DEP' then 1 else 0 end\n                     * decode(adtc.debit_credit_indicator, 'C', 1, -1) \n                     * decode(nvl(ats.disqualified,'N'), 'Y', 0, 1) \n                     * ats.ach_amount )                   as amex_released_amount, \n                sum( ats.daily_discount_amount + \n                     ats.daily_ic_amount )                as daily_disc_ic, \n                count(1)                                  as total_count, \n                sum( decode(adtc.debit_credit_indicator, 'C', 1, -1) \n                     * ats.ach_amount )                   as total_amount \n        from    ach_trident_detail      ad, \n                ach_trident_statement   ats, \n                ach_detail_tran_code    adtc \n        where    :1  !=  :2  \n                and ( :3  = -1 and ats.bank_number in (3941,3943,3858,3003) or ats.bank_number =  :4 )\n                and ad.origin_node like ats.bank_number||'%' \n                and ad.transmission_date =  :5 \n                and ad.post_date between ( :6 -decode( :7 ,to_date('07/28/2010','mm/dd/yyyy'),30,2)) and  :8 \n                and ( ad.entry_description = 'AMEX DEP' \n                      or ad.entry_description = nvl('MERCH DEP', uid))\n                and ad.tsys_load_filename is null   -- ignore all TSYS entries\n                and ats.trace_number = ad.trace_number \n                and ats.post_date between  :9 -15 and  :10 +180\n                and adtc.ach_detail_trans_code = ats.transaction_code \n        group by ats.bank_number, ad.transmission_date";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.startup.NetSuiteDailyUpload",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,RecordId);
   __sJT_st.setInt(2,DAILY_JE_SALES_ACTIVITY);
   __sJT_st.setInt(3,BankNumber);
   __sJT_st.setInt(4,BankNumber);
   __sJT_st.setDate(5,prevBatchDate);
   __sJT_st.setDate(6,ActiveDate);
   __sJT_st.setDate(7,prevBatchDate);
   __sJT_st.setDate(8,ActiveDate);
   __sJT_st.setDate(9,prevBatchDate);
   __sJT_st.setDate(10,ActiveDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"9com.mes.startup.NetSuiteDailyUpload",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1649^7*/
      
      rs = it.getResultSet();
      while( rs.next() )
      {
        for( int i = 0; i < ReportRows.size(); ++i )
        {
          SummaryData sm = (SummaryData)ReportRows.elementAt(i);
          if( sm.getBankNumber().equals(rs.getString("bank_number")) )
          {
            FundingSummary fs = sm.getFundingSummary(prevBatchDate);
            fs.setStdReleasedAmount( rs.getDouble("std_released_amount"));
            fs.setAmexReleasedAmount( rs.getDouble("amex_released_amount"));
          }
        }
      }
      it.close();
      
      /*@lineinfo:generated-code*//*@lineinfo:1667^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ 
//                      index (ad idx_ach_trident_dtl_mid_tdate) 
//                      use_hash(adtc,ad) 
//                  */
//                  mf.bank_number          as bank_number,
//                  ad.transmission_date    as batch_date,
//                  sum( decode(ad.entry_description,'AMEX DEP',1,0)
//                       * decode(adtc.debit_credit_indicator, 'C', 1, -1) 
//                       * ad.ach_amount )                   as amex_dep_amount,
//                  sum( decode(ad.entry_description,'MERCH DEP',1,0)
//                       * decode(adtc.debit_credit_indicator, 'C', 1, -1) 
//                       * ad.ach_amount )                   as merch_dep_amount,
//                  sum( decode(adtc.debit_credit_indicator, 'C', 1, -1) 
//                       * ad.ach_amount )                   as total_amount 
//          from    mif mf,
//                  ach_trident_detail      ad, 
//                  ach_detail_tran_code    adtc
//          where   :RecordId != :DAILY_JE_SALES_ACTIVITY
//                  and ((:BankNumber = -1 and mf.bank_number in (3941,3943,3858)) or mf.bank_number = :BankNumber) 
//                  and ad.merchant_number = mf.merchant_number
//                  and ad.origin_node = mf.bank_number*100000
//                  and ad.transmission_date = :prevBatchDate
//                  and ad.post_date between (:prevBatchDate-decode(:prevBatchDate,to_date('07/28/2010','mm/dd/yyyy'),30,2)) and :prevBatchDate
//                  and ( ad.entry_description = 'AMEX DEP' 
//                        or ad.entry_description = nvl('MERCH DEP', uid))
//                  and ad.tsys_load_filename is null   -- ignore all TSYS entries
//                  -- only pull files for the top level node
//                  and substr(ad.load_filename,2,13) = ('ach' || to_char(mf.bank_number) || '00000_')
//                  and adtc.ach_detail_trans_code = ad.transaction_code 
//          group by mf.bank_number, ad.transmission_date
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ \n                    index (ad idx_ach_trident_dtl_mid_tdate) \n                    use_hash(adtc,ad) \n                */\n                mf.bank_number          as bank_number,\n                ad.transmission_date    as batch_date,\n                sum( decode(ad.entry_description,'AMEX DEP',1,0)\n                     * decode(adtc.debit_credit_indicator, 'C', 1, -1) \n                     * ad.ach_amount )                   as amex_dep_amount,\n                sum( decode(ad.entry_description,'MERCH DEP',1,0)\n                     * decode(adtc.debit_credit_indicator, 'C', 1, -1) \n                     * ad.ach_amount )                   as merch_dep_amount,\n                sum( decode(adtc.debit_credit_indicator, 'C', 1, -1) \n                     * ad.ach_amount )                   as total_amount \n        from    mif mf,\n                ach_trident_detail      ad, \n                ach_detail_tran_code    adtc\n        where    :1  !=  :2 \n                and (( :3  = -1 and mf.bank_number in (3941,3943,3858,3003)) or mf.bank_number =  :4 ) \n                and ad.merchant_number = mf.merchant_number\n                and ad.origin_node = mf.bank_number*100000\n                and ad.transmission_date =  :5 \n                and ad.post_date between ( :6 -decode( :7 ,to_date('07/28/2010','mm/dd/yyyy'),30,2)) and  :8 \n                and ( ad.entry_description = 'AMEX DEP' \n                      or ad.entry_description = nvl('MERCH DEP', uid))\n                and ad.tsys_load_filename is null   -- ignore all TSYS entries\n                -- only pull files for the top level node\n                and substr(ad.load_filename,2,13) = ('ach' || to_char(mf.bank_number) || '00000_')\n                and adtc.ach_detail_trans_code = ad.transaction_code \n        group by mf.bank_number, ad.transmission_date";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.startup.NetSuiteDailyUpload",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,RecordId);
   __sJT_st.setInt(2,DAILY_JE_SALES_ACTIVITY);
   __sJT_st.setInt(3,BankNumber);
   __sJT_st.setInt(4,BankNumber);
   __sJT_st.setDate(5,prevBatchDate);
   __sJT_st.setDate(6,prevBatchDate);
   __sJT_st.setDate(7,prevBatchDate);
   __sJT_st.setDate(8,prevBatchDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"10com.mes.startup.NetSuiteDailyUpload",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1699^7*/
      
      rs = it.getResultSet();
      while( rs.next() )
      {
        for( int i = 0; i < ReportRows.size(); ++i )
        {
          SummaryData sm = (SummaryData)ReportRows.elementAt(i);
          if( sm.getBankNumber().equals(rs.getString("bank_number")) )
          {
            FundingSummary fs = sm.getFundingSummary(prevBatchDate);
          }
        }
      }
      it.close();

    }
    catch( java.sql.SQLException e )
    {
      e.printStackTrace();
      logEntry( "loadFundingSummary()", e.toString() );
    }
    finally
    {
      try { it.close(); } catch( Exception e ) { }
    }
  }
  
  public void loadSalesActivity()
  {
    ResultSetIterator   it          = null;
    ResultSet           rs          = null;
    
    try
    {

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.startup.NetSuiteDailyUpload",SQL_GET_SALES_ACTIVITY);
   // set IN parameters
   __sJT_st.setInt(1,BankNumber);
   __sJT_st.setInt(2,RecordId);
   __sJT_st.setInt(3,RecordId);
   __sJT_st.setInt(4,DAILY_JE_SALES_ACTIVITY);
   __sJT_st.setDate(5,ActiveDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"11com.mes.startup.NetSuiteDailyUpload",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1750^7*/
      
      rs = it.getResultSet();
      while( rs.next() )
      {
        SalesActivity dsa = new SalesActivity( rs );
        
        for( int i = 0; i < ReportRows.size(); ++i )
        {
          SummaryData sm = (SummaryData)ReportRows.elementAt(i);
          if( sm.getBankNumber().equals(rs.getString("bank_number")) )
          {
            sm.setSalesData( dsa );
          }
        }
      }
    }
    catch( java.sql.SQLException e )
    {
    }
    finally
    {
      try { it.close(); } catch( Exception e ) { }
    }
  }
  
  private void sendEmail()
  {
    StringBuffer  msgBody = new StringBuffer("NetSuite Daily Upload ");
    StringBuffer  subject = new StringBuffer("NetSuite Daily Upload ");
    
    try
    {
      MailMessage msg = new MailMessage();
      msg.setAddresses(DailyUploadEmailGroup);
      String curTime = com.mes.support.DateTimeFormatter.getCurDateTimeString();
      
      subject.append( curTime );
      msgBody.append( curTime );
      msgBody.append("\n\n");
      msgBody.append( "Active Date: " + DateTimeFormatter.getFormattedDate(ActiveDate,"MM/dd/yyyy"));
      msgBody.append("\n\n");
      msgBody.append(" Bank #      Record ID       Message \n");
      msgBody.append(" --------- --------------- --------------- \n");
      for( int i = 0; i < LogEntryRows.size();  i++ )
      {
        LogEntryRow row = (LogEntryRow)LogEntryRows.elementAt(i);
        msgBody.append(row.getBankNumber());
        msgBody.append("               ");
        msgBody.append(row.getItemId());
        msgBody.append("                     ");
        msgBody.append(row.getStatus());
        msgBody.append("\n");
      }
      msg.setSubject(subject.toString());
      msg.setText(msgBody.toString());
      msg.send();
    }      
    catch(Exception e)
    {
      logEntry("sendEmail()", e.toString());
    }
  }
  
  public void setBankNumber( int bankNumber )
  {
    BankNumber = bankNumber;
  }
  
  public void setRecordId( int recordId )
  {
    RecordId = recordId;
  }
  
  public void extractJeResponseParam()
  {
    int            itemIdx        = 0;
    JeRecord       rec            = null;
    LogEntryRow    entry          = null;
    
    try
    {
      LogEntryRows.clear();
      
      if( ResponseList == null )
      {
        return;
      }
      
      for( int i = 0; i < ReportRows.size(); ++i )
      {
        SummaryData sm = (SummaryData)ReportRows.elementAt(i);
        for( int j = 0; j < sm.getJeRecords().size(); ++j )
        {
          rec   = (JeRecord)sm.getJeRecords().elementAt(j);
          entry = new LogEntryRow(PT_DAILY, REC_TYPE_JE, ActiveDate );
          entry.setItemId( rec.getRecordId() );
          entry.setItemName( rec.getRecordName() );
          entry.setStatus( getStatus(itemIdx) );
          entry.setBankNumber( Integer.parseInt(sm.getBankNumber()));
          LogEntryRows.add(entry);
          ++itemIdx;
        }
      }
      
      if( BankNumber == -1 && (RecordId == -1 || RecordId == 7) )
      {
        entry = new LogEntryRow(PT_DAILY, REC_TYPE_JE, ActiveDate );
        entry.setItemId( Integer.parseInt(DAILY_JE_RECORDS[7][0]) );
        entry.setItemName( DAILY_JE_RECORDS[7][1] );
        entry.setStatus( getStatus(ResponseList.length - 1) );
        entry.setBankNumber(BankNumber);
        LogEntryRows.add(entry);
      }
    }
    catch( Exception e )
    {
      e.printStackTrace();
      logEntry( "extractJeResponseParam()", e.toString() );
    }
  }
  
  public boolean execute()
  {
    try
    {
      connect(true);
      
      /*@lineinfo:generated-code*//*@lineinfo:1878^7*/

//  ************************************************************
//  #sql [Ctx] { select  trunc(sysdate) - 1
//          
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  trunc(sysdate) - 1\n         \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.startup.NetSuiteDailyUpload",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   ActiveDate = (java.sql.Date)__sJT_rs.getDate(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1883^7*/
      
      loadData();
      buildJeList();
      uploadData();
      extractJeResponseParam();
      logResponses();
      sendEmail();
    }
    catch( Exception e )
    {
      logEntry( "execute()", e.toString() );
    }
    
    return true;
  }
  
  public static void main( String[] args )
  {
    NetSuiteDailyUpload  test        = null;
    Record[]             records     = null;
    WriteResponse[]      responses   = null;
    
      try {
        if (args.length > 0 && args[0].equals("testproperties")) {
          EventBase.printKeyListStatus(new String[]{
                  MesDefaults.DK_NETSUITE_WSDL_URL,
                  MesDefaults.DK_NETSUITE_WS_TEST_URL,
                  MesDefaults.DK_NETSUITE_WS_PROD_URL,
                  MesDefaults.DK_NETSUITE_LOGIN_EMAIL,
                  MesDefaults.DK_NETSUITE_LOGIN_PASSWORD,
                  MesDefaults.DK_NETSUITE_LOGIN_ROLE,
                  MesDefaults.DK_NETSUITE_LOGIN_ACCOUNT
          });
        }
      } catch (Exception e) {
        log.error(e.toString());
      }

    try
    { 
      test = new NetSuiteDailyUpload();
      test.connect();
            
      // args[0]: NetSuite enviroment
      //          "upload_test", upload to sandbox
      //          "upload_prod", upload to production
      // args[1]: batch date, "MM/dd/yyyy"
      // args[2]: bank number
      //          3858, 3941, 3943
      //          -1 : all banks
      // args[3]: record ID
      //          0: Funding Summary
      //          1: Visa Clearing
      //          2: MC Clearing
      //          3: DS Clearing
      //          4: Incoming Chargebacks
      //          5: Debit Adjustment
      //          6: Merchant Deposits
      //          7: Sales Activity
      //         -1: all
      if( args.length > 0 && (args[0].equals("upload_test") || args[0].equals("upload_prod")) )
      {
        if( args[0].equals("upload_test") )
        {
          test.setTestMode(true);
        }
        
        test.setActiveDate( DateTimeFormatter.parseSQLDate( args[1].toString(), "MM/dd/yyyy") );
        test.setBankNumber( Integer.parseInt(args[2]) );
        test.setRecordId( Integer.parseInt(args[3]) );
        test.loadData();
        test.buildJeList();
        test.uploadData();
        test.extractJeResponseParam();
        test.logResponses();
        test.sendEmail();
        for( int i = 0; i < test.LogEntryRows.size();  i++ )
        {
          LogEntryRow response = (LogEntryRow)test.LogEntryRows.elementAt(i);
        }
      }
      else
      {
        test.execute();
      }
    }
    catch( Exception e )
    {
      e.printStackTrace();
    }
    finally
    {
      try{ test.cleanUp(); } catch( Exception e ) {}
      Runtime.getRuntime().exit(0);
    }
  }
}/*@lineinfo:generated-code*/