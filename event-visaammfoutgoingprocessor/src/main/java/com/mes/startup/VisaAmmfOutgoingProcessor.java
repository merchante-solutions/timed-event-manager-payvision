/*************************************************************************

    FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/startup/VisaAmmfOutgoingProcessor.java $

  Description:

  Last Modified By   : $Author: ttran $
  Last Modified Date : $Date: 2015-04-08 15:28:05 -0700 (Wed, 08 Apr 2015) $
  Version            : $Revision: 23558 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.Date;
import org.apache.log4j.Logger;
import com.mes.config.MesDefaults;
import com.mes.constants.MesEmails;
import com.mes.net.MailMessage;
import com.mes.support.DateTimeFormatter;
import com.mes.utils.jaxb.AmmfJaxb;


public class VisaAmmfOutgoingProcessor extends EventBase 
{
  static Logger log = Logger.getLogger(VisaAmmfOutgoingProcessor.class);
  
  private boolean TestMode  = false;
  private Long    Mid       = -1L;
  private Long    ProcSeq   = 0L;
  private String  crlf      = "\r\n";
  
  public void buildFile(String outFilename) 
  {
    PreparedStatement ps          = null;
    ResultSet         rs          = null;
    String            qs          = null;
    int               recCount    = 0;

    try
    {
      qs = " select count(1)              " +
           " from   visa_auto_boarding    " +
           " where  process_sequence = ?  ";
           
      ps = con.prepareStatement(qs);
      ps.setLong(1, ProcSeq);
      rs = ps.executeQuery();
      if( rs.next() )
      {
        recCount = rs.getInt(1);
      }
      
      if( recCount > 0 )
      {
        // mark items
        if( ProcSeq == 0 )
        {
          qs = " select  visa_auto_board_seq.nextval " +
               " from    dual                        ";
          ps = con.prepareStatement(qs);
          rs = ps.executeQuery();
          if( rs.next() )
          {
            ProcSeq = rs.getLong(1);
          }
          
          log.debug("update visa_auto_boarding.process_sequence: " + ProcSeq);
          qs = " update visa_auto_boarding    " +
               " set    process_sequence = ?  " +
               " where  process_sequence = 0  " ;
          
          ps = con.prepareStatement(qs);
          ps.setLong(1, ProcSeq);
          ps.executeUpdate();
          ps.close();
          commit();
        }
        
        // load data
        qs = " select  vab.action                  as change_indicator,                                                     " +
             "         case                                                                                                 " +
             "           when mb.vs_bin_base_ii = 433239                                                                    " +
             "           then 10000238                                                                                      " +
             "           else mb.vs_acquirer_id                                                                             " +
             "         end                         as acquirer_bid,                                                         " +
             "         mb.vs_bin_base_ii           as acquirer_bin,                                                         " +
             "         mf.merchant_number          as acquirer_merchant_id,                                                 " +
             "         mf.merchant_number          as caid,                                                                 " +
             "         case                                                                                                 " +
             "           when mf.date_stat_chgd_to_dcb is null                                                              " +
             "           then 'O'                                                                                           " +
             "           else 'X'                                                                                           " +
             "         end                         as business_status,                                                      " +
             "         mf.dba_name                 as dba_name,                                                             " +
             "         nvl(mf.fdr_corp_name,                                                                                " +
             "         mf.dba_name)              as legal_name,                                                             " +
             "         case                                                                                                 " +
             "           when mf.bank_number = 3858                                                                         " +
             "           then nvl(mf.addr2_line_2, mf.addr2_line_1)                                                         " +
             "           else mf.dmaddr                                                                                     " +
             "         end                         as location_addr_street,                                                 " +
             "         case                                                                                                 " +
             "           when mf.bank_number = 3858                                                                         " +
             "           then mf.city2_line_4                                                                               " +
             "           else mf.dmcity                                                                                     " +
             "         end                         as location_addr_city,                                                   " +
             "         case                                                                                                 " +
             "           when mf.bank_number = 3858                                                                         " +
             "           then mf.state2_line_4                                                                              " +
             "           else mf.dmstate                                                                                    " +
             "         end                         as location_addr_state,                                                  " +
             "         case                                                                                                 " +
             "           when mf.bank_number = 3858                                                                         " +
             "           then substr(mf.zip2_line_4,1,5)                                                                    " +
             "           else substr(mf.dmzip,1,5)                                                                          " +
             "         end                         as location_addr_postal_code,                                            " +
             "         case                                                                                                 " +
             "           when mf.bank_number = 3858                                                                         " +
             "           then mf.dmaddr                                                                                     " +
             "           else nvl(mf.addr1_line_2, mf.addr1_line_1)                                                         " +
             "         end                         as mailing_addr_street,                                                  " +
             "         case                                                                                                 " +
             "           when mf.bank_number = 3858                                                                         " +
             "           then mf.dmcity                                                                                     " +
             "           else mf.city1_line_4                                                                               " +
             "         end                         as mailing_addr_city,                                                    " +
             "         case                                                                                                 " +
             "           when mf.bank_number = 3858                                                                         " +
             "           then mf.dmstate                                                                                    " +
             "           else mf.state1_line_4                                                                              " +
             "         end                         as mailing_addr_state,                                                   " +
             "         case                                                                                                 " +
             "           when mf.bank_number = 3858                                                                         " +
             "           then substr(mf.dmzip,1,5)                                                                          " +
             "           else substr(mf.zip1_line_4,1,5)                                                                    " +
             "         end                         as mailing_addr_postal_code,                                             " +
             "         mf.sic_code                 as mcc,                                                                  " +
             "         mf.date_stat_chgd_to_dcb    as severance_date,                                                       " +
             "         nvl(trunc(mc.date_changed),                                                                          " +
             "                 mif_date_opened(mf.date_opened)) as non_monetary_change_date,                                " +
             "         vab.date_created            as date_changed,                                                         " +
             "         case                                                                                                 " +
             "           when mr.bustype_code = 1 then 1                                                                    " +
             "           when mr.bustype_code = 2 then 3                                                                    " +
             "           when mr.bustype_code = 3 then 2                                                                    " +
             "           when mr.bustype_code = 4 then 3                                                                    " +
             "           when mr.bustype_code = 5 then 5                                                                    " +
             "           when mr.bustype_code = 6 then 6                                                                    " +
             "           when mr.bustype_code = 7 then 7                                                                    " +
             "           when mr.bustype_code = 8 then 9                                                                    " +
             "           else 3                                                                                             " +
             "         end                         as corporate_status,                                                     " +
             "         mf.federal_tax_id           as business_registration_id,                                             " +
             "         upper(nvl(substr(owner_name, 1, instr(owner_name, ' ')),bo.busowner_first_name))                     " +
             "                                     as owner_first_name,                                                     " +
             "         upper(nvl(substr(owner_name, instr(owner_name, ' ')+1, length(owner_name)),bo.busowner_last_name))   " +
             "                                     as owner_last_name,                                                      " +
             "         case                                                                                                 " +
             "           when mf.bank_number = 3858                                                                         " +
             "           then nvl(mf.addr2_line_1,mf.dmaddr)                                                                " +
             "           else mf.dmaddr                                                                                     " +
             "         end                         as corp_addr_street,                                                     " +
             "         case                                                                                                 " +
             "           when mf.bank_number = 3858                                                                         " +
             "           then nvl(mf.city2_line_4,mf.dmcity)                                                                " +
             "           else mf.dmcity                                                                                     " +
             "         end                         as corp_addr_city,                                                       " +
             "         case                                                                                                 " +
             "           when mf.bank_number = 3858                                                                         " +
             "           then nvl(mf.state2_line_4,mf.dmstate)                                                              " +
             "           else mf.dmstate                                                                                    " +
             "         end                         as corp_addr_state,                                                      " +
             "         case                                                                                                 " +
             "           when mf.bank_number = 3858                                                                         " +
             "           then substr(nvl(mf.zip2_line_4,mf.dmzip),1,5)                                                      " +
             "           else substr(mf.dmzip,1,5)                                                                          " +
             "         end                         as corp_addr_postal_code                                                 " +
             " from    visa_auto_boarding vab,                                                                              " +
             "         mif mf,                                                                                              " +
             "         mbs_banks mb,                                                                                        " +
             "         mif_changes mc,                                                                                      " +
             "         merchant mr,                                                                                         " +
             "         businessowner bo                                                                                     " +
             " where   vab.process_sequence = ?                                                                             " +
             "         and vab.merchant_number = mf.merchant_number                                                         " +
             "         and nvl(mf.test_account,'N') != 'Y'                                                                  " +
             "         and mf.bank_number = mb.bank_number                                                                  " +
             "         and mc.date_changed= ( select max(mc.date_changed)                                                   " +
             "                                from   mif_changes mc,                                                        " +
             "                                       mif mf                                                                 " +
             "                                where  mf.merchant_number = mc.merchant_number(+)                             " +
             "                              )                                                                               " +
             "         and vab.merchant_number = mr.merch_number                                                            " +
             "         and mr.app_seq_num = bo.app_seq_num                                                                  " +
             "         and bo.busowner_num = 1                                                                              " ;
        
        log.info("load data");
        ps = con.prepareStatement(qs);
        ps.setLong(1, ProcSeq);
        rs = ps.executeQuery();
        AmmfJaxb aj = new AmmfJaxb();
        while( rs.next() )
        {
         aj.addMerchant(rs);
        }
        rs.close();
        ps.close();

        log.info("print ammf to file");
        aj.print(outFilename);
        
        log.debug("update visa_auto_boarding.load_filename: " + outFilename);
        qs = " update visa_auto_boarding     " +
             " set    load_filename = ?      " +
             " where process_sequence = ?    ";
        ps = con.prepareStatement(qs);
        ps.setString(1, outFilename);
        ps.setLong(2, ProcSeq);
        ps.executeUpdate();
        ps.close();
        commit();
      } 
      else
      {
        log.info("No new entries to process");
      }
    }
    catch(Exception e)
    {
      e.printStackTrace();
      log.info(e.toString());
      logEntry("buildFile() ", e.toString());
      rollback();
      
      try 
      {
        qs = " update visa_auto_boarding     " +
             " set    process_sequence = 0   " +
             " where  process_sequence = ?   ";
       ps = con.prepareStatement(qs);
       ps.setLong(1, ProcSeq);
       ps.executeUpdate();
       ps.close();
       commit();
      } 
      catch (Exception ex) 
      {
        log.info(ex.toString());
      }
    }
    finally
    {
      try { rs.close(); } catch (Exception e) {}
      try { ps.close(); } catch (Exception e) {}
    }
  }
  
  private String buildFilename()
  {
    PreparedStatement ps          = null;
    ResultSet         rs          = null;
    String            qs          = null;
    String            filename    = null;
    Date              fileDate    = null;
    try
    {
      fileDate = Calendar.getInstance().getTime();
      filename = "ammf_" + DateTimeFormatter.getFormattedDate(fileDate,"MMddyy");
      
      qs = " select vab.*, substr(load_filename,-5,1) as file_id   " +
           " from   ( select  process_sequence, load_filename " +
           "          from    visa_auto_boarding " +
           "          where   load_filename like '" + filename + "%' " + 
           "          group by process_sequence, load_filename " +
           "          order by process_sequence desc " +
           "        ) vab " +
           " where rownum = 1 ";
      
      ps = con.prepareStatement(qs);
      rs = ps.executeQuery();
      if( rs.next() )
      {
        filename = filename + String.valueOf(rs.getInt("file_id") + 1);
      }
      else
      {
        filename = filename + "1";
      }
      filename = filename + ".xml";
    }
    catch( Exception ee )
    {
      filename = null;
      log.info(ee.toString());
      ee.printStackTrace();
    }
    
    return filename;
  }

  private void sendStatusEmail(boolean success, String filename)
  {
    StringBuffer  subject         = new StringBuffer("");
    StringBuffer  body            = new StringBuffer("");
    
    try
    {
      body.append("[");
      body.append(DateTimeFormatter.getFormattedDate(Calendar.getInstance().getTime(),"MM/dd/yyyy hh:mm:ss a"));
      body.append("]");
      
      if(success)
      {
        subject.append("Visa AMMF SUCCESS - ");
        body.append(" Successfully processed Visa AMMF outgoing file: ");
      }
      else
      {
        subject.append("Visa AMMF FAIL - ");
        body.append(" Failed to process Visa AMMF outgoing file: ");
      }
      
      subject.append(filename);
      body.append(filename + "\n\n");
      
      MailMessage msg = new MailMessage();
      msg.setAddresses(MesEmails.MSG_ADDRS_OUTGOING_VS_AMMF);
      
      msg.setSubject(subject.toString());
      msg.setText(body.toString());
      msg.send();
    }
    catch(Exception e)
    {
      logEntry("sendStatusEmail() ", e.toString());
    }
  }

  public boolean execute()
  {
    String    filename         = null;
    boolean   success          = false;
    
    try
    {
      connect(true);
      
      filename = buildFilename();
      if( filename == null )
      {
        log.info("filename = null");
        return false;
      }
      
      String ipAddr       = MesDefaults.getString(MesDefaults.DK_SNA_FTP_ADDRESS);
      String userId       = MesDefaults.getString(MesDefaults.DK_SNA_FTP_USER);
      String userPass     = MesDefaults.getString(MesDefaults.DK_SNA_FTP_PASSWORD);
      String destPath     = MesDefaults.getString(MesDefaults.DK_VISA_AMMF_OUTGOING_PATH);
      
      log.info("BUILD file " + filename);
      buildFile(filename);
      
      log.info("SFTP data file " + filename);
      success = sendDataFile(filename, ipAddr, userId, userPass, destPath, true, true);
      log.info("SFTP data file success=" + success);
      // send the file to archive host
      if ( success )
      {
        log.info("ARCHIVE data file " + filename);
        archiveDailyFile(filename);
      }
      else
      {
        log.info("SFTP failed");
      }
      
      log.info("sendStatusEmail()");
      sendStatusEmail(success, filename);
    }
    catch( Exception ee )
    {
      log.info(ee.toString());
      ee.printStackTrace();
      logEntry("execute() ", ee.toString());
    }
    finally
    {
      cleanUp();
    }
    
    return success;
  }
  
  public static void main(String[] args) 
  {
    try
    {

      if (args.length > 0 && args[0].equals("testproperties")) {
        EventBase.printKeyListStatus(new String[]{
                MesDefaults.DK_SNA_FTP_ADDRESS,
                MesDefaults.DK_SNA_FTP_USER,
                MesDefaults.DK_SNA_FTP_PASSWORD,
                MesDefaults.DK_VISA_AMMF_OUTGOING_PATH
        });
      }
    } catch (Exception e) {
      System.out.println(e.toString());
    }


    VisaAmmfOutgoingProcessor proc = null;
    try
    {
      proc = new VisaAmmfOutgoingProcessor();
      if( args.length > 0 && args[0].equals("process") )
      {
        proc.ProcSeq = Long.parseLong(args[1]);
      }
      
      proc.execute();
    }
    catch(Exception e)
    {
      e.printStackTrace();
      log.error(e.toString());
    }
    finally
    {
      try{ proc.cleanUp(); } catch( Exception ee ) {}
      log.debug("Command line execution of VisaAmmfOutgoingProcessor complete");
      Runtime.getRuntime().exit(0);
    }
  }
}

