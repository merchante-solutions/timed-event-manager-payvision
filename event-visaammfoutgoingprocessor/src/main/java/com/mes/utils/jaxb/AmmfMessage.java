/*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-vs-ammf/src/main/com/mes/util/jaxb/AmmfMessage.java $

  Description:

  Last Modified By   : $Author: $
  Last Modified Date : $Date: $
  Version            : $Revision: $

  Change History:
     See SVN database

  Copyright (C) 2000-2010,2011 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.utils.jaxb;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import org.apache.log4j.Logger;
//import com.mes.database.SQLJConnectionBase;
import com.mes.database.ConnectionBase;

public class AmmfMessage extends ConnectionBase
{
  static Logger log = Logger.getLogger(AmmfMessage.class);
  
  public long getMessageId() throws SQLException
  {
    long        msgId     = -1;
    Statement   stmt      = null;
    ResultSet   rs        = null;

    connect();
    /*
      #sql [Ctx]
      {
        select  mes.ammf_msg_id.nextval
        into    :msgId
        from    dual
      };
      cleanUp();
    */
    
    stmt = connectStatement();
    rs = stmt.executeQuery(
      " select ammf_msg_id.nextval   " +
      " from   dual   ");
    if( rs.next() )
    {
      msgId = rs.getLong(1);
    }
    cleanUp();
    
    return msgId;
  }
  
  public long getFileSequence() throws SQLException
  {
    long        fileSeq   = -1;
    Statement   stmt      = null;
    ResultSet   rs        = null;

    connect();
    /*
    #sql [Ctx]
    {
        select  mes.ammf_file_seq.nextval
        into    :fileSeq
        from    dual
    };
              stmt = connectStatement();
          rs = stmt.executeQuery(
            " select message_description   " +
            " from   visa_ammf_msg_codes   " +
            " where  message_type = 'Quality Code' and message_code= '" + code + "'");
          if(rs.next())
          {
            emailBody.append("Code " + code + ": " + rs.getString("message_description") + " \n");
          }

    */
    stmt = connectStatement();
    rs = stmt.executeQuery(
      " select ammf_file_seq.nextval   " +
      " from   dual   ");
    if( rs.next() )
    {
      fileSeq = rs.getLong(1);
    }
    cleanUp();
    
    return fileSeq;
  }

  public void print(Object o, String namespace, String filename) throws JAXBException, FileNotFoundException, IOException
  {
    JAXBContext jc = JAXBContext.newInstance(namespace);
    Marshaller m = jc.createMarshaller();
    m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

    if(filename==null)
    {
      m.marshal( o, System.out );
    }
    else
    {
      OutputStream os = new FileOutputStream( filename );
      m.marshal( o, os );
      os.flush();
      os.close();
    }
  }
    
  public void print(Object o, String namespace) throws JAXBException, FileNotFoundException, IOException
  {
    print(o, namespace, null);
  }
}