/*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-vs-ammf/src/main/com/mes/util/jaxb/MerchantJaxb.java $

  Description:

  Last Modified By   : $Author: $
  Last Modified Date : $Date: $
  Version            : $Revision: $

  Change History:
     See SVN database

  Copyright (C) 2000-2010,2011 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.utils.jaxb;

import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import org.apache.log4j.Logger;
import org.tempuri.xmlschema.Address;
import org.tempuri.xmlschema.CAID;
import org.tempuri.xmlschema.CAIDs;
import org.tempuri.xmlschema.CorporationDetail;
import org.tempuri.xmlschema.MCC;
import org.tempuri.xmlschema.MCCs;
import org.tempuri.xmlschema.Merchant;
import org.tempuri.xmlschema.MerchantAlias;
import org.tempuri.xmlschema.MultipleAddress;
import org.tempuri.xmlschema.ObjectFactory;
import org.tempuri.xmlschema.SoleProprietor;
import org.tempuri.xmlschema.TaxDetail;

public class MerchantJaxb extends AmmfMessage
{
  static Logger log = Logger.getLogger(MerchantJaxb.class);
  
  private static final String namespace = "org.tempuri.xmlschema";
  private ObjectFactory objFact = new ObjectFactory();
  private static final long country = 840;
  
  public Merchant createMerchant(ResultSet rs) 
  {
    Merchant merchant = null;
    try
    {
      merchant = objFact.createMerchant();
      merchant.setAcquirerBID(rs.getLong("acquirer_bid"));
      merchant.setAcquirerName("MERCHANT E-SOLUTIONS");
      merchant.setAcquirerMerchantID(rs.getString("acquirer_merchant_id"));

      CAIDs caids = objFact.createCAIDs();
      CAID caid = objFact.createCAID();
      caid.setAcquirerBIN(rs.getLong("acquirer_bin"));
      caid.setValue(rs.getString("caid"));
      caids.getCAID().add(caid);
      merchant.setCAIDs(caids);

      MerchantAlias alias = objFact.createMerchantAlias();
      alias.setDBAName(rs.getString("dba_name").replaceAll("[^a-zA-Z0-9 ]", " "));
      alias.setLegalName(rs.getString("legal_name").replaceAll("[^a-zA-Z0-9 ]", " "));
      merchant.setMerchantAlias(alias);

      Address locationAddr = objFact.createAddress();
      locationAddr.setStreet(rs.getString("location_addr_street"));
      locationAddr.setCity(rs.getString("location_addr_city"));
      locationAddr.setStateProvinceCode(rs.getString("location_addr_state"));
      locationAddr.setPostalCode(rs.getString("location_addr_postal_code"));
      locationAddr.setCountry(country);
      merchant.setLocationAddress(locationAddr);

      MultipleAddress mailingAddr = objFact.createMultipleAddress();
      mailingAddr.setStreet(rs.getString("mailing_addr_street"));
      mailingAddr.setCity(rs.getString("mailing_addr_city"));
      mailingAddr.setStateProvinceCode(rs.getString("mailing_addr_state"));
      mailingAddr.setPostalCode(rs.getString("mailing_addr_postal_code"));
      mailingAddr.setCountry(country);
      mailingAddr.setSequence(1);
      merchant.getMailingAddress().add(mailingAddr);

      MCCs mccs = objFact.createMCCs();
      MCC mcc = objFact.createMCC();
      mcc.setSequence(1);
      mcc.setValue(rs.getLong("mcc"));
      mccs.getMCC().add(mcc);
      merchant.setMCCs(mccs);

      Calendar cal = Calendar.getInstance();
      cal.setTime(rs.getDate("non_monetary_change_date"));
      SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
      merchant.setNonMonetaryChangeDate( sdf.format(cal.getTime()) );

      merchant.setChangeIndicator(rs.getString("change_indicator"));
      merchant.setLocationCountry(country);
      merchant.setBusinessStatus(rs.getString("business_status"));
      
      CorporationDetail cd = objFact.createCorporationDetail();
      Address corporateAddr = objFact.createAddress();
      corporateAddr.setStreet(rs.getString("corp_addr_street"));
      corporateAddr.setCity(rs.getString("corp_addr_city"));
      corporateAddr.setStateProvinceCode(rs.getString("corp_addr_state"));
      corporateAddr.setPostalCode(rs.getString("corp_addr_postal_code"));
      corporateAddr.setCountry(country);
      cd.setAddress(corporateAddr);
      merchant.setCorporationDetail(cd);
      
      TaxDetail td = objFact.createTaxDetail();
      td.setBusinessRegistrationID(rs.getString("business_registration_id"));
      td.setCorporateStatus(rs.getString("corporate_status"));
      if( ("1").equals(rs.getString("corporate_status")) )
      {
        SoleProprietor sp = objFact.createSoleProprietor();
        sp.setFirstName(rs.getString("owner_first_name"));
        sp.setLastName(rs.getString("owner_last_name"));
        td.setSoleProprietor(sp);
      }
      merchant.setTaxDetail(td);

      Date severanceDate = rs.getDate("severance_date");
      if(severanceDate!=null)
      {
        merchant.setSeveranceDate( sdf.format(severanceDate) );
      }
    }
    catch( Exception e )
    {
      e.printStackTrace();
      logEntry("createMerchant() ", e.toString());
    }
    
    return merchant;
  }
}
