/*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-vs-ammf/src/main/com/mes/util/jaxb/UnlinkableJaxb.java $

  Description:

  Last Modified By   : $Author: $
  Last Modified Date : $Date: $
  Version            : $Revision: $

  Change History:
     See SVN database

  Copyright (C) 2000-2010,2011 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.utils.jaxb;

import javax.xml.bind.JAXBException;
import org.apache.log4j.Logger;
import org.tempuri.xmlschema.ObjectFactory;
import org.tempuri.xmlschema.Unlinkable;

public class UnlinkableJaxb extends AmmfMessage
{
  static Logger log = Logger.getLogger(UnlinkableJaxb.class);

  public Unlinkable createUnlinkable(Unlinkable request, long bid, String merchId) throws JAXBException
  {
    ObjectFactory objFact = new ObjectFactory();
    Unlinkable msg = objFact.createUnlinkable();

    msg.setID(request.getID());
    msg.setAcquirerBID(bid);
    msg.setAcquirerBIN(request.getAcquirerBIN());
    msg.setAMMFMerchantID(merchId);

    return msg;
  }
}
