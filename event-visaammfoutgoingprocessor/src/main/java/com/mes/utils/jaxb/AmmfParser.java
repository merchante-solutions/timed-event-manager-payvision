/*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-vs-ammf/src/main/com/mes/util/jaxb/AmmfParser.java $

  Description:

  Last Modified By   : $Author: $
  Last Modified Date : $Date: $
  Version            : $Revision: $

  Change History:
     See SVN database

  Copyright (C) 2000-2010,2011 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.utils.jaxb;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.StringWriter;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.sax.SAXSource;
//import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.tempuri.xmlschema.AMMF;
import org.tempuri.xmlschema.Messages;
import org.tempuri.xmlschema.QualityError;
import org.tempuri.xmlschema.TypeCode;
import org.tempuri.xmlschema.impl.FieldQualityMsgImpl;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLFilterImpl;
//import com.mes.database.OracleConnectionPool;
import com.mes.database.ConnectionBase;
import com.mes.net.MailMessage;

public class AmmfParser extends ConnectionBase
{
  static Logger log = Logger.getLogger(AmmfParser.class);
  
  private static final String namespace = "org.tempuri.xmlschema";
  private String filename = "";

  public static AMMF getAmmf(String filename) throws JAXBException, FileNotFoundException, SAXException, ParserConfigurationException
  {
    JAXBContext jc = JAXBContext.newInstance(namespace);
    Unmarshaller u = jc.createUnmarshaller();

    // Create the XMLReader
    SAXParserFactory factory = SAXParserFactory.newInstance();
    XMLReader reader = factory.newSAXParser().getXMLReader();

    // The filter class to set the correct namespace
    XMLFilterImpl xmlFilter = new XMLNamespaceFilter("http://tempuri.org/XMLSchema.xsd", reader);
    reader.setContentHandler(u.getUnmarshallerHandler());
    InputStream inStream = new FileInputStream(new File(filename));
    SAXSource source = new SAXSource(xmlFilter, new InputSource(inStream));

    //AMMF root = (AMMF)u.unmarshal( new FileInputStream( filename ));

    return (AMMF)u.unmarshal(source);
  }

  public void parse(String f) throws JAXBException, SAXException, FileNotFoundException, ParserConfigurationException
  {
    filename=f;
    AMMF ammf = getAmmf(filename);
    Messages messages = ammf.getMessages();
    log.debug("Number of messages: " + messages.getCount());

    connect();

    if(!messages.getTypeCode().isEmpty())
    {
      updateErrorCodes(messages.getTypeCode());
    }

    //just email these messages
    emailErrors(messages);

    //ignore AssignedAMMFIdMsg

    cleanUp();
  }

  private void updateErrorCodes(List updates)
  {
    PreparedStatement ps       = null;
    ResultSet         rs       = null;
    Statement         stmt     = null;
    
    try
    {
      for(Iterator i= updates.iterator(); i.hasNext();)
      {
        TypeCode msg = (TypeCode)i.next();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date theDate = new java.sql.Date( sdf.parse(msg.getChangeDate()).getTime() );
        String descript = msg.getMessage();
        String type = msg.getType();
        long code = msg.getCode();
              
        stmt = connectStatement();
        stmt.executeUpdate(
          " update visa_ammf_msg_codes    " +
          " set    message_description = '" + descript + "',   " +
          "        last_change_date    = '" + theDate  + "'    " +
          " where  message_type = '" + type + "'  " +
          "        message_code = " + code + "  " +
          "        last_change_date < " + theDate + "  ");
          /*
            #sql [Ctx]
            {
              update mes.visa_ammf_msg_codes
              set    message_description=:descript,
                     last_change_date=:theDate
              where  message_type=:type and message_code=:code and last_change_date < :theDate
            };
          */
      }
    } 
    catch (Exception e)
    {
      logEntry("updateErrorCodes()", e.toString());
    }
  }
    
  private void emailErrors(Messages messages) throws JAXBException
  {
    JAXBContext jc = JAXBContext.newInstance(namespace);
    Marshaller m = jc.createMarshaller();
    StringWriter currMsg = new StringWriter();
    StringBuffer emailBody = new StringBuffer("New messages from Visa in file: " + filename + "\n\n");

    emailBody.append("FREE FORM MESSAGE: \n");
    appendMsg(emailBody, messages.getFreeFormMsg(), m, currMsg);

    emailBody.append("\nNOTIFICATION OF AMMF FILE REJECTS: \n");
    appendMsg(emailBody, messages.getFileRejectMsg(), m, currMsg);

    emailBody.append("\nNOTIFICATION OF AMMF RECORD REJECT: \n");
    appendMsg(emailBody, messages.getRecordRejectMsg(), m, currMsg);

    emailBody.append("\nREQUEST FOR UNLINKABLE MERCHANT TO AMMF: \n");
    appendMsg(emailBody, messages.getUnlinkable(), m, currMsg);

    emailBody.append("\nNOTIFICATION OF AMMF MERCHANT STATISTIC: \n");
    appendMsg(emailBody, messages.getStatisticMsg(), m, currMsg);

    emailBody.append("\nCERTIFICATION STATUS: \n");
    appendMsg(emailBody, messages.getCertificationStatus(), m, currMsg);

    emailBody.append("\nNOTIFICATION OF AMMF FIELD QUALITY ERROR: \n");
    appendMsg(emailBody, messages.getFieldQualityMsg(), m, currMsg, true);

    emailBody.append("\nNOTIFICATION OF MERCHANT DATA UPDATE: \n");
    appendMsg(emailBody, messages.getFieldUpdateMsg(), m, currMsg);

    sendEmail(emailBody);
  }
  
  private void appendMsg(StringBuffer emailBody, List msgs, Marshaller m, StringWriter currMsg, boolean isFieldQualityError) throws JAXBException
  {
    HashSet codeDescriptions = null;
    Object obj = null;
    for(Iterator i = msgs.iterator(); i.hasNext();)
    {
      currMsg.getBuffer().setLength(0);
      obj = i.next();
      m.marshal(obj, currMsg);
      emailBody.append(currMsg.getBuffer());

      if(isFieldQualityError)
      {
        List qualityErrors = ((FieldQualityMsgImpl)obj).getQualityErrors().getQualityError();
        codeDescriptions = new HashSet();
        for(Iterator i2 = qualityErrors.iterator(); i2.hasNext();)
        {
          QualityError qe = (QualityError)i2.next();
          codeDescriptions.add(String.valueOf(qe.getCode()));
        }
      }
    }
    
    if(msgs.size()>0 && isFieldQualityError)
    {
      String      code      = "";
      Statement   stmt      = null;
      ResultSet   rs        = null;
      
      try
      {
        for(Iterator i = codeDescriptions.iterator(); i.hasNext();)
        {
          code = (String)i.next();
          /*
          #sql [Ctx] it =
          {
                  select  message_description
                  from    visa_ammf_msg_codes
                  where   message_type='Quality Code' and message_code=:code
          };
          */
          
          stmt = connectStatement();
          rs = stmt.executeQuery(
            " select message_description   " +
            " from   visa_ammf_msg_codes   " +
            " where  message_type = 'Quality Code' and message_code= '" + code + "'");
          if(rs.next())
          {
            emailBody.append("Code " + code + ": " + rs.getString("message_description") + " \n");
          }
        }
      } 
      catch (SQLException e)
      {
        emailBody.append("***Error retrieving descriptions for error codes. Look in the database.***\n");
      }
      finally
      {
        try { rs.close(); } catch(Exception ee) {}
      }
    }
  }
  
  private void appendMsg(StringBuffer emailBody, List msgs, Marshaller m, StringWriter currMsg) throws JAXBException
  {
    appendMsg(emailBody, msgs, m, currMsg, false);
  }

  private void sendEmail(StringBuffer emailBody)
  {
    try
    {
      MailMessage msg = new MailMessage();
      msg.setAddresses(145);
      msg.setSubject("Visa AMMF Messages");
      msg.setText(emailBody.toString());
      msg.send();
    }
    catch(Exception e)
    {
      logEntry("sendEmail()", e.toString());
    }
  }

  public static void main(String[] args)
  {
    AmmfParser parser = new AmmfParser();
    try
    {
      parser.parse(args[0]);
    }
    catch (Exception e)
    {
      log.error("Error parsing AMMF file from Visa: " + args[0]);
    }
    finally
    {
      parser.cleanUp();
      Runtime.getRuntime().exit(0);
    }
  }
}
