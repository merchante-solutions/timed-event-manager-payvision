/*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-vs-ammf/src/main/com/mes/util/jaxb/XMLNamespaceFilter.java $

  Description:

  Last Modified By   : $Author: $
  Last Modified Date : $Date: $
  Version            : $Revision: $

  Change History:
     See SVN database

  Copyright (C) 2000-2010,2011 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.utils.jaxb;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLFilterImpl;

public class XMLNamespaceFilter extends XMLFilterImpl
{
  private String namespace = "";

  public XMLNamespaceFilter(String ns, XMLReader arg0)
  {
    super(arg0);
    namespace=ns;
  }

  public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException
  {
    super.startElement(namespace, localName, qName, attributes);
  }

  public void endElement(String uri, String localName, String qName) throws SAXException
  {
    super.endElement(namespace, localName, qName);
  }
}
