/*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-vs-ammf/src/main/com/mes/util/jaxb/AcquirerMerchantUpdateJaxb.java $

  Description:

  Last Modified By   : $Author: $
  Last Modified Date : $Date: $
  Version            : $Revision: $

  Change History:
     See SVN database

  Copyright (C) 2000-2010,2011 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.utils.jaxb;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.xml.bind.JAXBException;
import org.apache.log4j.Logger;
import org.tempuri.xmlschema.AcquirerMerchantUpdate;
import org.tempuri.xmlschema.ObjectFactory;

public class AcquirerMerchantUpdateJaxb extends AmmfMessage
{
  static Logger log = Logger.getLogger(AcquirerMerchantUpdateJaxb.class);

  public AcquirerMerchantUpdate createAcqMerchUpd(long oldBid, long newBid, String authName) throws JAXBException, SQLException
  {
    ObjectFactory objFact = new ObjectFactory();
    AcquirerMerchantUpdate msg = objFact.createAcquirerMerchantUpdate();

    msg.setMessageID(getMessageId());
    msg.setOldAcquirerBID(oldBid);
    msg.setNewAcquirerBID(newBid);
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    msg.setEffectiveDt( sdf.format(Calendar.getInstance()) );
    msg.setAuthName(authName);

    return msg;
  }
}
