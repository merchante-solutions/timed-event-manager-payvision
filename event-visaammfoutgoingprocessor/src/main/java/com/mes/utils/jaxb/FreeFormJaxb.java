/*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-vs-ammf/src/main/com/mes/util/jaxb/FreeFormJaxb.java $

  Description:

  Last Modified By   : $Author: $
  Last Modified Date : $Date: $
  Version            : $Revision: $

  Change History:
     See SVN database

  Copyright (C) 2000-2010,2011 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.utils.jaxb;

import java.sql.SQLException;
import javax.xml.bind.JAXBException;
import org.apache.log4j.Logger;
import org.tempuri.xmlschema.FreeFormMsg;
import org.tempuri.xmlschema.ObjectFactory;

public class FreeFormJaxb extends AmmfMessage
{
  static Logger log = Logger.getLogger(FreeFormJaxb.class);

  public FreeFormMsg createFreeFormMsg(String content) throws JAXBException, SQLException
  {
    ObjectFactory objFact = new ObjectFactory();
    FreeFormMsg msg = objFact.createFreeFormMsg();

    msg.setMessageID(getMessageId());
    msg.setMessage(content);

    return msg;
  }
}
