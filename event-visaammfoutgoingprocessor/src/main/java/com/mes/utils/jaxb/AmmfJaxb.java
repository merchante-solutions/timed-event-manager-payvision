/*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-vs-ammf/src/main/com/mes/util/jaxb/AmmfJaxb.java $

  Description:

  Last Modified By   : $Author: $
  Last Modified Date : $Date: $
  Version            : $Revision: $

  Change History:
     See SVN database

  Copyright (C) 2000-2010,2011 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.utils.jaxb;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Validator;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.log4j.Logger;
import org.tempuri.xmlschema.AMMF;
import org.tempuri.xmlschema.Merchant;
import org.tempuri.xmlschema.Merchants;
import org.tempuri.xmlschema.Messages;
import org.tempuri.xmlschema.ObjectFactory;
import org.tempuri.xmlschema.Unlinkable;
import org.xml.sax.SAXException;

public class AmmfJaxb extends AmmfMessage
{
  static Logger log = Logger.getLogger(AmmfJaxb.class);

  private static final String namespace = "org.tempuri.xmlschema";
  private AMMF ammf = null;
  private ObjectFactory objFact = new ObjectFactory();
  private MerchantJaxb mj = new MerchantJaxb();

  public AmmfJaxb() throws JAXBException, SQLException
  {
    createAmmf();
  }

  public String getNamespace()
  {
    return namespace;
  }
  
  
  

    

  public void createAmmf() throws JAXBException, SQLException
  {
    ammf = objFact.createAMMF();

    ammf.setProcessorBINCIB(435884);
    ammf.setProcessorName("Merchant e-Solutions");
    ammf.setFileSequence(getFileSequence());
    ammf.setVersion("2.2");
  }

  public void addMerchant(ResultSet rs) throws JAXBException, SQLException
  {
    Merchants merchants = ammf.getMerchants();
    if(merchants==null) {
        merchants = objFact.createMerchants();
        merchants.setCount(0);
        ammf.setMerchants(merchants);
    }

    //validate object before adding to doc
    Merchant merchant = mj.createMerchant(rs);
    try{
      JAXBContext jc = JAXBContext.newInstance(namespace);
      Validator validator = jc.createValidator();
      boolean isValid = validator.validate(merchant);

      if(isValid) {
        merchants.getMerchant().add(merchant);
        merchants.setCount(merchants.getCount()+1);
      } else {
        logEntry("addMerchant()", "Merchant with ID " + rs.getString("acquirer_merchant_id") + " in table VISA_AUTO_BOARDING with DATE_CREATED=" + rs.getString("date_changed") + " has invalid data. Please check its data.");
      }
    } catch(JAXBException je) {
      logEntry("addMerchant()", "Error processing merchant with ID " + rs.getString("acquirer_merchant_id") + " in table VISA_AUTO_BOARDING with DATE_CREATED=" + rs.getString("date_changed") + ". Please check its data.");
    }
  }

  private void messageElemSetup() throws JAXBException
  {
    Messages messages = ammf.getMessages();
    if(messages==null) {
        messages = objFact.createMessages();
        ammf.setMessages(messages);
    }

    messages.setCount(messages.getCount()+1);
  }
  
  public void addUnlinkableResponse(String filename, int requestIndex, long bid, String merchId) throws JAXBException, SAXException, FileNotFoundException, ParserConfigurationException
  {
    AMMF receivedAmmf = AmmfParser.getAmmf(filename);
    Unlinkable request = (Unlinkable)receivedAmmf.getMessages().getUnlinkable().get(requestIndex);

    messageElemSetup();

    UnlinkableJaxb uj = new UnlinkableJaxb();
    Unlinkable response = uj.createUnlinkable(request, bid, merchId);
    ammf.getMessages().getUnlinkable().add(response);
  }

  public void addFreeFormText(String content) throws JAXBException, SQLException
  {
    messageElemSetup();

    FreeFormJaxb fj = new FreeFormJaxb();
    ammf.getMessages().getFreeFormMsg().add(fj.createFreeFormMsg(content));
  }
    
  public void addFreeFormTextFromFile(String filename) throws IOException, JAXBException, SQLException
  {
    BufferedReader in = new BufferedReader( new FileReader(filename) );
    StringBuffer buf = new StringBuffer();
    String line;
    while( (line = in.readLine()) != null )
    {
        buf.append(line);
    }
    addFreeFormText(buf.toString());
  }

  public void addAcqMerchUpdate(long oldBid, long newBid, String authName) throws JAXBException, SQLException
  {
    messageElemSetup();

    AcquirerMerchantUpdateJaxb aj = new AcquirerMerchantUpdateJaxb();
    ammf.getMessages().getAcquirerMerchantUpdate().add(aj.createAcqMerchUpd(oldBid, newBid, authName));
  }

  public void addSponsor()
  {
    throw new UnsupportedOperationException("addSponsor() not yet implemented");
  }

  public void print(String outFilename) throws JAXBException, FileNotFoundException, IOException
  {
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    ammf.setCreateDate( sdf.format(Calendar.getInstance().getTime()) );

    if(outFilename==null) {
        super.print(ammf, namespace);
    } else {
        super.print(ammf, namespace, outFilename);
    }
  }

  public void print() throws JAXBException, FileNotFoundException, IOException
  {
    print(null);
  }

  public static void main(String[] args)
  {
    AmmfJaxb a = null;
    
    try 
    {
      a = new AmmfJaxb();

      if( args[0].equals("unlinkable") )
      {
        int requestIndex = Integer.parseInt(args[2]);
        long bid = Long.parseLong(args[3]);
        a.addUnlinkableResponse(args[1], requestIndex, bid, args[4]);
      }
      else if( args[0].equals("freeform") )
      {
        a.addFreeFormTextFromFile(args[1]);
      }
      else if( args[0].equals("acqMerchUpd") )
      {
        long oldBid = Long.parseLong(args[1]);
        long newBid = Long.parseLong(args[2]);
        a.addAcqMerchUpdate(oldBid, newBid, args[3]);
      }

      a.print();
    } 
    catch (JAXBException e)
    {
      log.error("Error creating AMMF message " + e.toString());
    }
    catch (SQLException e)
    {
      log.error("Error getting data from database for AMMF message " + e.toString());
    }
    catch (SAXException e)
    {
      log.error("SAX error while creating AMMF message " + e.toString());
    }
    catch (ParserConfigurationException e)
    {
      log.error("Parser configuration error while creating AMMF message " + e.toString());
    }
    catch (FileNotFoundException e)
    {
      log.error("Couldn't find file " + e.toString());
    }
    catch (IOException e)
    {
      log.error("Error reading input file " + e.toString());
    }
    finally
    {
      a.cleanUp();
      Runtime.getRuntime().exit(0);
    }
  }
}
