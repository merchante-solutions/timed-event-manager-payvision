/*@lineinfo:filename=GRSUpload*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/startup/ChargebackUpload.sqlj $

  Description:

  Last Modified By   : $Author: jduncan $
  Last Modified Date : $Date: 2010-07-22 15:16:29 -0700 (Thu, 22 Jul 2010) $
  Version            : $Revision: 17600 $

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.sql.ResultSet;
import org.apache.log4j.Logger;
import com.mes.config.DbProperties;
import com.mes.config.MesDefaults;
import com.mes.constants.MesEmails;
import com.mes.constants.MesFlatFiles;
import com.mes.flatfile.FlatFileRecord;
import com.mes.support.LoggingConfigurator;
import com.mes.support.NumberFormatter;
import sqlj.runtime.ResultSetIterator;

/**
 * GRSUpload
 *
 * Generates Golden Retriever file for daily upload to TSYS.
 *
 * TODO: Find more details about contents of file, who uses the data, etc.
 */
 
public class GRSUpload extends TsysFileBase
{
  static { LoggingConfigurator.configure(); }
  static Logger log = Logger.getLogger(GRSUpload.class);
  
  public boolean TestMode = false;
  
  public GRSUpload( )
  {
    PropertiesFilename = "grs.properties";
    EmailGroupSuccess = MesEmails.MSG_ADDRS_OUTGOING_GRS_NOTIFY;
    EmailGroupSuccess = MesEmails.MSG_ADDRS_OUTGOING_GRS_FAILURE;
  }
  
  public boolean execute( )
  {
    String            workFilename  = null;
    long              pSeq          = 0L;
    BufferedWriter    out           = null;
    ResultSetIterator it            = null;
    ResultSet         rs            = null;
    
    FlatFileRecord  ffd           = null;
    
    
    boolean result = false;
    
    try
    {
      connect();
      
      // see if there's anything to do
      int recCount = 0;
      /*@lineinfo:generated-code*//*@lineinfo:76^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(1)
//          
//          from    grs_file_process
//          where   process_sequence = 0
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(1)\n         \n        from    grs_file_process\n        where   process_sequence = 0";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.GRSUpload",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:82^7*/
      
      log.debug(""+recCount + " files found to process");
      
      if( recCount > 0 )
      {
        // get process sequence
        /*@lineinfo:generated-code*//*@lineinfo:89^9*/

//  ************************************************************
//  #sql [Ctx] { select  vapp_sequence.nextval
//            
//            from    dual
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  vapp_sequence.nextval\n           \n          from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.startup.GRSUpload",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   pSeq = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:94^9*/
        
        log.debug("Assigning sequence " + pSeq + " to grs_file_process entries");
        
        // update records to process
        /*@lineinfo:generated-code*//*@lineinfo:99^9*/

//  ************************************************************
//  #sql [Ctx] { update  grs_file_process
//            set     process_sequence = :pSeq,
//                    process_begin_date = sysdate
//            where   process_sequence = 0
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  grs_file_process\n          set     process_sequence =  :1 ,\n                  process_begin_date = sysdate\n          where   process_sequence = 0";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.startup.GRSUpload",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,pSeq);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:105^9*/
        
        String dateString = null;
        /*@lineinfo:generated-code*//*@lineinfo:108^9*/

//  ************************************************************
//  #sql [Ctx] { select  to_char(sysdate,'mmddrr')
//            
//            from    dual
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  to_char(sysdate,'mmddrr')\n           \n          from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.startup.GRSUpload",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   dateString = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:113^9*/
        String indexString = NumberFormatter.getPaddedInt( 
          getFileId("grs3941",dateString),3);
        workFilename = "grs3941_"+dateString+"_"+indexString+".dat";
        
        log.debug("Generating Golden Retriever file " + workFilename); 
      
        // build outgoing file
        out = new BufferedWriter( new FileWriter( workFilename, false ) );
        
        // build file header
        ffd = new FlatFileRecord(MesFlatFiles.DEF_TYPE_GRS_HEADER);
        
        /*@lineinfo:generated-code*//*@lineinfo:126^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ index(dt IDX_DDF_EXT_DT_FILENAME) */
//                    sysdate as header_date,
//                    :pSeq   as header_serial_number,
//                    count(1)+2  as header_total_records
//            from    grs_file_process gfp,
//                    daily_detail_file_ext_dt dt,
//                    mif mf
//            where   gfp.process_sequence = :pSeq
//                    and gfp.load_filename = dt.load_filename
//                    and dt.merchant_number = mf.merchant_number
//                    and upper(nvl(mf.grs,'N')) = 'Y'
//            group by sysdate, :pSeq        
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ index(dt IDX_DDF_EXT_DT_FILENAME) */\n                  sysdate as header_date,\n                   :1    as header_serial_number,\n                  count(1)+2  as header_total_records\n          from    grs_file_process gfp,\n                  daily_detail_file_ext_dt dt,\n                  mif mf\n          where   gfp.process_sequence =  :2 \n                  and gfp.load_filename = dt.load_filename\n                  and dt.merchant_number = mf.merchant_number\n                  and upper(nvl(mf.grs,'N')) = 'Y'\n          group by sysdate,  :3";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.startup.GRSUpload",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,pSeq);
   __sJT_st.setLong(2,pSeq);
   __sJT_st.setLong(3,pSeq);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.startup.GRSUpload",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:140^9*/
        
        rs = it.getResultSet();
        
        if( rs.next( ) )
        {
          ffd.setAllFieldData( rs );
        }
        out.write( ffd.spew() );
        out.newLine();
        
        rs.close();
        it.close();
        
        // detail records...
        ffd = new FlatFileRecord(MesFlatFiles.DEF_TYPE_GRS_DETAIL);

        /*@lineinfo:generated-code*//*@lineinfo:157^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  
//             /*+ index(dt IDX_DDF_EXT_DT_FILENAME) */
//             mf.merchant_number    as detail_mid,
//             sys.dbms_crypto.hash( 
//              utl_raw.cast_to_raw(
//               dukpt_decrypt_wrapper(dt.card_number_enc)),2) 
//                                   as detail_card_accnt_num,
//             dt.transaction_date   as detail_date_of_charge,
//             decode(dt.debit_credit_indicator,
//               'C', 'D', 'S')      as detail_transaction_type,
//             dt.debit_credit_indicator 
//                                   as debit_credit_indicator,
//             dt.transaction_amount as tran_amount,
//             ltrim(
//              rtrim(
//               to_char(dt.transaction_amount,'000000000000.00'))) 
//                                   as detail_charge_amount,
//             mf.sic_code           as detail_mcc_code,
//             mf.dba_name           as detail_dba_name,
//             dt.card_type          as detail_card_type,
//             dt.reference_number   as detail_ref_number,
//             '120000'              as detail_transaction_time
//            from
//             ( select 
//                distinct 
//                 process_sequence, 
//                 load_filename
//               from 
//                grs_file_process
//               where
//                process_sequence = :pSeq ) gfp,
//             daily_detail_file_ext_dt dt,
//             mif mf
//            where   
//             gfp.load_filename = dt.load_filename
//             and dt.merchant_number = mf.merchant_number
//             and upper(nvl(mf.grs,'N')) = 'Y'
//  
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  \n           /*+ index(dt IDX_DDF_EXT_DT_FILENAME) */\n           mf.merchant_number    as detail_mid,\n           sys.dbms_crypto.hash( \n            utl_raw.cast_to_raw(\n             dukpt_decrypt_wrapper(dt.card_number_enc)),2) \n                                 as detail_card_accnt_num,\n           dt.transaction_date   as detail_date_of_charge,\n           decode(dt.debit_credit_indicator,\n             'C', 'D', 'S')      as detail_transaction_type,\n           dt.debit_credit_indicator \n                                 as debit_credit_indicator,\n           dt.transaction_amount as tran_amount,\n           ltrim(\n            rtrim(\n             to_char(dt.transaction_amount,'000000000000.00'))) \n                                 as detail_charge_amount,\n           mf.sic_code           as detail_mcc_code,\n           mf.dba_name           as detail_dba_name,\n           dt.card_type          as detail_card_type,\n           dt.reference_number   as detail_ref_number,\n           '120000'              as detail_transaction_time\n          from\n           ( select \n              distinct \n               process_sequence, \n               load_filename\n             from \n              grs_file_process\n             where\n              process_sequence =  :1  ) gfp,\n           daily_detail_file_ext_dt dt,\n           mif mf\n          where   \n           gfp.load_filename = dt.load_filename\n           and dt.merchant_number = mf.merchant_number\n           and upper(nvl(mf.grs,'N')) = 'Y'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.startup.GRSUpload",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,pSeq);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.startup.GRSUpload",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:197^9*/
        
        rs = it.getResultSet();
        
        while(rs.next())
        {
          ffd.resetAllFields();
          
          ffd.setAllFieldData( rs );
          
          // update record counts and amounts so that email will be accurate
          ++super.FileRecordCount;
          
          if( "C".equals(rs.getString("debit_credit_indicator")) )
          {
            ++super.FileCreditsCount;
            super.FileCreditsAmount += rs.getDouble("tran_amount");
          }
          else
          {
            ++super.FileDebitsCount;
            super.FileDebitsAmount += rs.getDouble("tran_amount");
          }
          
          out.write( ffd.spew() );
          out.newLine();
        }
        
        rs.close();
        it.close();
        
        // trailer
        ffd = new FlatFileRecord(MesFlatFiles.DEF_TYPE_GRS_TRAILER);
        
        /*@lineinfo:generated-code*//*@lineinfo:231^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  
//             /*+ index(dt IDX_DDF_EXT_DT_FILENAME) */
//             sysdate     as trailer_date,
//             count(1)    as trailer_record_count,
//             ltrim(rtrim(to_char(sum(
//               decode(
//                dt.debit_credit_indicator,'C',-1,1) 
//                  * dt.transaction_amount),
//                '000000000000.00'))) 
//                         as trailer_net_dollar_amount
//            from    
//             ( select 
//                distinct 
//                 process_sequence, 
//                 load_filename
//               from 
//                grs_file_process
//               where
//                process_sequence = :pSeq ) gfp,
//             daily_detail_file_ext_dt dt,
//             mif mf 
//            where   
//             gfp.load_filename = dt.load_filename
//             and dt.merchant_number = mf.merchant_number   
//             and upper(nvl(mf.grs,'N')) = 'Y'
//            group by sysdate 
//            
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  \n           /*+ index(dt IDX_DDF_EXT_DT_FILENAME) */\n           sysdate     as trailer_date,\n           count(1)    as trailer_record_count,\n           ltrim(rtrim(to_char(sum(\n             decode(\n              dt.debit_credit_indicator,'C',-1,1) \n                * dt.transaction_amount),\n              '000000000000.00'))) \n                       as trailer_net_dollar_amount\n          from    \n           ( select \n              distinct \n               process_sequence, \n               load_filename\n             from \n              grs_file_process\n             where\n              process_sequence =  :1  ) gfp,\n           daily_detail_file_ext_dt dt,\n           mif mf \n          where   \n           gfp.load_filename = dt.load_filename\n           and dt.merchant_number = mf.merchant_number   \n           and upper(nvl(mf.grs,'N')) = 'Y'\n          group by sysdate";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.startup.GRSUpload",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,pSeq);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"6com.mes.startup.GRSUpload",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:260^9*/
        
        rs = it.getResultSet();
        
        if(rs.next())
        {
          ffd.setAllFieldData( rs );
          out.write( ffd.spew() );
          out.newLine();
        }
        
        rs.close();
        it.close();
        
        // close outgoing file
        out.close();


        String host = MesDefaults.getString(MesDefaults.DK_GRS_OUTGOING_HOST);
        String user = MesDefaults.getString(MesDefaults.DK_GRS_OUTGOING_USER);

          // send file
        if( TestMode == false &&  sendFile( workFilename,
                      host,
                      user,
                      MesDefaults.getString(MesDefaults.DK_GRS_OUTGOING_PASSWORD),
                      false) == true )
        {
          log.info("Successful file generation and transfer: " 
            + workFilename + " sent to " + user + "@" + host);
          archiveDailyFile(workFilename);
        }
        
        /*@lineinfo:generated-code*//*@lineinfo:293^9*/

//  ************************************************************
//  #sql [Ctx] { update  grs_file_process
//            set     process_end_date = sysdate
//            where   process_sequence = :pSeq
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  grs_file_process\n          set     process_end_date = sysdate\n          where   process_sequence =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"7com.mes.startup.GRSUpload",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,pSeq);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:298^9*/
      }
      
      result = true;
    }
    catch(Exception e)
    {
      log.error("Error in execute()",e);
      
      if (pSeq != 0L) {
      
        try {
        
          log.info("Flagging unsuccessful files in grs_file_process with"
            + " negative sequence: " + (pSeq * -1));
            
          /*@lineinfo:generated-code*//*@lineinfo:314^11*/

//  ************************************************************
//  #sql [Ctx] { update grs_file_process
//              set process_sequence = :pSeq * -1
//              where process_sequence = :pSeq
//              
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_4237 = pSeq * -1;
   String theSqlTS = "update grs_file_process\n            set process_sequence =  :1 \n            where process_sequence =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"8com.mes.startup.GRSUpload",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_4237);
   __sJT_st.setLong(2,pSeq);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:320^11*/
        }
        catch (Exception ie) {
          log.error("Unable to flag files in grs_file_process with sequence " + pSeq,ie);
        }
      }
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    
    return( result );
  }
  
  public static void main(String[] args)
  {
    try {
        if (args.length > 0 && args[0].equals("testproperties")) {
            EventBase.printKeyListStatus(new String[]{
                    MesDefaults.DK_GRS_OUTGOING_HOST,
                    MesDefaults.DK_GRS_OUTGOING_USER,
                    MesDefaults.DK_GRS_OUTGOING_PASSWORD
            });
        }

      GRSUpload worker = new GRSUpload();
      
      // make sure db properties are loaded
      DbProperties.requireConfiguration();
      
      // disabling this to allow actual manual reprocessing
      //worker.TestMode = true;
      
      worker.execute();
    }
    catch(Exception e)
    {
      System.out.println("main(): " + e.toString());
    }
  }
}/*@lineinfo:generated-code*/