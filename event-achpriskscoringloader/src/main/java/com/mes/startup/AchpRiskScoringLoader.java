/*@lineinfo:filename=AchpRiskScoringLoader*//*@lineinfo:user-code*//*@lineinfo:1^1*/package com.mes.startup;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import org.apache.log4j.Logger;
import com.mes.database.SQLJConnectionBase;
import com.mes.reports.RiskScoreDataBean;
import com.mes.support.DateTimeFormatter;
import com.mes.tools.FileTransmissionLogger;
import sqlj.runtime.ResultSetIterator;

public class AchpRiskScoringLoader extends EventBase {
	static Logger log = Logger.getLogger(AchpRiskScoringLoader.class);
	//define ACHP constants for each process type  (each risk rule). Also stored in DB - risk_process_types
	
		public static final int PT_ACHP_RETURN_POINTS 					= 37; 
		public static final int PT_ACHP_MONTHLY_PROCESSING_AMOUNT 		= 38;
		public static final int PT_ACHP_DUPLICATE_DDA_COUNT 			= 39; 	
		public static final int PT_ACHP_DUPLICATE_CUSTOMER_NAME_COUNT 	= 40;
		public static final int PT_ACHP_DAILY_SALES_COUNT 				= 41;
		public static final int PT_ACHP_INDIVIDUAL_SALE_AMOUNT 			= 42;
		public static final int PT_ACHP_INDIVIDUAL_CREDIT_AMOUNT 		= 43;
		public static final int PT_ACHP_DAILY_SALES_VOLUME 				= 44;
		public static final int PT_ACHP_DAILY_CREDIT_COUNT 				= 45;
		public static final int PT_ACHP_AVERAGE_TICKET_AMOUNT			= 46;
		public static final int PT_ACHP_DAILY_CREDIT_VOLUME 			= 47;
		
		
		//constants for achp risk rules (these are also stored in DB - ACHP_RISK_RULE_DEF)
		
		public static final String RISK_RULE_SALE_COUNT = "SALE_COUNT";
		public static final String RISK_RULE_SALE_VOLUME = "SALE_VOLUME";
		public static final String RISK_RULE_SALE_AMOUNT = "SALE_AMOUNT";	
		public static final String RISK_RULE_CREDIT_COUNT = "CREDIT_COUNT";
		public static final String RISK_RULE_CREDIT_VOLUME = "CREDIT_VOLUME";
		public static final String RISK_RULE_CREDIT_AMOUNT = "CREDIT_AMOUNT";
		public static final String RISK_RULE_AVERAGE_TICKET = "AVERAGE_TICKET";
		public static final String RISK_RULE_SUMMARIZE = "SUMMARIZE";
		public static final String RISK_RULE_ACH_RETURN_COUNT = "ACH_RETURN_COUNT";
		public static final String RISK_RULE_MONTHLY_PROCE_AMT = "MONTHLY_PROCESSING_AMT";
		public static final String RISK_RULE_DUPLICATE_DDA_COUNT = "DUPLICATE_DDA_COUNT";
		public static final String RISK_RULE_DUP_CUST_NAME = "DUPLICATE_CUSTOMER_NAME_COUNT";
	

	public AchpRiskScoringLoader() {
	}

	public int getPoints(long merchantId, int catId, double value) {
		return (getPoints(merchantId, catId, value, 0));
	}

	public int getPoints( long merchantId, int catId, double value, long thresholdValue )
	  {
	    long      nodeId        = merchantId;
	    int       retVal        = 0;
	    int       rowCount      = 0;

	    try
	    {
	      while( true )
	      {
	    	  //----------------------------------------------------------
	    	  //get risk_score_system records count for merchantId first.
	    	  //if you find it, break the while loop. Go to #B Section
	    	  //----------------------------------------------------------
	        /*@lineinfo:generated-code*//*@lineinfo:74^10*/

//  ************************************************************
//  #sql [Ctx] { select  count(hierarchy_node) 
//  	          from    risk_score_system rss
//  	          where   rss.hierarchy_node = :nodeId and
//  	                  rss.risk_cat_id = :catId
//  	         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(hierarchy_node)  \n\t          from    risk_score_system rss\n\t          where   rss.hierarchy_node =  :1  and\n\t                  rss.risk_cat_id =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.AchpRiskScoringLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   __sJT_st.setInt(2,catId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   rowCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:80^10*/

	        if( rowCount > 0 )
	        {
	          break;
	        }

	        // no rows found for the current node, get the parent node id
	        try
	        {
	          /*@lineinfo:generated-code*//*@lineinfo:90^12*/

//  ************************************************************
//  #sql [Ctx] { select  orgp.org_group        
//  	            from    organization      orgp,
//  	                    organization      orgc,
//  	                    parent_org        po
//  	            where   orgc.org_group = :nodeId and
//  	                    po.org_num = orgc.org_num and
//  	                    orgp.org_num = po.parent_org_num
//  	           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  orgp.org_group         \n\t            from    organization      orgp,\n\t                    organization      orgc,\n\t                    parent_org        po\n\t            where   orgc.org_group =  :1  and\n\t                    po.org_num = orgc.org_num and\n\t                    orgp.org_num = po.parent_org_num";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.startup.AchpRiskScoringLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   nodeId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:99^12*/
	        }
	        catch( java.sql.SQLException e )
	        {
	          // no parent found, return 0L
	          nodeId = 0L;
	          break;
	        }
	      }

	      //------------------------------------------------------------------------------------------------------
	      // #B Section
	      //get record count from RSS for given nodeId and CatId and threshhold values is >= table threshhold
	      //return the points.
	      //------------------------------------------------------------------------------------------------------
	      
	      
	      /*@lineinfo:generated-code*//*@lineinfo:116^8*/

//  ************************************************************
//  #sql [Ctx] { select  count(rss.points),
//  	                nvl(max(rss.points),0)   
//  	        from    risk_score_system   rss
//  	        where   rss.hierarchy_node = :nodeId and
//  	                rss.risk_cat_id = :catId and
//  	                :value between rss.lower_range and rss.upper_range and
//  	                :thresholdValue >= nvl(rss.threshold,0)
//  	       };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(rss.points),\n\t                nvl(max(rss.points),0)    \n\t        from    risk_score_system   rss\n\t        where   rss.hierarchy_node =  :1  and\n\t                rss.risk_cat_id =  :2  and\n\t                 :3  between rss.lower_range and rss.upper_range and\n\t                 :4  >= nvl(rss.threshold,0)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.startup.AchpRiskScoringLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   __sJT_st.setInt(2,catId);
   __sJT_st.setDouble(3,value);
   __sJT_st.setLong(4,thresholdValue);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   rowCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   retVal = __sJT_rs.getInt(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:125^8*/
	    }
	    catch( Exception e )
	    {
	      StringBuffer    buf = new StringBuffer("getPoints(");

	      buf.append( merchantId );
	      buf.append( "," );
	      buf.append( catId );
	      buf.append( "," );
	      buf.append( value );
	      buf.append( "," );
	      buf.append( thresholdValue );
	      buf.append( ")" );

	      logEntry( buf.toString(),e.toString() );
	    }
	    return( retVal );
	  }
	
	
	/*
	 * Returns all risk rules proces types in list format
	 * 
	 */
	public static ArrayList getRiskRules(){
		ArrayList<Integer> riskRules = new ArrayList();
		
		riskRules.add(PT_ACHP_AVERAGE_TICKET_AMOUNT);
		riskRules.add(PT_ACHP_DAILY_CREDIT_COUNT);
		riskRules.add(PT_ACHP_DAILY_CREDIT_VOLUME);
		riskRules.add(PT_ACHP_DAILY_SALES_COUNT);
		riskRules.add(PT_ACHP_DAILY_SALES_VOLUME);
		riskRules.add(PT_ACHP_DUPLICATE_CUSTOMER_NAME_COUNT);
		riskRules.add(PT_ACHP_DUPLICATE_DDA_COUNT);
		riskRules.add(PT_ACHP_INDIVIDUAL_CREDIT_AMOUNT);
		riskRules.add(PT_ACHP_INDIVIDUAL_SALE_AMOUNT);
		riskRules.add(PT_ACHP_MONTHLY_PROCESSING_AMOUNT);
		riskRules.add(PT_ACHP_RETURN_POINTS);
		return riskRules;
		
	}

	/*
	 * * METHOD execute** Entry point from timed event manager.
	 */
	public boolean execute() {
		log.debug("Executing AchpRiskScoringLoader event...");
		boolean fileProcessed = false;
		ResultSetIterator it = null;
		int itemCount = 0;
		String loadFilename = null;
		int procType = -1;
		long recId = 0L;
		ResultSet resultSet = null;
		int sequenceId = 0;
		

		try {
			connect(true);
			try
		      {
		        procType = Integer.parseInt(getEventArg(0));
		      }
		      catch ( Exception ee )
		      {
		        procType = -1;
		      }
			
			//nach394100000_092814_T001.dat
			//When nach file is sent to bank, entry will be made to risk_process
			//table with 0 proc_seq. Using this file, we will identify transactions
			//that will be scanned for risk
			
		    if ( procType == -1 )   // process table event
		      {
		    	/*@lineinfo:generated-code*//*@lineinfo:201^8*/

//  ************************************************************
//  #sql [Ctx] { select  count(load_filename)
//  		    	          
//  		    	          from    mes.risk_process
//  		    	          where   process_sequence = 0
//  		    	          and load_filename like 'nach%'
//  		    	         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(load_filename)\n\t\t    \t           \n\t\t    \t          from    mes.risk_process\n\t\t    \t          where   process_sequence = 0\n\t\t    \t          and load_filename like 'nach%'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.startup.AchpRiskScoringLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   itemCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:208^16*/
		    	        
		    	        if ( itemCount > 0 )
		    	        {

		    	        	/*@lineinfo:generated-code*//*@lineinfo:213^17*/

//  ************************************************************
//  #sql [Ctx] { select  risk_process_sequence.nextval 
//  		    	        	  from    dual
//  		    	             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  risk_process_sequence.nextval  \n\t\t    \t        \t  from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.startup.AchpRiskScoringLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   sequenceId = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:217^20*/

		    	            //update ach files
		    	        	 /*@lineinfo:generated-code*//*@lineinfo:220^18*/

//  ************************************************************
//  #sql [Ctx] { update  risk_process
//  		    	        	   set     process_sequence = :sequenceId
//  		    	        	    where   process_sequence = 0
//  		    	        	    and load_filename like 'nach%'
//  		    	        	   };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  risk_process\n\t\t    \t        \t   set     process_sequence =  :1 \n\t\t    \t        \t    where   process_sequence = 0\n\t\t    \t        \t    and load_filename like 'nach%'";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"5com.mes.startup.AchpRiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,sequenceId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:226^19*/

		    	        	          /*@lineinfo:generated-code*//*@lineinfo:228^27*/

//  ************************************************************
//  #sql [Ctx] it = { select  rec_id,
//  		    	        	                    load_filename,
//  		    	        	                    process_type
//  		    	        	            from    risk_process
//  		    	        	            where   process_sequence = :sequenceId
//  		    	        	            	    and load_filename like 'nach%'
//  		    	        	            order by process_type
//  		    	        	           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  rec_id,\n\t\t    \t        \t                    load_filename,\n\t\t    \t        \t                    process_type\n\t\t    \t        \t            from    risk_process\n\t\t    \t        \t            where   process_sequence =  :1 \n\t\t    \t        \t            \t    and load_filename like 'nach%'\n\t\t    \t        \t            order by process_type";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.startup.AchpRiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,sequenceId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"6com.mes.startup.AchpRiskScoringLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:237^27*/
		    	        	          resultSet = it.getResultSet();
		    	        	          
		    	        	          //for each of the process types, run separate method 

		    	        	          while( resultSet.next() )
		    	        	          {
		    	        	            loadFilename  = resultSet.getString("load_filename");
		    	        	            procType      = resultSet.getInt("process_type");
		    	        	            recId         = resultSet.getLong("rec_id");

		    	        	            recordTimestampBegin( recId );
		    	        	           

		    	        	            fileProcessed = false;     // default

		    	        	            switch( procType )
		    	        	            {		
		    	        	            	case PT_ACHP_DAILY_CREDIT_VOLUME:
		    	        	            		fileProcessed = loadPointsDailyCreditVolume(loadFilename);
		    	        	            		break;
		    	        	            		
		    	        	            	case PT_ACHP_DAILY_SALES_VOLUME:
		    	        	            		fileProcessed = loadPointsDailySalesVolume(loadFilename);
		    	        	            		break;
		    	        	            		
		    	        	            	case PT_ACHP_DAILY_SALES_COUNT:
		    	        	            		fileProcessed = loadPointsDailySalesCount(loadFilename);
		    	        	            		break;
		    	        	            		
		    	        	            	case PT_ACHP_DAILY_CREDIT_COUNT:
		    	        	            		fileProcessed = loadPointsDailyCreditCount(loadFilename);
		    	        	            		break;
		    	        	            		
		    	        	            	case PT_ACHP_AVERAGE_TICKET_AMOUNT:
		    	        	            		fileProcessed = loadPointsAvgTicketAmount(loadFilename);
		    	        	            		break;
		    	        	            		
		    	        	            	case PT_ACHP_DUPLICATE_CUSTOMER_NAME_COUNT:
		    	        	            		fileProcessed = loadPointsDuplicateCustomerName(loadFilename);
		    	        	            		break;

		    	        	            	case PT_ACHP_DUPLICATE_DDA_COUNT:
		    	        	            		fileProcessed = loadPointsDuplicateDDA(loadFilename);
		    	        	            		break;
		    	        	            		
		    	        	            	case PT_ACHP_INDIVIDUAL_SALE_AMOUNT:
		    	        	            		fileProcessed = loadPointsIndividualSaleAmt(loadFilename);
		    	        	            		break;
		    	        	            	case PT_ACHP_INDIVIDUAL_CREDIT_AMOUNT:
		    	        	            		fileProcessed = loadPointsIndividualCreditAmt(loadFilename);
		    	        	            		break;
		    	        	            	case PT_ACHP_MONTHLY_PROCESSING_AMOUNT:
		    	        	            		fileProcessed = loadPointsMonthlyProcessingVolume(loadFilename);
		    	        	            		break;
		    	        	            	case PT_ACHP_RETURN_POINTS:
		    	        	            		fileProcessed = loadPointsACHPReturns(loadFilename);
		    	        	            		break;
		    	        	            		
		    	        	            }
		    	        	            if ( fileProcessed == true )
		    	        	            {
		    	        	              
		    	        	              FileTransmissionLogger.logStage( loadFilename, FileTransmissionLogger.STAGE_RISK_SUMMARY );
		    	        	              recordTimestampEnd( recId );
		    	        	            }
		    	        	          }
		    	       }
		    	        

		      }
			

		} catch (Exception e) {
			logEvent(this.getClass().getName(), "execute()", e.toString());
			logEntry("execute()", e.toString());
		} finally {
			try {
				it.close();
			} catch (Exception e) {
			}
			cleanUp();
		}

		return (true);
	}

	public long  getAchFileId(String fileName) throws SQLException {
			
		long fileId = 0;
			
		 /*@lineinfo:generated-code*//*@lineinfo:328^4*/

//  ************************************************************
//  #sql [Ctx] { select file_id  from mes.achp_files 
//  			where file_name = :fileName
//  		   };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select file_id   from mes.achp_files \n\t\t\twhere file_name =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.startup.AchpRiskScoringLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,fileName);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   fileId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:332^5*/
			          
	    return fileId;
		}
	
	private void storeRiskScore(long merchantId, int points, String desc, Date activityDate, int procType, String fileName, long achpFileId) throws SQLException {
		
		log.info("Scored " + points + " point for procType " + procType);
		System.out.println("Scored " + points + " for procType " + procType);
		 /*@lineinfo:generated-code*//*@lineinfo:341^4*/

//  ************************************************************
//  #sql [Ctx] { insert into mes.risk_score
//  		              (
//  		                merchant_number,
//  		                activity_date,
//  		                risk_cat_id,
//  		                points,
//  		                score_desc,
//  		                load_filename,
//  		                load_file_id
//  		              )
//  		              values
//  		              (
//  		                :merchantId,
//  		                trunc(:activityDate),
//  		                :procType,
//  		                :points,
//  		                substr( :desc.toString(),1,64 ),
//  		                :fileName,
//  		                :achpFileId
//  		              )
//  		             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3657 = desc.toString();
   String theSqlTS = "insert into mes.risk_score\n\t\t              (\n\t\t                merchant_number,\n\t\t                activity_date,\n\t\t                risk_cat_id,\n\t\t                points,\n\t\t                score_desc,\n\t\t                load_filename,\n\t\t                load_file_id\n\t\t              )\n\t\t              values\n\t\t              (\n\t\t                 :1 ,\n\t\t                trunc( :2 ),\n\t\t                 :3 ,\n\t\t                 :4 ,\n\t\t                substr(  :5 ,1,64 ),\n\t\t                 :6 ,\n\t\t                 :7 \n\t\t              )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"8com.mes.startup.AchpRiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,activityDate);
   __sJT_st.setInt(3,procType);
   __sJT_st.setInt(4,points);
   __sJT_st.setString(5,__sJT_3657);
   __sJT_st.setString(6,fileName);
   __sJT_st.setLong(7,achpFileId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:363^15*/
		
	}
	
	/* Lower Range : % of Threshold
	 * Upper Range : % of Threshold
	 * Threshold   : daily sales$ / daily sales count
	 */
	
	private boolean loadPointsAvgTicketAmount(String fileName){
		log.info("Calculating points for avg ticket amount for fileName - " + fileName);
		
		StringBuffer                desc            = new StringBuffer();
	    boolean                     fileProcessed   = false;
	    ResultSetIterator           it              = null;
	    long                        achpFileId      = 0L;
	    int                         points          = 0;
	    long 						threshold		= 0L;
	    ResultSet                   resultSet       = null;
	    
	    try{
	    	connect(true);
	    	 achpFileId =  getAchFileId(fileName);
	    	
	    	 // remove any previous entries in the scoring system
	    	 /*@lineinfo:generated-code*//*@lineinfo:388^8*/

//  ************************************************************
//  #sql [Ctx] { delete
//  		        from    risk_score
//  		        where   load_file_id = :achpFileId
//  		        and risk_cat_id = :PT_ACHP_AVERAGE_TICKET_AMOUNT
//  		        };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n\t\t        from    risk_score\n\t\t        where   load_file_id =  :1 \n\t\t        and risk_cat_id =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.startup.AchpRiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,achpFileId);
   __sJT_st.setInt(2,PT_ACHP_AVERAGE_TICKET_AMOUNT);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:392^10*/
	    		 
		       //get data
		       /*@lineinfo:generated-code*//*@lineinfo:395^10*/

//  ************************************************************
//  #sql [Ctx] it = { select trunc (achpf.create_ts) as activity_date,
//                         achpf.file_id as fileid,
//                         achpt.merch_num as merchant_number,
//                         achpt.profile_id as profile_id,         
//                         achpt.tran_type as tran_type,                       
//                         count(achpt.profile_id) as tot_tran_count,
//                         sum(achpt.amount) as to_sale_amount,
//                         nvl(sum(achpt.amount), 0) /  nvl(count(achpt.profile_id), 1)  as avg_ticket,
//                         arr.rule_data as threshold_data,                       
//                         round((decode(to_number (
//                               substr (trim( arr.rule_data), instr (trim( arr.rule_data), '=') + 1)), 0, 1, (nvl(sum(achpt.amount), 0) /nvl(count(achpt.profile_id), 1) )/ to_number (
//                               substr (trim( arr.rule_data), instr (trim( arr.rule_data), '=') + 1)) ) *100),2)
//                           as avg_ticket_percent
//                    from mes.achp_files achpf,
//                         mes.achp_file_details achpfd,
//                         mes.achp_transactions achpt,
//                         mes.achp_risk_rules arr
//                   where     achpf.file_id = :achpFileId
//                         and achpf.file_id = achpfd.file_id
//                         and achpfd.source_id = achpt.tran_id
//                         and achpt.profile_id = arr.profile_id
//                         and arr.def_code = :RISK_RULE_AVERAGE_TICKET
//                         and achpt.tran_type = 'SALE'
//                group by achpt.profile_id,
//                         achpt.tran_type,
//                         achpf.create_ts,
//                         arr.rule_data,
//                         achpf.file_id,
//                         achpt.merch_num			     
//  			    	  };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select trunc (achpf.create_ts) as activity_date,\n                       achpf.file_id as fileid,\n                       achpt.merch_num as merchant_number,\n                       achpt.profile_id as profile_id,         \n                       achpt.tran_type as tran_type,                       \n                       count(achpt.profile_id) as tot_tran_count,\n                       sum(achpt.amount) as to_sale_amount,\n                       nvl(sum(achpt.amount), 0) /  nvl(count(achpt.profile_id), 1)  as avg_ticket,\n                       arr.rule_data as threshold_data,                       \n                       round((decode(to_number (\n                             substr (trim( arr.rule_data), instr (trim( arr.rule_data), '=') + 1)), 0, 1, (nvl(sum(achpt.amount), 0) /nvl(count(achpt.profile_id), 1) )/ to_number (\n                             substr (trim( arr.rule_data), instr (trim( arr.rule_data), '=') + 1)) ) *100),2)\n                         as avg_ticket_percent\n                  from mes.achp_files achpf,\n                       mes.achp_file_details achpfd,\n                       mes.achp_transactions achpt,\n                       mes.achp_risk_rules arr\n                 where     achpf.file_id =  :1 \n                       and achpf.file_id = achpfd.file_id\n                       and achpfd.source_id = achpt.tran_id\n                       and achpt.profile_id = arr.profile_id\n                       and arr.def_code =  :2 \n                       and achpt.tran_type = 'SALE'\n              group by achpt.profile_id,\n                       achpt.tran_type,\n                       achpf.create_ts,\n                       arr.rule_data,\n                       achpf.file_id,\n                       achpt.merch_num";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.startup.AchpRiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,achpFileId);
   __sJT_st.setString(2,RISK_RULE_AVERAGE_TICKET);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"10com.mes.startup.AchpRiskScoringLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:424^10*/
		       
		       	resultSet = it.getResultSet(); 
			    while(resultSet.next()){
			    	
			    	//get threshhold value from achp_risk_rules table for the merchant
			    	threshold = getThreshold( resultSet.getString("profile_id"), this.RISK_RULE_AVERAGE_TICKET);
			    	
			    	//get points and add to risk score
			    	  points = getPoints( resultSet.getLong("merchant_number"),
	                            RiskScoreDataBean.TT_AVERAGE_TICKET_AMOUNT ,
	                            resultSet.getDouble("avg_ticket_percent"), threshold);
			    		//add to risk score table
				    	
				    	 if ( points != 0 )
				          {
				    		 desc.setLength(0);
					         desc.append( "Daily Avg Ticket Amount : ");
					         desc.append(resultSet.getDouble("avg_ticket_percent"));
					         desc.append(" %. Threshold is ");
					         desc.append(threshold);
					            
				    		 storeRiskScore(resultSet.getLong("merchant_number"), points, desc.toString(), resultSet.getDate("activity_date"), this.PT_ACHP_AVERAGE_TICKET_AMOUNT, fileName, achpFileId);
				          }			    	
				    }
				    
				    resultSet.close();
				    it.close();
				    fileProcessed = true;
		       
	    }catch(Exception e){
	    	logEntry("loadPointsAvgTicketAmount() : ",e.toString());
	    	log.error("loadPointsAvgTicketAmount() : " + e.toString());
	    }finally{
	    	try{ it.close();   cleanUp(); } catch(Exception ee){}
	    }
		return fileProcessed;	
		
	}

	/**
	 * Calculating ACHP Return points.
	 * Checking how many returns per profile are recorded in ACHP_RETURN table and showing that count
	 */
	
	private boolean loadPointsACHPReturns(String fileName){
		log.info("Calculating points for ACHP return count");
		
		StringBuffer                desc            = new StringBuffer();
	    boolean                     fileProcessed   = false;
	    ResultSetIterator           it              = null;	    
	    int                         points          = 0;
	    long                        achpFileId      = 0L;
	    long 						threshold		= 0L;
	    ResultSet                   resultSet       = null;
	    
	    try{
	    	connect(true);	    	
	    	 achpFileId =  getAchFileId(fileName);
	    	 // remove any previous entries in the scoring system for today
	    	 /*@lineinfo:generated-code*//*@lineinfo:484^8*/

//  ************************************************************
//  #sql [Ctx] { delete
//  		        from    risk_score
//  		        where   activity_date = sysdate
//  		        and risk_cat_id = :PT_ACHP_RETURN_POINTS
//  		        };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n\t\t        from    risk_score\n\t\t        where   activity_date = sysdate\n\t\t        and risk_cat_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.startup.AchpRiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,PT_ACHP_RETURN_POINTS);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:488^10*/
	    		 
		       //get data
		       /*@lineinfo:generated-code*//*@lineinfo:491^10*/

//  ************************************************************
//  #sql [Ctx] it = { select ar.return_id, ar.tran_id, ar.profile_id as profile_id, 
//  		    		   ar.create_ts as activity_date, at.merch_num as merchant_number, count(ar.return_id) as return_count
//  		    		   from achp_returns ar, achp_transactions at
//  		    		   where ar.tran_id = at.tran_id
//  		    		   and ar.profile_id = at.profile_id
//  		    		   and create_ts > sysdate -1
//  		    		   group by ar.profile_id, ar.return_id, ar.tran_id,ar.create_ts, at.merch_num	     
//  			    	  };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select ar.return_id, ar.tran_id, ar.profile_id as profile_id, \n\t\t    \t\t   ar.create_ts as activity_date, at.merch_num as merchant_number, count(ar.return_id) as return_count\n\t\t    \t\t   from achp_returns ar, achp_transactions at\n\t\t    \t\t   where ar.tran_id = at.tran_id\n\t\t    \t\t   and ar.profile_id = at.profile_id\n\t\t    \t\t   and create_ts > sysdate -1\n\t\t    \t\t   group by ar.profile_id, ar.return_id, ar.tran_id,ar.create_ts, at.merch_num";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.startup.AchpRiskScoringLoader",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"12com.mes.startup.AchpRiskScoringLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:498^10*/
		       
		       	resultSet = it.getResultSet(); 
			    while(resultSet.next()){
			    	
			    	//get threshhold value from achp_risk_rules table for the merchant
			    	threshold = getThreshold( resultSet.getString("profile_id"), this.RISK_RULE_ACH_RETURN_COUNT);
			    	
			    	//get points and add to risk score
			    	  points = getPoints( resultSet.getLong("merchant_number"),
	                            RiskScoreDataBean.TT_ACHP_RETURNS ,
	                            resultSet.getInt("return_count"), threshold);
			    		//add to risk score table
				    	
				    	 if ( points != 0 )
				          {
				    		 desc.setLength(0);
					         desc.append( "Daily ACHP Return Count : ");
					         desc.append(resultSet.getInt("return_count"));
					         desc.append(" for profile Id ");
					         desc.append(resultSet.getString("profile_id"));
					         desc.append(" . Threshold is ");
					         desc.append(threshold);
					            
				    		 storeRiskScore(resultSet.getLong("merchant_number"), points, desc.toString(), resultSet.getDate("activity_date"), this.PT_ACHP_RETURN_POINTS, fileName, achpFileId);
				          }			    	
				    }
				    
				    resultSet.close();
				    it.close();
				    fileProcessed = true;
		       
	    }catch(Exception e){
	    	logEntry("loadPointsACHPReturns() : ", e.toString());
	    	log.error("loadPointsACHPReturns() : " + e.toString());
	    }finally{
	    	try{ it.close();   cleanUp(); } catch(Exception ee){}
	    }
		return fileProcessed;	
		
	}

	
	private boolean loadPointsDuplicateCustomerName(String fileName){
		log.info("Calculating points for daily credit count for fileName - " + fileName);
		
		StringBuffer                desc            = new StringBuffer();
	    boolean                     fileProcessed   = false;
	    ResultSetIterator           it              = null;
	    long                        achpFileId      = 0L;
	    int                         points          = 0;
	    long 						threshold		= 0L;
	    ResultSet                   resultSet       = null;
	    
	    try{
	    	connect(true);
	    	 achpFileId =  getAchFileId(fileName);
	    	
	    	 // remove any previous entries in the scoring system
	    	 /*@lineinfo:generated-code*//*@lineinfo:557^8*/

//  ************************************************************
//  #sql [Ctx] { delete
//  		        from    risk_score
//  		        where   load_file_id = :achpFileId
//  		        and risk_cat_id = :PT_ACHP_DUPLICATE_CUSTOMER_NAME_COUNT
//  		        };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n\t\t        from    risk_score\n\t\t        where   load_file_id =  :1 \n\t\t        and risk_cat_id =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"13com.mes.startup.AchpRiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,achpFileId);
   __sJT_st.setInt(2,PT_ACHP_DUPLICATE_CUSTOMER_NAME_COUNT);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:561^10*/
	    		 
		       //get data
		       /*@lineinfo:generated-code*//*@lineinfo:564^10*/

//  ************************************************************
//  #sql [Ctx] it = { select trunc (achpf.create_ts) as activity_date,
//                         achpf.file_id as fileid,
//                         achpt.merch_num as merchant_number,
//                         achpt.profile_id as profile_id,
//                         arr.rule_data as threshold_data,
//                         achpt.cust_name as cust_name,
//                         count(achpt.cust_name) as dup_name_count,
//                         round(decode(to_number (
//                               substr (trim( arr.rule_data), instr (trim( arr.rule_data), '=') + 1)), 0, 1, (nvl(count(achpt.cust_name), 0)/ to_number (
//                               substr (trim( arr.rule_data), instr (trim( arr.rule_data), '=') + 1)) ) *100),2)
//                           as dup_name_count_perc
//                    from mes.achp_files achpf,
//                         mes.achp_file_details achpfd,
//                         mes.achp_transactions achpt,
//                         mes.achp_risk_rules arr
//                   where     achpf.file_id = :achpFileId
//                         and achpf.file_id = achpfd.file_id
//                         and achpfd.source_id = achpt.tran_id
//                         and achpt.profile_id = arr.profile_id
//                         and arr.def_code = :RISK_RULE_DUP_CUST_NAME                  
//                group by achpt.profile_id,                       
//                         achpf.create_ts,
//                         arr.rule_data,
//                         achpf.file_id,
//                         achpt.merch_num,    
//                         achpt.cust_name
//                having  count(achpt.cust_name) > 1	     
//  			    	  };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select trunc (achpf.create_ts) as activity_date,\n                       achpf.file_id as fileid,\n                       achpt.merch_num as merchant_number,\n                       achpt.profile_id as profile_id,\n                       arr.rule_data as threshold_data,\n                       achpt.cust_name as cust_name,\n                       count(achpt.cust_name) as dup_name_count,\n                       round(decode(to_number (\n                             substr (trim( arr.rule_data), instr (trim( arr.rule_data), '=') + 1)), 0, 1, (nvl(count(achpt.cust_name), 0)/ to_number (\n                             substr (trim( arr.rule_data), instr (trim( arr.rule_data), '=') + 1)) ) *100),2)\n                         as dup_name_count_perc\n                  from mes.achp_files achpf,\n                       mes.achp_file_details achpfd,\n                       mes.achp_transactions achpt,\n                       mes.achp_risk_rules arr\n                 where     achpf.file_id =  :1 \n                       and achpf.file_id = achpfd.file_id\n                       and achpfd.source_id = achpt.tran_id\n                       and achpt.profile_id = arr.profile_id\n                       and arr.def_code =  :2                   \n              group by achpt.profile_id,                       \n                       achpf.create_ts,\n                       arr.rule_data,\n                       achpf.file_id,\n                       achpt.merch_num,    \n                       achpt.cust_name\n              having  count(achpt.cust_name) > 1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"14com.mes.startup.AchpRiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,achpFileId);
   __sJT_st.setString(2,RISK_RULE_DUP_CUST_NAME);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"14com.mes.startup.AchpRiskScoringLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:591^10*/
		       
		       resultSet = it.getResultSet(); 
			    while(resultSet.next()){
			    	
			    	//get threshhold value from achp_risk_rules table for the merchant
			    	threshold = getThreshold( resultSet.getString("profile_id"), this.RISK_RULE_DUP_CUST_NAME);
			    	
			    	//get points and add to risk score
			    	  points = getPoints( resultSet.getLong("merchant_number"),
	                            RiskScoreDataBean.TT_ACHP_DUPLICATE_CUSTOMER_NAME,
	                            resultSet.getDouble("dup_name_count_perc"), threshold);
			    		//add to risk score table
				    	
				    	 if ( points != 0 )
				          {
				    		 desc.setLength(0);
					         desc.append( "Duplicate Customer Name Count : ");
					         desc.append(resultSet.getDouble("dup_name_count_perc"));
					         desc.append(" % for name " + resultSet.getString("cust_name") + ". ");
					         desc.append("Threshold is " + threshold);
					            
				    		 storeRiskScore(resultSet.getLong("merchant_number"), points, desc.toString(), resultSet.getDate("activity_date"), this.PT_ACHP_DUPLICATE_CUSTOMER_NAME_COUNT, fileName, achpFileId);
				          }			    	
				    }
				    
				    resultSet.close();
				    it.close();
				    fileProcessed = true;
		       
	    }catch(Exception e){
	    	logEntry("loadPointsDuplicateCustomerName() : ",e.toString());
	    	log.error("loadPointsDuplicateCustomerName() : " + e.toString());
	    }finally{
	    	try{ it.close();   cleanUp(); } catch(Exception ee){}
	    }
		return fileProcessed;		
	}
	
	
	private boolean loadPointsDuplicateDDA(String fileName){
		log.info("Calculating points for Duplicate DDA for fileName - " + fileName);
		
		StringBuffer                desc            = new StringBuffer();
	    boolean                     fileProcessed   = false;
	    ResultSetIterator           it              = null;
	    long                        achpFileId      = 0L;
	    int                         points          = 0;
	    long 						threshold		= 0L;
	    ResultSet                   resultSet       = null;
	    
	    try{
	    	connect(true);
	    	 achpFileId =  getAchFileId(fileName);
	    	
	    	 // remove any previous entries in the scoring system
	    	 /*@lineinfo:generated-code*//*@lineinfo:647^8*/

//  ************************************************************
//  #sql [Ctx] { delete
//  		        from    risk_score
//  		        where   load_file_id = :achpFileId
//  		        and risk_cat_id = :PT_ACHP_DUPLICATE_DDA_COUNT
//  		        };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n\t\t        from    risk_score\n\t\t        where   load_file_id =  :1 \n\t\t        and risk_cat_id =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"15com.mes.startup.AchpRiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,achpFileId);
   __sJT_st.setInt(2,PT_ACHP_DUPLICATE_DDA_COUNT);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:651^10*/
	    		 
		       //get data
		       /*@lineinfo:generated-code*//*@lineinfo:654^10*/

//  ************************************************************
//  #sql [Ctx] it = { select trunc (achpf.create_ts) as activity_date,
//                         achpf.file_id as fileid,
//                         achpt.merch_num as merchant_number,
//                         achpt.profile_id as profile_id,
//                         arr.rule_data as threshold_data,
//                         achpt.account_num as account_num,                       
//                         count(achpt.account_num) as dup_dda_count,
//                         round(decode(to_number (
//                               substr (trim( arr.rule_data), instr (trim( arr.rule_data), '=') + 1)), 0, 1, (nvl(count(achpt.account_num), 0)/ to_number (
//                               substr (trim( arr.rule_data), instr (trim( arr.rule_data), '=') + 1)) ) *100),2)
//                           as dup_dda_count_perc
//                    from mes.achp_files achpf,
//                         mes.achp_file_details achpfd,
//                         mes.achp_transactions achpt,
//                         mes.achp_risk_rules arr
//                   where     achpf.file_id = :achpFileId
//                         and achpf.file_id = achpfd.file_id
//                         and achpfd.source_id = achpt.tran_id
//                         and achpt.profile_id = arr.profile_id
//                         and arr.def_code = :RISK_RULE_DUPLICATE_DDA_COUNT                      
//                group by achpt.profile_id,                       
//                         achpf.create_ts,
//                         arr.rule_data,
//                         achpf.file_id,
//                         achpt.merch_num,    
//                         achpt.account_num
//                having  count(achpt.account_num) > 1     
//  			    	  };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select trunc (achpf.create_ts) as activity_date,\n                       achpf.file_id as fileid,\n                       achpt.merch_num as merchant_number,\n                       achpt.profile_id as profile_id,\n                       arr.rule_data as threshold_data,\n                       achpt.account_num as account_num,                       \n                       count(achpt.account_num) as dup_dda_count,\n                       round(decode(to_number (\n                             substr (trim( arr.rule_data), instr (trim( arr.rule_data), '=') + 1)), 0, 1, (nvl(count(achpt.account_num), 0)/ to_number (\n                             substr (trim( arr.rule_data), instr (trim( arr.rule_data), '=') + 1)) ) *100),2)\n                         as dup_dda_count_perc\n                  from mes.achp_files achpf,\n                       mes.achp_file_details achpfd,\n                       mes.achp_transactions achpt,\n                       mes.achp_risk_rules arr\n                 where     achpf.file_id =  :1 \n                       and achpf.file_id = achpfd.file_id\n                       and achpfd.source_id = achpt.tran_id\n                       and achpt.profile_id = arr.profile_id\n                       and arr.def_code =  :2                       \n              group by achpt.profile_id,                       \n                       achpf.create_ts,\n                       arr.rule_data,\n                       achpf.file_id,\n                       achpt.merch_num,    \n                       achpt.account_num\n              having  count(achpt.account_num) > 1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"16com.mes.startup.AchpRiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,achpFileId);
   __sJT_st.setString(2,RISK_RULE_DUPLICATE_DDA_COUNT);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"16com.mes.startup.AchpRiskScoringLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:681^10*/
		       
		       resultSet = it.getResultSet(); 
			    while(resultSet.next()){
			    	
			    	//get threshhold value from achp_risk_rules table for the merchant
			    	threshold = getThreshold( resultSet.getString("profile_id"), this.RISK_RULE_DUPLICATE_DDA_COUNT);
			    	
			    	//get points and add to risk score
			    	  points = getPoints( resultSet.getLong("merchant_number"),
	                            RiskScoreDataBean.TT_ACHP_DUPLICATE_DDA,
	                            resultSet.getDouble("dup_dda_count_perc"), threshold);
			    		//add to risk score table
				    	
				    	 if ( points != 0 )
				          {
				    		 desc.setLength(0);
					         desc.append( "Duplicate DDA count : ");
					         desc.append(resultSet.getDouble("dup_dda_count_perc"));
					         desc.append(" % for  " + resultSet.getString("account_num") + ". ");
					         desc.append("Threshold is " + threshold);
					            
				    		 storeRiskScore(resultSet.getLong("merchant_number"), points, desc.toString(), resultSet.getDate("activity_date"), this.PT_ACHP_DUPLICATE_DDA_COUNT , fileName, achpFileId);
				          }			    	
				    }
				    
				    resultSet.close();
				    it.close();
				    fileProcessed = true;
		       
	    }catch(Exception e){
	    	logEntry("loadPointsDuplicateDDA() : ",e.toString());
	    	log.error("loadPointsDuplicateDDA() : " + e.toString());
	    }finally{
	    	try{ it.close();   cleanUp(); } catch(Exception ee){}
	    }
		return fileProcessed;		
	}
	
	private boolean loadPointsDailyCreditCount(String fileName){
		log.info("Calculating points for daily credit count for fileName - " + fileName);
		
		StringBuffer                desc            = new StringBuffer();
	    boolean                     fileProcessed   = false;
	    ResultSetIterator           it              = null;
	    long                        achpFileId      = 0L;
	    int                         points          = 0;
	    long 						threshold		= 0L;
	    ResultSet                   resultSet       = null;
	    
	    try{
	    	connect(true);
	    	 achpFileId =  getAchFileId(fileName);
	    	
	    	 // remove any previous entries in the scoring system
	    	 /*@lineinfo:generated-code*//*@lineinfo:736^8*/

//  ************************************************************
//  #sql [Ctx] { delete
//  		        from    risk_score
//  		        where   load_file_id = :achpFileId
//  		        and risk_cat_id = :PT_ACHP_DAILY_CREDIT_COUNT
//  		        };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n\t\t        from    risk_score\n\t\t        where   load_file_id =  :1 \n\t\t        and risk_cat_id =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"17com.mes.startup.AchpRiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,achpFileId);
   __sJT_st.setInt(2,PT_ACHP_DAILY_CREDIT_COUNT);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:740^10*/
	    		 
		       //get data
		       /*@lineinfo:generated-code*//*@lineinfo:743^10*/

//  ************************************************************
//  #sql [Ctx] it = { select trunc (achpf.create_ts) as activity_date,
//                         achpf.file_id as fileid,
//                         achpt.merch_num as merchant_number,
//                         achpt.profile_id as profile_id,         
//                         achpt.tran_type as tran_type,                       
//                         count(achpt.profile_id) as tran_count,
//                         arr.rule_data as threshold_data,
//                         round(decode(to_number (
//                               substr (trim( arr.rule_data), instr (trim( arr.rule_data), '=') + 1)), 0, 1, (nvl(count(achpt.profile_id), 0)/ to_number (
//                               substr (trim( arr.rule_data), instr (trim( arr.rule_data), '=') + 1)) ) *100),2)
//                           as credit_count_ratio
//                    from mes.achp_files achpf,
//                         mes.achp_file_details achpfd,
//                         mes.achp_transactions achpt,
//                         mes.achp_risk_rules arr
//                   where     achpf.file_id = :achpFileId
//                         and achpf.file_id = achpfd.file_id
//                         and achpfd.source_id = achpt.tran_id
//                         and achpt.profile_id = arr.profile_id
//                         and arr.def_code = :RISK_RULE_CREDIT_COUNT
//                         and achpt.tran_type = 'CREDIT'
//                group by achpt.profile_id,
//                         achpt.tran_type,
//                         achpf.create_ts,
//                         arr.rule_data,
//                         achpf.file_id,
//                         achpt.merch_num			     
//  			    	  };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select trunc (achpf.create_ts) as activity_date,\n                       achpf.file_id as fileid,\n                       achpt.merch_num as merchant_number,\n                       achpt.profile_id as profile_id,         \n                       achpt.tran_type as tran_type,                       \n                       count(achpt.profile_id) as tran_count,\n                       arr.rule_data as threshold_data,\n                       round(decode(to_number (\n                             substr (trim( arr.rule_data), instr (trim( arr.rule_data), '=') + 1)), 0, 1, (nvl(count(achpt.profile_id), 0)/ to_number (\n                             substr (trim( arr.rule_data), instr (trim( arr.rule_data), '=') + 1)) ) *100),2)\n                         as credit_count_ratio\n                  from mes.achp_files achpf,\n                       mes.achp_file_details achpfd,\n                       mes.achp_transactions achpt,\n                       mes.achp_risk_rules arr\n                 where     achpf.file_id =  :1 \n                       and achpf.file_id = achpfd.file_id\n                       and achpfd.source_id = achpt.tran_id\n                       and achpt.profile_id = arr.profile_id\n                       and arr.def_code =  :2 \n                       and achpt.tran_type = 'CREDIT'\n              group by achpt.profile_id,\n                       achpt.tran_type,\n                       achpf.create_ts,\n                       arr.rule_data,\n                       achpf.file_id,\n                       achpt.merch_num";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"18com.mes.startup.AchpRiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,achpFileId);
   __sJT_st.setString(2,RISK_RULE_CREDIT_COUNT);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"18com.mes.startup.AchpRiskScoringLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:770^10*/
		       
		       resultSet = it.getResultSet(); 
			    while(resultSet.next()){
			    	
			    	//get threshhold value from achp_risk_rules table for the merchant
			    	threshold = getThreshold( resultSet.getString("profile_id"), this.RISK_RULE_SALE_COUNT);
			    	
			    	//get points and add to risk score
			    	  points = getPoints( resultSet.getLong("merchant_number"),
	                            RiskScoreDataBean.TT_DAILY_SALES_COUNT,
	                            resultSet.getDouble("credit_count_ratio"), threshold);
			    		//add to risk score table
				    	
				    	 if ( points != 0 )
				          {
				    		 desc.setLength(0);
					         desc.append( "Daily ACHP Credit Count : ");
					         desc.append(resultSet.getDouble("credit_count_ratio"));
					         desc.append(" %. Threshold is ");
					         desc.append(threshold);
					            
				    		 storeRiskScore(resultSet.getLong("merchant_number"), points, desc.toString(), resultSet.getDate("activity_date"), this.PT_ACHP_DAILY_CREDIT_COUNT, fileName, achpFileId);
				          }			    	
				    }
				    
				    resultSet.close();
				    it.close();
				    fileProcessed = true;
		       
	    }catch(Exception e){
	    	logEntry("loadPointsDailyCreditCount() : ",e.toString());
	    	log.error("loadPointsDailyCreditCount() : " + e.toString());
	    	
	    }finally{
	    	try{ it.close();   cleanUp(); } catch(Exception ee){}
	    }
		return fileProcessed;		
	}
	
	
	
	
	
	private boolean loadPointsDailySalesCount(String fileName){
		log.info("Calculating points for daily sales count for fileName - " + fileName);
		
		StringBuffer                desc            = new StringBuffer();
	    boolean                     fileProcessed   = false;
	    ResultSetIterator           it              = null;
	    long                        achpFileId      = 0L;
	    int                         points          = 0;
	    long 						threshold		= 0L;
	    ResultSet                   resultSet       = null;
	    
	    try{
	    	connect(true);
	    	 achpFileId =  getAchFileId(fileName);
	    	
	    	 // remove any previous entries in the scoring system
	    	 /*@lineinfo:generated-code*//*@lineinfo:830^8*/

//  ************************************************************
//  #sql [Ctx] { delete
//  		        from    risk_score
//  		        where   load_file_id = :achpFileId
//  		        and risk_cat_id = :PT_ACHP_DAILY_SALES_COUNT
//  		        };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n\t\t        from    risk_score\n\t\t        where   load_file_id =  :1 \n\t\t        and risk_cat_id =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"19com.mes.startup.AchpRiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,achpFileId);
   __sJT_st.setInt(2,PT_ACHP_DAILY_SALES_COUNT);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:834^10*/
	    		 
		       //get data
		       /*@lineinfo:generated-code*//*@lineinfo:837^10*/

//  ************************************************************
//  #sql [Ctx] it = { select trunc (achpf.create_ts) as activity_date,
//                         achpf.file_id as fileid,
//                         achpt.merch_num as merchant_number,
//                         achpt.profile_id as profile_id,         
//                         achpt.tran_type as tran_type,                       
//                         count(achpt.profile_id) as tran_count,
//                         arr.rule_data as threshold_data,
//                         round(decode(to_number (
//                               substr (trim( arr.rule_data), instr (trim( arr.rule_data), '=') + 1)), 0, 1, (nvl(count(achpt.profile_id), 0)/ to_number (
//                               substr (trim( arr.rule_data), instr (trim( arr.rule_data), '=') + 1)) ) *100),2)
//                           as sales_count_ratio
//                    from mes.achp_files achpf,
//                         mes.achp_file_details achpfd,
//                         mes.achp_transactions achpt,
//                         mes.achp_risk_rules arr
//                   where     achpf.file_id = :achpFileId
//                         and achpf.file_id = achpfd.file_id
//                         and achpfd.source_id = achpt.tran_id
//                         and achpt.profile_id = arr.profile_id
//                         and arr.def_code = :RISK_RULE_SALE_COUNT
//                         and achpt.tran_type = 'SALE'
//                group by achpt.profile_id,
//                         achpt.tran_type,
//                         achpf.create_ts,
//                         arr.rule_data,
//                         achpf.file_id,
//                         achpt.merch_num			     
//  			    	  };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select trunc (achpf.create_ts) as activity_date,\n                       achpf.file_id as fileid,\n                       achpt.merch_num as merchant_number,\n                       achpt.profile_id as profile_id,         \n                       achpt.tran_type as tran_type,                       \n                       count(achpt.profile_id) as tran_count,\n                       arr.rule_data as threshold_data,\n                       round(decode(to_number (\n                             substr (trim( arr.rule_data), instr (trim( arr.rule_data), '=') + 1)), 0, 1, (nvl(count(achpt.profile_id), 0)/ to_number (\n                             substr (trim( arr.rule_data), instr (trim( arr.rule_data), '=') + 1)) ) *100),2)\n                         as sales_count_ratio\n                  from mes.achp_files achpf,\n                       mes.achp_file_details achpfd,\n                       mes.achp_transactions achpt,\n                       mes.achp_risk_rules arr\n                 where     achpf.file_id =  :1 \n                       and achpf.file_id = achpfd.file_id\n                       and achpfd.source_id = achpt.tran_id\n                       and achpt.profile_id = arr.profile_id\n                       and arr.def_code =  :2 \n                       and achpt.tran_type = 'SALE'\n              group by achpt.profile_id,\n                       achpt.tran_type,\n                       achpf.create_ts,\n                       arr.rule_data,\n                       achpf.file_id,\n                       achpt.merch_num";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"20com.mes.startup.AchpRiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,achpFileId);
   __sJT_st.setString(2,RISK_RULE_SALE_COUNT);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"20com.mes.startup.AchpRiskScoringLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:864^10*/
		       
		       resultSet = it.getResultSet(); 
			    while(resultSet.next()){
			    	
			    	//get threshhold value from achp_risk_rules table for the merchant
			    	threshold = getThreshold( resultSet.getString("profile_id"), this.RISK_RULE_SALE_COUNT);
			    	
			    	//get points and add to risk score
			    	  points = getPoints( resultSet.getLong("merchant_number"),
	                            RiskScoreDataBean.TT_DAILY_SALES_COUNT,
	                            resultSet.getDouble("sales_count_ratio"), threshold);
			    		//add to risk score table
				    	
				    	 if ( points != 0 )
				          {
				    		 desc.setLength(0);
					         desc.append( "Daily ACHP Sales Count : ");
					         desc.append(resultSet.getDouble("sales_count_ratio"));
					         desc.append(" %. Threshold is ");
					         desc.append(threshold);
					            
				    		 storeRiskScore(resultSet.getLong("merchant_number"), points, desc.toString(), resultSet.getDate("activity_date"), this.PT_ACHP_DAILY_SALES_COUNT, fileName, achpFileId);
				          }			    	
				    }
				    
				    resultSet.close();
				    it.close();
				    fileProcessed = true;
		       
	    }catch(Exception e){
	    	logEntry("loadPointsDailySalesCount() : ",e.toString());
	    	log.error("loadPointsDailySalesCount() : " + e.toString());
	    	
	    }finally{
	    	try{ it.close();   cleanUp(); } catch(Exception ee){}
	    }
		return fileProcessed;
		
	}
	
	
	/*
	 * Individual sale amount of single sale from daily batch. Matched against threshold set on profile.
	 * 
	 */
	private boolean loadPointsIndividualSaleAmt(String fileName){
		log.info("Calculating points for individual sale amount for fileName - " + fileName);
		
		StringBuffer                desc            = new StringBuffer();
	    boolean                     fileProcessed   = false;
	    ResultSetIterator           it              = null;
	    long                        achpFileId      = 0L;
	    int                         points          = 0;
	    long 						threshold		= 0L;
	    ResultSet                   resultSet       = null;
	    
	    try{
	    	connect(true);
	    	 achpFileId =  getAchFileId(fileName);
	    	
	    	 // remove any previous entries in the scoring system
	    	 /*@lineinfo:generated-code*//*@lineinfo:926^8*/

//  ************************************************************
//  #sql [Ctx] { delete
//  		        from    risk_score
//  		        where   load_file_id = :achpFileId
//  		        and risk_cat_id = :PT_ACHP_INDIVIDUAL_SALE_AMOUNT
//  		        };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n\t\t        from    risk_score\n\t\t        where   load_file_id =  :1 \n\t\t        and risk_cat_id =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"21com.mes.startup.AchpRiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,achpFileId);
   __sJT_st.setInt(2,PT_ACHP_INDIVIDUAL_SALE_AMOUNT);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:930^10*/
	    		 
		       //get data
		       /*@lineinfo:generated-code*//*@lineinfo:933^10*/

//  ************************************************************
//  #sql [Ctx] it = { select trunc(achpf.create_ts) as activity_date,
//                         achpf.file_id as fileid,
//                         achpt.merch_num as merchant_number,
//                         achpt.profile_id as profile_id,         
//                         achpt.tran_type as tran_type,
//                         achpt.amount as sale_amt,                       
//                         arr.rule_data as threshold_data,
//                         round(decode(to_number (
//                               substr (trim(arr.rule_data), instr (trim(arr.rule_data), '=') + 1)), 0, 1, (nvl(achpt.amount, 0)/ to_number (
//                               substr (trim(arr.rule_data), instr (trim(arr.rule_data), '=') + 1)) ) *100),2)
//                           as ind_sale_amt_perc
//                    from mes.achp_files achpf,
//                         mes.achp_file_details achpfd,
//                         mes.achp_transactions achpt,
//                         mes.achp_risk_rules arr
//                   where     achpf.file_id = :achpFileId
//                         and achpf.file_id = achpfd.file_id
//                         and achpfd.source_id = achpt.tran_id
//                         and achpt.profile_id = arr.profile_id
//                         and arr.def_code = :RISK_RULE_SALE_AMOUNT
//                         and achpt.tran_type = 'SALE'
//                group by achpt.profile_id,
//                         achpt.tran_type,
//                         achpf.create_ts,
//                         achpf.file_id,
//                         achpt.merch_num,
//                         achpt.tran_type,
//                         achpt.amount,
//                         arr.rule_data			     
//  			    	  };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select trunc(achpf.create_ts) as activity_date,\n                       achpf.file_id as fileid,\n                       achpt.merch_num as merchant_number,\n                       achpt.profile_id as profile_id,         \n                       achpt.tran_type as tran_type,\n                       achpt.amount as sale_amt,                       \n                       arr.rule_data as threshold_data,\n                       round(decode(to_number (\n                             substr (trim(arr.rule_data), instr (trim(arr.rule_data), '=') + 1)), 0, 1, (nvl(achpt.amount, 0)/ to_number (\n                             substr (trim(arr.rule_data), instr (trim(arr.rule_data), '=') + 1)) ) *100),2)\n                         as ind_sale_amt_perc\n                  from mes.achp_files achpf,\n                       mes.achp_file_details achpfd,\n                       mes.achp_transactions achpt,\n                       mes.achp_risk_rules arr\n                 where     achpf.file_id =  :1 \n                       and achpf.file_id = achpfd.file_id\n                       and achpfd.source_id = achpt.tran_id\n                       and achpt.profile_id = arr.profile_id\n                       and arr.def_code =  :2 \n                       and achpt.tran_type = 'SALE'\n              group by achpt.profile_id,\n                       achpt.tran_type,\n                       achpf.create_ts,\n                       achpf.file_id,\n                       achpt.merch_num,\n                       achpt.tran_type,\n                       achpt.amount,\n                       arr.rule_data";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"22com.mes.startup.AchpRiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,achpFileId);
   __sJT_st.setString(2,RISK_RULE_SALE_AMOUNT);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"22com.mes.startup.AchpRiskScoringLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:962^10*/
		       
		       resultSet = it.getResultSet(); 
			    while(resultSet.next()){
			    	
			    	//get threshhold value from achp_risk_rules table for the merchant
			    	threshold = getThreshold( resultSet.getString("profile_id"), this.RISK_RULE_SALE_AMOUNT);
			    	
			    	//get points and add to risk score
			    	  points = getPoints( resultSet.getLong("merchant_number"),
	                            RiskScoreDataBean.TT_INDIVIDUAL_SALES_AMT ,
	                            resultSet.getDouble("ind_sale_amt_perc"), threshold);
			    		//add to risk score table
				    	
				    	 if ( points != 0 )
				          {
				    		 desc.setLength(0);
					         desc.append( "Daily Individial Sale Amt : ");
					         desc.append(resultSet.getDouble("ind_sale_amt_perc"));
					         desc.append(" %. Threshold is ");
					         desc.append(threshold);
					            
				    		 storeRiskScore(resultSet.getLong("merchant_number"), points, desc.toString(), resultSet.getDate("activity_date"), this.PT_ACHP_INDIVIDUAL_SALE_AMOUNT, fileName, achpFileId);
				          }			    	
				    }
				    
				    resultSet.close();
				    it.close();
				    fileProcessed = true;
		       
	    }catch(Exception e){
	    	logEntry("loadPointsIndividualSaleAmt() : ",e.toString());
	    	log.error("loadPointsIndividualSaleAmt() : " + e.toString());
	    	
	    }finally{
	    	try{ it.close();   cleanUp(); } catch(Exception ee){}
	    }
		return fileProcessed;
		
	}
	
	/*
	 * If CREDIT type of transactions accumulate more than the threshold set on profile on any day in the month, 
	 * then points will be scored
	 *  
	 *  It is checking monthly processing volume for CREDIT type of transactions.
	 */
	
	private boolean loadPointsIndividualCreditAmt(String fileName){
		log.info("Calculating points for individual credit amount per month for fileName - " + fileName);
		
		StringBuffer                desc            = new StringBuffer();
	    boolean                     fileProcessed   = false;
	    ResultSetIterator           it              = null;
	    long                        achpFileId      = 0L;
	    int                         points          = 0;
	    long 						threshold		= 0L;
	    ResultSet                   resultSet       = null;
	    
	    try{
	    	connect(true);
	    	 achpFileId =  getAchFileId(fileName);
	    	
	    	 // remove any previous entries in the scoring system
	    	 /*@lineinfo:generated-code*//*@lineinfo:1026^8*/

//  ************************************************************
//  #sql [Ctx] { delete
//  		        from    risk_score
//  		        where   load_file_id = :achpFileId
//  		        and risk_cat_id = :PT_ACHP_INDIVIDUAL_CREDIT_AMOUNT
//  		        };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n\t\t        from    risk_score\n\t\t        where   load_file_id =  :1 \n\t\t        and risk_cat_id =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"23com.mes.startup.AchpRiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,achpFileId);
   __sJT_st.setInt(2,PT_ACHP_INDIVIDUAL_CREDIT_AMOUNT);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1030^10*/
	    		 
		       //get data
		       /*@lineinfo:generated-code*//*@lineinfo:1033^10*/

//  ************************************************************
//  #sql [Ctx] it = { select trunc(achpf.create_ts) as activity_date,
//                         achpt.tran_id as tran_id,
//                         achpt.merch_num as merchant_number,
//                         achpt.profile_id as profile_id,         
//                         achpt.tran_type as tran_type,
//                         achpt.amount as sales_amt,
//                         incremental_credits.incr_credit_amt,                                              
//                         arr.rule_data as threshold_data,
//                         round(decode(to_number (
//                         substr (trim(arr.rule_data), instr (trim(arr.rule_data), '=') + 1)), 0, 1, (nvl( incremental_credits.incr_credit_amt, 0)/ to_number (
//                         substr (trim(arr.rule_data), instr (trim(arr.rule_data), '=') + 1)) ) *100),2)
//                         as tot_credit_amt_perc
//                 from mes.achp_files achpf,
//                         mes.achp_file_details achpfd,
//                         mes.achp_transactions achpt,
//                         mes.achp_risk_rules arr,
//                         (select tran_id, tran_ts, profile_id, amount, sum(amount) over (order by tran_id) as incr_credit_amt from achp_transactions achpt
//                         where tran_type = 'CREDIT'
//                         and tran_ts > (trunc(sysdate, 'mm')) 
//                         and tran_ts <= sysdate                       
//                         order by tran_id) incremental_credits 
//                 where   achpf.file_id = :achpFileId
//                         and achpf.file_id = achpfd.file_id
//                         and achpfd.source_id = achpt.tran_id
//                         and achpt.profile_id = arr.profile_id
//                         and arr.def_code = :RISK_RULE_CREDIT_AMOUNT
//                         and achpt.tran_type = 'CREDIT'
//                         and incremental_credits.tran_id = achpt.tran_id                       
//                 group by achpt.tran_id,    
//                         achpt.profile_id,
//                         achpt.tran_type,
//                         achpf.create_ts,                     
//                         achpt.merch_num,                                          
//                         achpt.amount,
//                         arr.rule_data,
//                         incremental_credits.incr_credit_amt 
//                         order by achpt.tran_id   		     
//  			    	  };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select trunc(achpf.create_ts) as activity_date,\n                       achpt.tran_id as tran_id,\n                       achpt.merch_num as merchant_number,\n                       achpt.profile_id as profile_id,         \n                       achpt.tran_type as tran_type,\n                       achpt.amount as sales_amt,\n                       incremental_credits.incr_credit_amt,                                              \n                       arr.rule_data as threshold_data,\n                       round(decode(to_number (\n                       substr (trim(arr.rule_data), instr (trim(arr.rule_data), '=') + 1)), 0, 1, (nvl( incremental_credits.incr_credit_amt, 0)/ to_number (\n                       substr (trim(arr.rule_data), instr (trim(arr.rule_data), '=') + 1)) ) *100),2)\n                       as tot_credit_amt_perc\n               from mes.achp_files achpf,\n                       mes.achp_file_details achpfd,\n                       mes.achp_transactions achpt,\n                       mes.achp_risk_rules arr,\n                       (select tran_id, tran_ts, profile_id, amount, sum(amount) over (order by tran_id) as incr_credit_amt from achp_transactions achpt\n                       where tran_type = 'CREDIT'\n                       and tran_ts > (trunc(sysdate, 'mm')) \n                       and tran_ts <= sysdate                       \n                       order by tran_id) incremental_credits \n               where   achpf.file_id =  :1 \n                       and achpf.file_id = achpfd.file_id\n                       and achpfd.source_id = achpt.tran_id\n                       and achpt.profile_id = arr.profile_id\n                       and arr.def_code =  :2 \n                       and achpt.tran_type = 'CREDIT'\n                       and incremental_credits.tran_id = achpt.tran_id                       \n               group by achpt.tran_id,    \n                       achpt.profile_id,\n                       achpt.tran_type,\n                       achpf.create_ts,                     \n                       achpt.merch_num,                                          \n                       achpt.amount,\n                       arr.rule_data,\n                       incremental_credits.incr_credit_amt \n                       order by achpt.tran_id";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"24com.mes.startup.AchpRiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,achpFileId);
   __sJT_st.setString(2,RISK_RULE_CREDIT_AMOUNT);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"24com.mes.startup.AchpRiskScoringLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1070^10*/
		       
		       resultSet = it.getResultSet(); 
			    while(resultSet.next()){
			    	
			    	//get threshhold value from achp_risk_rules table for the merchant
			    	threshold = getThreshold( resultSet.getString("profile_id"), this.RISK_RULE_CREDIT_AMOUNT);
			    	
			    	//get points and add to risk score
			    	  points = getPoints( resultSet.getLong("merchant_number"),
	                            RiskScoreDataBean.TT_INDIVIDUAL_CREDIT_AMT ,
	                            resultSet.getDouble("tot_credit_amt_perc"), threshold);
			    		
			    	  //add to risk score table				    	
				    	 if ( points != 0 )
				          {
				    		 desc.setLength(0);
					         desc.append( "Daily Individial Credit Amt : ");
					         desc.append(resultSet.getDouble("tot_credit_amt_perc"));
					         desc.append(" %. Threshold is ");
					         desc.append(threshold);
					            
				    		 storeRiskScore(resultSet.getLong("merchant_number"), points, desc.toString(), resultSet.getDate("activity_date"), this.PT_ACHP_INDIVIDUAL_CREDIT_AMOUNT, fileName, achpFileId);
				          }			    	
				    }
			    //TODO - here
				    
				    resultSet.close();
				    it.close();
				    fileProcessed = true;
		       
	    }catch(Exception e){
	    	logEntry("loadPointsIndividualCreditAmt() : ",e.toString());
	    	log.error("loadPointsIndividualCreditAmt() : " + e.toString());
	    	
	    }finally{
	    	try{ it.close();   cleanUp(); } catch(Exception ee){}
	    }
		return fileProcessed;
		
	}
	
	
	
	/*
	 * If SALE type of transactions accumulate more than the threshold set on profile on any day in the month, 
	 * then points will be scored
	 *  
	 *  It is checking monthly processing volume for SALE type of transactions.
	 */
	
	private boolean loadPointsMonthlyProcessingVolume(String fileName){
		log.info("Calculating points for individual sale amount tran triggering monthly processing volume rule per month for fileName - " + fileName);
		
		StringBuffer                desc            = new StringBuffer();
	    boolean                     fileProcessed   = false;
	    ResultSetIterator           it              = null;
	    long                        achpFileId      = 0L;
	    int                         points          = 0;
	    long 						threshold		= 0L;
	    ResultSet                   resultSet       = null;
	    
	    try{
	    	connect(true);
	    	 achpFileId =  getAchFileId(fileName);
	    	
	    	 // remove any previous entries in the scoring system
	    	 /*@lineinfo:generated-code*//*@lineinfo:1137^8*/

//  ************************************************************
//  #sql [Ctx] { delete
//  		        from    risk_score
//  		        where   load_file_id = :achpFileId
//  		        and risk_cat_id = :PT_ACHP_MONTHLY_PROCESSING_AMOUNT
//  		        };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n\t\t        from    risk_score\n\t\t        where   load_file_id =  :1 \n\t\t        and risk_cat_id =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"25com.mes.startup.AchpRiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,achpFileId);
   __sJT_st.setInt(2,PT_ACHP_MONTHLY_PROCESSING_AMOUNT);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1141^10*/
	    		 
		       //get data
		       /*@lineinfo:generated-code*//*@lineinfo:1144^10*/

//  ************************************************************
//  #sql [Ctx] it = { select trunc(achpf.create_ts) as activity_date,
//  		                achpt.tran_id as tran_id,
//  		                achpt.merch_num as merchant_number,
//  		                achpt.profile_id as profile_id,         
//  		                achpt.tran_type as tran_type,
//  		                achpt.amount as sales_amt,
//  		                incremental_sales.incr_sale_amt,                                              
//  		                arr.rule_data as threshold_data,
//  		                round(decode(to_number (
//  		                substr (trim(arr.rule_data), instr (trim(arr.rule_data), '=') + 1)), 0, 1, (nvl( incremental_sales.incr_sale_amt, 0)/ to_number (
//  		                substr (trim(arr.rule_data), instr (trim(arr.rule_data), '=') + 1)) ) *100),2)
//  		                as tot_sale_amt_perc
//  		        from mes.achp_files achpf,
//  		                mes.achp_file_details achpfd,
//  		                mes.achp_transactions achpt,
//  		                mes.achp_risk_rules arr,
//  		                (select tran_id, tran_ts, profile_id, amount, sum(amount) over (order by tran_id) as incr_sale_amt from achp_transactions achpt
//  		                where tran_type = 'SALE'
//  		                and tran_ts > (trunc(sysdate, 'mm')) 
//  		                and tran_ts <= sysdate                       
//  		                order by tran_id) incremental_sales 
//  		        where   achpf.file_id = :achpFileId
//  		                and achpf.file_id = achpfd.file_id
//  		                and achpfd.source_id = achpt.tran_id
//  		                and achpt.profile_id = arr.profile_id
//  		                and arr.def_code = :RISK_RULE_MONTHLY_PROCE_AMT
//  		                and achpt.tran_type = 'SALE'
//  		                and incremental_sales.tran_id = achpt.tran_id                       
//  		        group by achpt.tran_id,    
//  		        		achpt.profile_id,
//  		                achpt.tran_type,
//  		                achpf.create_ts,                     
//  		                achpt.merch_num,                                          
//  		                achpt.amount,
//  		                arr.rule_data,
//  		                incremental_sales.incr_sale_amt  
//  		                order by achpt.tran_id     		     
//  			    	  };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select trunc(achpf.create_ts) as activity_date,\n\t\t                achpt.tran_id as tran_id,\n\t\t                achpt.merch_num as merchant_number,\n\t\t                achpt.profile_id as profile_id,         \n\t\t                achpt.tran_type as tran_type,\n\t\t                achpt.amount as sales_amt,\n\t\t                incremental_sales.incr_sale_amt,                                              \n\t\t                arr.rule_data as threshold_data,\n\t\t                round(decode(to_number (\n\t\t                substr (trim(arr.rule_data), instr (trim(arr.rule_data), '=') + 1)), 0, 1, (nvl( incremental_sales.incr_sale_amt, 0)/ to_number (\n\t\t                substr (trim(arr.rule_data), instr (trim(arr.rule_data), '=') + 1)) ) *100),2)\n\t\t                as tot_sale_amt_perc\n\t\t        from mes.achp_files achpf,\n\t\t                mes.achp_file_details achpfd,\n\t\t                mes.achp_transactions achpt,\n\t\t                mes.achp_risk_rules arr,\n\t\t                (select tran_id, tran_ts, profile_id, amount, sum(amount) over (order by tran_id) as incr_sale_amt from achp_transactions achpt\n\t\t                where tran_type = 'SALE'\n\t\t                and tran_ts > (trunc(sysdate, 'mm')) \n\t\t                and tran_ts <= sysdate                       \n\t\t                order by tran_id) incremental_sales \n\t\t        where   achpf.file_id =  :1 \n\t\t                and achpf.file_id = achpfd.file_id\n\t\t                and achpfd.source_id = achpt.tran_id\n\t\t                and achpt.profile_id = arr.profile_id\n\t\t                and arr.def_code =  :2 \n\t\t                and achpt.tran_type = 'SALE'\n\t\t                and incremental_sales.tran_id = achpt.tran_id                       \n\t\t        group by achpt.tran_id,    \n\t\t        \t\tachpt.profile_id,\n\t\t                achpt.tran_type,\n\t\t                achpf.create_ts,                     \n\t\t                achpt.merch_num,                                          \n\t\t                achpt.amount,\n\t\t                arr.rule_data,\n\t\t                incremental_sales.incr_sale_amt  \n\t\t                order by achpt.tran_id";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"26com.mes.startup.AchpRiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,achpFileId);
   __sJT_st.setString(2,RISK_RULE_MONTHLY_PROCE_AMT);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"26com.mes.startup.AchpRiskScoringLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1181^10*/
		       
		       resultSet = it.getResultSet(); 
			    while(resultSet.next()){
			    	
			    	//get threshhold value from achp_risk_rules table for the merchant
			    	threshold = getThreshold( resultSet.getString("profile_id"), this.RISK_RULE_MONTHLY_PROCE_AMT);
			    	
			    	//get points and add to risk score
			    	  points = getPoints( resultSet.getLong("merchant_number"),
	                            RiskScoreDataBean.TT_ACHP_MONTHLY_PROCESSING_AMT ,
	                            resultSet.getDouble("tot_sale_amt_perc"), threshold);
			    		
			    	  //add to risk score table				    	
				    	 if ( points != 0 )
				          {
				    		 desc.setLength(0);
					         desc.append( "Monthly Processing Volume : ");
					         desc.append(resultSet.getDouble("tot_sale_amt_perc"));
					         desc.append(" %. Threshold is ");
					         desc.append(threshold);					            
				    		 storeRiskScore(resultSet.getLong("merchant_number"), points, desc.toString(), resultSet.getDate("activity_date"), this.PT_ACHP_MONTHLY_PROCESSING_AMOUNT, fileName, achpFileId);
				          }			    	
				    }
				    
				    resultSet.close();
				    it.close();
				    fileProcessed = true;
		       
	    }catch(Exception e){
	    	logEntry("loadPointsMonthlyProcessingVolume() : ", e.toString());
	    	log.error("loadPointsMonthlyProcessingVolume() : " + e.toString());
	    	
	    }finally{
	    	try{ it.close();   cleanUp(); } catch(Exception ee){}
	    }
		return fileProcessed;
		
	}
	
	/*
	 * This function will calculate sum of sales amount for each profileId 
	 * Then it will calculate the ratio based on the threshold mentioned on the profile
	 * Calculate points based on the range defined for the hierarchy nodes 
	 * 
	 */
	private boolean loadPointsDailySalesVolume(String fileName){
		log.info("Calculating points for daily sales volume for fileName - " + fileName);
		
		StringBuffer                desc            = new StringBuffer();
	    boolean                     fileProcessed   = false;
	    ResultSetIterator           it              = null;
	    long                        achpFileId      = 0L;
	    int                         points          = 0;
	    long 						threshold		= 0L;
	    ResultSet                   resultSet       = null;
		
	    try{
	    	 connect(true);
	    	 achpFileId =  getAchFileId(fileName);
	    	// remove any previous entries in the scoring system
	    	 /*@lineinfo:generated-code*//*@lineinfo:1242^8*/

//  ************************************************************
//  #sql [Ctx] { delete
//  		        from    risk_score
//  		        where   load_file_id = :achpFileId
//  		        and risk_cat_id = :PT_ACHP_DAILY_SALES_VOLUME
//  		        };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n\t\t        from    risk_score\n\t\t        where   load_file_id =  :1 \n\t\t        and risk_cat_id =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"27com.mes.startup.AchpRiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,achpFileId);
   __sJT_st.setInt(2,PT_ACHP_DAILY_SALES_VOLUME);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1246^10*/
	    		 
		      
		       //get data
		       /*@lineinfo:generated-code*//*@lineinfo:1250^10*/

//  ************************************************************
//  #sql [Ctx] it = { select trunc (achpf.create_ts) as activity_date,
//                         achpf.file_id as fileid,
//                         achpt.merch_num as merchant_number,
//                         achpt.profile_id as profile_id,         
//                         achpt.tran_type as tran_type,
//                         sum (achpt.amount) as sales_volume,
//                         count(achpt.profile_id) as tran_count,
//                         arr.rule_data as threshold_data,
//                         round(decode(to_number (
//                               substr (trim(arr.rule_data), instr (trim(arr.rule_data), '=') + 1)), 0, 1, (nvl(sum (achpt.amount), 0)/ to_number (
//                               substr (trim(arr.rule_data), instr (trim(arr.rule_data), '=') + 1)) ) *100),2)
//                           as sales_ratio
//                    from mes.achp_files achpf,
//                         mes.achp_file_details achpfd,
//                         mes.achp_transactions achpt,
//                         mes.achp_risk_rules arr
//                   where     achpf.file_id = :achpFileId
//                         and achpf.file_id = achpfd.file_id
//                         and achpfd.source_id = achpt.tran_id
//                         and achpt.profile_id = arr.profile_id
//                         and arr.def_code = :RISK_RULE_SALE_VOLUME
//                         and achpt.tran_type = 'SALE'
//                group by achpt.profile_id,
//                         achpt.tran_type,
//                         achpf.create_ts,
//                         arr.rule_data,
//                         achpf.file_id,
//                         achpt.merch_num			     
//  			    	  };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select trunc (achpf.create_ts) as activity_date,\n                       achpf.file_id as fileid,\n                       achpt.merch_num as merchant_number,\n                       achpt.profile_id as profile_id,         \n                       achpt.tran_type as tran_type,\n                       sum (achpt.amount) as sales_volume,\n                       count(achpt.profile_id) as tran_count,\n                       arr.rule_data as threshold_data,\n                       round(decode(to_number (\n                             substr (trim(arr.rule_data), instr (trim(arr.rule_data), '=') + 1)), 0, 1, (nvl(sum (achpt.amount), 0)/ to_number (\n                             substr (trim(arr.rule_data), instr (trim(arr.rule_data), '=') + 1)) ) *100),2)\n                         as sales_ratio\n                  from mes.achp_files achpf,\n                       mes.achp_file_details achpfd,\n                       mes.achp_transactions achpt,\n                       mes.achp_risk_rules arr\n                 where     achpf.file_id =  :1 \n                       and achpf.file_id = achpfd.file_id\n                       and achpfd.source_id = achpt.tran_id\n                       and achpt.profile_id = arr.profile_id\n                       and arr.def_code =  :2 \n                       and achpt.tran_type = 'SALE'\n              group by achpt.profile_id,\n                       achpt.tran_type,\n                       achpf.create_ts,\n                       arr.rule_data,\n                       achpf.file_id,\n                       achpt.merch_num";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"28com.mes.startup.AchpRiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,achpFileId);
   __sJT_st.setString(2,RISK_RULE_SALE_VOLUME);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"28com.mes.startup.AchpRiskScoringLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1278^10*/
		       
		       resultSet = it.getResultSet(); 
			    while(resultSet.next()){
			    	
			    	//get threshhold value from achp_risk_rules table for the merchant
			    	threshold = getThreshold( resultSet.getString("profile_id"), this.RISK_RULE_SALE_VOLUME);
			    	
			    	//get points and add to risk score
			    	  points = getPoints( resultSet.getLong("merchant_number"),
	                            RiskScoreDataBean.TT_DAILY_SALES_VOLUME ,
	                            resultSet.getDouble("sales_ratio"), threshold);
			    		//add to risk score table
				    	
				    	 if ( points != 0 )
				          {
				    		 desc.setLength(0);
					         desc.append( "Daily ACHP Sales Volume : ");
					         desc.append(resultSet.getDouble("sales_ratio"));
					         desc.append(" %. Threshold is ");
					         desc.append(threshold);
					            
				    		 storeRiskScore(resultSet.getLong("merchant_number"), points, desc.toString(), resultSet.getDate("activity_date"), this.PT_ACHP_DAILY_SALES_VOLUME, fileName, achpFileId);
				          }			    	
				    }
				    
				    resultSet.close();
				    it.close();
				    fileProcessed = true;	    		 
	    }catch(Exception e){
	    	logEntry("loadPointsDailySalesVolume() : ",e.toString());
	    	log.error("loadPointsDailySalesVolume() : " + e.toString());
	    	
	    }finally{
	    	try{ it.close();   cleanUp(); } catch(Exception ee){}
	    }
		return fileProcessed;
		
	    
	}
	
	private boolean loadPointsDailyCreditVolume(String fileName){
		
		log.info("Calculating points for daily credit volume for fileName - " + fileName);
		
		//Credit Volume : sum of credit sale amount for each profile Id having credit sale amount > threshhold mentioned on merchant ACHP profile page
		StringBuffer                desc            = new StringBuffer();
	    boolean                     fileProcessed   = false;
	    ResultSetIterator           it              = null;
	    long                        achpFileId      = 0L;
	    int                         points          = 0;
	    ResultSet                   resultSet       = null;
	    long threshold	= 0L;
	    
		try{
			
			  connect(true);
			  achpFileId =  getAchFileId(fileName);
			// remove any previous entries in the scoring system
		      /*@lineinfo:generated-code*//*@lineinfo:1337^9*/

//  ************************************************************
//  #sql [Ctx] { delete
//  		        from    risk_score
//  		        where   load_file_id = :achpFileId
//  		        and risk_cat_id = :PT_ACHP_DAILY_CREDIT_VOLUME
//  		       };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n\t\t        from    risk_score\n\t\t        where   load_file_id =  :1 \n\t\t        and risk_cat_id =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"29com.mes.startup.AchpRiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,achpFileId);
   __sJT_st.setInt(2,PT_ACHP_DAILY_CREDIT_VOLUME);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1343^9*/
		      
		      //get data for the 
			    /*@lineinfo:generated-code*//*@lineinfo:1346^8*/

//  ************************************************************
//  #sql [Ctx] it = { select trunc (achpf.create_ts) as activity_date,
//                          achpf.file_id as fileid,
//                          achpt.merch_num as merchant_number,
//                          achpt.profile_id as profile_id,         
//                          achpt.tran_type as tran_type,
//                          sum (achpt.amount) as credit_volume,
//                          count (achpt.profile_id) as tran_count,
//                          arr.rule_data as threshold_data,
//                          round( decode(to_number (
//                                substr (trim(arr.rule_data), instr (trim(arr.rule_data), '=') + 1)), 0, 1, (nvl(sum (achpt.amount), 0)/ to_number (
//                                substr (trim(arr.rule_data), instr (trim(arr.rule_data), '=') + 1)) ) *100),2)
//                            as ratio
//                     from mes.achp_files achpf,
//                          mes.achp_file_details achpfd,
//                          mes.achp_transactions achpt,
//                          mes.achp_risk_rules arr
//                    where     achpf.file_id = :achpFileId
//                          and achpf.file_id = achpfd.file_id
//                          and achpfd.source_id = achpt.tran_id
//                          and achpt.profile_id = arr.profile_id
//                          and arr.def_code = :RISK_RULE_CREDIT_VOLUME
//                          and achpt.tran_type = 'CREDIT'
//                 group by achpt.profile_id,
//                          achpt.tran_type,
//                          achpf.create_ts,
//                          arr.rule_data,
//                          achpf.file_id,
//                          achpt.merch_num			     
//  			    	 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select trunc (achpf.create_ts) as activity_date,\n                        achpf.file_id as fileid,\n                        achpt.merch_num as merchant_number,\n                        achpt.profile_id as profile_id,         \n                        achpt.tran_type as tran_type,\n                        sum (achpt.amount) as credit_volume,\n                        count (achpt.profile_id) as tran_count,\n                        arr.rule_data as threshold_data,\n                        round( decode(to_number (\n                              substr (trim(arr.rule_data), instr (trim(arr.rule_data), '=') + 1)), 0, 1, (nvl(sum (achpt.amount), 0)/ to_number (\n                              substr (trim(arr.rule_data), instr (trim(arr.rule_data), '=') + 1)) ) *100),2)\n                          as ratio\n                   from mes.achp_files achpf,\n                        mes.achp_file_details achpfd,\n                        mes.achp_transactions achpt,\n                        mes.achp_risk_rules arr\n                  where     achpf.file_id =  :1 \n                        and achpf.file_id = achpfd.file_id\n                        and achpfd.source_id = achpt.tran_id\n                        and achpt.profile_id = arr.profile_id\n                        and arr.def_code =  :2 \n                        and achpt.tran_type = 'CREDIT'\n               group by achpt.profile_id,\n                        achpt.tran_type,\n                        achpf.create_ts,\n                        arr.rule_data,\n                        achpf.file_id,\n                        achpt.merch_num";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"30com.mes.startup.AchpRiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,achpFileId);
   __sJT_st.setString(2,RISK_RULE_CREDIT_VOLUME);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"30com.mes.startup.AchpRiskScoringLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1375^9*/
		      
			    resultSet = it.getResultSet(); 
			    while(resultSet.next()){
			    	
			    	//get threshhold value from achp_risk_rules table for the merchant
			    	threshold = getThreshold( resultSet.getString("profile_id"), this.RISK_RULE_CREDIT_VOLUME);
			    	
			    	//get points and add to risk score
			    	  points = getPoints( resultSet.getLong("merchant_number"),
	                            RiskScoreDataBean.TT_DAILY_CREDIT_VOLUME ,
	                            resultSet.getDouble("ratio"), threshold);
			    	
			    	//add to risk score table
			    	
			    	 if ( points != 0 )
			          {
			    		 desc.setLength(0);
				         desc.append( "Daily ACHP Credit Volume : ");
				         desc.append(resultSet.getDouble("ratio"));
				         desc.append(" %. Threshold is ");
				         desc.append(threshold);
				            
			    		 storeRiskScore(resultSet.getLong("merchant_number"), points, desc.toString(), resultSet.getDate("activity_date"), this.PT_ACHP_DAILY_CREDIT_VOLUME, fileName, achpFileId);
			          }			    	
			    }
			    
			    resultSet.close();
			      it.close();
			      fileProcessed = true;
			      
		}catch(Exception e){
			 logEntry("loadPointsDailyCreditVolume()",e.toString());
			 log.error("loadPointsDailyCreditVolume()" + e.toString());
			 
		}finally{
			try{ it.close();   cleanUp(); } catch(Exception ee){}
		}
		
		return fileProcessed;
		
	}
	
	private long getThreshold(String profileId, String defCode){
		long threshold  = 0L;
		try{
		
			/*@lineinfo:generated-code*//*@lineinfo:1422^4*/

//  ************************************************************
//  #sql [Ctx] { select to_number (substr (rule_data, instr (rule_data, '=') + 1)) 			        
//  			        from mes.achp_risk_rules arr
//  			        where profile_id = :profileId and def_code = :defCode
//  					 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select to_number (substr (rule_data, instr (rule_data, '=') + 1))  \t\t\t        \n\t\t\t        from mes.achp_risk_rules arr\n\t\t\t        where profile_id =  :1  and def_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"31com.mes.startup.AchpRiskScoringLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,profileId);
   __sJT_st.setString(2,defCode);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   threshold = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1427^6*/
			
		}catch(SQLException e){
			
			StringBuffer    buf = new StringBuffer("getThreshold(");
		      buf.append( profileId );
		      buf.append( "," );
		      buf.append( defCode);
		      buf.append( ")" );
		      logEntry( buf.toString(),e.toString() );
		}
		
		return threshold;
	}

	
	

	
	 protected void recordTimestampEnd( long recId )
	  {
	    Timestamp     beginTime   = null;
	    long          elapsed     = 0L;
	    Timestamp     endTime     = null;

	    try
	    {
	      /*@lineinfo:generated-code*//*@lineinfo:1454^8*/

//  ************************************************************
//  #sql [Ctx] { select  process_begin_date 
//  	        from    risk_process
//  	        where   rec_id = :recId
//  	       };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  process_begin_date  \n\t        from    risk_process\n\t        where   rec_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"32com.mes.startup.AchpRiskScoringLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,recId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   beginTime = (java.sql.Timestamp)__sJT_rs.getTimestamp(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1459^8*/
	      endTime = new Timestamp(Calendar.getInstance().getTime().getTime());
	      elapsed = (endTime.getTime() - beginTime.getTime());

	      /*@lineinfo:generated-code*//*@lineinfo:1463^8*/

//  ************************************************************
//  #sql [Ctx] { update  risk_process
//  	        set     process_end_date = :endTime,
//  	                process_elapsed = :DateTimeFormatter.getFormattedTimestamp(elapsed)
//  	        where   rec_id = :recId
//  	       };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3658 = DateTimeFormatter.getFormattedTimestamp(elapsed);
   String theSqlTS = "update  risk_process\n\t        set     process_end_date =  :1 ,\n\t                process_elapsed =  :2 \n\t        where   rec_id =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"33com.mes.startup.AchpRiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setTimestamp(1,endTime);
   __sJT_st.setString(2,__sJT_3658);
   __sJT_st.setLong(3,recId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1469^8*/
	    }
	    catch(Exception e)
	    {
	      logEvent(this.getClass().getName(), "recordTimestampEnd()", e.toString());
	      setError("recordTimestampEnd(): " + e.toString());
	      logEntry("recordTimestampEnd()", e.toString());
	    }
	  }
	
	protected void recordTimestampBegin( long recId )
	  {
	    Timestamp     beginTime   = null;

	    try
	    {
	      beginTime = new Timestamp(Calendar.getInstance().getTime().getTime());

	      /*@lineinfo:generated-code*//*@lineinfo:1487^8*/

//  ************************************************************
//  #sql [Ctx] { update  risk_process
//  	        set     process_begin_date = :beginTime
//  	        where   rec_id = :recId
//  	       };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  risk_process\n\t        set     process_begin_date =  :1 \n\t        where   rec_id =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"34com.mes.startup.AchpRiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setTimestamp(1,beginTime);
   __sJT_st.setLong(2,recId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1492^8*/
	    }
	    catch(Exception e)
	    {
	      logEvent(this.getClass().getName(), "recordTimestampBegin()", e.toString());
	      setError("recordTimestampBegin(): " + e.toString());
	      logEntry("recordTimestampBegin()", e.toString());
	    }
	  }

	public static void main(String[] args) {
		AchpRiskScoringLoader loader = null;

		SQLJConnectionBase.initStandalone("DEBUG");
	
		try {
			loader = new AchpRiskScoringLoader();
			loader.connect();
			loader.execute();  

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				loader.cleanUp();
			} catch (Exception e) {
			}
		}
	}
}/*@lineinfo:generated-code*/