package com.mes.bankserv.constants;

public enum BankServConstants {
	NEXT_DAY_FUNDING_CLAUSE("and atd.same_day_funding = 'N'");
	
	private String value;	
	
	public String getValue() {
		return value;
	}
	
	private BankServConstants(String value){
		this.value = value;
	}
	
}
