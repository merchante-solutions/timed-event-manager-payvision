/*@lineinfo:filename=BankServTransmit*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/startup/BankServTransmit.sqlj $

  Description:

    AccountLookup

    Support bean for account lookup page in account maintenance.

  Last Modified By   : $Author: tbaker $
  Last Modified Date : $Date: 2014-10-31 14:22:01 -0700 (Fri, 31 Oct 2014) $
  Version            : $Revision: 23228 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.io.File;
import java.sql.ResultSet;
import java.util.Vector;
import org.apache.log4j.Logger;
import com.mes.ach.AchEntryData;
import com.mes.bankserv.constants.BankServConstants;
import com.mes.constants.MesEmails;
import com.mes.net.MailMessage;
import com.mes.support.DateTimeFormatter;
import com.mes.support.NumberFormatter;
import sqlj.runtime.ResultSetIterator;

public class BankServTransmit extends EventBase
{
  static Logger log = Logger.getLogger(BankServTransmit.class);

  public static final String    INBOUND_PATH  = "./inbound/";
  public static final String    CONFIRM_PATH  = "./download/";
    
  private   int           processSequence     = 0;
  private   StringBuffer  fileName            = new StringBuffer("");
  
  private   String    outFileDate;
  private   String    outFileName;
  private   String    debitCount;
  private   String    debitAmount;
  private   String    creditCount;
  private   String    creditAmount;
  
  private   int       TestProcessSequence     = 0;
  
  public BankServTransmit()
  {
  }
  
  protected boolean inTestMode()
  {
    return( TestProcessSequence != 0 );
  }
  
  public void resendEmail(int processSequence)
  {
    try
    {
      connect();
      this.processSequence = processSequence;
      
      sendEmail();
    }
    catch(Exception e)
    {
      logEntry("resendEmail(" + processSequence + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
  private void sendEmail()
  {
    ResultSetIterator   itMain    = null;
    ResultSet           rsMain    = null;
    
    ResultSetIterator   it    = null;
    ResultSet           rs    = null;
    
    try
    {
      // get data for generating email for bankserv
      /*@lineinfo:generated-code*//*@lineinfo:105^7*/

//  ************************************************************
//  #sql [Ctx] itMain = { select  trunc(bad.date_transmitted)                           file_date,
//                  bad.process_filename                                  file_name,
//                  sum(decode(bad.credit_debit_ind, 'D', 1, 0))          debit_count,
//                  sum(decode(bad.credit_debit_ind, 'D', bad.amount, 0)) debit_amount,
//                  sum(decode(bad.credit_debit_ind, 'C', 1, 0))          credit_count,
//                  sum(decode(bad.credit_debit_ind, 'C', bad.amount, 0)) credit_amount
//          from    bankserv_ach_detail         bad,
//                  bankserv_funding_merchants  bfm
//          where   bad.process_sequence = :processSequence and
//                  bad.merchant_number = bfm.merchant_number(+) and
//                  bfm.enabled(+) = 'Y' and
//                  bfm.merchant_number is null
//          group by trunc(bad.date_transmitted), bad.process_filename
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  trunc(bad.date_transmitted)                           file_date,\n                bad.process_filename                                  file_name,\n                sum(decode(bad.credit_debit_ind, 'D', 1, 0))          debit_count,\n                sum(decode(bad.credit_debit_ind, 'D', bad.amount, 0)) debit_amount,\n                sum(decode(bad.credit_debit_ind, 'C', 1, 0))          credit_count,\n                sum(decode(bad.credit_debit_ind, 'C', bad.amount, 0)) credit_amount\n        from    bankserv_ach_detail         bad,\n                bankserv_funding_merchants  bfm\n        where   bad.process_sequence =  :1   and\n                bad.merchant_number = bfm.merchant_number(+) and\n                bfm.enabled(+) = 'Y' and\n                bfm.merchant_number is null\n        group by trunc(bad.date_transmitted), bad.process_filename";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.BankServTransmit",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,processSequence);
   // execute query
   itMain = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.startup.BankServTransmit",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:120^7*/
      
      rsMain = itMain.getResultSet();
      
      while(rsMain.next()) {
    	  // assign status variables
	      outFileDate  = DateTimeFormatter.getFormattedDate(rsMain.getDate("file_date"), "MM/dd/yyyy");
	      outFileName  = rsMain.getString("file_name");
	      debitCount   = rsMain.getString("debit_count");
	      debitAmount  = NumberFormatter.getDoubleString(rsMain.getDouble("debit_amount"), "$###,###,##0.00");
	      creditCount  = rsMain.getString("credit_count");
	      creditAmount = NumberFormatter.getDoubleString(rsMain.getDouble("credit_amount"), "$###,###,##0.00");
	      
		  // get funding transaction statistics
		  /*@lineinfo:generated-code*//*@lineinfo:134^5*/

//  ************************************************************
//  #sql [Ctx] it = { select  m.dba_name                                            merchant_name,
//  		    	sum(decode(bad.credit_debit_ind, 'D', 1, 0))          debit_count,
//  		        sum(decode(bad.credit_debit_ind, 'D', bad.amount, 0)) debit_amount,
//  		        sum(decode(bad.credit_debit_ind, 'C', 1, 0))          credit_count,
//  		        sum(decode(bad.credit_debit_ind, 'C', bad.amount, 0)) credit_amount
//  		    from    bankserv_ach_detail         bad,
//  		    		bankserv_funding_merchants  bfm,
//  		            mif                         m
//  		    where   bad.process_sequence = :processSequence and
//  		    		bad.merchant_number = bfm.merchant_number and
//  		            bfm.enabled = 'Y' and
//  		            bad.merchant_number = m.merchant_number
//  		    group by m.dba_name
//  		   };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  m.dba_name                                            merchant_name,\n\t\t    \tsum(decode(bad.credit_debit_ind, 'D', 1, 0))          debit_count,\n\t\t        sum(decode(bad.credit_debit_ind, 'D', bad.amount, 0)) debit_amount,\n\t\t        sum(decode(bad.credit_debit_ind, 'C', 1, 0))          credit_count,\n\t\t        sum(decode(bad.credit_debit_ind, 'C', bad.amount, 0)) credit_amount\n\t\t    from    bankserv_ach_detail         bad,\n\t\t    \t\tbankserv_funding_merchants  bfm,\n\t\t            mif                         m\n\t\t    where   bad.process_sequence =  :1   and\n\t\t    \t\tbad.merchant_number = bfm.merchant_number and\n\t\t            bfm.enabled = 'Y' and\n\t\t            bad.merchant_number = m.merchant_number\n\t\t    group by m.dba_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.startup.BankServTransmit",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,processSequence);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.startup.BankServTransmit",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:149^5*/
	      
	      rs = it.getResultSet();
	      
	      sendStatusEmail(rs);
	      
	      rs.close();
	      it.close();
      }
    }
    catch(Exception e)
    {
      logEntry("sendEmail()", e.toString());
    }
    finally
    {
      try { rsMain.close(); } catch(Exception e) {}
      try { itMain.close(); } catch(Exception e) {}
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
  }
  
  public void addTridentAchDetail(int processSequence, boolean testMode)
  {
    ResultSetIterator   it              = null;
    ResultSet           rs              = null;
    Vector              achEntries      = null;
    AchEntryData        achData         = null;
    long                recId           = 0L;
    long                traceNumber     = 0L;
    boolean             wasStale        = false;
    
    try
    {
      if( isConnectionStale() )
      {
        wasStale = true;
        log.debug("connecting to database");
        connect();
      }
      
      achEntries    = new Vector();
      
      log.debug("retrieving records for sequence: " + processSequence);
      /*@lineinfo:generated-code*//*@lineinfo:194^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  bad.ach_seq_num                       as rec_id,
//                  case 
//                    when mf.bank_number = 3858 
//                      then 385800000
//                    else 
//                      decode(bads.description_id, 1057, 394100000, mf.bank_number*100000)
//                    end                                 as origin_node,
//                  mf.merchant_number                    as merchant_number,
//                  mf.dba_name                           as dba_name,
//                  nvl(lpad(bad.target_routing_number,9,'0'),
//                    lpad(mf.transit_routng_num,9,'0'))  as transit_routing,
//                  nvl(bad.target_account_number,
//                    mf.dda_num)                         as dda_num,
//                  trunc(sysdate)                        as post_date,
//                  'CCD'                                 as class_code,
//                  :AchEntryData.ED_MANUAL_ADJ         as entry_desc,
//                  bads.description_id                   as manual_description_id,
//                  bad.process_sequence                  as ach_sequence,        
//                  0                                     as credits_count,
//                  0                                     as credits_amount,
//                  0                                     as sales_count,
//                  0                                     as sales_amount,
//                  decode(bad.credit_debit_ind,
//                    'D', -1,
//                    1)*bad.amount                       as net_amount
//          from    bankserv_ach_detail         bad,
//                  bankserv_ach_descriptions   bads,
//                  mif                         mf
//          where   bad.process_sequence = :processSequence
//                  and bad.entry_description = bads.description
//                  and bad.merchant_number = mf.merchant_number
//          order by bads.description_id,  mf.merchant_number, bad.date_created       
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  bad.ach_seq_num                       as rec_id,\n                case \n                  when mf.bank_number = 3858 \n                    then 385800000\n                  else \n                    decode(bads.description_id, 1057, 394100000, mf.bank_number*100000)\n                  end                                 as origin_node,\n                mf.merchant_number                    as merchant_number,\n                mf.dba_name                           as dba_name,\n                nvl(lpad(bad.target_routing_number,9,'0'),\n                  lpad(mf.transit_routng_num,9,'0'))  as transit_routing,\n                nvl(bad.target_account_number,\n                  mf.dda_num)                         as dda_num,\n                trunc(sysdate)                        as post_date,\n                'CCD'                                 as class_code,\n                 :1           as entry_desc,\n                bads.description_id                   as manual_description_id,\n                bad.process_sequence                  as ach_sequence,        \n                0                                     as credits_count,\n                0                                     as credits_amount,\n                0                                     as sales_count,\n                0                                     as sales_amount,\n                decode(bad.credit_debit_ind,\n                  'D', -1,\n                  1)*bad.amount                       as net_amount\n        from    bankserv_ach_detail         bad,\n                bankserv_ach_descriptions   bads,\n                mif                         mf\n        where   bad.process_sequence =  :2  \n                and bad.entry_description = bads.description\n                and bad.merchant_number = mf.merchant_number\n        order by bads.description_id,  mf.merchant_number, bad.date_created";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.startup.BankServTransmit",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,AchEntryData.ED_MANUAL_ADJ);
   __sJT_st.setInt(2,processSequence);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.startup.BankServTransmit",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:228^7*/
      
      rs = it.getResultSet();
      
      log.debug("processing records");
      while(rs.next())
      {
        recId         = rs.getLong("rec_id");
        
        /*@lineinfo:generated-code*//*@lineinfo:237^9*/

//  ************************************************************
//  #sql [Ctx] { select  ach_trident_trace_sequence.nextval
//            
//            from    dual
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  ach_trident_trace_sequence.nextval\n           \n          from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.startup.BankServTransmit",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   traceNumber = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:242^9*/
        
        achData = new AchEntryData();
        achData.setData(rs);
        achData.setManualDescriptionId(rs.getInt("manual_description_id"));
        
        achData.setOriginNode(rs.getLong("origin_node"));
        
        achData.setTraceNumber(traceNumber);
        achEntries.add(achData);
        achData.addStatementData(rs);
        
        // update original record to show trace number
        /*@lineinfo:generated-code*//*@lineinfo:255^9*/

//  ************************************************************
//  #sql [Ctx] { update  bankserv_ach_detail
//            set     trace_number = :traceNumber
//            where   ach_seq_num = :recId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  bankserv_ach_detail\n          set     trace_number =  :1  \n          where   ach_seq_num =  :2 ";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.startup.BankServTransmit",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,traceNumber);
   __sJT_st.setLong(2,recId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:260^9*/
      }
      
      rs.close();
      it.close();
      
      log.debug("adding " + achEntries.size() + " records to ach_trident_detail");
      
      AchEntryData.storeVector(achEntries, false, testMode, BankServConstants.NEXT_DAY_FUNDING_CLAUSE.getValue());
      
      log.debug("DONE");
      commit();
    }
    catch(Exception e)
    {
      logEntry("addTridentAchDetail(" + processSequence + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      if( wasStale )
      {
        cleanUp();
      }
    }
  }
  
  /*
  ** METHOD execute()
  **
  ** Entry point from timed event manager
  */
  public boolean execute()
  {
    boolean               result      = false;
    File                  dir         = null;
    File[]                fileList    = null;
    
    try
    {
      connect();
      
      this.processSequence = 0;     // reset the process sequence
      
      if ( TestProcessSequence != 0 )
      {
        this.processSequence = TestProcessSequence;
      }
      else    // assign
      {
        // determine if there are any records to process
        int   achCount = 0;
        /*@lineinfo:generated-code*//*@lineinfo:313^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(ach_seq_num)
//            
//            from    bankserv_ach_detail
//            where   process_sequence is null and
//                    date_authorized is not null
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(ach_seq_num)\n           \n          from    bankserv_ach_detail\n          where   process_sequence is null and\n                  date_authorized is not null";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.startup.BankServTransmit",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   achCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:320^9*/
      
        if(achCount > 0)
        {
          // establish process sequence
          /*@lineinfo:generated-code*//*@lineinfo:325^11*/

//  ************************************************************
//  #sql [Ctx] { select  bankserv_process_sequence.nextval
//              
//              from    dual
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  bankserv_process_sequence.nextval\n             \n            from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.startup.BankServTransmit",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   processSequence = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:330^11*/
      
      
          // update records with process sequence so they can be retrieved easily
          /*@lineinfo:generated-code*//*@lineinfo:334^11*/

//  ************************************************************
//  #sql [Ctx] { update  bankserv_ach_detail
//              set     process_sequence = :processSequence
//              where   process_sequence is null and
//                      date_authorized is not null
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  bankserv_ach_detail\n            set     process_sequence =  :1  \n            where   process_sequence is null and\n                    date_authorized is not null";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"7com.mes.startup.BankServTransmit",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,processSequence);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:340^11*/
      
          commit();
        }
      }
      
      // put items in ach_trident_detail so they will be sent to JPMC
      addTridentAchDetail(processSequence, false);
      
      // Update entries to Synovus ACH = PROD-1967
      /*@lineinfo:generated-code*//*@lineinfo:350^7*/

//  ************************************************************
//  #sql [Ctx] { update  bankserv_ach_detail bad
//          set     process_filename = 'Sent via Synovus ACH',
//                  date_transmitted = sysdate
//          where   process_sequence = :processSequence
//          		and exists (select 1 
//          					from mif mf
//          					where merchant_number = bad.merchant_number 
//          						and mf.bank_number = 3858) 
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  bankserv_ach_detail bad\n        set     process_filename = 'Sent via Synovus ACH',\n                date_transmitted = sysdate\n        where   process_sequence =  :1  \n        \t\tand exists (select 1 \n        \t\t\t\t\tfrom mif mf\n        \t\t\t\t\twhere merchant_number = bad.merchant_number \n        \t\t\t\t\t\tand mf.bank_number = 3858)";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"8com.mes.startup.BankServTransmit",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,processSequence);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:360^7*/
      
      // Update remaining entries to JPMC
      /*@lineinfo:generated-code*//*@lineinfo:363^7*/

//  ************************************************************
//  #sql [Ctx] { update  bankserv_ach_detail
//          set     process_filename = 'Sent via JPMC ACH',
//                  date_transmitted = sysdate
//          where   process_sequence = :processSequence
//          		and process_filename is null 
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  bankserv_ach_detail\n        set     process_filename = 'Sent via JPMC ACH',\n                date_transmitted = sysdate\n        where   process_sequence =  :1  \n        \t\tand process_filename is null";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"9com.mes.startup.BankServTransmit",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,processSequence);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:370^7*/
      
      // send email indicating totals
      sendEmail();
      
      result = true;
    }
    catch(Exception e)
    {
      logEvent(this.getClass().getName(), "execute()", e.toString());
      logEntry("execute()", e.toString());
    }
    finally
    {
      cleanUp();
    }
    
    return result;
  }
  
  private void sendStatusEmail(ResultSet rs)
  {
    StringBuffer    subject   = new StringBuffer("");
    StringBuffer    body      = new StringBuffer("");
    
    try
    {
      if( outFileDate != null && ! "".equals(outFileDate) )
      {
    	  // This email is internal to MeS, sent to Finance
        if( getEventArg(0) != null && getEventArg(0).equals("JPMC") )
        {
        	if(outFileName != null && outFileName.indexOf("JPMC") >=0)
        		subject.append("JPMC Adjustment file generated: ");
        	else
        		subject.append("Synovus Adjustment file generated: ");
        }
        else
        {
          subject.append("Merchant e-Solutions file sent ");
        }
        subject.append(outFileDate);
      
        body.append("File Date:     ");
        body.append(outFileDate);
        body.append("\nFile Name:     ");
        body.append(outFileName);
        body.append("\nDebit Count:   ");
        body.append(debitCount);
        body.append("\nDebit Amount:  ");
        body.append(debitAmount);
        body.append("\nCredit Count:  ");
        body.append(creditCount);
        body.append("\nCredit Amount: ");
        body.append(creditAmount);
      
        body.append("\n\n---Special Funding---\n\n");
      
        while(rs.next())
        {
          body.append(rs.getString("merchant_name"));
          body.append(":");
          body.append("\nDebit Count:   ");
          body.append(rs.getString("debit_count"));
          body.append("\nDebit Amount:  ");
          body.append(NumberFormatter.getDoubleString(rs.getDouble("debit_amount"), "$###,###,##0.00"));
          body.append("\nCredit Count:  ");
          body.append(rs.getString("credit_count"));
          body.append("\nCredit Amount: ");
          body.append(NumberFormatter.getDoubleString(rs.getDouble("credit_amount"), "$###,###,##0.00"));
          body.append("\n\n");
        }
      
        MailMessage msg = new MailMessage();
      
        if( getEventArg(0) != null && getEventArg(0).equals("JPMC") )
        {
          msg.setAddresses(MesEmails.MSG_ADDRS_BANKSERV_JPMC);
        }
        else
        {
          msg.setAddresses(MesEmails.MSG_ADDRS_BANKSERV_TRANSMIT);
        }
      
        msg.setSubject(subject.toString());
        msg.setText(body.toString());
        msg.send();
        
        log.debug(body.toString());
      }
    }
    catch(Exception e)
    {
      logEntry("sendStatusEmail()", e.toString());
    }
  }
  
  public static void main (String[] args)
  {
    try
    {
      BankServTransmit bst = new BankServTransmit();
      if (args.length > 0) bst.setEventArgs(args);
      bst.execute();
    }
    catch(Exception e)
    {
      log.error("main: " + e.toString());
    }
  }
}/*@lineinfo:generated-code*/